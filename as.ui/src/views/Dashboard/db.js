import React, { Component, lazy, Suspense, useState, useEffect } from 'react';
import { Redirect } from "react-router-dom";
import API from "./../../Componentes/Constantes/Api";
import {
    Button,
    ButtonGroup,
    ButtonToolbar,
    Card,
    CardBody,
    CardFooter,
    CardTitle,
    Col,
    Row,
    Table,
    CardHeader,
} from "reactstrap";
import { Bar, Line } from "react-chartjs-2";
import { CustomTooltips } from "@coreui/coreui-plugin-chartjs-custom-tooltips";
import { getStyle, hexToRgba } from "@coreui/coreui/dist/js/coreui-utilities";
import moment from "moment";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const Widget03 = lazy(() => import('../../views/Widgets/Widget03'));

//Random Numbers
function random(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

var elements = 27;
var data1 = [];
var data2 = [];
var data3 = [];

for (var i = 0; i <= elements; i++) {
    data1.push(random(50, 200));
    data2.push(random(80, 100));
    data3.push(65);
}

const brandInfo = getStyle("--info");

const mainChartOpts1 = {
    tooltips: {
        enabled: false,
        custom: CustomTooltips,
        intersect: true,
        mode: "index",
        position: "nearest",
        callbacks: {
            labelColor: function (tooltipItem, chart) {
                return {
                    backgroundColor:
                        chart.data.datasets[tooltipItem.datasetIndex].borderColor,
                };
            },
        },
    },
    maintainAspectRatio: false,
    legend: {
        display: false,
    },
    scales: {
        xAxes: [
            {
                gridLines: {
                    drawOnChartArea: false,
                },
            },
        ],
        yAxes: [
            {
                ticks: {
                    beginAtZero: true,
                    maxTicksLimit: 5,
                    stepSize: Math.ceil(100 / 5),
                    max: 35,
                },
            },
        ],
    },
    elements: {
        point: {
            radius: 0,
            hitRadius: 10,
            hoverRadius: 4,
            hoverBorderWidth: 3,
        },
    },
};

const mainChart1 = {
    labels: [],
    datasets: [
        {
            label: "Ventas: ",
            backgroundColor: hexToRgba(brandInfo, 10),
            borderColor: brandInfo,
            pointHoverBackgroundColor: "#fff",
            borderWidth: 2,
            data: [],
        },
    ],
};

const cardChartData4 = {
    labels: [],
    datasets: [
        {
            label: "Clientes por proyecto",
            backgroundColor: hexToRgba(brandInfo, 10),
            borderColor: "transparent",
            data: [],
        },
    ],
};

const cardChartOpts4 = {
    tooltips: {
        enabled: false,
        custom: CustomTooltips,
    },
    maintainAspectRatio: false,
    legend: {
        display: false,
    },
    scales: {
        xAxes: [
            {
                display: true,
                barPercentage: 0.6,
            },
        ],
        yAxes: [
            {
                display: true,
            },
        ],
    },
};

const cardChartData1 = {
    labels: [],
    datasets: [
        {
            label: "Clientes por tipo de contacto",
            backgroundColor: hexToRgba(brandInfo, 10),
            borderColor: "transparent",
            data: [],
        },
    ],
};

const cardChartOpts1 = {
    tooltips: {
        enabled: false,
        custom: CustomTooltips,
    },
    maintainAspectRatio: false,
    legend: {
        display: false,
    },
    scales: {
        xAxes: [
            {
                display: true,
                barPercentage: 0.6,
            },
        ],
        yAxes: [
            {
                display: true,
            },
        ],
    },
};


const cardChartDataNegotiation = {
    labels: [],
    datasets: [
        {
            label: "Negociacion por institucion financiera",
            backgroundColor: hexToRgba(brandInfo, 10),
            borderColor: "transparent",
            data: [],
        },
    ],
};

const cardChartDataNegotiationBySalesAdvisor = {
    labels: [],
    datasets: [
        {
            label: "Negociacion por usuario",
            backgroundColor: hexToRgba(brandInfo, 10),
            borderColor: "transparent",
            data: [],
        },
    ],
};
const cardChartOptsNegotiations = {
    tooltips: {
        enabled: false,
        custom: CustomTooltips,
    },
    maintainAspectRatio: true,
    legend: {
        display: false,
    },
    scales: {
        xAxes: [
            {
                display: true,
                barPercentage: 0.6,
            },
        ],
        yAxes: [
            {
                display: true,
            },
        ],
    },
};

const DashboardData = (props) => {
    const [mainChart, setMainChart] = useState(mainChart1);
    // const [cardChartData, setCardChartData] = useState(cardChartData4);
    // const [cardChartDataCT, setCardChartDataCT] = useState(cardChartData1);
    // const [AlarmNotificationsState, setAlarmNotificationsState] = useState([]);
    // const [NegotiationPhasestate, setNegotiationPhasestate] = useState([]);
    // const [NegotiationsStaticState, setNegotiationsStaticState] = useState(cardChartDataNegotiation);
    // const [NegotiationsStatusState, setNegotiationsStatusState] = useState({});
    // const [ClientCountState, setClientCountState] = useState(0);
    // const [NegotiationsBySalesAdvisorState, setNegotiationsBySalesAdvisorState] = useState(cardChartDataNegotiationBySalesAdvisor);

    useEffect(() => {
        const token = sessionStorage.getItem("token");
        if (!token) {
            props.history.push("/login");
        } else {
            handleData();
        }
    }, []);

    function onRadioBtnClick(number) {
        if (number === 12) {
            handleData();
            return;
        }
        if (number === 6) {
            get6monthsData();
            return;
        }
        if (number === 3) {
            get3monthsData();
            return;
        }
        if (number === 1) {
            get1monthData();
            return;
        }
    }

    function get6monthsData() {
        const url = `GraficosData/by-date-six`;
        API.get(url)
            .then((res) => {
                const data1 = {
                    labels: res.data.fechas.map((s) => moment(s).format("MMM Do YY")),
                    datasets: [
                        {
                            label: "Ventas: ",
                            backgroundColor: hexToRgba(brandInfo, 10),
                            borderColor: brandInfo,
                            pointHoverBackgroundColor: "#fff",
                            borderWidth: 2,
                            data: res.data.countVentas,
                        },
                    ],
                };
                setMainChart(data1);
            })
            .catch((error) => {
                if (error.message === "Network Error") {
                    toast.warn(
                        "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                    );
                } else {
                    if (error.response.status !== undefined) {
                        if (error.response.status === 401) {
                            props.history.push("/login");
                            return <Redirect to="/login" />;
                        }
                    } else {
                        toast.warn(
                            "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                        );
                    }
                }
            });
    }

    function get3monthsData() {
        const url = `GraficosData/by-date-three`;
        API.get(url)
            .then((res) => {
                const data1 = {
                    labels: res.data.fechas.map((s) => moment(s).format("MMM Do YY")),
                    datasets: [
                        {
                            label: "Ventas: ",
                            backgroundColor: hexToRgba(brandInfo, 10),
                            borderColor: brandInfo,
                            pointHoverBackgroundColor: "#fff",
                            borderWidth: 2,
                            data: res.data.countVentas,
                        },
                    ],
                };
                setMainChart(data1);
            })
            .catch((error) => {
                if (error.message === "Network Error") {
                    toast.warn(
                        "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                    );
                } else {
                    if (error.response.status !== undefined) {
                        if (error.response.status === 401) {
                            props.history.push("/login");
                            return <Redirect to="/login" />;
                        }
                    } else {
                        toast.warn(
                            "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                        );
                    }
                }
            });
    }
    function get1monthData() {
        const url = `GraficosData/by-date-one-month`;
        API.get(url)
            .then((res) => {
                const data1 = {
                    labels: res.data.fechas.map((s) => moment(s).format("MMM Do YY")),
                    datasets: [
                        {
                            label: "Ventas: ",
                            backgroundColor: hexToRgba(brandInfo, 10),
                            borderColor: brandInfo,
                            pointHoverBackgroundColor: "#fff",
                            borderWidth: 2,
                            data: res.data.countVentas,
                        },
                    ],
                };
                setMainChart(data1);
            })
            .catch((error) => {
                if (error.message === "Network Error") {
                    toast.warn(
                        "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                    );
                } else {
                    if (error.response.status !== undefined) {
                        if (error.response.status === 401) {
                            props.history.push("/login");
                            return <Redirect to="/login" />;
                        }
                    } else {
                        toast.warn(
                            "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                        );
                    }
                }
            });
    }

    function handleData() {
        const login = sessionStorage.getItem("login");
        if (login === false || login === undefined || login === null) {
            sessionStorage.removeItem("login");
            sessionStorage.removeItem("token");
            props.history.push("/login");
            return;
        }
        const url = `GraficosData/by-date-one`;
        API.get(url)
            .then((res) => {
                const data1 = {
                    labels: res.data.fechas.map((s) => moment(s).format("MMM Do YY")),
                    datasets: [
                        {
                            label: "Ventas: ",
                            backgroundColor: hexToRgba(brandInfo, 10),
                            borderColor: brandInfo,
                            pointHoverBackgroundColor: "#fff",
                            borderWidth: 2,
                            data: res.data.countVentas,
                        },
                    ],
                };
                setMainChart(data1);
            })
            .catch((error) => {
                if (error.message === "Network Error") {
                    toast.warn(
                        "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                    );
                } else {
                    if (error.response.status !== undefined) {
                        if (error.response.status === 401) {
                            props.history.push("/login");
                            return <Redirect to="/login" />;
                        }
                    } else {
                        toast.warn(
                            "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                        );
                    }
                }
            });
    }




  
    const thStyle = {
        background: "#20a8d8",
        color: "white",
        textAlign: "center",
    };


    return (
        <div className="animated fadeIn">
                    <Row>
                        <Col>
                            <Card>
                                <CardBody>
                                    <Row>
                                        <Col sm="5">
                                            <CardTitle className="mb-0">
                                                <strong>Registro de ventas por fecha</strong>
                                            </CardTitle>
                                            <div className="small text-muted">
                                                Cantidad de ventas por fecha
                      </div>
                                        </Col>
                                        <Col sm="7" className="d-none d-sm-inline-block">
                                            <ButtonToolbar
                                                className="float-right"
                                                aria-label="Toolbar with button groups"
                                            >
                                                <ButtonGroup className="mr-3" aria-label="First group">
                                                    <Button
                                                        color="primary"
                                                        onClick={() => onRadioBtnClick(6)}
                                                    >
                                                        <strong>6 Meses</strong>
                                                    </Button>
                                                    <Button
                                                        color="primary"
                                                        onClick={() => onRadioBtnClick(3)}
                                                    >
                                                        <strong>3 Meses</strong>
                                                    </Button>
                                                    <Button
                                                        color="primary"
                                                        onClick={() => onRadioBtnClick(1)}
                                                    >
                                                        <strong>1 Mes</strong>
                                                    </Button>
                                                </ButtonGroup>
                                            </ButtonToolbar>
                                        </Col>
                                    </Row>
                                    <div
                                        className="chart-wrapper"
                                        style={{ height: 300 + "px", marginTop: 40 + "px" }}
                                    >
                                        <Line
                                            data={mainChart}
                                            options={mainChartOpts1}
                                            height={300}
                                        />
                                    </div>
                                </CardBody>
                                <CardFooter></CardFooter>
                            </Card>
                        </Col>
                    </Row>
        </div>
    );
};

export default DashboardData;
