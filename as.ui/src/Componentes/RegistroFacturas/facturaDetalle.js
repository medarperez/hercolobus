import React, { useState, useEffect } from "react";
import {
    Card,
    CardBody,
    CardHeader,
    Col,
    Row,
    FormGroup,
    Label,
    Input,
    Button,
    Tooltip,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Table
} from "reactstrap";
import moment from "moment";

const thStyle = {
    background: "#20a8d8",
    color: "white",
    minWidth: '80px',
    textAlign: 'center'
};

const tdStyle = {
    textAlign: 'center'
};





const FacturaDetalle = props => {
    const {
        registroFacturaState,
        ParametroState,
        imprimirFactura,
        setSubTotalState,
        setImpuestoState,
        setTotalState,
        returnToFacturaList
    } = props;
    const [tooltipOpen, setTooltipOpen] = useState(false);
    const toggle = () => setTooltipOpen(!tooltipOpen);

    function ObetneSubTotal(lista) {

        var subTotal = lista.sum(s => s.total);
        var st = subTotal.toFixed(2);
        setSubTotalState(st);
        return st;
        // var impuesto = subTotal * (ParametroState.impuesto / 100);
        // var total = subTotal + impuesto;
    };


    function ObetneImpuesto(lista) {
        var subTotal = lista.sum(s => s.total);
        var impuesto = subTotal * (ParametroState.impuesto / 100);
        var i = impuesto.toFixed(2);
        setImpuestoState(i);
        return i;
        // var total = subTotal + impuesto;
    };

    function ObetneTotal(lista) {
        var subTotal = lista.sum(s => s.total);
        var impuesto = subTotal * (ParametroState.impuesto / 100);
        var total = subTotal + impuesto;
        var t = total.toFixed(2);
        setTotalState(t);
        return t;
    };

    


    return(
        <div className="animated fadeIn" >
            <Row >
                <Col md="3" sm="6" xs="12">
                    <FormGroup>
                        <Label htmlFor="Impuesto">Empresa</Label>
                        <Input readOnly type="text" id="empresa" name="empresa"
                            value={ParametroState.empresa}
                        />
                    </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                    <FormGroup>
                        <Label htmlFor="Impuesto">Razon Social</Label>
                        <Input readOnly type="text" id="empresa" name="empresa"
                            value={ParametroState.razonSocial}
                        />
                    </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                    <FormGroup>
                        <Label htmlFor="Impuesto">RTN</Label>
                        <Input readOnly type="text" id="empresa" name="empresa"
                            value={ParametroState.rtn}
                        />
                    </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                    <FormGroup  >
                        <Label htmlFor="Direccion">Direccion</Label>
                        <Input readOnly type="text" id="Direccion" name="Direccion"
                            value={ParametroState.direccion}

                        />
                    </FormGroup>
                </Col>
            </Row>
            <Row>
                <Col md="3" sm="6" xs="12">
                    <FormGroup>
                        <Label htmlFor="Impuesto">FacturaId</Label>
                        <Input readOnly type="text" id="empresa" name="empresa"
                            value={registroFacturaState.currentFactura.facturaId}
                            readOnly
                        />
                    </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                    <FormGroup>
                        <Label htmlFor="Impuesto">Rtn Cliente</Label>
                        <Input type="text" id="empresa" name="empresa"
                            value={registroFacturaState.currentFactura.rtnCliente}
                            readOnly
                        />
                    </FormGroup>
                </Col> 
                <Col md="3" sm="6" xs="12">
                    <FormGroup>
                        <Label htmlFor="Impuesto">Cliente</Label>
                        <Input type="text" id="empresa" name="empresa"
                            value={registroFacturaState.currentFactura.cliente}
                            readOnly
                        />
                    </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                    <FormGroup>
                        <Label htmlFor="Impuesto">CAI</Label>
                        <Input type="text" id="empresa" name="empresa"
                            value={registroFacturaState.currentFactura.cai}
                            readOnly
                        />
                    </FormGroup>
                </Col>
            </Row>
            <Row>
                <Col md="3" sm="6" xs="12">
                    <FormGroup>
                        <Label htmlFor="Impuesto">Emision</Label>
                        <Input type="text" id="empresa" name="empresa"
                            value={moment(registroFacturaState.currentFactura.fechaEmision).format("DD-MM-YYYY HH:mm:ss")}
                            readOnly
                        />
                    </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                    <FormGroup>
                        <Label htmlFor="Impuesto">Rango Inicial</Label>
                        <Input type="text" id="empresa" name="empresa"
                            value={registroFacturaState.currentFactura.rangoInicial}
                            readOnly
                        />
                    </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                    <FormGroup>
                        <Label htmlFor="Impuesto">Rango Final</Label>
                        <Input type="text" id="empresa" name="empresa"
                            value={registroFacturaState.currentFactura.rangoFinal}
                            readOnly
                        />
                    </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                    <Row>
                        <Col md="3" sm="6" xs="12"  >
                            <Button color="primary" id="ImprimirFactura" onClick={imprimirFactura} >
                                <i className="fa fa-money"></i><br />Imprimir Copia
                    </Button>
                            <Tooltip placement="right" isOpen={tooltipOpen} target="ImprimirFactura" toggle={toggle}>
                                imprimir factura
                    </Tooltip>
                        </Col>
                    </Row>
                   
                </Col>
                <Col>
                    <Row>
                        <Col md="3" sm="6" xs="12"  >
                            <Button color="success" id="ReturnLista" onClick={returnToFacturaList} >
                                <i className="cui-arrow-left"></i><br />Ir a lista
                    </Button>
                            <Tooltip placement="right" isOpen={tooltipOpen} target="ReturnLista" toggle={toggle}>
                                imprimir factura
                    </Tooltip>
                        </Col>
                    </Row>
                </Col>
            </Row>
                {/* 
                <Col md="3" sm="6" xs="12">
                    <FormGroup>
                        <Label htmlFor="Impuesto">Cliente</Label>
                        <Input type="text" id="cliente" name="cliente"
                            value={ClienteState}
                            readOnly
                        />
                    </FormGroup>
                </Col>  */}

            <Row>
                <Col >
                    <Table hover bordered striped responsive size="sm">
                        <thead>
                            <tr>
                                <th style={thStyle}>Linea</th>
                                <th style={thStyle}>Codigo</th>
                                <th style={thStyle}>Articulo</th>
                                <th style={thStyle}>Descripcion</th>
                                <th style={thStyle}>Precio</th>
                                <th style={thStyle}>Cantidad</th>
                                <th style={thStyle}>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            {registroFacturaState.currentFactura.detalle.map((item) => (
                                <tr key={item.id}>
                                    {/* <td>
                                        <Button
                                            block
                                            color="warning"
                                            // onClick={() => {
                                            //     editRow(negotiation);
                                            // }} 
                                            >
                                            Editar
                                        </Button>
                                    </td> */}
                                    <td style={tdStyle}>{item.idLinea}</td>
                                    <td style={tdStyle}>{item.codigoArticulo}</td>
                                    <td style={tdStyle}>{item.articulo}</td>
                                    <td style={tdStyle}>{item.descripcion}</td>
                                    <td style={tdStyle}>{item.precio}</td>
                                    <td style={tdStyle}>{item.cantidad}</td>
                                    <td style={tdStyle}>{item.total}</td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </Col>
            </Row>
            <Row>
                <Col md="3" sm="6" xs="12" >
                    <Label htmlFor="Impuesto">Sub-Total: {ObetneSubTotal(registroFacturaState.currentFactura.detalle)}</Label>
                </Col>
                <Col md="3" sm="6" xs="12" >
                    <Label htmlFor="Impuesto">Impuesto: {ObetneImpuesto(registroFacturaState.currentFactura.detalle)}</Label>
                </Col>
                <Col md="3" sm="6" xs="12" >
                    <Label htmlFor="Impuesto">Total: {ObetneTotal(registroFacturaState.currentFactura.detalle)}</Label>
                </Col>
            </Row>
            
        </div>
    );
}

export default FacturaDetalle;