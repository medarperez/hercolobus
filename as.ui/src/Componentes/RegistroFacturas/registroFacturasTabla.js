import React, { useState, useEffect } from "react";
import {
    Col,
    Table,
    FormGroup,
    InputGroup,
    InputGroupAddon,
    Button,
    Input
} from "reactstrap";
import moment from "moment";

const step = 1;

const RegistroFacurasTabla = props => {
    const {
        fetchFacturas,
        facurasState,
        nextPage,
        prevPage,
        editRow
    } = props;
    const [searchValue, setSearchValue] = useState("");

    function nextStep() {
        if(searchValue === '' || searchValue === null)
        {
            nextPage(facurasState.pageIndex + 1, null);
        }
        else
        {
            nextPage(facurasState.pageIndex + 1, searchValue);
        }
       
    }

    function prevStep() {
        if (searchValue === '' || searchValue === null) {
            prevPage(facurasState.pageIndex + 1, null);
        }
        else {
            prevPage(facurasState.pageIndex + 1, searchValue);
        }
    }

    function handleValueChange(e) {
        fetchFacturas(e.target.value);
        setSearchValue(e.target.value);
    }

    const thStyle = {
        background: "#20a8d8",
        color: "white"
    };

    const userRows =
        facurasState === null ? (
            <Table hover bordered striped responsive size="sm">
                <thead>
                    <tr>
                        <td colSpan={16}>Buscando...</td>
                    </tr>
                </thead>
            </Table>
        ) : facurasState.length === 0 ? (
            <Table hover bordered striped responsive size="sm">
                <thead>
                    <tr>
                        <td colSpan={16}>
                            No se encontraron facurasState con el filtro especificado.</td>
                    </tr>
                </thead>
            </Table>
        ) : (
                    <div>
                        <FormGroup row>
                            <Col>
                                <InputGroup>
                                    <InputGroupAddon addonType="prepend">
                                        <Button type="button" color="primary">
                                            <i className="fa fa-search" /> Buscar
                                     </Button>
                                    </InputGroupAddon>
                                    <Input
                                        type="text"
                                        id="input1-group2"
                                        name="input1-group2"
                                        placeholder="Buscar"
                                        value={searchValue}
                                        onChange={handleValueChange}
                                        style={{ width: "600px" }}
                                    />
                                </InputGroup>
                            </Col>
                        </FormGroup>

                        <FormGroup row>
                            <Col md="12">
                                <Table hover bordered striped responsive size="sm">
                                    <thead>
                                        <tr>
                                            <th style={thStyle}>Imprimir</th>
                                            <th style={thStyle}>Factura No.</th>
                                            <th style={thStyle}>Emision</th>
                                            <th style={thStyle}>Cliente</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {facurasState.facturas.map(item => (
                                            <tr key={item.id}>
                                                <td>
                                                    <Button
                                                        block
                                                        color="success"
                                                        onClick={() => {
                                                            editRow(item);
                                                        }}
                                                    >
                                                        Detalles</Button>
                                                </td>
                                                <td>{item.facturaId}</td>
                                                <td>{moment(item.fechaEmision).format("YYYY-MM-DD HH:MM A")}</td>
                                                <td>{item.cliente}</td>
                                                {/*<td>{item.articulo}</td>
                                                <td>{item/*.descripcion}</td>
                                                <td>{item.precio}</td> */}
                                            </tr>
                                        ))}
                                    </tbody>
                                </Table>
                            </Col>
                            <Col md="12">
                                <InputGroup>
                                    <InputGroupAddon addonType="prepend">
                                        <Button type="button" color="primary" onClick={prevStep}>
                                            <i className="icon-arrow-left-circle" /> Anterior
                                </Button>
                                    </InputGroupAddon>
                                    <div
                                        className="animated fadeIn"
                                        style={{
                                            marginTop: "7px",
                                            marginLeft: "8px",
                                            marginRight: "8px"
                                        }}
                                    >
                                        <h5>
                                            <strong>Paginas: {facurasState.pageCount}</strong>
                                        </h5>
                                    </div>
                                    <div
                                        className="animated fadeIn"
                                        style={{
                                            marginTop: "7px",
                                            marginLeft: "8px",
                                            marginRight: "8px"
                                        }}
                                    >
                                        <h5>
                                            <strong>Actual: {facurasState.pageIndex}</strong>
                                        </h5>
                                    </div>
                                    <InputGroupAddon addonType="prepend">
                                        <Button type="button" color="primary" onClick={nextStep}>
                                            Proximo <i className="icon-arrow-right-circle" />
                                        </Button>
                                    </InputGroupAddon>
                                </InputGroup>
                            </Col>
                        </FormGroup>
                    </div>
                );
    return userRows;

}

export default RegistroFacurasTabla;