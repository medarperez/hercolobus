import React from "react";
// import logo from "../../../assets/img/brand/logoJRE.svg";
import moment from "moment";
import { CardBody, CardHeader, Card, Row, Col } from "reactstrap";
import styled from "styled-components";
// import './registroFacturas.css';

const getClientFullName = (client) => {
    const name = client
        ? (client.firstName || "") +
        " " +
        (client.middleName || "") +
        " " +
        (client.firstSurname || "") +
        " " +
        (client.secondSurname || "")
        : "";

    return name;
};

const GridContainer = styled.div`
  display: grid;
  grid-template-rows: 30px 30px 30px 30px 30px 30px 30px 30px 5px 30px 5px Auto 30px ;
  grid-gap: 10px;
  height: auto;
  padding: 10px;
`;

const GridContainerColumns = styled.div`
  display: grid;
  grid-template-columns: 200px 400px 200px;
  grid-gap: 10px;
  padding: 10px;
`;

const GridContainerColumnsDetalle = styled.div`
  display: grid;
  grid-template-columns: 30px 240px 30px;
  grid-gap: 10px;
  padding: 10px;
`;

const GridContainerColumnsInverso = styled.div`
  display: grid;
  grid-template-columns: 200px 50px 50px;
  grid-gap: 10px;
  padding: 10px;
`;

const GridContainerDetalle = styled.div`
  display: grid;
  grid-template-rows: 10px;
  grid-gap: 2px;
  padding: 2px;
`;

const GridContainerColumnsDelimitador = styled.div`
  display: grid;
  grid-template-columns: 300px;
  grid-gap: 10px;
  padding: 10px;
`;


const getTrackingInfo = (clientTracking) => {
    return clientTracking.map((tracking) => (
        <Card>
            <CardHeader>Cita {moment(tracking.fechaCita).format("L")}</CardHeader>
            <CardBody>
                <Row>
                    <Col>
                        <strong>Descripcion: </strong>
                        {tracking.description}
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <strong>Comentarios: </strong>
                        {tracking.comentarios}
                    </Col>
                </Row>
            </CardBody>
        </Card>
    ));
};


class FacturaCopiaPrintPreview extends React.Component {


    render() {
        const factruaToPrint = this.props.factruaToPrint;
        const ParametroState = this.props.ParametroState;
        const TotalState = this.props.TotalState;
        const ImpuestoState = this.props.ImpuestoState;
        const SubTotalState = this.props.SubTotalState;

        return (
            <div >
                <body>
                    <div style={{ width: '1700px', maxWidth: '1700px' }}>
                        <p style={{ textAlign: 'center', alignContent: 'center', fontSize: '80px', fontWeight: 'bold', textDecorationLine: 'underline' }}>
                            COPIA
                        </p>
                        <p style={{ textAlign: 'center', alignContent: 'center', fontSize: '80px' }}>                          
                             {ParametroState.razonSocial}
                            <br />{ParametroState.empresa}
                            <br />RTN: {ParametroState.rtn}</p>
                        <p style={{ textAlign: 'center', alignContent: 'center', fontSize: '75px' }}>
                            CAI: {ParametroState.cai}
                            <br />
                            Impuesto: {ParametroState.impuesto}%
                        </p>
                        <p style={{ textAlign: 'center', alignContent: 'center', fontSize: '75px' }}>
                            Tel: {ParametroState.contacto}
                            <br />
                            Cliente: {factruaToPrint.cliente}
                            <br />
                            RTN Cliente: {factruaToPrint.rtnCliente}
                            <br />
                            Factura No: {ParametroState.proximoCorrelativo}
                            <br />
                             Fecha: {moment(factruaToPrint.fechaEmision).format("DD-MM-YYYY HH:mm:ss")}
                            <br />
                            Pago de contado
                        </p>
                    </div>
                    <table>
                        <thead>
                            <tr>
                                <th style={{ width: '250px', maxWidth: '250px', wordBreak: 'break-all', fontSize: '70px' }}>CANT</th>
                                <th style={{ width: '700px', maxWidth: '700px', fontSize: '70px' }}>PRODUCTO</th>
                                <th style={{ width: '400px', maxWidth: '400px', wordBreak: 'break-all', fontSize: '70px' }}>$.C/U</th>
                                <th style={{ width: '300px', maxWidth: '300px', wordBreak: 'break-all', fontSize: '70px' }}>$$</th>
                            </tr>
                        </thead>
                    </table>
                    <table>   
                        <tbody>
                            {factruaToPrint.detalle.map(item => (
                                <tr>
                                    <td style={{ width: '250px', maxWidth: '250px', wordBreak: 'break-all', fontSize: '70px' }}>{item.cantidad}</td>
                                    <td style={{ width: '700px', maxWidth: '700px', fontSize: '70px' }}>{item.descripcion}</td>
                                    <td style={{ width: '400px', maxWidth: '400px', wordBreak: 'break-all', fontSize: '70px' }}>{item.precio}</td>
                                    <td style={{ width: '300px', maxWidth: '300px', wordBreak: 'break-all', fontSize: '70px' }}>{(parseFloat(item.precio) * parseInt(item.cantidad)).toFixed(2)}</td>
                                </tr>
                            ))}
                            <tr>
                                <td style={{ width: '250px', maxWidth: '250px', wordBreak: 'break-all', fontSize: '70px' }}></td>
                                <td style={{ width: '700px', maxWidth: '700px', fontSize: '70px' }}></td>
                                <td style={{ width: '400px', maxWidth: '400px', wordBreak: 'break-all', fontSize: '70px' }}><strong>SubTotal</strong></td>
                                <td style={{ width: '300px', maxWidth: '300px', wordBreak: 'break-all', fontSize: '70px' }}>{SubTotalState}</td>
                            </tr>
                            <tr>
                                <td style={{ width: '250px', maxWidth: '250px', wordBreak: 'break-all', fontSize: '70px' }}></td>
                                <td style={{ width: '700px', maxWidth: '700px', fontSize: '70px' }}></td>
                                <td style={{ width: '400px', maxWidth: '400px', wordBreak: 'break-all', fontSize: '70px' }}><strong>Impuesto</strong></td>
                                <td style={{ width: '300px', maxWidth: '200px', wordBreak: 'break-all', fontSize: '70px' }}>{ImpuestoState}</td>
                            </tr>
                            <tr>
                                <td style={{ width: '250px', maxWidth: '250px', wordBreak: 'break-all', fontSize: '70px' }}></td>
                                <td style={{ width: '700px', maxWidth: '700px', fontSize: '70px' }}></td>
                                <td style={{ width: '400px', maxWidth: '400px', wordBreak: 'break-all', fontSize: '70px' }}><strong>Total</strong></td>
                                <td style={{ width: '300px', maxWidth: '300px', wordBreak: 'break-all', fontSize: '70px' }}>{TotalState}</td>
                            </tr>
                        </tbody>
                    </table>
                    <div style={{ width: '1700px', maxWidth: '1700px' }}>
                        <p style={{ textAlign: 'center', alignContent: 'center', fontSize: '65px' }}>
                            Rango Autorizado: {ParametroState.rangoFacturaInicio} - {ParametroState.rangoFacturaFinal}
                            <br />
                            Fecha limite : {moment(ParametroState.fechaLimiteEmision).format("L")}
                        </p>
                    </div>
                   
                </body>
            </div>);
    }
}

export default FacturaCopiaPrintPreview;
