import React, { useState, useEffect } from "react";
import {
    Card,
    CardBody,
    CardHeader,
    Col,
    Row,
    FormGroup,
    Button
} from "reactstrap";
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import API from './../Constantes/Api';
import { ToastContainer, toast } from "react-toastify";
import RegistroFacturasTabla from './registroFacturasTabla';
import FacturaDetalle from './facturaDetalle';
import PrintFacturaCopia from './printFacturaCopia';
import moment from "moment";


const initialFacturaState = {
    step: 1,
    editMode: "None",
    currentFactura: {
        id: 0,
        facturaId: "string",
        cliente: "string",
        estado: "string",
        cai: "string",
        fechaEmision: Date.now,
        rangoInicial: "string",
        rangoFinal: "string",
        proximoCorrelativo: "string",
        proximoNumeroFactura: 0,
        rtnCliente: "",
        status: "string",
        validationErrorMessage: "string",
        enabled: true,
        detalle: [
            {
                facturaId: "string",
                idEncabezado: 0,
                idLinea: 0,
                idArticulo: 0,
                descripcion: "string",
                precio: 0,
                cantidad: 0,
                id: 0,
                status: "string",
                validationErrorMessage: "string",
                enabled: true
            }
        ],
    },
    state: {
        large: false,
    },
};

const RegistroFacuras = props => {
    const [registroFacturaState, setRegistroFacturaState] = useState(initialFacturaState);
    const [facurasState, setfacurasState] = useState({facturas:[]});
    const [ParametroState, setParametroState] = useState({});
    const [apiCallInProgress, setApiCallInProgress] = useState(false);
    const [factruaToPrint, setFacturaToPrint] = useState({});
    const [infoImprimirFacturaState, setInfoImprimirFacturaState] = useState([]);
    const [SubTotalState, setSubTotalState] = useState(0);
    const [ImpuestoState, setImpuestoState] = useState(0);
    const [TotalState, setTotalState] = useState(0);


    function nextPage(step, query) {

        if(query === null)
        {
            fetchFacturas(null, step);
        }
        else
        {
            fetchFacturas(query, step);
        }
       
    }

    function prevPage(step, query) {

        if (query === null) {
            fetchFacturas(query, step);
        }
        else{

        }
    }

    function returnToFacturaList()
    {
        setRegistroFacturaState(initialFacturaState);
    }

    function prevStep() {
        const { step } = registroFacturaState;

        setRegistroFacturaState({ ...registroFacturaState, step: step - 1 });
    }

    function nextStep(supplier) {
        // const { step } = catalogoState;

        // if (supplier === null || supplier === undefined) {
        //     supplier = catalogoState.currentPuesto;
        // }
        // setCatalogoState({ ...catalogoState, currentPuesto: supplier, step: step + 1 });8
    }

    function fetchFacturas(query, step) {
        const index = step === undefined || step === null ? 0 : step;
        var rol = sessionStorage.getItem('rolId');
        if (query === null) {
            var url = `Factura/paged?PageSize=10&PageIndex=${index}`
            API.get(url).then(res => {
                    setfacurasState(res.data);
            }).catch(error => {
                if (error.message === "Network Error") {
                    toast.warn(
                        "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                    );
                } else {
                    if (error.response.status !== undefined) {
                        if (error.response.status === 401) {
                            props.history.push("/login");
                            return <Redirect to="/login" />;
                        }
                    } else {
                        toast.warn(
                            "Se encontraron problemas para obtenre lista de facturas, favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                        );
                    }
                }
            });
        }
        else {
            var url1 = `Factura/paged?pageSize=10&pageIndex=${index}&value=${query}`;
            API.get(url1).then(res => {
                if (res.data) {
                    setfacurasState(res.data.facturas);
                }
            }).catch(error => {
                if (error.message === "Network Error") {
                    toast.warn(
                        "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                    );
                } else {
                    if (error.response.status !== undefined) {
                        if (error.response.status === 401) {
                            props.history.push("/login");
                            return <Redirect to="/login" />;
                        }
                    } else {
                        toast.warn(
                            "Se encontraron problemas para obtenre lista de facturas, favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                        );
                    }
                }
            });
        }
    }

    function fetchParametro() {
        setApiCallInProgress(true);
        var url = `Parametro`
        API.get(url).then(res => {
            setParametroState(res.data);
           
            setApiCallInProgress(false);
        }).catch(error => {
            setApiCallInProgress(false);
            if (error.message === "Network Error") {
                toast.warn(
                    "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                );
            } else {
                if (error.response.status !== undefined) {
                    if (error.response.status === 401) {
                        props.history.push("/login");
                        return <Redirect to="/login" />;
                    }
                } else {
                    toast.warn(
                        "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con el administrador"
                    );
                }
            }
        });
    }

    const editRow = factura => {
        setRegistroFacturaState({
            ...registroFacturaState,
            editMode: "Editing",
            currentFactura: factura,
            step: 2,
        });
    };

    function imprimirFactura() {
        printRow(registroFacturaState.currentFactura);
    }

    const printRow = (factura) => {
        setFacturaToPrint(factura);
        setRegistroFacturaState({ ...registroFacturaState, currentFactura: factura, step: 3 });
    };

    useEffect(() => {
        fetchFacturas(null);
        fetchParametro();
    }, []);

    const cancelPrintRow = () => {
        setRegistroFacturaState({ ...registroFacturaState, step: 1 });
    };

    function GetCurrentStepComponent(step) {
        switch (step) {
            case 1:
                return (
                    <CardBody >
                        <FormGroup row>
                            <Col xs="12">
                                <RegistroFacturasTabla
                                    fetchFacturas={fetchFacturas}
                                    facurasState={facurasState}
                                    nextPage={nextPage}
                                    prevPage={prevPage} 
                                    editRow={editRow}/>
                            </Col>
                        </FormGroup>
                    </CardBody>
                );
            case 2:
                return( 
                    <CardBody >
                        <FormGroup row>
                            <Col xs="12">
                                <FacturaDetalle
                                    registroFacturaState={registroFacturaState}
                                    ParametroState={ParametroState}
                                    imprimirFactura={imprimirFactura} 
                                    setSubTotalState={setSubTotalState}
                                    setImpuestoState={setImpuestoState}
                                    setTotalState={setTotalState}
                                    returnToFacturaList={returnToFacturaList}/>
                            </Col>       
                        </FormGroup>
                    </CardBody>      
                );
            case 3:
                return (
                    <PrintFacturaCopia
                        factruaToPrint={factruaToPrint}
                        cancelPrintRow={cancelPrintRow}
                        ParametroState={ParametroState}
                        TotalState={TotalState}
                        ImpuestoState={ImpuestoState}
                        SubTotalState={SubTotalState}
                    />
                );
            default:
                return null;
        }
    }


    const userStep = GetCurrentStepComponent(registroFacturaState.step);

    return (
        <div className="animated fadeIn" >
            <Card >
                <Row>
                    <Col>
                        <Card>
                            <CardHeader style={{ textAlign: 'center' }}>
                                <i className="fa fa-align-justify" /> articulos
                            </CardHeader>
                            {userStep}
                        </Card>
                    </Col>
                </Row>
            </Card>
            <ToastContainer autoClose={5000} />
        </div>
    );
}

export default RegistroFacuras;