import React, { useState, useEffect } from "react";
import {
    Card,
    CardBody,
    CardHeader,
    Col,
    Row,
    FormGroup,
    Button
} from "reactstrap";
import CatalogoTabla from './catalogoTabla'
import CatalogoDetalle from './catalogoDetalle'
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import API from './../Constantes/Api';
import { ToastContainer, toast } from "react-toastify";



const initialCatalogoState = {
    step: 1,
    editMode: "None",
    currentCatalogo: {
        id: "",
        codigoArticulo: "",
        articulo: "",
        descripcion: "",
        precio: 0.0,
    },
};

// const heightStyle = {
//     height: "600px"
// };

const heightStyleDiv = {
    height: "600px",
    paddingLeft: '0px',
    paddingRight: '0px'
};



const Catalogo = props => {
    const [catalogoState, setCatalogoState] = useState(initialCatalogoState);
    const [articulos, setArticulos] = useState([]);

    const notify = () => toast("Wow so easy !");

    function nextStep(supplier) {
        const { step } = catalogoState;

        if (supplier === null || supplier === undefined) {
            supplier = catalogoState.currentPuesto;
        }
        setCatalogoState({ ...catalogoState, currentPuesto: supplier, step: step + 1 });
    }

    function fetchArticulos(query, step) {
        const index = step === undefined || step === null ? 0 : step;
         var rol = sessionStorage.getItem('rolId');
        //  if (rol === "3") {
        //      props.history.push('/dashboard');
        //      return (<Redirect to='/dashboard'></Redirect>);
        //  }
        if (query === null) {
            var url = `Catalogo/paged?PageSize=10&PageIndex=${index}`
            API.get(url).then(res => {
                setArticulos(res.data);
            }).catch(error => {
                if (error.message === "Network Error") {
                    toast.warn(
                        "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                    );
                } else {
                    if (error.response.status !== undefined) {
                        if (error.response.status === 401) {
                            props.history.push("/login");
                            return <Redirect to="/login" />;
                        }
                    } else {
                        toast.warn(
                            "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                        );
                    }
                }
            });
        }
        else {
            var url1 = `puesto/paged?pageSize=10&pageIndex=${index}&value=${query}`;
            API.get(url1).then(res => {
                setArticulos(res.data);
            }).catch(error => {
                if (error.message === "Network Error") {
                    toast.warn(
                        "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                    );
                } else {
                    if (error.response.status !== undefined) {
                        if (error.response.status === 401) {
                            props.history.push("/login");
                            return <Redirect to="/login" />;
                        }
                    } else {
                        toast.warn(
                            "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                        );
                    }
                }
            });
        }
    }

    function add(newPuesto) {
        if (
            !newPuesto ||
            !newPuesto.articulo ||
            !newPuesto.precio
        ) {
            toast.warn("Faltan datos para poder crear un registro");
            return;
        }
        const url = `Catalogo`;

        API.post(url, newPuesto).then(res => {
            if (res.id === 0) {
                toast.error("Error al intentar agregar puesto");
            } else {
                toast.success("puesto agregado satisfactoriamente");

                fetchArticulos(null);
                setCatalogoState(initialCatalogoState);
            }
        }).catch(error => {
            if (error.message === "Network Error") {
                toast.warn(
                    "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                );
            } else {
                if (error.response.status !== undefined) {
                    if (error.response.status === 401) {
                        props.history.push("/login");
                        return <Redirect to="/login" />;
                    }
                } else {
                    toast.warn(
                        "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                    );
                }
            }
        });
    }

    function update(puesto) {
        if (
            !puesto ||
            !puesto.id
        ) {

            toast.warn("Faltan datos para poder crear un registro");
            return;
        }
        const url = `Catalogo`;
        API.put(url, puesto).then(res => {
            if (res.id === 0) {
                toast.error("Error al intentar agregar puesto");
            } else {
                toast.success("puesto agregado satisfactoriamente");

                // setUsers([...users.users, userAdded]);
                fetchArticulos(null);
                setCatalogoState(initialCatalogoState);
            }
        }).catch(error => {
            if (error.message === "Network Error") {
                toast.warn(
                    "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                );
            } else {
                if (error.response.status !== undefined) {
                    if (error.response.status === 401) {
                        props.history.push("/login");
                        return <Redirect to="/login" />;
                    }
                } else {
                    toast.warn(
                        "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                    );
                }
            }
        });
    }

    const editRow = puesto => {
        setCatalogoState({
            ...catalogoState,
            editMode: "Editing",
            currentPuesto: puesto,
            step: 2,
        });
    };

    const disabledRegister = id => {
        var usuario = sessionStorage.getItem("usuarioId");
        const url = `Catalogo/disabled`;

        const register = {
            id: id,
            user: usuario,
        }
        API.put(url, register).then(res => {
            if (res.data === null || res.data === undefined) {
                toast.error("error al intentar deshabilitar puesto");
                return;
            }
            fetchArticulos(null);
            setCatalogoState(initialCatalogoState);
            toast.success("Registro deshabilitado satisfactoriamente");
        }).catch(error => {
            if (error.message === "Network Error") {
                toast.warn(
                    "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                );
            } else {
                if (error.response.status !== undefined) {
                    if (error.response.status === 401) {
                        props.history.push("/login");
                        return <Redirect to="/login" />;
                    }
                } else {
                    toast.warn(
                        "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                    );
                }
            }
        });
    };

    function nextPage(step) {
        fetchArticulos(null, step);
    }

    function prevPage(step) {
        fetchArticulos(null, step)
    }

    function prevStep() {
        const { step } = catalogoState;

        setCatalogoState({ ...catalogoState, step: step - 1 });
    }

    useEffect(() => {
        fetchArticulos(null);
        // fectRoles();
    }, []);


    function GetCurrentStepComponent(step) {
        switch (step) {
            case 1:
                return (
                    <CardBody >
                        <FormGroup row>
                            <Col />
                            <Col xs="auto">
                                <Button
                                    type="button"
                                    color="success"
                                    onClick={() => nextStep()}>
                                    <i className="fa fa-new" /> Agregar articulo</Button>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col xs="12">
                                <CatalogoTabla 
                                    fetchArticulos={fetchArticulos} 
                                    articulos={articulos} 
                                    nextPage={nextPage}
                                    prevPage={prevPage} 
                                    editRow={editRow} 
                                    disabledRegister={disabledRegister} />
                            </Col>
                        </FormGroup>
                    </CardBody>
                );
            case 2:
                return <CatalogoDetalle currentState={catalogoState} setCatalogoState={setCatalogoState}
                    nextStep={nextStep} prevStep={prevStep}
                    add={add}
                    update={update}
                />;
            default:
                return null;
        }
    }

    const userStep = GetCurrentStepComponent(catalogoState.step);

    return (
        <div className="animated fadeIn" >
            <Card >
                <Row>
                    <Col>
                        <Card>
                            <CardHeader>
                                <i className="fa fa-align-justify" /> articulos
                            </CardHeader>
                            {userStep}
                        </Card>
                    </Col>
                </Row>
            </Card>
            <ToastContainer autoClose={5000} />
        </div>
    );
}
export default Catalogo;