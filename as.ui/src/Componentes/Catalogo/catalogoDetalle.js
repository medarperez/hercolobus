import React, { useState, useEffect } from "react";
import {
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    FormGroup,
    Input,
    Label,
    Row
} from "reactstrap";
import "react-toastify/dist/ReactToastify.css";

const CatalogoDetalle = props => {
    const { currentState, setCatalogoState, nextStep, prevStep, add, update } = props;

    const handleInputChange = event => {
        const { name, value } = event.target;
        setCatalogoState({
            ...currentState,
            currentCatalogo: {
                ...currentState.currentCatalogo,
                [name]: value
            }
        });
    };

    const handleNumericInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        setCatalogoState({
            ...currentState,
            currentCatalogo: {
                ...currentState.currentCatalogo,
                [name]: parseInt(value, 10)
            }
        });
        
    }


    const handleInputChangeMayus = event => {
        const { name, value } = event.target;
        setCatalogoState({
            ...currentState,
            currentCatalogo: {
                ...currentState.currentCatalogo,
                [name]: value.toUpperCase()
            }
        });
    };

    const handleCleanValues = () => {
        const initialClientState = {
            step: 1,
            editMode: "None",
            currentCatalogo: {
                id: "",
                codigoArticulo: "",
                articulo: "",
                descripcion: "",
                precio: 0.0,
            },
        };
        setCatalogoState(initialClientState);
    };

    const handleSaveChanges = () => {
        var usuario = sessionStorage.getItem("userId");
        switch (currentState.editMode) {
            case 'Editing': {
                const service = {
                    id: currentState.currentCatalogo.id,
                    codigoArticulo: currentState.currentCatalogo.codigoArticulo,
                    articulo: currentState.currentCatalogo.articulo,
                    descripcion: currentState.currentCatalogo.descripcion,
                    precio: currentState.currentCatalogo.precio,
                    user: usuario,
                }
                update(service);
                break;
            }

            default: {
                const newService = {
                    codigoArticulo: currentState.currentCatalogo.codigoArticulo,
                    articulo: currentState.currentCatalogo.articulo,
                    descripcion: currentState.currentCatalogo.descripcion,
                    precio: currentState.currentCatalogo.precio,
                    user: usuario,
                }
                add(newService);
                break;
            }
        };
    }



    switch (currentState.editMode) {
        case 'Editing': return (

            <div className="animated fadeIn">
                <Card>
                    <CardHeader>
                        <strong> Datos a editar </strong>
                    </CardHeader>
                    <CardBody>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Id">Codigo Articulo</Label>
                                    <Input
                                        autoFocus
                                        type="text"
                                        name="codigoArticulo"
                                        id="codigoArticulo"
                                        value={currentState.currentCatalogo.codigoArticulo}
                                        onChange={handleInputChangeMayus}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Id">Articulo</Label>
                                    <Input
                                        type="text"
                                        name="articulo"
                                        id="articulo"
                                        value={currentState.currentCatalogo.articulo}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Descripcion</Label>
                                    <Input type="text" id="descripcion" name="descripcion"
                                        value={currentState.currentCatalogo.descripcion}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Precio</Label>
                                    <Input type="number" id="precio" name="precio"
                                        value={currentState.currentCatalogo.precio}
                                        onChange={handleNumericInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            </Row>
                    </CardBody>
                    <CardFooter>
                        <Button type="submit" size="sm" color="success" onClick={handleSaveChanges}>
                            <i className="fa fa-dot-circle-o" /> Guardar
          </Button>
                        <Button type="reset" size="sm" color="danger" onClick={handleCleanValues}>
                            <i className="fa fa-ban" /> Cancelar
          </Button>
                    </CardFooter>
                </Card>
            </div>
        );
        default: return (

            <div className="animated fadeIn">
                <Card>
                    <CardHeader>
                        <strong> Datos de nuevo articulo </strong>
                    </CardHeader>
                    <CardBody>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Id">Codigo Articulo</Label>
                                    <Input
                                        autoFocus
                                        type="text"
                                        name="codigoArticulo"
                                        id="codigoArticulo"
                                        value={currentState.currentCatalogo.codigoArticulo}
                                        onChange={handleInputChangeMayus}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Id">Articulo</Label>
                                    <Input
                                        type="text"
                                        name="articulo"
                                        id="articulo"
                                        value={currentState.currentCatalogo.articulo}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Descripcion</Label>
                                    <Input type="text" id="descripcion" name="descripcion"
                                        value={currentState.currentCatalogo.descripcion}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Precio</Label>
                                    <Input type="number" id="precio" name="precio"
                                        value={currentState.currentCatalogo.precio}
                                        onChange={handleNumericInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                        </Row>

                    </CardBody>
                    <CardFooter>
                        <Button type="submit" size="sm" color="success" onClick={handleSaveChanges}>
                            <i className="fa fa-dot-circle-o" /> Guardar
          </Button>
                        <Button type="reset" size="sm" color="danger" onClick={handleCleanValues}>
                            <i className="fa fa-ban" /> Cancelar
          </Button>
                    </CardFooter>
                </Card>
            </div>
        );

    }
}
export default CatalogoDetalle;

