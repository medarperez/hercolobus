import React, { useState, useEffect } from "react";
import {
    Card,
    CardBody,
    CardHeader,
    Col,
    Row,
    FormGroup,
    Button,
    Label,
    Input,
    CardFooter,
} from "reactstrap";
import moment from "moment";
import "react-toastify/dist/ReactToastify.css";
import ReactLoading from "react-loading";



const ParametroDetalle = props => {
    const { parametroState, setParametroState, add, apiCallInProgress } = props;


    const handleInputChange = event => {
        const { name, value } = event.target;
        setParametroState({
            ...parametroState,
            currentParametro: {
                ...parametroState.currentParametro,
                [name]: value
            }
        });
    };
    

    function getFooterBuild(e) {
        if (apiCallInProgress) {
            return (
                <CardFooter>
                    <Col md="3" sm="6" xs="12"></Col>
                    <Col md="3" sm="6" xs="12">
                        <ReactLoading
                            type="bars"
                            color="#5DBCD2"
                            height={"30%"}
                            width={"30%"}
                        />
                    </Col>
                    <Col md="3" sm="6" xs="12"></Col>
                </CardFooter>
            );
        } else {
            return(
                <CardFooter>
                    <Button type="submit" size="sm" color="success" onClick={() => { add(parametroState.currentParametro) }}>
                        <i className="fa fa-dot-circle-o" /> Guardar Cambios
                 </Button>
                </CardFooter>
            )
        }
    }

    return (<div className="animated fadeIn">
        <Card>
            <CardHeader>
                <strong> Datos bascios de empresa </strong>
            </CardHeader>
            <CardBody>
                <Row>
                    <Col md="3" sm="6" xs="12">
                        <FormGroup>
                            <Label htmlFor="Id">Empresa</Label>
                            <Input
                                autoFocus
                                type="text"
                                name="empresa"
                                id="empresa"
                                value={parametroState.currentParametro.empresa}
                                onChange={handleInputChange}
                                required
                            />
                        </FormGroup>
                    </Col>
                    <Col md="3" sm="6" xs="12">
                        <FormGroup>
                            <Label htmlFor="Id">Direccion</Label>
                            <Input
                                type="text"
                                name="direccion"
                                id="direccion"
                                value={parametroState.currentParametro.direccion}
                                onChange={handleInputChange}
                            />
                        </FormGroup>
                    </Col>
                    <Col md="3" sm="6" xs="12">
                        <FormGroup>
                            <Label htmlFor="Contacto">Contacto</Label>
                            <Input type="text" id="contacto" name="contacto"
                                value={parametroState.currentParametro.contacto}
                                onChange={handleInputChange}
                            />
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col md="3" sm="6" xs="12">
                        <FormGroup>
                            <Label htmlFor="Impuesto">Impuesto</Label>
                            <Input type="number" id="impuesto" name="impuesto"
                                value={parametroState.currentParametro.impuesto}
                                onChange={handleInputChange}
                                required
                            />
                        </FormGroup>
                    </Col>
                    <Col md="3" sm="6" xs="12">
                        <FormGroup>
                            <Label htmlFor="Rtn">RTN</Label>
                            <Input type="text" id="rtn" name="rtn"
                                value={parametroState.currentParametro.rtn}
                                onChange={handleInputChange}
                            />
                        </FormGroup>
                    </Col>
                    <Col md="3" sm="6" xs="12">
                        <FormGroup>
                            <Label htmlFor="Rtn">Razon Social</Label>
                            <Input type="text" id="razonSocial" name="razonSocial"
                                value={parametroState.currentParametro.razonSocial}
                                onChange={handleInputChange}
                            />
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col md="3" sm="6" xs="12">
                        <FormGroup>
                            <Label htmlFor="Cai">CAI</Label>
                            <Input type="text" id="cai" name="cai"
                                value={parametroState.currentParametro.cai}
                                onChange={handleInputChange}
                            />
                        </FormGroup>
                    </Col>
                    <Col md="3" sm="6" xs="12">
                        <FormGroup>
                            <Label htmlFor="RangoFacturaInicio">Rango Inicio</Label>
                            <Input type="text" id="rangoFacturaInicio" name="rangoFacturaInicio"
                                value={parametroState.currentParametro.rangoFacturaInicio}
                                onChange={handleInputChange}
                            />
                        </FormGroup>
                    </Col>
                    <Col md="3" sm="6" xs="12">
                        <FormGroup>
                            <Label htmlFor="RangoFacturaFinal">Rango Final</Label>
                            <Input type="text" id="rangoFacturaFinal" name="rangoFacturaFinal"
                                value={parametroState.currentParametro.rangoFacturaFinal}
                                onChange={handleInputChange}
                            />
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col md="3" sm="6" xs="12">
                        <FormGroup>
                            <Label htmlFor="fechaLimiteEmision">FechaLimiteEmision</Label>
                            <Input
                                type="date"
                                name="fechaLimiteEmision"
                                id="fechaLimiteEmision"
                                placeholder="Fecha Limite Emision"
                                value={moment(
                                    parametroState.currentParametro.fechaLimiteEmision
                                ).format("YYYY-MM-DD")}
                                onChange={handleInputChange}
                            />
                        </FormGroup>
                    </Col>
                    <Col md="3" sm="6" xs="12">
                        <FormGroup>
                            <Label htmlFor="UltimaFacturaGenerada">Ultima Factura Generada</Label>
                            <Input readOnly type="text" id="ultimaFacturaGenerada" name="ultimaFacturaGenerada"
                                value={parametroState.currentParametro.ultimaFacturaGenerada}
                                onChange={handleInputChange}
                            />
                        </FormGroup>
                    </Col>
                    <Col md="3" sm="6" xs="12">
                        <FormGroup>
                            <Label htmlFor="UltimoCorrelativoUtilizado">Ultimo Correlativo</Label>
                            <Input type="text" id="ultimoCorrelativoUtilizado" name="ultimoCorrelativoUtilizado"
                                value={parametroState.currentParametro.ultimoCorrelativoUtilizado}
                                onChange={handleInputChange}
                            />
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col md="3" sm="6" xs="12">
                        <FormGroup>
                            <Label htmlFor="UltimaFacturaGenerada">Proxima factura a generar</Label>
                            <Input readOnly type="text" id="proximaFacturaGenerada" name="proximaFacturaGenerada"
                                value={parametroState.currentParametro.proximoCorrelativo}
                                onChange={handleInputChange}
                            />
                        </FormGroup>
                    </Col>
                    <Col md="3" sm="6" xs="12">
                        <FormGroup>
                            <Label htmlFor="UltimaFacturaGenerada">Proxima numero de factura</Label>
                            <Input readOnly type="text" id="proximoNumero" name="proximoNumero"
                                value={parametroState.currentParametro.proximoNumeroFactura}
                                onChange={handleInputChange}
                            />
                        </FormGroup>
                    </Col>
                </Row>
            </CardBody>
           {getFooterBuild()}
        </Card>
    </div>)
};

export default ParametroDetalle;