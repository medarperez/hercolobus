import React, { useState, useEffect } from "react";
import {
    Card,
    CardBody,
    CardHeader,
    Col,
    Row,
    FormGroup,
    Button
} from "reactstrap";
import ParametroDetalle from './parametroDetalle'
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import API from './../Constantes/Api';
import { ToastContainer, toast } from "react-toastify";

const initialParametroState = {
    step: 1,
    editMode: "None",
    currentParametro: {
        id: 0,
        empresa: "",
        direccion: "",
        contacto: "",
        impuesto: 0.0,
        rtn: "",
        razonSocial: "",
        cai: "",
        rangoFacturaInicio: "",
        rangoFacturaFinal: "",
        fechaLimiteEmision: "",
        ultimaFacturaGenerada: "",
        ultimoCorrelativoUtilizado: 0,
        proximoCorrelativo: "",
        proximoNumeroFactura: 0,
        user: "",
    },
};

const heightStyleDiv = {
    height: "600px",
    paddingLeft: '0px',
    paddingRight: '0px'
};

const Parametro = props => {
    const [parametroState, setParametroState] = useState(initialParametroState);
    const [user, setUser] = useState(sessionStorage.getItem("userId"));
    const [apiCallInProgress, setApiCallInProgress] = useState(false);
   
    
    useEffect(() => {
        fetchParametro();
    }, []);

    function fetchParametro() {
        setApiCallInProgress(true);
            var url = `Parametro`
            API.get(url).then(res => {
                setParametroState({
                    ...parametroState,
                    currentParametro: res.data
                });
                setApiCallInProgress(false);
            }).catch(error => {
                setApiCallInProgress(false);
                if (error.message === "Network Error") {
                    toast.warn(
                        "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                    );
                } else {
                    if (error.response.status !== undefined) {
                        if (error.response.status === 401) {
                            props.history.push("/login");
                            return <Redirect to="/login" />;
                        }
                    } else {
                        toast.warn(
                            "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                        );
                    }
                }
            });
    }

    function add(parametros) {
        setApiCallInProgress(true);
        if (
            !parametros ||
            !parametros.empresa 
        ) {
            toast.warn("Faltan datos para poder crear un registro");
            setApiCallInProgress(false);
            return;
        }

        setParametroState({
            ...parametroState,
            currentParametro: {
                ...parametroState.currentParametro,
                user: user
            }
        });
        const url = `Parametro`;

        if(parametroState.currentParametro.id === 0){

            API.post(url, parametros).then(res => {
                if (res.id === 0) {
                    toast.error("Error al intentar agregar puesto");
                    setApiCallInProgress(false);
                } else {
                    toast.success("puesto agregado satisfactoriamente");

                    setParametroState({
                        ...parametroState,
                        currentParametro: res.data
                    });
                    setApiCallInProgress(false);
                }
            }).catch(error => {
                if (error.message === "Network Error") {
                    toast.warn(
                        "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                    );
                    setApiCallInProgress(false);
                } else {
                    if (error.response.status !== undefined) {
                        if (error.response.status === 401) {
                            props.history.push("/login");
                            setApiCallInProgress(false);
                            return <Redirect to="/login" />;
                           
                        }
                    } else {
                        toast.warn(
                            "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                        );
                        setApiCallInProgress(false);
                    }
                }
            });
        }
        else{
            API.put(url, parametros).then(res => {
                if (res.id === 0) {
                    toast.error("Error al intentar agregar puesto");
                    setApiCallInProgress(false);
                } else {
                    toast.success("puesto agregado satisfactoriamente");

                    setParametroState({
                        ...parametroState,
                        currentParametro: res.data
                    });
                    setApiCallInProgress(false);
                }
            }).catch(error => {
                setApiCallInProgress(false);
                if (error.message === "Network Error") {
                    toast.warn(
                        "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                    );
                    setApiCallInProgress(false);
                } else {
                    if (error.response.status !== undefined) {
                        if (error.response.status === 401) {
                            props.history.push("/login");
                            setApiCallInProgress(false);
                            return <Redirect to="/login" />;
                            
                        }
                    } else {
                        toast.warn(
                            "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                        );
                        setApiCallInProgress(false);
                    }
                }
            });
        }

        

      
    }

    const userStep = GetCurrentStepComponent(parametroState.step);

    function GetCurrentStepComponent(step) {
        switch (step) {
            case 1:
                return (
                    <CardBody >
                        <FormGroup row>
                            <Col xs="12">
                                <ParametroDetalle
                                    parametroState={parametroState}
                                    setParametroState={setParametroState}
                                    add ={add}
                                    apiCallInProgress={apiCallInProgress} />
                            </Col>
                        </FormGroup>
                    </CardBody>
                );
            default:
                return null;
        }
    }

    return(
        <div className="animated fadeIn" >
            <Card >
                <Row>
                    <Col>
                        <Card>
                            <CardHeader>
                                <i className="fa fa-align-justify" /> Informacion general
                            </CardHeader>
                            {userStep}
                        </Card>
                    </Col>
                </Row>
            </Card>
            <ToastContainer autoClose={5000} />
        </div>
    );
}

export default Parametro;