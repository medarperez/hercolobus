import React, { useState, useEffect } from "react";
import ReporteFacturasTabla from './reporteFacturasTabla';
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import API from './../../../Constantes/Api';
import { ToastContainer, toast } from "react-toastify";
import moment from "moment";
import {
    Card,
    CardBody,
    CardHeader,
    Col,
    Row,
    FormGroup,
    Button
} from "reactstrap";


const initialFacturaState = {
    step: 1,
    editMode: "None",
    currentFactura: {
        id: 0,
        facturaId: "string",
        cliente: "string",
        estado: "string",
        cai: "string",
        fechaEmision: Date.now,
        rangoInicial: "string",
        rangoFinal: "string",
        proximoCorrelativo: "string",
        proximoNumeroFactura: 0,
        rtnCliente: "",
        status: "string",
        validationErrorMessage: "string",
        enabled: true,
        detalle: [
            {
                facturaId: "string",
                idEncabezado: 0,
                idLinea: 0,
                idArticulo: 0,
                descripcion: "string",
                precio: 0,
                cantidad: 0,
                id: 0,
                status: "string",
                validationErrorMessage: "string",
                enabled: true
            }
        ],
    },
    state: {
        large: false,
    },
};

const FacturasPorFecha = (props) => {
    const [registroFacturaState, setRegistroFacturaState] = useState(initialFacturaState);
    const [initialDate, setInitialDate] = useState("");
    const [finalDate, setFinalDate] = useState("");
    const [facutrasState, setfacutrasState] = useState([]);
    const [ParametroState, setParametroState] = useState({});
    const [facturaAxportarState, setfacturaAxportarState] = useState([]);

    useEffect(() => {
        fetchParametro();
    }, []);



    function fetchfacturasReport() {;
        if (initialDate === undefined || initialDate === null) return;
        if (finalDate === undefined || finalDate === null) return;

        const stDate = moment(initialDate).format("YYYY-MM-DD HH:mm:ss");
        const endDate = moment(finalDate).format("YYYY-MM-DD HH:mm:ss");
        const user = sessionStorage.getItem("userId");
        const url = `FacturaReportes?request.user=${user}&request.diaActual=false&request.fechaInicial=${stDate}&request.fechaFinal=${endDate}`;

        API.get(url).then((res) => {

            // 
            ObtenerLista(res.data);
        });
    }


    function ObetnerTotalProducto(lista) {
        var conteo = lista.detalle.count();
        return conteo;
    };

    function ObetnerSubTotal(lista) {
        var subTotal = lista.detalle.sum(s => s.total);
        var st = subTotal.toFixed(2);
        return st;
    };

    function ObetnerImpuesto(lista) {
        var subTotal = lista.detalle.sum(s => s.total);
        var impuesto = subTotal * (ParametroState.impuesto / 100);
        var i = impuesto.toFixed(2);
        return i;
    };

    function ObetnerTotal(lista) {
        var subTotal = lista.detalle.sum(s => s.total);
        var impuesto = subTotal * (ParametroState.impuesto / 100);
        var total = subTotal + impuesto;
        var t = total.toFixed(2);
        return t;
    };

    function obtenerReporteFecha(diario) {
        if(diario)
        {
            var fechaActual = Date.now();
            const endDate = moment(fechaActual).format("YYYY-MM-DD HH:mm:ss");
            setInitialDate(fechaActual);
            setFinalDate(fechaActual);
            const user = sessionStorage.getItem("userId");
            const url = `FacturaReportes?request.user=${user}&request.diaActual=${diario}&request.fechaInicial=${endDate}&request.fechaFinal=${endDate}`;

            API.get(url).then((res) => {

                setfacutrasState([]);
                ObtenerLista(res.data);
            });
        }
        else
        {
            setfacutrasState([]);
        }
       
    };

    function ObtenerLista(data) {
        var lista = [];
        data.forEach(element => {
            lista.push({
                facturaNo: element.facturaId,
                fechaEmision: moment(element.fechaEmision).format('DD-MM-YYYY HH:mm:ss'),
                cliente: element.cliente,
                rtnCliente: element.rtnCliente,
                porentajeImpuesto: ParametroState.impuesto,
                cai: element.cai,
                rango: element.rangoInicial + '-' + element.rangoFinal,
                estado: element.estado,
                productos: ObetnerTotalProducto(element),
                subTotal: ObetnerSubTotal(element),
                impuesto: ObetnerImpuesto(element),
                total: ObetnerTotal(element)
            });
        });
        setfacutrasState(lista);
    }

    function fetchParametro() {
        // setApiCallInProgress(true);
        var url = `Parametro`
        API.get(url).then(res => {
            setParametroState(res.data);

            // setApiCallInProgress(false);
        }).catch(error => {
            // setApiCallInProgress(false);
            if (error.message === "Network Error") {
                toast.warn(
                    "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                );
            } else {
                if (error.response.status !== undefined) {
                    if (error.response.status === 401) {
                        props.history.push("/login");
                        return <Redirect to="/login" />;
                    }
                } else {
                    toast.warn(
                        "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con el administrador"
                    );
                }
            }
        });
    }

    function GetCurrentStepComponent(step) {
        switch (step) {
            case 1:
                return (
                    <CardBody >
                        <FormGroup row>
                            <Col xs="12">
                                <ReporteFacturasTabla 
                                    setInitialDate={setInitialDate}
                                    initialDate={initialDate}
                                    setFinalDate={setFinalDate}
                                    finalDate={finalDate}
                                    facutrasState={facutrasState}
                                    fetchfacturasReport={fetchfacturasReport}
                                    ParametroState={ParametroState}
                                    facturaAxportarState={facturaAxportarState}
                                    obtenerReporteFecha={obtenerReporteFecha}/>
                            </Col>
                        </FormGroup>
                    </CardBody>
                );
            default:
                return null;
        }
    }
    const userStep = GetCurrentStepComponent(registroFacturaState.step);

    return (
        <div className="animated fadeIn" >
            <Card >
                <Row>
                    <Col>
                        <Card>
                            <CardHeader style={{ textAlign: 'center' }}>
                                <i className="fa fa-align-justify" /> Datos
                            </CardHeader>
                            {userStep}
                        </Card>
                    </Col>
                </Row>
            </Card>
            <ToastContainer autoClose={5000} />
        </div>);
}

export default FacturasPorFecha;
