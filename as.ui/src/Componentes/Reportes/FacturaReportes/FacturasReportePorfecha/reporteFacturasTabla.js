import React, { useState, useEffect } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import {
    Col,
    Table,
    FormGroup,
    InputGroup,
    InputGroupAddon,
    Button,
    Input,
    Row, 
    Label
} from "reactstrap";
import ReactExport from "react-data-export";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const ReportefacturaTabla = (props) => {
    const {
        setInitialDate
        ,initialDate
        ,setFinalDate
        ,finalDate
        ,facutrasState
        ,fetchfacturasReport
        ,ParametroState
        ,facturaAxportarState,
        obtenerReporteFecha
    } = props;
    const [reporteDiarioState, setreporteDiarioState] = useState(false);

    const thStyle = {
        background: "#20a8d8",
        color: "white"
    };

    const handleChangeInitialDate = (date) => {
        setInitialDate(date);
    };
    const handleChangeFinalDate = (date) => {
        setFinalDate(date);
    };

    const handleInputChange = (event) => {
        setreporteDiarioState(!reporteDiarioState);
        obtenerReporteFecha(!reporteDiarioState);
    };

    
    const excelExportButton = (
        
        <ExcelFile
            filename='ReporteFacturas'
            element={
                <Button type="button" color="success">
                    <i className="fa fa-new" /> Descargar archivo
        </Button>
            }
        >
            <ExcelSheet data={facutrasState} name="Facturas">
                <ExcelColumn label="Factura No." value="facturaNo" />
                <ExcelColumn label="Porcentaje Impuesto" value="porentajeImpuesto" />
                <ExcelColumn label="Emision" value="fechaEmision" />
                <ExcelColumn label="Cliente" value="cliente" />
                <ExcelColumn label="RTN Cliente" value="rtnCliente" />
                <ExcelColumn label="CAI" value="cai" />
                <ExcelColumn label="Rango facturacion" value="rango" />
                <ExcelColumn label="Estado" value="estado" />
                <ExcelColumn label="Productos" value="productos" />
                <ExcelColumn label="Sub-Total" value="subTotal" />
                <ExcelColumn label="Impuesto" value="impuesto" />
                <ExcelColumn label="Total" value="total" />
            </ExcelSheet>
        </ExcelFile>
    );

    const facturasDataRows =
                    <div>
                        <FormGroup row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Row style={{ marginLeft: 3 }}>
                                        <Label htmlFor="Fecha"> Fecha Inicial</Label>
                                    </Row>
                                    <Row style={{ marginLeft: 3 }}>
                                        <DatePicker
                                            style={{ margin: 10 }}
                                            selected={initialDate}
                                            onChange={handleChangeInitialDate}
                                        />
                                    </Row>
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Row style={{ marginLeft: 3 }}>
                                        <Label htmlFor="Fecha"> Fecha Final</Label>
                                    </Row>
                                    <Row style={{ marginLeft: 3 }}>
                                        <DatePicker
                                            style={{ margin: 10 }}
                                            selected={finalDate}
                                            onChange={handleChangeFinalDate}
                                        />
                                    </Row>
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12" style={{ marginTop: "20px" }}>
                                <InputGroup>
                                    <InputGroupAddon addonType="prepend">
                            <Button type="button" color="primary" onClick={fetchfacturasReport}>
                                            Generar <i className="icon-arrow-right-circle" />
                                        </Button>
                                    </InputGroupAddon>
                                </InputGroup>
                            </Col>
                        <Col md="3" sm="6" xs="12" style={{ marginTop: "20px" }}>
                            <FormGroup>
                                <Row>
                                    <Label check>
                                <Input type="checkbox" 
                                    checked={reporteDiarioState}
                                    onChange={handleInputChange} 
                                   />{' '}
                                                    Dia actual
                                                    </Label>
                                </Row>
                            </FormGroup>
                        </Col>
                        <Col md="3" sm="6" xs="12" style={{ marginTop: "20px" }}>{excelExportButton}</Col>
                        </FormGroup>

                        <FormGroup row>
                            <Col md="12">
                                <Table hover bordered striped responsive size="sm">
                                    <thead>
                                        <tr>
                                            <th style={thStyle}>Factura No:</th>
                                            <th style={thStyle}>Fecha Emision</th>
                                            <th style={thStyle}>Cliente</th>
                                            <th style={thStyle}>RTN Cliente</th>
                                            <th style={thStyle}>CAI</th>
                                            <th style={thStyle}>Rango Facturacion</th>
                                            <th style={thStyle}>Estado</th>
                                            <th style={thStyle}>Total prouductos</th>
                                            <th style={thStyle}>Sub Total</th>
                                            <th style={thStyle}>Impuesto</th>
                                            <th style={thStyle}>Total</th>
                                         </tr>
                                    </thead>
                                    <tbody>
                                        {facutrasState.map(item => (
                                            <tr key={item.facturaNo}>
                                                <td>{item.facturaNo}</td>
                                                <td>{item.fechaEmision}</td>
                                                <td>{item.cliente}</td>
                                                <td>{item.rtnCliente}</td>
                                                <td>{item.cai}</td>
                                                <td>{item.rango}</td>
                                                <td>{item.estado}</td>
                                                <td>{item.productos}</td>
                                                <td>{item.subTotal}</td>
                                                <td>{item.impuesto}</td>
                                                <td>{item.total}</td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </Table>
                            </Col>
                        </FormGroup>
                    </div>;
    return facturasDataRows;
}

export default ReportefacturaTabla;
