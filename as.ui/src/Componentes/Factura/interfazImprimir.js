import React, { useState, useEffect } from "react";
import {
    Col,
    Row,
    Button,
    Tooltip,
} from "reactstrap";

const InterfazInprimir = props => {
    const {
        infoImprimirFacturaState,
        imprimirFactura,
        cancelPrintRow
    } = props;
    const [tooltipOpen, setTooltipOpen] = useState(false);

    const toggle = () => setTooltipOpen(!tooltipOpen);
 return(
 <div>
         <Row>
             <Col md="3" sm="6" xs="12" >
                 <Button color="primary" id="FacturarArticulos" onClick={cancelPrintRow}>
                     <i className="fa fa-money"></i><br />Regresar
                    </Button>
                 <Tooltip placement="right" isOpen={tooltipOpen} target="FacturarArticulos" toggle={toggle}>
                     Regresar
                    </Tooltip>
             </Col>
 
             <Col md="3" sm="6" xs="12" >
                 <Button color="primary" id="ImprimirFactura" onClick={imprimirFactura} >
                     <i className="fa fa-money"></i><br />Imprimir
                    </Button>
                 <Tooltip placement="right" isOpen={tooltipOpen} target="ImprimirFactura" toggle={toggle}>
                     imprimir factura
                    </Tooltip>
             </Col>
             <Col></Col>
             <Col></Col>
         </Row>
 </div>);
}

export default InterfazInprimir;