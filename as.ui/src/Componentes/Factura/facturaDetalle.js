import React, { useState, useEffect } from "react";
import API from './../Constantes/Api';
import { ToastContainer, toast } from "react-toastify";
import { Typeahead } from 'react-bootstrap-typeahead';
import ReactLoading from "react-loading";
import {
    Card,
    CardBody,
    CardHeader,
    Col,
    Row,
    FormGroup,
    Label,
    Input,
    Button,
    Tooltip,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter, 
    Table
} from "reactstrap";


const heightStyleDiv = {
    height: "600px",
    paddingLeft: '0px',
    paddingRight: '0px'
};  

const thStyle = {
    background: "#20a8d8",
    color: "white",
    minWidth: '80px',
    textAlign: 'center'
};

const tdStyle = {
    textAlign: 'center'
};

const FacturaDetalle = props => {
    const { 
        FacturaState, 
        ParametroState, 
        toggleAddNewItem,
        CatalogoArticuloState,
        ItemsDetailState,
        agergarArticuloALista,
        SubTotalState,
        ImpuestoState,
        TotalState,
        RemoverItemDelaFactura,
        facturar,
        ClienteState,
        setClienteState,
        RTNClienteState,
        setRTNClienteState,
        LimpiarDatos,
        printRow,
        apiCallInProgress,
        imprimirFactura,
        facturaGuardadaState
    } = props;
    const [tooltipOpen, setTooltipOpen] = useState(false);
    const [singleSelections, setSingleSelections] = useState([]);
    const [ItemSelectedState, setItemSelectedState] = useState([]);
    const [CantidadState, setCantidadState] = useState(0);

    const toggle = () => setTooltipOpen(!tooltipOpen);

    const handleInputChange = event => {
        setItemSelectedState(event);
       if(event)
       {
           if(event.lastItem !== undefined)
           {
               setSingleSelections(event.lastItem);
               setCantidadState(1);
           }
           else{
               setSingleSelections([]);
           }
       }
    };

    const handleInputChangeRTNCliente = event => {
        const { name, value } = event.target;
        setRTNClienteState(value);
    };
    const handleInputChangeCliente = event => {
        const { name, value } = event.target;
        setClienteState(value);
    };

    const handleInputQuantityChange = event => {
        const { name, value } = event.target;

        setCantidadState(value);
    }

    const handleNumericInputChange = (event) => {
        const { name, value } = event.target;
        setCantidadState(parseInt(value, 10));
    };
    
    const cancelAddNewItem = () => {
        setSingleSelections([]);
        setItemSelectedState([]);
        setCantidadState(0);
        toggleAddNewItem();
    };

    function AddItem() {
        agergarArticuloALista(singleSelections, CantidadState);
        setSingleSelections([]);
        setItemSelectedState([]);
        setCantidadState(0);
        toggleAddNewItem();
    };


    function getFooterBuild(e) {
        if (apiCallInProgress) {
            return (
                <Row>
                    <Col md="3" sm="6" xs="12"></Col>
                    <Col md="3" sm="6" xs="12">
                        <ReactLoading
                            type="bars"
                            color="#5DBCD2"
                            height={"30%"}
                            width={"30%"}
                        />
                    </Col>
                    <Col md="3" sm="6" xs="12"></Col>
                    </Row>
            );
        } else {
            return (
                <Row>
                    <Col md="3" sm="6" xs="12" hidden={facturaGuardadaState} >
                        <Button color="primary" id="FacturarArticulos" onClick={facturar}>
                            <i className="fa fa-money"></i><br />Facturar
                    </Button>
                        <Tooltip placement="right" isOpen={tooltipOpen} target="FacturarArticulos" toggle={toggle}>
                            Generar  factura
                    </Tooltip>
                    </Col>
                    <Col md="3" sm="6" xs="12" style={{ margin: '2px' }} >
                        <Button color="primary" id="limpiarfactura" onClick={LimpiarDatos}>
                            <i className="fa fa-refresh"></i><br />Limpiar
                    </Button>
                        <Tooltip placement="right" isOpen={tooltipOpen} target="limpiarfactura" toggle={toggle}>
                            Presiona para limpiar pantalla
                    </Tooltip>
                    </Col>
                    <Col></Col>
                    <Col md="3" sm="6" xs="12" hidden={!facturaGuardadaState} >
                        <Button color="primary" id="ImprimirFactura" onClick={imprimirFactura} >
                            <i className="fa fa-money"></i><br />Imprimir
                    </Button>
                        <Tooltip placement="right" isOpen={tooltipOpen} target="ImprimirFactura" toggle={toggle}>
                            imprimir factura
                    </Tooltip>
                    </Col>
                </Row>
                
            );
        }
    }

    return(
        <div className="animated fadeIn" >
            <Row >
                
                <Col md="3" sm="6" xs="12">
                    <FormGroup>
                        <Label htmlFor="Impuesto">Empresa</Label>
                        <Input readOnly type="text" id="empresa" name="empresa"
                            value={ParametroState.empresa}
                            required
                        />
                    </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                    <FormGroup>
                        <Label htmlFor="Impuesto">Razon Social</Label>
                        <Input readOnly type="text" id="empresa" name="empresa"
                            value={ParametroState.razonSocial}
                            required
                        />
                    </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                    <FormGroup>
                        <Label htmlFor="Impuesto">Direccion</Label>
                        <Input readOnly type="text" id="empresa" name="empresa"
                            value={ParametroState.direccion}
                            required
                        />
                    </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                    <FormGroup>
                        <Label htmlFor="Impuesto">RTN</Label>
                        <Input readOnly type="text" id="empresa" name="empresa"
                            value={ParametroState.rtn}
                            required
                        />
                    </FormGroup>
                </Col>
            </Row>
            <Row>
                <Col md="3" sm="6" xs="12">
                    <FormGroup>
                        <Label htmlFor="Impuesto">FacturaId</Label>
                        <Input readOnly type="text" id="empresa" name="empresa"
                            value={ParametroState.proximoCorrelativo}
                            required
                        />
                    </FormGroup>
                </Col> 
                <Col md="3" sm="6" xs="12">
                    <FormGroup>
                        <Label htmlFor="Impuesto">Rtn Cliente</Label>
                        <Input type="text" id="empresa" name="empresa"
                            value={RTNClienteState}
                            onChange={handleInputChangeRTNCliente}
                            required
                        />
                    </FormGroup>
                </Col> 
                <Col md="3" sm="6" xs="12">
                    <FormGroup>
                        <Label htmlFor="Impuesto">Cliente</Label>
                        <Input type="text" id="cliente" name="cliente"
                            value={ClienteState}
                            onChange={handleInputChangeCliente}
                        />
                    </FormGroup>
                </Col> 
            </Row>
            <Row>
                <Col>
                    <Button color="primary" id="AgregarArticulo" onClick={toggleAddNewItem}>
                        <i className="fa fa-cart-plus"></i><br />Agregar
                    </Button>
                    <Tooltip placement="right" isOpen={tooltipOpen} target="AgregarArticulo" toggle={toggle}>
                        Agregar item a factura
                    </Tooltip>
                </Col>
            </Row>
            <Row>
                <Col >
                    <Table hover bordered striped responsive size="sm">
                        <thead>
                            <tr>
                                <th style={thStyle}>Borrar</th>
                                <th style={thStyle}>Linea</th>
                                <th style={thStyle}>Codigo</th>
                                <th style={thStyle}>Articulo</th>
                                <th style={thStyle}>Descripcion</th>
                                <th style={thStyle}>Precio</th>
                                <th style={thStyle}>Cantidad</th>
                                <th style={thStyle}>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            {ItemsDetailState.map((item) => (
                                <tr key={item.id}>
                                    {/* <td>
                                        <Button
                                            block
                                            color="warning"
                                            // onClick={() => {
                                            //     editRow(negotiation);
                                            // }} 
                                            >
                                            Editar
                                        </Button>
                                    </td> */}
                                    <td>
                                        <Button
                                            block
                                            color="warning"
                                        onClick={() => {
                                            RemoverItemDelaFactura(item);
                                        }} 
                                        >
                                            Remover
                                        </Button>
                                    </td>
                                    <td style={tdStyle}>{item.idLinea}</td>
                                    <td style={tdStyle}>{item.codigoArticulo}</td>
                                    <td style={tdStyle}>{item.articulo}</td>
                                    <td style={tdStyle}>{item.descripcion}</td>
                                    <td style={tdStyle}>{item.precio}</td>
                                    <td style={tdStyle}>{item.cantidad}</td>
                                    <td style={tdStyle}>{item.total}</td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
              </Col>
            </Row>
            <Row>
                <Col md="3" sm="6" xs="12" >
                    <Label htmlFor="Impuesto">Sub-Total: {SubTotalState}</Label>
                </Col>
                <Col md="3" sm="6" xs="12" >
                    <Label htmlFor="Impuesto">Impuesto: {ImpuestoState}</Label>
                </Col>
                <Col md="3" sm="6" xs="12" >
                    <Label htmlFor="Impuesto">Total: {TotalState}</Label>
                </Col>
            </Row>
            {getFooterBuild()}
            {/* <Row>
                
                <Col md="3" sm="6" xs="12" >
                    <Button color="primary" id="FacturarArticulos" onClick={facturar}>
                        <i className="fa fa-money"></i><br />Facturar
                    </Button>
                    <Tooltip placement="right" isOpen={tooltipOpen} target="FacturarArticulos" toggle={toggle}>
                        Generar  factura
                    </Tooltip>
                </Col>
                <Col md="3" sm="6" xs="12" style={{ margin: '2px'}}>
                    <Button color="primary" id="limpiarfactura" onClick={LimpiarDatos}>
                        <i className="fa fa-refresh"></i><br />Limpiar
                    </Button>
                    <Tooltip placement="right" isOpen={tooltipOpen} target="limpiarfactura" toggle={toggle}>
                       Presiona para limpiar pantalla 
                    </Tooltip>
                </Col>
                <Col></Col>
            </Row> */}
            <Modal
                isOpen={FacturaState.state.large}
                toggle={toggleAddNewItem}
                className={"modal-lg"}
            >
                <ModalHeader toggle={toggleAddNewItem}>
                    Agregar nuevo item a factura
                </ModalHeader>
                <ModalBody>
                        <Card>
                           <Row>
                                <Col md="3" sm="6" xs="12" >
                                   <Label htmlFor="Impuesto">Ingresa un articulo</Label>
                                    <Typeahead
                                        id="listaCatalogo"
                                        labelKey="articulo"
                                        onChange={handleInputChange}
                                        options={CatalogoArticuloState}
                                        placeholder="Elige un articulo..."
                                        selected={ItemSelectedState}
                                    />
                                    <Label htmlFor="descripcion">Codigo: {singleSelections.codigoArticulo} - {singleSelections.descripcion}</Label>
                               </Col>
                                <Col md="3" sm="6" xs="12">
                                    <Label htmlFor="Impuesto">Precio unitario</Label>
                                    <Input type="text" id="empresa" name="empresa"
                                        value={singleSelections.precio}
                                        readOnly
                                    />
                                </Col>
                                <Col md="3" sm="6" xs="12">
                                    <Label htmlFor="Canidad">Ingrese cantidad</Label>
                                    <Input type="number" id="cantidad" name="cantidad"
                                    onChange={handleNumericInputChange}
                                        value={CantidadState}
                                    />
                                </Col>
                           </Row>
                        </Card>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={AddItem}>
                        Agregar
                    </Button>
                    <Button color="danger" onClick={cancelAddNewItem}>
                        Cancelar
                    </Button>
                </ModalFooter>
            </Modal>
        </div>
    );
}

export default FacturaDetalle;