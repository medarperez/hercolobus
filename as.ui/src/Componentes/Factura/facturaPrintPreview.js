import React from "react";
// import logo from "../../../assets/img/brand/logoJRE.svg";
import moment from "moment";
import { CardBody, CardHeader, Card, Row, Col } from "reactstrap";
import styled from "styled-components";

const getClientFullName = (client) => {
    const name = client
        ? (client.firstName || "") +
        " " +
        (client.middleName || "") +
        " " +
        (client.firstSurname || "") +
        " " +
        (client.secondSurname || "")
        : "";

    return name;
};

const GridContainer = styled.div`
  display: grid;
  grid-template-rows: 30px 30px 30px 30px 30px 30px 30px 30px 5px 30px 5px Auto 30px ;
  grid-gap: 10px;
  height: auto;
  padding: 10px;
`;

const GridContainerColumns = styled.div`
  display: grid;
  grid-template-columns: 200px 400px 200px;
  grid-gap: 10px;
  padding: 10px;
`;

const GridContainerColumnsDetalle = styled.div`
  display: grid;
  grid-template-columns: 30px 240px 30px;
  grid-gap: 10px;
  padding: 10px;
`;

const GridContainerColumnsInverso = styled.div`
  display: grid;
  grid-template-columns: 200px 50px 50px;
  grid-gap: 10px;
  padding: 10px;
`;

const GridContainerDetalle = styled.div`
  display: grid;
  grid-template-rows: 10px;
  grid-gap: 2px;
  padding: 2px;
`;

const GridContainerColumnsDelimitador = styled.div`
  display: grid;
  grid-template-columns: 300px;
  grid-gap: 10px;
  padding: 10px;
`;





const getTrackingInfo = (clientTracking) => {
    return clientTracking.map((tracking) => (
        <Card>
            <CardHeader>Cita {moment(tracking.fechaCita).format("L")}</CardHeader>
            <CardBody>
                <Row>
                    <Col>
                        <strong>Descripcion: </strong>
                        {tracking.description}
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <strong>Comentarios: </strong>
                        {tracking.comentarios}
                    </Col>
                </Row>
            </CardBody>
        </Card>
    ));
};


class FacturaPrintPreview extends React.Component {

    
    render() {
        const factruaToPrint = this.props.factruaToPrint;
        const ParametroState = this.props.ParametroState;
        const TotalState = this.props.TotalState;
        const ImpuestoState = this.props.ImpuestoState;
        const SubTotalState = this.props.SubTotalState;

        // return (
        //         <GridContainer>
        //             <GridContainerColumns>
        //                 <label></label>
        //                 <label style={{fontSize:'10px', textAlign:'center'}}><strong>{ParametroState.empresa}</strong></label>
        //             </GridContainerColumns>

        //             <GridContainerColumns>
        //                 <label></label>
        //                 <label style={{ fontSize: '10px', textAlign: 'center' }}><strong>{ParametroState.razonSocial}</strong></label>
        //                 <label></label>
        //             </GridContainerColumns>

        //             <GridContainerColumns>
        //                 <label></label>
        //             <label style={{ fontSize: '10px', textAlign: 'center' }}>RTN: {ParametroState.rtn}</label>
        //             </GridContainerColumns>

        //             <GridContainerColumns>
        //              <label></label>
        //             <label style={{ fontSize: '10px', textAlign: 'center' }}>TEL. {ParametroState.contacto}</label>
        //             </GridContainerColumns>

        //             <GridContainerColumnsInverso>
        //                 <label style={{ fontSize: '10px', textAlign: 'center' }}>Fac. {factruaToPrint.facturaId}</label>
        //                 <label></label>
        //             </GridContainerColumnsInverso>

        //             <GridContainerColumnsInverso> 
        //                 <label style={{ fontSize: '10px', textAlign: 'left' }}>Cliente. {factruaToPrint.cliente}</label>
        //                 <label></label>
        //             </GridContainerColumnsInverso>

        //             <GridContainerColumnsInverso> 
        //                 <label style={{ fontSize: '10px', textAlign: 'left' }}>RTN cliente. {factruaToPrint.rtnCliente}</label>
        //                 <label></label>
        //             </GridContainerColumnsInverso>

        //             <GridContainerColumnsInverso>
        //                 <label style={{ fontSize: '10px', textAlign: 'left' }}>Pago de contado. {factruaToPrint.rtnCliente}</label>
        //                 <label></label>
        //             </GridContainerColumnsInverso>

        //             <GridContainerColumnsInverso>    
        //             <label style={{ fontSize: '10px', textAlign: 'left' }}>Emision. {moment(factruaToPrint.fechaEmision).format(
        //                 "YYYY-MM-DD"
        //             )}</label>
        //                 <label></label>
        //             </GridContainerColumnsInverso>

        //             <GridContainerColumnsDelimitador>
        //             <label style={{ fontSize: '10px', textAlign: 'left' }}>________________________________________________________________________</label>
        //             </GridContainerColumnsDelimitador>

        //             <GridContainerColumns>
        //                 <label style={{ fontSize: '10px', textAlign: 'left' }}>Cant.</label>
        //                 <label style={{ fontSize: '10px', textAlign: 'center' }}>Descripcion</label>
        //                 <label style={{ fontSize: '10px', textAlign: 'left' }}>Total</label>
        //             </GridContainerColumns>

        //             <GridContainerColumnsDelimitador>
        //                 <label style={{ fontSize: '10px', textAlign: 'left' }}>________________________________________________________________________</label>
        //             </GridContainerColumnsDelimitador>
        //             <GridContainerColumns>
        //                 {factruaToPrint.detalle.map(item => (
        //                     <GridContainerDetalle>
        //                         <GridContainerColumnsDetalle>
        //                             <label style={{ fontSize: '10px', textAlign: 'left' }}>{item.cantidad}</label>
        //                             <label style={{ fontSize: '9px', textAlign: 'center' }}>{item.descripcion} <label style={{ fontSize: '6px' }}>Precio.Unit.LPS{item.precio}</label></label>
        //                             <label style={{ fontSize: '10px', textAlign: 'left' }}>{parseFloat(item.precio) * parseInt(item.cantidad)}</label>
        //                         </GridContainerColumnsDetalle>
        //                     </GridContainerDetalle>
        //                 ))}
        //             </GridContainerColumns>                
        //         </GridContainer>
        // );



        return(
            <div >
            <body>
                    <div style={{ width: '1700px', maxWidth: '1700px'}}>
                        <p style={{ textAlign: 'center', alignContent: 'center', fontSize: '80px'}}>
                             {ParametroState.razonSocial}
                            <br/>{ParametroState.empresa}
                            <br/>RTN: {ParametroState.rtn}</p>
                        <p style={{ textAlign: 'center', alignContent: 'center', fontSize: '75px' }}>
                            CAI: {ParametroState.cai}
                            <br />
                            Rango: {ParametroState.rangoFacturaInicio} - {ParametroState.rangoFacturaFinal}
                            <br />
                            Fecha limite : {moment(ParametroState.fechaLimiteEmision).format("L")}
                            <br />
                            Impuesto: {ParametroState.impuesto}%
                        </p>
                        <p style={{ textAlign: 'center', alignContent: 'center', fontSize: '75px' }}>
                            Tel: {ParametroState.contacto}
                            <br/>
                            Cliente: {factruaToPrint.cliente}
                            <br/>
                            RTN Cliente: {factruaToPrint.rtnCliente}
                            <br/>
                            Factura No: {ParametroState.proximoCorrelativo}
                            <br/>
                             Fecha: {moment(factruaToPrint.fechaEmision).format("DD-MM-YYYY HH:mm:ss")}
                             <br/>
                            Pago de contado
                        </p>
                    </div>
                    <table>
                        <thead>
                            <tr>
                                <th style={{ width: '250px', maxWidth: '250px', wordBreak: 'break-all', fontSize: '70px'}}>CANT</th>
                                <th style={{ width: '700px', maxWidth: '700px', fontSize: '70px'}}>PRODUCTO</th>
                                <th style={{ width: '400px', maxWidth: '400px', wordBreak: 'break-all', fontSize: '70px'}}>$.C/U</th>
                                <th style={{ width: '300px', maxWidth: '300px', wordBreak: 'break-all', fontSize: '70px' }}>$$</th>
                            </tr>
                        </thead>
                        <tbody>
                            {factruaToPrint.detalle.map(item => (
                                <tr>
                                    <td style={{ width: '250px', maxWidth: '250px', wordBreak: 'break-all', fontSize: '70px' }}>{item.cantidad}</td>
                                    <td style={{ width: '700px', maxWidth: '700px', fontSize: '70px' }}>{item.descripcion}</td>
                                    <td style={{ width: '400px', maxWidth: '400px', wordBreak: 'break-all', fontSize: '70px' }}>{item.precio}</td>
                                    <td style={{ width: '300px', maxWidth: '300px', wordBreak: 'break-all', fontSize: '70px' }}>{(parseFloat(item.precio) * parseInt(item.cantidad)).toFixed(2)}</td>
                                </tr>
                            ))}
                            <tr>
                                <td style={{ width: '250px', maxWidth: '250px', wordBreak: 'break-all', fontSize: '70px' }}></td>
                                <td style={{ width: '700px', maxWidth: '700px', fontSize: '70px' }}></td>
                                <td style={{ width: '400px', maxWidth: '400px', wordBreak: 'break-all', fontSize: '70px' }}><strong>SubTotal</strong></td>
                                <td style={{ width: '300px', maxWidth: '300px', wordBreak: 'break-all', fontSize: '70px' }}>{SubTotalState}</td>
                            </tr>
                            <tr>
                                <td style={{ width: '250px', maxWidth: '250px', wordBreak: 'break-all', fontSize: '70px' }}></td>
                                <td style={{ width: '700px', maxWidth: '700px', fontSize: '70px' }}></td>
                                <td style={{ width: '400px', maxWidth: '400px', wordBreak: 'break-all', fontSize: '70px' }}><strong>Impuesto</strong></td>
                                <td style={{ width: '300px', maxWidth: '200px', wordBreak: 'break-all', fontSize: '70px' }}>{ImpuestoState}</td>
                            </tr>
                            <tr>
                                <td style={{ width: '250px', maxWidth: '250px', wordBreak: 'break-all', fontSize: '70px' }}></td>
                                <td style={{ width: '700px', maxWidth: '700px', fontSize: '70px' }}></td>
                                <td style={{ width: '400px', maxWidth: '400px', wordBreak: 'break-all', fontSize: '70px' }}><strong>Total</strong></td>
                                <td style={{ width: '300px', maxWidth: '300px', wordBreak: 'break-all', fontSize: '70px' }}>{TotalState}</td>
                            </tr>
                        </tbody>
                    </table>
            </body>
        </div>);
    }
}

export default FacturaPrintPreview;
