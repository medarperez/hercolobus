import React, { useState, useEffect } from "react";
import API from './../Constantes/Api';
import { ToastContainer, toast } from "react-toastify";
import {
    Card,
    CardBody,
    CardHeader,
    Col,
    Row,
    FormGroup,
} from "reactstrap";
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import FacturaDetalle from './facturaDetalle';
import PrintFactura from "./printFactura";
import InterfazImprimir from "./interfazImprimir";

const heightStyleDiv = {
    height: "600px",
    paddingLeft: '0px',
    paddingRight: '0px'
};


const initialFacturaState = {
    step: 1,
    editMode: "None",
    currentFactura: {
        id: 0,
        facturaId: "string",
        cliente: "string",
        estado: "string",
        cai: "string",
        fechaEmision: Date.now,
        rangoInicial: "string",
        rangoFinal: "string",
        proximoCorrelativo: "string",
        proximoNumeroFactura: 0,
        rtnCliente: "",
        status: "string",
        validationErrorMessage: "string",
        enabled: true,
        detalle: [
            {
                facturaId: "string",
                idEncabezado: 0,
                idLinea: 0,
                idArticulo: 0,
                descripcion: "string",
                precio: 0,
                cantidad: 0,
                id: 0,
                status: "string",
                validationErrorMessage: "string",
                enabled: true
            }
        ],
    },
    state: {
        large: false,
    },
};
const ImpresionFacturaState = {
    step: 3,
    editMode: "None",
    currentFactura: {
        id: 0,
        facturaId: "string",
        cliente: "string",
        estado: "string",
        cai: "string",
        fechaEmision: Date.now,
        rangoInicial: "string",
        rangoFinal: "string",
        proximoCorrelativo: "string",
        proximoNumeroFactura: 0,
        rtnCliente: "",
        status: "string",
        validationErrorMessage: "string",
        enabled: true,
        detalle: [
            {
                facturaId: "string",
                idEncabezado: 0,
                idLinea: 0,
                idArticulo: 0,
                descripcion: "string",
                precio: 0,
                cantidad: 0,
                id: 0,
                status: "string",
                validationErrorMessage: "string",
                enabled: true
            }
        ],
    },
    state: {
        large: false,
    },
};


const Factura = props => {
    const [FacturaState, setFacturaState] = useState(initialFacturaState);
    const [factuaAImprimirState, setFactuaAImprimirState] = useState({});
    const [ParametroState, setParametroState] = useState({});
    const [apiCallInProgress, setApiCallInProgress] = useState(false);
    const [CatalogoArticuloState, setCatalogoArticuloState] = useState([]);
    const [ItemsDetailState, setItemsDetailState] = useState([]);
    const [SubTotalState, setSubTotalState] = useState(0);
    const [ImpuestoState, setImpuestoState] = useState(0);
    const [TotalState, setTotalState] = useState(0);
    const [ClienteState, setClienteState] = useState("");
    const [RTNClienteState, setRTNClienteState] = useState("");
    const [factruaToPrint, setFacturaToPrint] = useState([]);
    const [infoImprimirFacturaState, setInfoImprimirFacturaState] = useState([]);
    const [facturaGuardadaState, setFacturaGuardadaState] = useState(false);

    useEffect(() => {
        fetchParametro();
        fetchCatalogo();
    }, []);


    function LimpiarDatos(){
        setParametroState({});
        setFacturaState(initialFacturaState)
        setItemsDetailState([]);
        setSubTotalState(0);
        setImpuestoState(0);
        setTotalState("");
        setRTNClienteState("");
        setClienteState("");
        fetchParametro();
    }
    function RemoverItemDelaFactura(item) {

        var lista = ItemsDetailState;
        var listaNueva = lista.where(s => s.idLinea !== item.idLinea);
 
            var numero = 1;
           listaNueva.forEach(element => {

                element.idLinea = numero;
                numero += 1;
            });
        setItemsDetailState(listaNueva);

        var subTotal = listaNueva.sum(s => s.total);
        var impuesto = subTotal * (ParametroState.impuesto / 100);
        var total = subTotal + impuesto;
        setImpuestoState(Math.round(impuesto));
        setSubTotalState(Math.round(subTotal));
        setTotalState(Math.round(total));
    }

    function agergarArticuloALista(articulo, cantidad){
      var item = {
          facturaId: ParametroState.proximoCorrelativo,
          idLinea: 0,
          idArticulo: articulo.id,
          codigoArticulo: articulo.codigoArticulo,
          descripcion: articulo.descripcion,
          precio: parseFloat(articulo.precio),
          cantidad: cantidad,
          articulo: articulo.articulo,
          total: parseFloat(articulo.precio) *  cantidad,
          id: 0
      };
       
      
      var lista = ItemsDetailState;
      if(lista.length == 0)
      {
          item.idLinea = 1
          lista.push(item);
          setItemsDetailState(lista);
      }
      else{

          lista.push(item);
          var numero= 1;
          lista.forEach(element => {
            
              element.idLinea =  numero;
              numero += 1;
          });
          setItemsDetailState(lista);
      }
        var subTotal = lista.sum(s => s.total);
        var impuesto = subTotal * (ParametroState.impuesto/100);
        var total = subTotal + impuesto;
        setImpuestoState(impuesto.toFixed(2));
        setSubTotalState(subTotal.toFixed(2));
        setTotalState(total.toFixed(2));
    }

    function fetchParametro() {
        setApiCallInProgress(true);
        var url = `Parametro`
        API.get(url).then(res => {
            setParametroState(res.data);
            setFacturaState({
                ...FacturaState,
                currentFactura: {
                    ...FacturaState.currentFactura,
                    facturaId: res.data.proximoCorrelativo,
                    proximoNumeroFactura: res.data.proximoNumeroFactura,
                    proximoCorrelativo: res.data.proximoCorrelativo
                }})
            setApiCallInProgress(false);
        }).catch(error => {
            setApiCallInProgress(false);
            if (error.message === "Network Error") {
                toast.warn(
                    "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                );
            } else {
                if (error.response.status !== undefined) {
                    if (error.response.status === 401) {
                        props.history.push("/login");
                        return <Redirect to="/login" />;
                    }
                } else {
                    toast.warn(
                        "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con el administrador"
                    );
                }
            }
        });
    }

    function procesofacturar(){
        setApiCallInProgress(true);
        var url = `Factura`;
        // var usuario = sessionStorage.getItem("usuarioId");

    }

    function facturar(){
        setApiCallInProgress(true);
        var url = `Factura`;
        var usuario = sessionStorage.getItem("usuarioId");
        // var usuario = 'sistema';
        if(ItemsDetailState === undefined || ItemsDetailState === null || ItemsDetailState.length === 0)
        {
          toast.warn("No se puede crear una factura vacia!");
            setApiCallInProgress(false);
          return;
        }
        // toast.warn("Armando el requets");
        var request = {
            facturaId: ParametroState.proximoCorrelativo,
            cliente: ClienteState,
            estado: "ACTIVA",
            cai: ParametroState.cai,
            fechaEmision: new Date(),
            rangoInicial: ParametroState.rangoFacturaInicio,
            rangoFinal: ParametroState.rangoFacturaFinal,
            proximoCorrelativo: ParametroState.proximoCorrelativo,
            proximoNumeroFactura: ParametroState.proximoNumeroFactura,
            rtnCliente: RTNClienteState,
            detalle: [],
            user: usuario,
            role: "string",
            transaction: "string",
            id: 0
        };

        // toast.warn("Armando el detalle requets");
        var detalle = [];
        ItemsDetailState.forEach(element => {
            detalle.push(
                {
                    facturaId: ParametroState.proximoCorrelativo,
                    idLinea: element.idLinea,
                    idArticulo: element.idArticulo,
                    descripcion: element.descripcion,
                    precio: element.precio,
                    cantidad: element.cantidad,
                    user: usuario,
                    role: "string",
                    transaction: "string",
                    id: 0
                }
            )
        });

        // toast.warn("Asignando el detalle");
        request.detalle = detalle;
        API.post(url, request).then(res => {
            toast.success(
                "Facturando"
            );
            setApiCallInProgress(false);
            // setFactuaAImprimirState(res.data);
            // printRow(res.data);
             setInfoImprimirFacturaState(res.data);
            // LimpiarDatos();
            setFacturaGuardadaState(true);
            // setFacturaState(ImpresionFacturaState);
        })
        .catch(error => {
            if (error.message === "Network Error") {
                toast.warn(
                    "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                );
                setFacturaGuardadaState(false);
                setApiCallInProgress(false);
            } else {
                setApiCallInProgress(false);
                setFacturaGuardadaState(false);
                if (error.response.status !== undefined) {
                    if (error.response.status === 401) {
                        props.history.push("/login");
                        setApiCallInProgress(false);
                        return <Redirect to="/login" />;

                    }
                    else
                    {
                        toast.warn("Error desconocido");
                        setApiCallInProgress(false);

                    }
                } else {
                    toast.warn(
                        "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                    );
                    setApiCallInProgress(false);
                }
            }
        });

    }

    function imprimirFactura(){
        printRow(infoImprimirFacturaState);
    }
        

    function fetchCatalogo(){
        setApiCallInProgress(true);
        var url = `Catalogo`
        API.get(url).then(res => {
            // debugger;
            setCatalogoArticuloState(res.data);
            setApiCallInProgress(false);

        }).catch(error => {
            setApiCallInProgress(false);
            if (error.message === "Network Error") {
                toast.warn(
                    "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                );
            } else {
                if (error.response.status !== undefined) {
                    if (error.response.status === 401) {
                        props.history.push("/login");
                        return <Redirect to="/login" />;
                    }
                } else {
                    toast.warn(
                        "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con el administrador"
                    );
                }
            }
        });
    }

    const printRow = (factura) => {
        setFacturaToPrint(factura);
        setFacturaState({ ...FacturaState, currentFactura: factura, step: 2 });
    };

    const toggleAddNewItem = () => {
        setFacturaState({
            ...FacturaState,
            state: {
                large: !FacturaState.state.large,
            },
        });
    };


    const cancelPrintRow = () => {
        setFacturaState({ ...FacturaState, step: 1 });
    };

    const userStep = GetCurrentStepComponent(FacturaState.step);

    function GetCurrentStepComponent(step) {
        switch (step) {
            case 1:
                return (
                    <CardBody >
                        <FormGroup row>
                            <Col xs="12">
                                <FacturaDetalle
                                    FacturaState={FacturaState}
                                    ParametroState={ParametroState}
                                    toggleAddNewItem={toggleAddNewItem}
                                    CatalogoArticuloState={CatalogoArticuloState}
                                    ItemsDetailState={ItemsDetailState}
                                    agergarArticuloALista={agergarArticuloALista}
                                    SubTotalState={SubTotalState}
                                    ImpuestoState={ImpuestoState}
                                    TotalState={TotalState}
                                    RemoverItemDelaFactura={RemoverItemDelaFactura}
                                    facturar={facturar}
                                    ClienteState={ClienteState}
                                    setClienteState={setClienteState}
                                    RTNClienteState={RTNClienteState}
                                    setRTNClienteState={setRTNClienteState}
                                    LimpiarDatos={LimpiarDatos}
                                    printRow={printRow}
                                    apiCallInProgress={apiCallInProgress}
                                    imprimirFactura={imprimirFactura}
                                    facturaGuardadaState={facturaGuardadaState}
                                     />
                            </Col>
                        </FormGroup>
                    </CardBody>
                );
            case 2:
                return (
                    <PrintFactura
                        factruaToPrint={factruaToPrint}
                        cancelPrintRow={cancelPrintRow}
                        ParametroState={ParametroState}
                        TotalState={TotalState}
                        ImpuestoState={ImpuestoState}
                        SubTotalState={SubTotalState}
                    />
                );
            case 3:
                return(
                    <InterfazImprimir
                        infoImprimirFacturaState={infoImprimirFacturaState}
                        imprimirFactura={imprimirFactura}
                        cancelPrintRow={cancelPrintRow}/>
                );
            default:
                return null;
        }
    }

    return (
        <div className="animated fadeIn" >
            <Card >
                <Row>
                    <Col>
                        <Card>
                            <CardHeader style={{ textAlign:'center'}}>
                                <i className="fa fa-align-justify" /> <strong>Facturación</strong>
                            </CardHeader>
                            {userStep}
                        </Card>
                    </Col>
                </Row>
            </Card>
            <ToastContainer autoClose={5000} />
        </div>
    );
}

export default Factura;