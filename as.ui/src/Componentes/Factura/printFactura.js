import React from "react";
import ReactToPrint from "react-to-print";
import FacturaPrintPreview from "./facturaPrintPreview";
import { Col, FormGroup, Button } from "reactstrap";

class PrintFactura extends React.Component {
    render() {
        const factruaToPrint = this.props.factruaToPrint;
        const ParametroState = this.props.ParametroState;
        const TotalState = this.props.TotalState;
        const ImpuestoState = this.props.ImpuestoState;
        const SubTotalState = this.props.SubTotalState;
        return (
            <div>
                <FormGroup row>
                    <Col />
                    <Col />

                    <Col>
                        <ReactToPrint
                            trigger={() => (
                                <Button block color="success">
                                    Imprimir Factura
                                </Button>
                            )}
                            content={() => this.componentRef}
                        />
                    </Col>

                    <Col>
                        <Button block color="success" onClick={this.props.cancelPrintRow}>
                            Cancelar
            </Button>
                    </Col>
                </FormGroup>
                {/* <div style={{ height:'auto',  minHeight: 'auto', margin: '0px', maxHeight: 'auto'}} > */}
                    <FacturaPrintPreview
                        factruaToPrint={factruaToPrint}
                        ParametroState={ParametroState}
                        TotalState={TotalState}
                        ImpuestoState={ImpuestoState}
                        SubTotalState={SubTotalState}
                        ref={(el) => (this.componentRef = el)}
                    />
                {/* </div> */}

            </div>
        );
    }
}

export default PrintFactura;
