import React, { useState, useEffect } from "react";
import {
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    FormGroup,
    Input,
    Label,
    Row
} from "reactstrap";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer, toast } from "react-toastify";

const ClienteDetalle = props => {
    const { currentState, setClientestate, nextStep, prevStep, add, update } = props;

    const handleInputChange = event => {
        const { name, value } = event.target;
        setClientestate({
            ...currentState,
            currentCliente: {
                ...currentState.currentCliente,
                [name]: value
            }
        });
    };
    const handleCleanValues = () => {
        const initialClientState = {
            step: 1,
            editMode: "None",
            currentCliente: {
                id: "",
                nombre: "",
                apellido: "",
                identidad: "",
                direccion: "",
                telefono: "",
                celular: "",
                correo: "",
                enabled: true,
            },
        };
        setClientestate(initialClientState);
    };

    const handleSaveChanges = () => {
        var usuario = sessionStorage.getItem("userId");
        switch (currentState.editMode) {
            case 'Editing': {
                if (currentState.currentCliente.nombre === undefined || currentState.currentCliente.nombre === "") {
                    toast.warn("El nombre es necesario!");
                    break;
                }
                if (currentState.currentCliente.apellido === undefined || currentState.currentCliente.apellido === "") {
                    toast.warn("El apellido es necesario!");
                    break;
                }
                // if (currentState.currentCliente.identidad === undefined || currentState.currentCliente.identidad === "") {
                //     toast.warn("El apellido es necesario!");
                //     break;
                // }
                // if (currentState.currentCliente.telefono === undefined || currentState.currentCliente.telefono === "") {
                //     toast.warn("El apellido es necesario!");
                //     break;
                // }
                const service = {
                    id: currentState.currentCliente.id,
                    nombre: currentState.currentCliente.nombre,
                    apellido: currentState.currentCliente.apellido,
                    identidad: currentState.currentCliente.identidad,
                    direccion: currentState.currentCliente.direccion,
                    telefono: currentState.currentCliente.telefono,
                    celular: currentState.currentCliente.celular,
                    correo: currentState.currentCliente.correo,
                    user: usuario,
                }
                update(service);
                break;
            }

            default: {
                if (currentState.currentCliente.nombre === undefined || currentState.currentCliente.nombre === "")
                {
                    toast.warn("El nombre es necesario!");
                    break;
                }
                if (currentState.currentCliente.apellido === undefined || currentState.currentCliente.apellido === "") {
                    toast.warn("El apellido es necesario!");
                    break;
                }
                    // if (currentState.currentCliente.identidad === undefined || currentState.currentCliente.identidad === "") {
                    //     toast.warn("El apellido es necesario!");
                    //     break;
                    // }
                    // if (currentState.currentCliente.telefono === undefined || currentState.currentCliente.telefono === "") {
                    //     toast.warn("El apellido es necesario!");
                    //     break;
                    // }
                const newService = {
                    nombre: currentState.currentCliente.nombre,
                    apellido: currentState.currentCliente.apellido,
                    identidad: currentState.currentCliente.identidad,
                    direccion: currentState.currentCliente.direccion,
                    telefono: currentState.currentCliente.telefono,
                    celular: currentState.currentCliente.celular,
                    correo: currentState.currentCliente.correo,
                    user: usuario,
                }
                add(newService);
                break;
            }
        };
    }



    switch (currentState.editMode) {
        case 'Editing': return (

            <div className="animated fadeIn">
                <Card>
                    <CardHeader>
                        <strong> Datos a editar </strong>
                    </CardHeader>
                    <CardBody>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Id">Nombre</Label>
                                    <Input
                                        autoFocus
                                        type="text"
                                        name="nombre"
                                        id="nombre"
                                        readonly
                                        value={currentState.currentCliente.nombre}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Apellido</Label>
                                    <Input type="text" id="apellido" name="apellido"
                                        value={currentState.currentCliente.apellido}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Identidad</Label>
                                    <Input type="text" id="identidad" name="identidad"
                                        value={currentState.currentCliente.identidad}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Direccion</Label>
                                    <Input type="text" id="direccion" name="direccion"
                                        value={currentState.currentCliente.direccion}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>  
                        </Row>
                        <Row>     
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Telefono</Label>
                                    <Input type="text" id="telefono" name="telefono"
                                        value={currentState.currentCliente.telefono}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col> 
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Celular</Label>
                                    <Input type="text" id="celular" name="celular"
                                        value={currentState.currentCliente.celular}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col> 
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Correo</Label>
                                    <Input type="text" id="correo" name="correo"
                                        value={currentState.currentCliente.correo}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>     
                        </Row>
                    </CardBody>
                    <CardFooter>
                        <Button type="submit" size="sm" color="success" onClick={handleSaveChanges}>
                            <i className="fa fa-dot-circle-o" /> Guardar
          </Button>
                        <Button type="reset" size="sm" color="danger" onClick={handleCleanValues}>
                            <i className="fa fa-ban" /> Cancelar
          </Button>
                    </CardFooter>
                </Card>
            </div>
        );
        default: return (

            <div className="animated fadeIn">
                <Card>
                    <CardHeader>
                        <strong> Datos de nuevo puesto </strong>
                    </CardHeader>
                    <CardBody>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Id">Nombre</Label>
                                    <Input
                                        autoFocus
                                        type="text"
                                        name="nombre"
                                        id="nombre"
                                        value={currentState.currentCliente.nombre}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Apellido</Label>
                                    <Input type="text" id="apellido" name="apellido"
                                        value={currentState.currentCliente.apellido}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Identidad</Label>
                                    <Input type="text" id="identidad" name="identidad"
                                        value={currentState.currentCliente.identidad}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Direccion</Label>
                                    <Input type="text" id="direccion" name="direccion"
                                        value={currentState.currentCliente.direccion}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Telefono</Label>
                                    <Input type="text" id="telefono" name="telefono"
                                        value={currentState.currentCliente.telefono}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Celular</Label>
                                    <Input type="text" id="celular" name="celular"
                                        value={currentState.currentCliente.celular}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Correo</Label>
                                    <Input type="text" id="correo" name="correo"
                                        value={currentState.currentCliente.correo}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                    </CardBody>
                    <CardFooter>
                        <Button type="submit" size="sm" color="success" onClick={handleSaveChanges}>
                            <i className="fa fa-dot-circle-o" /> Guardar
          </Button>
                        <Button type="reset" size="sm" color="danger" onClick={handleCleanValues}>
                            <i className="fa fa-ban" /> Cancelar
          </Button>
                    </CardFooter>
                </Card>
                <ToastContainer autoClose={5000} />
            </div>
        );

    }
}
export default ClienteDetalle;