import React, { useState, useEffect } from "react";
import {
    Card,
    CardBody,
    CardHeader,
    Col,
    Row,
    FormGroup,
    Button
} from "reactstrap";
import ClienteTabla from './clienteTabla';
import ClienteDetalle from './clienteDetalle';
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import API from './../Constantes/Api';
import { ToastContainer, toast } from "react-toastify";



const initialclienteState = {
    step: 1,
    editMode: "None",
    currentCliente: {
        id: "",
        nombre: "",
        apellido: "",
        identidad: "",
        direccion: "",
        telefono: "",
        celular: "",
        correo: "",
        enabled: true,
    },
};

const heightStyleDiv = {
    height: "600px",
    paddingLeft: '0px',
    paddingRight: '0px'
};

const Cliente = props => {
    const [clienteState, setClientestate] = useState(initialclienteState);
    const [clientes, setClientes] = useState([]);

    const notify = () => toast("Wow so easy !");

    function nextStep(supplier) {
        const { step } = clienteState;

        if (supplier === null || supplier === undefined) {
            supplier = clienteState.currentCliente;
        }
        setClientestate({ ...clienteState, currentCliente: supplier, step: step + 1 });
    }

    function fetchclientes(query, step) {
        const index = step === undefined || step === null ? 0 : step;
        //  var rol = sessionStorage.getItem('rolId');
        //  if (rol === "3") {
        //      props.history.push('/dashboard');
        //      return (<Redirect to='/dashboard'></Redirect>);
        //  }
        if (query === null) {
            var url = `Cliente/paged?PageSize=10&PageIndex=${index}`
            API.get(url).then(res => {
                setClientes(res.data);
            }).catch(error => {
                if (error.message === "Network Error") {
                    toast.warn(
                        "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                    );
                } else {
                    if (error.response.status !== undefined) {
                        if (error.response.status === 401) {
                            props.history.push("/login");
                            return <Redirect to="/login" />;
                        }
                    }
                    else {
                        toast.warn(
                            "Se encontraron problemas para realizar trsancción favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                        );
                    }
                }
            });
        }
        else {
            var url1 = `Cliente/paged?pageSize=10&pageIndex=${index}&value=${query}`;
            API.get(url1).then(res => {
                setClientes(res.data);
            });
        }
    }

    function add(newPuesto) {
        if (
            !newPuesto ||
            !newPuesto.nombre ||
            !newPuesto.apellido
        ) {
            toast.warn("Faltan datos para poder crear un registro");
            return;
        }
        const url = `Cliente`;

        API.post(url, newPuesto).then(res => {
            if (res.id === 0) {
                toast.error("Error al intentar agregar puesto");
            } else {
                toast.success("puesto agregado satisfactoriamente");

                fetchclientes(null);
                setClientestate(initialclienteState);
            }
        }).catch(error => {
            if (error.message === "Network Error") {
                toast.warn(
                    "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                );
            } else {
                if (error.response.status !== undefined) {
                    if (error.response.status === 401) {
                        props.history.push("/login");
                        return <Redirect to="/login" />;
                    }
                }
                else {
                    toast.warn(
                        "Se encontraron problemas para realizar trsancción favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                    );
                }
            }
        });;
    }

    function update(puesto) {
        if (
            !puesto ||
            !puesto.nombre
        ) {

            toast.warn("Faltan datos para poder crear un registro");
            return;
        }
        const url = `Cliente`;
        API.put(url, puesto).then(res => {
            if (res.id === 0) {
                toast.error("Error al intentar agregar puesto");
            } else {
                toast.success("puesto agregado satisfactoriamente");

                // setUsers([...users.users, userAdded]);
                fetchclientes(null);
                setClientestate(initialclienteState);
            }
        }).catch(error => {
            if (error.message === "Network Error") {
                toast.warn(
                    "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                );
            } else {
                if (error.response.status !== undefined) {
                    if (error.response.status === 401) {
                        props.history.push("/login");
                        return <Redirect to="/login" />;
                    }
                }
                else {
                    toast.warn(
                        "Se encontraron problemas para realizar trsancción favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                    );
                }
            }
        });
    }

    const editRow = registro => {
        setClientestate({
            ...clienteState,
            editMode: "Editing",
            currentCliente: registro,
            step: 2,
        });
    };

    const disabledRegister = id => {
        var usuario = sessionStorage.getItem("usuarioId");
        const url = `Cliente/disabled`;

        const register = {
            id: id,
            user: usuario,
        }
        API.put(url, register).then(res => {
            if (res.data === null || res.data === undefined) {
                toast.error("error al intentar deshabilitar puesto");
                return;
            }
            fetchclientes(null);
            setClientestate(initialclienteState);
            toast.success("Registro deshabilitado satisfactoriamente");
        }).catch(error => {
            if (error.message === "Network Error") {
                toast.warn(
                    "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                );
            } else {
                if (error.response.status !== undefined) {
                    if (error.response.status === 401) {
                        props.history.push("/login");
                        return <Redirect to="/login" />;
                    }
                }
                else {
                    toast.warn(
                        "Se encontraron problemas para realizar trsancción favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                    );
                }
            }
        });
    };

    function nextPage(step) {
        fetchclientes(null, step);
    }

    function prevPage(step) {
        fetchclientes(null, step)
    }

    function prevStep() {
        const { step } = clienteState;

        setClientestate({ ...clienteState, step: step - 1 });
    }

    useEffect(() => {
        fetchclientes(null);
        // fectRoles();
    }, []);


    function GetCurrentStepComponent(step) {
        switch (step) {
            case 1:
                return (
                    <CardBody >
                        <FormGroup row>
                            <Col />
                            <Col xs="auto">
                                <Button
                                    type="button"
                                    color="success"
                                    onClick={() => nextStep()}>
                                    <i className="fa fa-new" /> Agregar cliente</Button>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col xs="12">
                                <ClienteTabla fetchclientes={fetchclientes} clientes={clientes} nextPage={nextPage}
                                    prevPage={prevPage} editRow={editRow} disabledRegister={disabledRegister} />
                            </Col>
                        </FormGroup>
                    </CardBody>
                );
            case 2:
                return <ClienteDetalle currentState={clienteState} setClientestate={setClientestate}
                    nextStep={nextStep} prevStep={prevStep}
                    add={add}
                    update={update}
                />;
            default:
                return null;
        }
    }

    const userStep = GetCurrentStepComponent(clienteState.step);

    return (
        <div className="animated fadeIn" >
            <Card >
                <Row>
                    <Col>
                        <Card>
                            <CardHeader>
                                <i className="fa fa-align-justify" /> Clientes
                            </CardHeader>
                            {userStep}
                        </Card>
                    </Col>
                </Row>
            </Card>
            <ToastContainer autoClose={5000} />
        </div>
    );
}
export default Cliente;