import React, { useState, useEffect } from "react";
import {
    Col,
    Table,
    FormGroup,
    InputGroup,
    InputGroupAddon,
    Button,
    Input
} from "reactstrap";

const step = 1;
const ClienteTabla = props => {
    const {
        fetchclientes,
        clientes,
        nextPage,
        prevPage,
        editRow,
        disabledRegister
    } = props;
    const [searchValue, setSearchValue] = useState("");

    function nextStep() {
        nextPage(step + 1);
    }

    function prevStep() {
        prevPage(step - 1);
    }

    function handleValueChange(e) {
        fetchclientes(e.target.value);
        setSearchValue(e.target.value);
    }

    const thStyle = {
        background: "#20a8d8",
        color: "white"
    };

    const userRows =
        clientes === null ? (
            <Table hover bordered striped responsive size="sm">
                <thead>
                    <tr>
                        <td colSpan={16}>Buscando...</td>
                    </tr>
                </thead>
            </Table>
        ) : clientes.length === 0 ? (
            <Table hover bordered striped responsive size="sm">
                <thead>
                    <tr>
                        <td colSpan={16}>
                            No se encontraron usuarios con el filtro especificado.</td>
                    </tr>
                </thead>
            </Table>
        ) : (
                    <div>
                        <FormGroup row>
                            <Col>
                                <InputGroup>
                                    <InputGroupAddon addonType="prepend">
                                        <Button type="button" color="primary">
                                            <i className="fa fa-search" /> Buscar
                                     </Button>
                                    </InputGroupAddon>
                                    <Input
                                        type="text"
                                        id="input1-group2"
                                        name="input1-group2"
                                        placeholder="Buscar"
                                        value={searchValue}
                                        onChange={handleValueChange}
                                        style={{ width: "600px" }}
                                    />
                                </InputGroup>
                            </Col>
                        </FormGroup>

                        <FormGroup row>
                            <Col md="12">
                                <Table hover bordered striped responsive size="sm">
                                    <thead>
                                        <tr>
                                            <th style={thStyle}>Editar</th>
                                            <th style={thStyle}>Borrar</th>
                                            <th style={thStyle}>ID</th>
                                            <th style={thStyle}>Nombre</th>
                                            <th style={thStyle}>Apellido</th>
                                            <th style={thStyle}>Identidad</th>
                                            <th style={thStyle}>Direccion</th>
                                            <th style={thStyle}>Telefono</th>
                                            <th style={thStyle}>Celular</th>
                                            <th style={thStyle}>Correo</th>
                                            <th style={thStyle}>Estado</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {clientes.items.map(item => (
                                            <tr key={item.id}>
                                                <td>
                                                    <Button
                                                        block
                                                        color="warning"
                                                        onClick={() => {
                                                            editRow(item);
                                                        }}
                                                    >
                                                        Editar</Button>
                                                </td>
                                                <td>
                                                    <Button
                                                        block
                                                        color="danger"
                                                        onClick={() => disabledRegister(item.id)}
                                                    >Deshabilitar</Button>
                                                </td>
                                                <td>{item.id}</td>
                                                <td>{item.nombre}</td>
                                                <td>{item.apellido}</td>
                                                <td>{item.identidad}</td>
                                                <td>{item.direccion}</td>
                                                <td>{item.telefono}</td>
                                                <td>{item.celular}</td>
                                                <td>{item.correo}</td>
                                                <td>{item.enabled ? 'Activo' : 'Inactivo'}</td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </Table>
                            </Col>
                            <Col md="12">
                                <InputGroup>
                                    <InputGroupAddon addonType="prepend">
                                        <Button type="button" color="primary" onClick={prevStep}>
                                            <i className="icon-arrow-left-circle" /> Anterior
                                </Button>
                                    </InputGroupAddon>
                                    <div
                                        className="animated fadeIn"
                                        style={{
                                            marginTop: "7px",
                                            marginLeft: "8px",
                                            marginRight: "8px"
                                        }}
                                    >
                                        <h5>
                                            <strong>Paginas: {clientes.pageCount}</strong>
                                        </h5>
                                    </div>
                                    <div
                                        className="animated fadeIn"
                                        style={{
                                            marginTop: "7px",
                                            marginLeft: "8px",
                                            marginRight: "8px"
                                        }}
                                    >
                                        <h5>
                                            <strong>Actual: {clientes.pageIndex}</strong>
                                        </h5>
                                    </div>
                                    <InputGroupAddon addonType="prepend">
                                        <Button type="button" color="primary" onClick={nextStep}>
                                            Proximo <i className="icon-arrow-right-circle" />
                                        </Button>
                                    </InputGroupAddon>
                                </InputGroup>
                            </Col>
                        </FormGroup>
                    </div>
                );
    return userRows;
}
export default ClienteTabla;