import React, { useState, useEffect } from "react";
import {
    Card,
    CardBody,
    CardHeader,
    Col,
    Row,
    FormGroup,
    Button
} from "reactstrap";
import PuestoTable from './puestoTable'
import PuestoDetalle from './puestoDetalle'
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import API from './../../Constantes/Api';
import { ToastContainer, toast } from "react-toastify";



const initialPuestoState = {
    step: 1,
    editMode: "None",
    currentPuesto: {
        id: "",
        nombre: "",
        descripcion: ""
    },
};

// const heightStyle = {
//     height: "600px"
// };

const heightStyleDiv = {
    height: "600px",
    paddingLeft: '0px',
    paddingRight: '0px'
};



const Puesto = props => {
    const [puestoState, setPuestoState] = useState(initialPuestoState);
    const [puestos, setPuestos] = useState([]);

    const notify = () => toast("Wow so easy !");

    function nextStep(supplier) {
        const { step } = puestoState;

        if (supplier === null || supplier === undefined) {
            supplier = puestoState.currentPuesto;
        }
        setPuestoState({ ...puestoState, currentPuesto: supplier, step: step + 1 });
    }

    function fetchPuestos(query, step) {
         const index = step === undefined || step === null ? 0 : step;
        //  var rol = sessionStorage.getItem('rolId');
        //  if (rol === "3") {
        //      props.history.push('/dashboard');
        //      return (<Redirect to='/dashboard'></Redirect>);
        //  }
         if (query === null) {
             var url = `puesto/paged?PageSize=10&PageIndex=${index}`
             API.get(url).then(res => {
                 setPuestos(res.data);
             }).catch(error => {
                 if (error.message === "Network Error") {
                     toast.warn(
                         "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                     );
                 } else {
                     if (error.response.status !== undefined) {
                         if (error.response.status === 401) {
                             props.history.push("/login");
                             return <Redirect to="/login" />;
                         }
                     }
                     else {
                         toast.warn(
                             "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                         );
                     }
                 }
             });
         }
         else {
             var url1 = `puesto/paged?pageSize=10&pageIndex=${index}&value=${query}`;
             API.get(url1).then(res => {
                 setPuestos(res.data);
             });
         }
    }

    function add(newPuesto) {
        if (
            !newPuesto ||
            !newPuesto.nombre ||
            !newPuesto.descripcion 
        ) {
            toast.warn("Faltan datos para poder crear un registro");
            return;
        }
        const url = `Puesto`;

        API.post(url, newPuesto).then(res => {
            if (res.id === 0) {
                toast.error("Error al intentar agregar puesto");
            } else {
                toast.success("puesto agregado satisfactoriamente");

                fetchPuestos(null);
                setPuestoState(initialPuestoState);
            }
        });
    }

    function update(puesto) {
        if (
            !puesto ||
            !puesto.nombre
        ) {
            
            toast.warn("Faltan datos para poder crear un registro");
            return;
        }
        const url = `Puesto`;
        API.put(url, puesto).then(res => {
            if (res.id === 0) {
                toast.error("Error al intentar agregar puesto");
            } else {
                toast.success("puesto agregado satisfactoriamente");

                // setUsers([...users.users, userAdded]);
                fetchPuestos(null);
                setPuestoState(initialPuestoState);
            }
        }).catch(error => {
            if (error.message === "Network Error") {
                toast.warn(
                    "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                );
            } else {
                if (error.response.status !== undefined) {
                    if (error.response.status === 401) {
                        props.history.push("/login");
                        return <Redirect to="/login" />;
                    }
                }
                else {
                    toast.warn(
                        "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                    );
                }
            }
        });
    }

    const editRow = puesto => {
        setPuestoState({
            ...puestoState,
            editMode: "Editing",
            currentPuesto: puesto,
            step: 2,
        });
    };

    const disabledRegister = id => {
        var usuario = sessionStorage.getItem("usuarioId");
        const url = `Puesto/disabled`;

        const register = {
            id: id,
            user: usuario,
        }
        API.put(url, register).then(res => {
            if (res.data === null || res.data === undefined) {
                toast.error("error al intentar deshabilitar puesto");
                return;
            }
            fetchPuestos(null);
            setPuestoState(initialPuestoState);
            toast.success("Registro deshabilitado satisfactoriamente");
        }).catch(error => {
            if (error.message === "Network Error") {
                toast.warn(
                    "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                );
            } else {
                if (error.response.status !== undefined) {
                    if (error.response.status === 401) {
                        props.history.push("/login");
                        return <Redirect to="/login" />;
                    }
                }
                else {
                    toast.warn(
                        "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                    );
                }
            }
        });
    };

    function nextPage(step) {
        fetchPuestos(null, step);
    }

    function prevPage(step) {
        fetchPuestos(null, step)
    }

    function prevStep() {
        const { step } = puestoState;

        setPuestoState({ ...puestoState, step: step - 1 });
    }

    useEffect(() => {
        fetchPuestos(null);
        // fectRoles();
    }, []);


    function GetCurrentStepComponent(step) {
        switch (step) {
            case 1:
                return (
                    <CardBody >
                        <FormGroup row>
                            <Col />
                            <Col xs="auto">
                                <Button
                                    type="button"
                                    color="success"
                                    onClick={() => nextStep()}>
                                    <i className="fa fa-new" /> Agregar puesto</Button>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col xs="12">
                                <PuestoTable fetchPuestos={fetchPuestos} puestos={puestos} nextPage={nextPage} 
                                prevPage={prevPage} editRow={editRow} disabledRegister={disabledRegister}/>
                            </Col>
                        </FormGroup>
                    </CardBody>
                );
            case 2:
                return <PuestoDetalle currentState={puestoState} setPuestoState={setPuestoState} 
                nextStep={nextStep} prevStep={prevStep} 
                add={add} 
                update={update}
                />;
            default:
                return null;
        }
    }

    const userStep = GetCurrentStepComponent(puestoState.step);

    return (
        <div className="animated fadeIn" >
            <Card > 
                <Row>
                    <Col>
                        <Card>
                            <CardHeader>
                                <i className="fa fa-align-justify" /> Puestos
                            </CardHeader>
                            {userStep}
                        </Card>
                    </Col>
                </Row>
            </Card>
            <ToastContainer autoClose={5000}/>
        </div>
    );
}
export default Puesto;