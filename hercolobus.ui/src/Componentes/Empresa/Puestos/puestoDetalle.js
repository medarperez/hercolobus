import React, { useState, useEffect } from "react";
import {
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    FormGroup,
    Input,
    Label,
    Row
} from "reactstrap";
import "react-toastify/dist/ReactToastify.css";

const PuestoDetalle = props => {
    const { currentState, setPuestoState, nextStep, prevStep, add, update } = props;

    const handleInputChange = event => {
        const { name, value } = event.target;
        setPuestoState({
            ...currentState,
            currentPuesto: {
                ...currentState.currentPuesto,
                [name]: value
            }
        });
    };
    const handleCleanValues = () => {
        const initialClientState = {
            step: 1,
            editMode: "None",
            currentPuesto: {
                id: "",
                name: "",
                description: ""
            }
        };
        setPuestoState(initialClientState);
    };

    const handleSaveChanges = () => {
        var usuario = sessionStorage.getItem("userId");
        switch (currentState.editMode) {
            case 'Editing': {
                const service = {
                    id: currentState.currentPuesto.id,
                    nombre: currentState.currentPuesto.nombre,
                    descripcion: currentState.currentPuesto.descripcion,
                    user: usuario,
                }
                update(service);
                break;
            }

            default: {
                const newService = {
                    nombre: currentState.currentPuesto.nombre,
                    descripcion: currentState.currentPuesto.descripcion,
                    user: usuario,
                }
                add(newService);
                break;
            }
        };
    }

   

    switch (currentState.editMode) {
        case 'Editing': return (

            <div className="animated fadeIn">
                <Card>
                    <CardHeader>
                        <strong> Datos a editar </strong>
                    </CardHeader>
                    <CardBody>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Id">Nombre</Label>
                                    <Input
                                        autoFocus
                                        type="text"
                                        name="nombre"
                                        id="nombre"
                                        readonly
                                        value={currentState.currentPuesto.nombre}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Descripcion</Label>
                                    <Input type="text" id="descripcion" name="descripcion"
                                        value={currentState.currentPuesto.descripcion}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                    </CardBody>
                    <CardFooter>
                        <Button type="submit" size="sm" color="success" onClick={handleSaveChanges}>
                            <i className="fa fa-dot-circle-o" /> Guardar
          </Button>
                        <Button type="reset" size="sm" color="danger" onClick={handleCleanValues}>
                            <i className="fa fa-ban" /> Cancelar
          </Button>
                    </CardFooter>
                </Card>
            </div>
        );
        default: return (

            <div className="animated fadeIn">
                <Card>
                    <CardHeader>
                        <strong> Datos de nuevo puesto </strong>
                    </CardHeader>
                    <CardBody>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Id">Nombre</Label>
                                    <Input
                                        autoFocus
                                        type="text"
                                        name="nombre"
                                        id="nombre"
                                        value={currentState.currentPuesto.nombre}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Descripcion</Label>
                                    <Input type="text" id="descripcion" name="descripcion"
                                        value={currentState.currentPuesto.descripcion}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                        </Row>

                    </CardBody>
                    <CardFooter>
                        <Button type="submit" size="sm" color="success" onClick={handleSaveChanges}>
                            <i className="fa fa-dot-circle-o" /> Guardar
          </Button>
                        <Button type="reset" size="sm" color="danger" onClick={handleCleanValues}>
                            <i className="fa fa-ban" /> Cancelar
          </Button>
                    </CardFooter>
                </Card>
            </div>
        );

    }
}
export default PuestoDetalle;

