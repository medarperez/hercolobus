import React, { useState } from "react";
import {
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    FormGroup,
    Input,
    Label,
    Row,
    Alert
} from "reactstrap";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const PasswordResetDetail = props => {
    const { UpdateChange } = props;
    const [oldpassword, setOldpassword] = useState("");
    const [newPassword, setNewPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [stateBoolShowMessage, setBoolShowMessageState] = useState(false);
    const [stateTypeAlert, setTypeAlertState] = useState("info");
    const [
        stateMessagePasswordValidation,
        setMessagePasswordValidationState
    ] = useState("Password vacio");
    //const [stateVerifiedPassword, setVerifiedPasswordState] = useState("");

    const handleInputOldPassword = event => {
        const { value } = event.target;
        setOldpassword(value);
    };

    const handleInputNewPassword = event => {
        const { value } = event.target;
        setNewPassword(value);
    };

    const handleInputConfirmPassword = event => {
        const { value } = event.target;
        setConfirmPassword(value);
        if (value !== newPassword) {
            setBoolShowMessageState(true);
            setTypeAlertState("danger");
            setMessagePasswordValidationState("Contraseña invalida!");
            // setVerifiedPasswordState(value);
        } else {
            setBoolShowMessageState(true);
            setTypeAlertState("success");
            setMessagePasswordValidationState("Contraseña confirmada!");
            // setVerifiedPasswordState(value);
        }
    };

    const handleSaveChanges = () => {
        const userId = sessionStorage.getItem("userId");
        UpdateChange(userId, oldpassword, newPassword);
        setOldpassword("");
        setNewPassword("");
        setConfirmPassword("");
    };

    return (
        <div className="animated fadeIn">
            <Card>
                <CardHeader>
                    <strong> Cambio de contraseña </strong>
                </CardHeader>
                <CardBody>
                    <Row>
                        <Col md="3" sm="6" xs="12">
                            <FormGroup>
                                <Label htmlFor="Id">Contraseña anterior</Label>
                                <Input
                                    type="password"
                                    name="Id"
                                    id="Id"
                                    value={oldpassword}
                                    onChange={handleInputOldPassword}
                                    required
                                />
                            </FormGroup>
                        </Col>
                        <Col md="3" sm="6" xs="12">
                            <FormGroup>
                                <Label htmlFor="Primer Nombre">Nueva contraseña</Label>
                                <Input
                                    type="password"
                                    name="Id"
                                    id="NewPassword"
                                    value={newPassword}
                                    onChange={handleInputNewPassword}
                                    required
                                />
                            </FormGroup>
                        </Col>
                        <Col md="3" sm="6" xs="12">
                            <FormGroup>
                                <Label htmlFor="Segundo Nombre">Confirmar contraseña</Label>
                                <Input
                                    type="password"
                                    name="Id"
                                    id="ConfirmPassword"
                                    value={confirmPassword}
                                    onChange={handleInputConfirmPassword}
                                    required
                                />
                            </FormGroup>
                        </Col>
                        <Col md="3" sm="6" xs="12">
                            <FormGroup>
                                <CardBody>
                                    <Alert color={stateTypeAlert} isOpen={stateBoolShowMessage}>
                                        {stateMessagePasswordValidation}
                                    </Alert>
                                </CardBody>
                            </FormGroup>
                        </Col>
                    </Row>
                </CardBody>
                <CardFooter>
                    <Button
                        type="submit"
                        size="sm"
                        color="success"
                        onClick={handleSaveChanges}
                    >
                        <i className="fa fa-dot-circle-o" /> Cambiar contraseña
          </Button>
                </CardFooter>
            </Card>
            <ToastContainer />
        </div>
    );
};

export default PasswordResetDetail;
