import React from "react";
import PasswordResetDetail from "./passwordResetDetail";
import API from './../../Constantes/Api';
import { Redirect } from "react-router-dom";
import { Card, CardBody, CardHeader, Col, Row, FormGroup } from "reactstrap";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const heightStyle = {
    height: "500px"
};

const initialClientState = {
    step: 1,
    editMode: "None"
};
const ResetPassword = props => {

    function UpdateChange(usuario, password, newpassword) {
        const url = `Usuario/changePassword`;

        const changePasswordrequets = {
            User: usuario,
            Password: password,
            NewPassword: newpassword
        };

        API.put(url, changePasswordrequets)
            .then(res => {
                if (res.data === "") {
                    toast.success("Contraseña actualizada");
                    sessionStorage.removeItem("login");
                    sessionStorage.removeItem("token");
                    setTimeout(function () {
                        props.history.push("/login");
                        return <Redirect to="/login" />;
                    }, 5000);
                }

                if (res.data === "Usuario no encontrado") {
                    toast.warn("Contraseña actual no es correcta");
                }
            })
            .catch(error => {
                if (error.message === "Network Error") {
                    toast.warn(
                        "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                    );
                } else {
                    if (error.response.status !== undefined) {
                        if (error.response.status === 401) {
                            props.history.push("/login");
                            return <Redirect to="/login" />;
                        }
                    }
                    else {
                        toast.warn(
                            "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                        );
                    }
                }
            });
    }

    function GetCurrentStepComponent(step) {
        switch (step) {
            case 1:
                return (
                    <CardBody>
                        <FormGroup row>
                            <Col xs="12">
                                <PasswordResetDetail UpdateChange={UpdateChange} />
                            </Col>
                        </FormGroup>
                    </CardBody>
                );
            default:
                return null;
        }
    }

    const userStep = GetCurrentStepComponent(initialClientState.step);

    return (
        <div className="animated fadeIn" style={heightStyle}>
            <Card>
                <Row>
                    <Col>
                        <Card>
                            <CardHeader>
                                <i className="fa fa-align-justify" /> Usuario
                            </CardHeader>
                            {userStep}
                        </Card>
                    </Col>
                </Row>
            </Card>
            <ToastContainer />
        </div>
    );
};

export default ResetPassword;
