import React, { useState, useEffect } from "react";
import moment from "moment";
import { Input } from "reactstrap";
import ReactLoading from "react-loading";
import ReactExport from "react-data-export";
import {
    Col,
    Table,
    FormGroup,
    InputGroup,
    InputGroupAddon,
    Button,
    Row,
    Card,
    Label,
} from "reactstrap";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const thStyle = {
    background: "#20a8d8",
    color: "white",
};

const Reporte = props => {
    const { 
            fetchClientsGeneralReport, 
            clients, 
            setClients, 
            apiCallInProgress, 
            setfindValuestate, 
            findValuestate,
            handleClean } = props;

    const [initialDate, setInitialDate] = useState("");
    const [CodigoClienteState, setCodigoClienteState] = useState("");
    const [ExcelDataState, setExcelDataState] = useState([]);

    function findClients() {
        if (
            CodigoClienteState === ""
        ) {
            fetchClientsGeneralReport();
            return
        }
        
        fetchClientsGeneralReport();
    }

    function handleValueChange(e) {
        setfindValuestate(e.target.value);
    }

    const multiDataSet = [
        {
            columns: [
                { title: "Codigo Contrato" },
                { title: "Codigo Cliente" },
                { title: "Cliente" },
                { title: "#Identidad" },
                { title: "Telefono" },
                { title: "Correo" },
                { title: "Codigo paquete" },
                { title: "Paquete" },
                { title: "Descripcion de paquete" },
                { title: "Precio" },
                { title: "Estdado Conexion" },
            ],
            data: [clients],
        }
        ]


    function getFooterBuild(e) {
     
        if (apiCallInProgress) {
       
            return (
                <Card style={{ alignItems: 'center', alignSelf: 'center',}}>
                    <Col md="3" sm="6" xs="12"></Col>
                    <Col md="3" sm="6" xs="12">
                        <ReactLoading
                            type="bars"
                            color="#5DBCD2"
                            height={"40%"}
                            width={"40%"}
                        />
                    </Col>
                    <Col md="3" sm="6" xs="12"></Col>
                </Card>
            );
        }
        else
        {
            return clientRows;
        }
    }

    const clientRows = (
        <div>
            <FormGroup row>
                <Col md="3" sm="6" xs="12">
                    <FormGroup>
                        <Row style={{ marginLeft: 3 }}>
                            <Label htmlFor="Cliente"> Codigo Cliente</Label>
                        </Row>
                        <Row style={{ marginLeft: 3 }}>
                            <Input
                                type="text"
                                id="input1-group2"
                                name="input1-group2"
                                placeholder="id de cliente"
                                value={findValuestate}
                                onChange={handleValueChange}
                                style={{ width: "600px" }}
                            />
                        </Row>
                    </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12" style={{ marginTop: "22px" }}>
                    <InputGroup>
                        <InputGroupAddon addonType="prepend">
                            <Button type="button" color="primary" onClick={handleClean}>
                                Refrescar
                            </Button>
                        </InputGroupAddon>
                    </InputGroup>
                </Col>
                <Col md="3" sm="6" xs="12" style={{ marginTop: "22px" }}>
                    <InputGroup>
                        <InputGroupAddon addonType="prepend">
                            <Button type="button" color="primary" onClick={findClients}>
                                Generar <i className="icon-arrow-right-circle" />
                            </Button>
                        </InputGroupAddon>
                    </InputGroup>
                </Col>
                <Col />

                {/* <Col xs="auto">
                                <Button
                                    type="button"
                                    color="success"
                                    onClick={() => nextStep()}>
                                    <i className="fa fa-new" /> Descargar reporte
                                </Button>
                            </Col> */}
                <Col xs="auto">
                    <ExcelFile 
                        element={
                            <Button type="button" color="success">
                                <i className="fa fa-new" /> Descargar archivo
                    </Button>
                        } filename= "Clientes"
                       
                    >
                        <ExcelSheet data={clients} name="Clientes" >
                            <ExcelColumn label="Codigo Contrato" value="contratoCodigo" />
                            <ExcelColumn label="Codigo Cliente" value="codigoCliente" />
                            <ExcelColumn label="Cliente" value="cliente" />
                            <ExcelColumn label="#Identidad" value="identidad" />
                            <ExcelColumn label="Telefono" value="telefono" />
                            <ExcelColumn label="Correo" value="correo"/>
                            <ExcelColumn label="Codigo paquete" value="codigoPaquete" />
                            <ExcelColumn label="Paquete" value="paquete" />
                            <ExcelColumn label="Descripcion de paquete" value="descripcionPaquete" />
                            <ExcelColumn label="Precio" value="precio" />
                            <ExcelColumn label="Estdado Conexion" value="estadoConexion" />                        
                            </ExcelSheet>
                    </ExcelFile>
                </Col>
            </FormGroup>

            <FormGroup row>
                <Col md="12">
                    <Table hover bordered striped responsive size="sm">
                        <thead>
                            <tr>
                                <th style={thStyle}>Codigo Contrato</th>
                                <th style={thStyle}>Codigo Cliente</th>
                                <th style={thStyle}>Cliente</th>
                                <th style={thStyle}>#Identidad</th>

                                <th style={thStyle}>Telefono</th>
                                <th style={thStyle}>Correo</th>
                                <th style={thStyle}>Codigo paquete</th>
                                <th style={thStyle}>Paquete</th>

                                <th style={thStyle}>Descripcion de paquete</th>
                                <th style={thStyle}>Precio</th>
                                <th style={thStyle}>Estdado Conexion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {clients.map((client) => (
                                <tr key={client.contratoCodigo}>
                                    <td nowrap>{client.contratoCodigo}</td>
                                    <td style={{ whiteSpace: "nowrap" }}>{client.codigoCliente}</td>
                                    <td style={{ whiteSpace: "nowrap" }}>{client.cliente}</td>
                                    <td style={{ whiteSpace: "nowrap" }}>{client.identidad}</td>
                                    <td style={{ whiteSpace: "nowrap" }}>{client.telefono}</td>
                                    <td style={{ whiteSpace: "nowrap" }}>{client.correo}</td>
                                    <td style={{ whiteSpace: "nowrap" }}>{client.codigoPaquete}</td>
                                    <td style={{ whiteSpace: "nowrap" }}>{client.paquete}</td>
                                    <td style={{ whiteSpace: "nowrap" }}>{client.descripcionPaquete}</td>
                                    <td style={{ whiteSpace: "nowrap" }}>{client.precio}</td>
                                    <td style={{ whiteSpace: "nowrap" }}>{client.estadoConexion}</td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </Col>
            </FormGroup>
        </div>
    );
    return getFooterBuild();
};

export default Reporte;