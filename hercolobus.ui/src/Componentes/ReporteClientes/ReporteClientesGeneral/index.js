import React, { useState, useEffect } from "react";
import moment from "moment";
import API from './../../Constantes/Api';
import Reporte from './reporte';
import { ToastContainer, toast } from "react-toastify";

const initialClienteState = { 
    step: 1,
    editMode: "None",
    clienteActual: {
        contratoCodigo:	"",
        codigoPaquete: "",
        paquete: "",
        codigoCliente: "",
        cliente: "",
        identidad: "",
        telefono: "",
        celular: "",
        correo: "",
        inicioContrato: "",
        estadoConexion: "",
        descripcionPaquete: "",
        id: 0,
        status: "",
        validationErrorMessage: "",
        enabled: true,
        Precio: 0,
    },
}

const heightStyle = {
    height: "500px",
};

const ReporteClienteGeneral = props => {
    const [clientState, setClientState] = useState(initialClienteState);
    const [apiCallInProgress, setApiCallInProgress] = useState(false);
    const [clients, setClients] = useState([]);
    const [findValuestate, setfindValuestate] = useState('');


    function handleClean() {
        setfindValuestate('');
        setClients([]);
        setClientState(initialClienteState);
    }


    function fetchClientsGeneralReport() {
        // const stDate = moment(initialDate).format("L");
        // const endDate = moment(finalDate).format("L");
        const user = sessionStorage.getItem("userId");
        if (findValuestate === '' || findValuestate === ' ' || findValuestate === undefined)
        {
            const url = `ReporteCliente?user=${user}`;
            setApiCallInProgress(true);
            API.get(url).then((res) => {
                var listClients = res.data;
                listClients.forEach(function (element) {
                    element.inicioContrato = moment(element.inicioContrato).format("L");
                });
                setClients(listClients);
                setApiCallInProgress(false);
            }).catch((error) => {
                setApiCallInProgress(false);

                if (error.message === "Network Error") {
                    console.log(error);
                    toast.warn(
                        "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                    );
                } else {
                    toast.warn(
                        "Se encontraron problemas para crear el nuevo cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                    );
                }
            });
        }
        else{
            const url = `ReporteCliente?user=${user}&codigioCliente=${findValuestate}`;
            setApiCallInProgress(true);
            API.get(url).then((res) => {
                var listClients = res.data;
                listClients.forEach(function (element) {
                    element.inicioContrato = moment(element.inicioContrato).format("L");
                });
                setClients(listClients);
                setApiCallInProgress(false);
            }).catch((error) => {
                setApiCallInProgress(false);

                if (error.message === "Network Error") {
                    console.log(error);
                    toast.warn(
                        "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                    );
                } else {
                    toast.warn(
                        "Se encontraron problemas para crear el nuevo cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                    );
                }
            });
        }
       
    }

    function GetCurrentStepComponent(step) {
        switch (step) {
            case 1:
                return (
                    <Reporte 
                        fetchClientsGeneralReport={fetchClientsGeneralReport} 
                        clients={clients} 
                        setClients={setClients} 
                        apiCallInProgress={apiCallInProgress}
                        setfindValuestate={setfindValuestate}
                        findValuestate={findValuestate}
                        handleClean={handleClean}/>)
        }   
    }

    const clientsStep = GetCurrentStepComponent(clientState.step);

    return (
        <div className="animated fadeIn" style={heightStyle}>
            {clientsStep}
        </div>
    );
}

export default ReporteClienteGeneral;