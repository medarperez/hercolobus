import React, { useState, useEffect } from "react";
import {
    Card,
    CardBody,
    CardHeader,
    Col,
    Row,
    FormGroup,
    Button
} from "reactstrap";
import ServicioTable from './servicioTable'
import ServicioDetail from './servicioDetail'
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import API from './../Constantes/Api';
import { ToastContainer, toast } from "react-toastify";

const initialservicioState = {
    step: 1,
    editMode: "None",
    currentServicio: {
        id: "",
        nombre: "",
        descripcion: "",
        codigoServicio: "",
    },
};

// const heightStyle = {
//     height: "600px"
// };

const heightStyleDiv = {
    height: "600px",
    paddingLeft: '0px',
    paddingRight: '0px'
};


const Servicio = props => {
    const [servicioState, setServiciostate] = useState(initialservicioState);
    const [servicios, setServicios] = useState([]);

    const notify = () => toast("Wow so easy !");

    function nextStep(supplier) {
        const { step } = servicioState;

        if (supplier === null || supplier === undefined) {
            supplier = servicioState.currentServicio;
        }
        setServiciostate({ ...servicioState, currentServicio: supplier, step: step + 1 });
    }

    function fetchservicios(query, step) {
        const index = step === undefined || step === null ? 0 : step;
        //  var rol = sessionStorage.getItem('rolId');
        //  if (rol === "3") {
        //      props.history.push('/dashboard');
        //      return (<Redirect to='/dashboard'></Redirect>);
        //  }
        if (query === null) {
            var url = `servicio/paged?PageSize=10&PageIndex=${index}`
            API.get(url).then(res => {
                setServicios(res.data);
            }).catch(error => {
                if (error.message === "Network Error") {
                    toast.warn(
                        "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                    );
                } else {
                    if (error.response.status !== undefined) {
                        if (error.response.status === 401) {
                            props.history.push("/login");
                            return <Redirect to="/login" />;
                        }
                    }
                    else {
                        toast.warn(
                            "Se encontraron problemas para realizar trsancción favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                        );
                    }
                }
            });
        }
        else {
            var url1 = `servicio/paged?pageSize=10&pageIndex=${index}&value=${query}`;
            API.get(url1).then(res => {
                setServicios(res.data);
            });
        }
    }

    function add(newServicio) {
        if (
            !newServicio ||
            !newServicio.nombre ||
            !newServicio.descripcion
        ) {
            toast.warn("Faltan datos para poder crear un registro");
            return;
        }
        const url = `Servicio`;

        API.post(url, newServicio).then(res => {
            if (res.id === 0) {
                toast.error("Error al intentar agregar registro");
            } else {
                toast.success("registro agregado satisfactoriamente");

                fetchservicios(null);
                setServiciostate(initialservicioState);
            }
        }).catch(error => {
            if (error.message === "Network Error") {
                toast.warn(
                    "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                );
            } else {
                if (error.response.status !== undefined) {
                    if (error.response.status === 401) {
                        props.history.push("/login");
                        return <Redirect to="/login" />;
                    }
                }
                else {
                    toast.warn(
                        "Se encontraron problemas para realizar trsancción favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                    );
                }
            }
        });
    }

    function update(puesto) {
        if (
            !puesto ||
            !puesto.nombre
        ) {

            toast.warn("Faltan datos para poder crear un registro");
            return;
        }
        const url = `Servicio`;
        API.put(url, puesto).then(res => {
            if (res.id === 0) {
                toast.error("Error al intentar agregar puesto");
            } else {
                toast.success("puesto agregado satisfactoriamente");

                // setUsers([...users.users, userAdded]);
                fetchservicios(null);
                setServiciostate(initialservicioState);
            }
        }).catch(error => {
            if (error.message === "Network Error") {
                toast.warn(
                    "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                );
            } else {
                if (error.response.status !== undefined) {
                    if (error.response.status === 401) {
                        props.history.push("/login");
                        return <Redirect to="/login" />;
                    }
                }
                else {
                    toast.warn(
                        "Se encontraron problemas para realizar trsancción favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                    );
                }
            }
        });
    }

    const editRow = puesto => {
        setServiciostate({
            ...servicioState,
            editMode: "Editing",
            currentServicio: puesto,
            step: 2,
        });
    };

    const disabledRegister = id => {
        var usuario = sessionStorage.getItem("userId");
        const url = `servicio/disabled`;

        const register = {
            id: id,
            user: usuario,
        }
        API.put(url, register).then(res => {
            if (res.data === null || res.data === undefined) {
                toast.error("error al intentar deshabilitar registro");
                return;
            }
            fetchservicios(null);
            setServiciostate(initialservicioState);
            toast.success("Registro deshabilitado satisfactoriamente");
        }).catch(error => {
            if (error.message === "Network Error") {
                toast.warn(
                    "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                );
            } else {
                if (error.response.status !== undefined) {
                    if (error.response.status === 401) {
                        props.history.push("/login");
                        return <Redirect to="/login" />;
                    }
                }
                else {
                    toast.warn(
                        "Se encontraron problemas para realizar trsancción favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                    );
                }
            }
        });
    };

    function nextPage(step) {
        fetchservicios(null, step);
    }

    function prevPage(step) {
        fetchservicios(null, step)
    }

    function prevStep() {
        const { step } = servicioState;

        setServiciostate({ ...servicioState, step: step - 1 });
    }

    useEffect(() => {
        fetchservicios(null);
        // fectRoles();
    }, []);


    function GetCurrentStepComponent(step) {
        switch (step) {
            case 1:
                return (
                    <CardBody >
                        <FormGroup row>
                            <Col />
                            <Col xs="auto">
                                <Button
                                    type="button"
                                    color="success"
                                    onClick={() => nextStep()}>
                                    <i className="fa fa-new" /> Agregar Servicio</Button>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col xs="12">
                                <ServicioTable fetchservicios={fetchservicios} servicios={servicios} nextPage={nextPage}
                                    prevPage={prevPage} editRow={editRow} disabledRegister={disabledRegister} />
                            </Col>
                        </FormGroup>
                    </CardBody>
                );
            case 2:
                return <ServicioDetail currentState={servicioState} setServiciostate={setServiciostate}
                    nextStep={nextStep} prevStep={prevStep}
                    add={add}
                    update={update}
                />;
            default:
                return null;
        }
    }

    const userStep = GetCurrentStepComponent(servicioState.step);

    return (
        <div className="animated fadeIn" >
            <Card >
                <Row>
                    <Col>
                        <Card>
                            <CardHeader>
                                <i className="fa fa-align-justify" /> servicios
                            </CardHeader>
                            {userStep}
                        </Card>
                    </Col>
                </Row>
            </Card>
            <ToastContainer autoClose={5000} />
        </div>
    );
}
export default Servicio;