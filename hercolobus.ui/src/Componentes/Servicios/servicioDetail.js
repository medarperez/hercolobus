import React, { useState, useEffect } from "react";
import {
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    FormGroup,
    Input,
    Label,
    Row
} from "reactstrap";
import "react-toastify/dist/ReactToastify.css";

const ServicioDetail = props => {
    const { currentState, setServiciostate, nextStep, prevStep, add, update } = props;

    const handleInputChange = event => {
        const { name, value } = event.target;
        setServiciostate({
            ...currentState,
            currentServicio: {
                ...currentState.currentServicio,
                [name]: value
            }
        });
    };
    const handleCleanValues = () => {
        const initialClientState = {
            step: 1,
            editMode: "None",
            currentServicio: {
                id: "",
                nombre: "",
                descripcion: "",
                codigoServicio: "",
            },
        };
        setServiciostate(initialClientState);
    };

    const handleSaveChanges = () => {
        var usuario = sessionStorage.getItem("userId");
        switch (currentState.editMode) {
            case 'Editing': {
                const service = {
                    id: currentState.currentServicio.id,
                    nombre: currentState.currentServicio.nombre,
                    descripcion: currentState.currentServicio.descripcion,
                    codigoServicio: currentState.currentServicio.codigoServicio,
                    user: usuario,
                }
                update(service);
                break;
            }

            default: {
                const newService = {
                    nombre: currentState.currentServicio.nombre,
                    descripcion: currentState.currentServicio.descripcion,
                    codigoServicio: currentState.currentServicio.codigoServicio,
                    user: usuario,
                }
                add(newService);
                break;
            }
        };
    }



    switch (currentState.editMode) {
        case 'Editing': return (

            <div className="animated fadeIn">
                <Card>
                    <CardHeader>
                        <strong> Datos a editar </strong>
                    </CardHeader>
                    <CardBody>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Id">Nombre</Label>
                                    <Input
                                        autoFocus
                                        type="text"
                                        name="nombre"
                                        id="nombre"
                                        readonly
                                        value={currentState.currentServicio.nombre}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Descripcion</Label>
                                    <Input type="text" id="descripcion" name="descripcion"
                                        value={currentState.currentServicio.descripcion}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">CodigoServicio</Label>
                                    <Input type="text" id="codigoServicio" name="codigoServicio"
                                        value={currentState.currentServicio.codigoServicio}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                    </CardBody>
                    <CardFooter>
                        <Button type="submit" size="sm" color="success" onClick={handleSaveChanges}>
                            <i className="fa fa-dot-circle-o" /> Guardar
          </Button>
                        <Button type="reset" size="sm" color="danger" onClick={handleCleanValues}>
                            <i className="fa fa-ban" /> Cancelar
          </Button>
                    </CardFooter>
                </Card>
            </div>
        );
        default: return (

            <div className="animated fadeIn">
                <Card>
                    <CardHeader>
                        <strong> Datos de nuevo Servicio </strong>
                    </CardHeader>
                    <CardBody>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Id">Nombre</Label>
                                    <Input
                                        autoFocus
                                        type="text"
                                        name="nombre"
                                        id="nombre"
                                        value={currentState.currentServicio.nombre}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Descripcion</Label>
                                    <Input type="text" id="descripcion" name="descripcion"
                                        value={currentState.currentServicio.descripcion}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">CodigoServicio</Label>
                                    <Input type="text" id="codigoServicio" name="codigoServicio"
                                        value={currentState.currentServicio.codigoServicio}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                        </Row>

                    </CardBody>
                    <CardFooter>
                        <Button type="submit" size="sm" color="success" onClick={handleSaveChanges}>
                            <i className="fa fa-dot-circle-o" /> Guardar
          </Button>
                        <Button type="reset" size="sm" color="danger" onClick={handleCleanValues}>
                            <i className="fa fa-ban" /> Cancelar
          </Button>
                    </CardFooter>
                </Card>
            </div>
        );

    }
}
export default ServicioDetail;