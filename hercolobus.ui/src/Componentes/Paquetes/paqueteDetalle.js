import React, { useState, useEffect } from "react";
import {
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    FormGroup,
    Input,
    Label,
    Row,
    Table,
    Tooltip,
} from "reactstrap";
import "react-toastify/dist/ReactToastify.css";
import { IoIosAddCircleOutline } from "react-icons/io";
import { firstOrDefault, Any } from "mini-linq-js"
import { ToastContainer, toast } from "react-toastify";
import API from './../Constantes/Api';
import { Redirect } from "react-router-dom";

const thStyleButton = {
    background: "#20a8d8",
    color: "white",
    width: 200,
};


const thStyle = {
    background: "#20a8d8",
    color: "white",
};
const servicio = {
    id: "",
    nombre: "",
    descripcion: "",
    enabled: true,
};
const initialServicioState = [
];
const PaqueteDetalle = props => {
    const { currentState, setPaquetestate, nextStep, prevStep, add, 
        update, servicios, paqueteDetalle, setPaqueteDetalle, 
        deleteDetailRegisterEdit, fetchPaquetes } = props;
    const [servicioSeleccionado, setServicioSeleccionado] = useState([]);
    
    const [tooltipOpen, settooltipOpen] = useState(false);

    const handleInputChange = event => {
        const { name, value } = event.target;
        setPaquetestate({
            ...currentState,
            currentPaquete: {
                ...currentState.currentPaquete,
                [name]: value
            }
        });
    };

    const handleNumericInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        setPaquetestate({
            ...currentState,
            currentPaquete: {
                ...currentState.currentPaquete,
                [name]: parseFloat(value)
            }
        });

    }
    const handleCleanValues = () => {
        const initialClientState = {
            step: 1,
            editMode: "None",
            currentPaquete: {
                id: "",
                nombre: "",
                codigoPaquete: "",
                descripcion: "",
                precio: "",
                enabled: true,
            },
        };
        setPaquetestate(initialClientState);
        setPaqueteDetalle(initialServicioState);
        fetchPaquetes(null);
    };

    const handleSaveChanges = () => {
        var usuario = sessionStorage.getItem("userId");
        if (currentState.currentPaquete.precio === undefined)
        {
            toast.warn("Debe especificar un precio");
            return;
        }
        switch (currentState.editMode) {
            case 'Editing': {
                const paquete = {
                    id: currentState.currentPaquete.id,
                    nombre: currentState.currentPaquete.nombre,
                    descripcion: currentState.currentPaquete.descripcion,
                    codigoPaquete: currentState.currentPaquete.codigoPaquete,
                    precio: currentState.currentPaquete.precio,
                    detalle: serviciosPaqueteDetalle,
                    user: usuario,
                }
                update(paquete);
                break;
            }

            default: {
                const newPaquete = {
                    nombre: currentState.currentPaquete.nombre,
                    descripcion: currentState.currentPaquete.descripcion,
                    codigoPaquete: currentState.currentPaquete.codigoPaquete,
                    precio: currentState.currentPaquete.precio,
                    detalle: serviciosPaqueteDetalle,
                    user: usuario,
                }
                add(newPaquete);
                break;
            }
        };
    }

    function deleteDetailRegisterNuevoPaquete(id) {
            var lista = paqueteDetalle;
        var filtered = lista.filter(i => i.id !== id)
            setPaqueteDetalle(filtered);
    };



    const serviciosPaqueteDetalle = paqueteDetalle.map(p => {
        return ({
            codigoPauete: 0,
            codigoServicio: parseInt(p.id)
        }
        );
    });

    const serviciosOptions = servicios.map(p => {
        return (
            <option id={p.id} key={p.id} value={p.id}>
                {p.nombre}
            </option>
        );
    });

   

    const handleInputChangeSelected = event => {
        const target = event.target;
        const value = target.type === "checkbox" ? target.checked : target.value;
        const name = target.name;

        console.log("value", value);
        console.log("name", name);

        const servicio = servicios.firstOrDefault(s => s.id === parseInt(value));
        
        setServicioSeleccionado(servicio);

    };

    function toggle() {
       settooltipOpen(!tooltipOpen);
    }

    const handleAddServicio = () => {
        if (servicioSeleccionado.length === 0)
        {
            toast.warn("Debe seleccionar un servicio");
            return;
        }
        if (servicioSeleccionado.id === 0) {
            toast.warn("Debe seleccionar un servicio");
            return;
        }
        switch (currentState.editMode) {
            case 'Editing': {
                const NuevoDetalle = {
                    codigoPauete: currentState.currentPaquete.id,
                    codigoServicio: servicioSeleccionado.id,
                }
                addServicioAPaqueteExistente(NuevoDetalle);
                break;
            }
            default: {
                const service = {
                    id: servicioSeleccionado.id,
                    nombre: servicioSeleccionado.nombre,
                    descripcion: servicioSeleccionado.descripcion,
                    codigoServicio: servicioSeleccionado.codigoServicio,
                }

                setPaqueteDetalle(paqueteDetalle => [...paqueteDetalle, service]);
                break;
            }
     }
    }

    function addServicioAPaqueteExistente(nuevoDetalle) {
        var usuario = sessionStorage.getItem("userId");

        if (
            !nuevoDetalle ||
            !nuevoDetalle.codigoPauete ||
            !nuevoDetalle.codigoServicio
        ) {
            toast.warn("Faltan datos para poder crear un registro");
            return;
        }
        const url = `PaqueteDetalle`;
        API.post(url, nuevoDetalle).then(res => {
            
            if (res.id === 0) {
                toast.error("Error al intentar agregar registro");
            } else {
                toast.success("registro agregado satisfactoriamente");
            }
            const service = {
                id: res.data.id,
                nombre: servicioSeleccionado.nombre,
                descripcion: servicioSeleccionado.descripcion,
                codigoServicio: servicioSeleccionado.codigoServicio,
            }

            setPaqueteDetalle(paqueteDetalle => [...paqueteDetalle, service]);
        }).catch(error => {
            if (error.message === "Network Error") {
                toast.warn(
                    "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                );
            } else {
                if (error.response.status !== undefined) {
                    if (error.response.status === 401) {
                        props.history.push("/login");
                        return <Redirect to="/login" />;
                    }
                }
                else {
                    toast.warn(
                        "Se encontraron problemas para realizar trsancción favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                    );
                }
            }
        });
    }

    switch (currentState.editMode) {
        case 'Editing': return (

            <div className="animated fadeIn">
                <Card>
                    <CardHeader>
                        <strong> Datos a editar </strong>
                    </CardHeader>
                    <CardBody>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Id">Nombre</Label>
                                    <Input
                                        autoFocus
                                        type="text"
                                        name="nombre"
                                        id="nombre"
                                        readonly
                                        value={currentState.currentPaquete.nombre}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Descripcion</Label>
                                    <Input type="text" id="descripcion" name="descripcion"
                                        value={currentState.currentPaquete.descripcion}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                        {/* </Row>
                        <Row> */}
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Codigo Paquete</Label>
                                    <Input type="text" id="codigoPaquete" name="codigoPaquete"
                                        value={currentState.currentPaquete.codigoPaquete}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Precio</Label>
                                    <Input type="number" id="precio" name="precio"
                                        value={currentState.currentPaquete.precio}
                                        onChange={handleNumericInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            
                        </Row>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label>Servicios</Label>
                                    <Input
                                        type="select"
                                        name="salesServicioId"
                                        onChange={handleInputChangeSelected}
                                        required
                                    >
                                        {serviciosOptions}
                                    </Input>
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <div style={{ verticalAlign: 'bottom', marginTop: '27px', }}>
                                    <Button color="success" id="TooltipExample" onClick={handleAddServicio}>
                                        <i className="fa fa-plus-square" ></i>
                                    </Button>
                                    <Tooltip placement="right" isOpen={tooltipOpen} target="TooltipExample" toggle={toggle}>
                                        Agregar servicio!
                                    </Tooltip>
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col md="12">
                                <Table hover bordered striped responsive size="sm">
                                    <thead>
                                        <tr>
                                            <th style={thStyleButton}>Borrar</th>
                                            <th style={thStyle}>ID</th>
                                            <th style={thStyle}>Nombre</th>
                                            <th style={thStyle}>Descripcion</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {paqueteDetalle.map(item => (
                                            <tr key={item.id}>
                                                <td>
                                                    <Button
                                                        block
                                                        color="danger"
                                                        onClick={() => deleteDetailRegisterEdit(item.id)}
                                                    >Eliminar</Button>
                                                </td>
                                                <td>{item.id}</td>
                                                <td>{item.nombre}</td>
                                                <td>{item.descripcion}</td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </Table>
                            </Col>
                        </Row>
                        
                    </CardBody>
                    <CardFooter>
                        <Button type="submit" size="sm" color="success" onClick={handleSaveChanges}>
                            <i className="fa fa-dot-circle-o" /> Guardar
          </Button>
                        <Button type="reset" size="sm" color="danger" onClick={handleCleanValues}>
                            <i className="fa fa-ban" /> Cancelar
          </Button>
                    </CardFooter>
                </Card>
            </div>
        );
        default: return (

            <div className="animated fadeIn">
                <Card>
                    <CardHeader>
                        <strong> Datos de nuevo registro </strong>
                    </CardHeader>
                    <CardBody>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Id">Nombre</Label>
                                    <Input
                                        autoFocus
                                        type="text"
                                        name="nombre"
                                        id="nombre"
                                        value={currentState.currentPaquete.nombre}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Descripcion</Label>
                                    <Input type="text" id="descripcion" name="descripcion"
                                        value={currentState.currentPaquete.descripcion}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                        {/* </Row>
                        <Row> */}
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Codigo Paquete</Label>
                                    <Input type="text" id="codigoPaquete" name="codigoPaquete"
                                        value={currentState.currentPaquete.codigoPaquete}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Precio</Label>
                                    <Input type="number" id="precio" name="precio"
                                        value={currentState.currentPaquete.precio}
                                        onChange={handleNumericInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label>Servicios</Label>
                                    <Input
                                        type="select"
                                        name="salesServicioId"
                                        onChange={handleInputChangeSelected}
                                        required
                                    >
                                        {serviciosOptions}
                                    </Input>
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <div style={{ verticalAlign: 'bottom', marginTop: '27px',}}>
                                    <Button color="success" id="TooltipExample" onClick={handleAddServicio}>
                                        <i className="fa fa-plus-square" ></i>
                                    </Button>
                                    <Tooltip placement="right" isOpen={tooltipOpen} target="TooltipExample" toggle={toggle}>
                                        Agregar servicio!
                                    </Tooltip>
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col md="12">
                                <Table hover bordered striped responsive size="sm">
                                    <thead>
                                        <tr>
                                            <th style={thStyleButton}>Borrar</th>
                                            <th style={thStyle}>ID</th>
                                            <th style={thStyle}>Nombre</th>
                                            <th style={thStyle}>Descripcion</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {paqueteDetalle.map(item => (
                                            <tr key={item.id}>
                                                <td>
                                                    <Button
                                                        block
                                                        color="danger"
                                                        onClick={() => deleteDetailRegisterNuevoPaquete(item.id)}
                                                    >Eliminar</Button>
                                                </td>
                                                <td>{item.id}</td>
                                                <td>{item.nombre}</td>
                                                <td>{item.descripcion}</td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </Table>
                            </Col>
                        </Row>
                    </CardBody>
                    <CardFooter>
                        <Button type="submit" size="sm" color="success" onClick={handleSaveChanges}>
                            <i className="fa fa-dot-circle-o" /> Guardar
          </Button>
                        <Button type="reset" size="sm" color="danger" onClick={handleCleanValues}>
                            <i className="fa fa-ban" /> Cancelar
          </Button>
                    </CardFooter>
                </Card>
                <ToastContainer autoClose={5000} />
            </div>
        );

    }
}
export default PaqueteDetalle;