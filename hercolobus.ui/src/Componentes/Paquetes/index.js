import React, { useState, useEffect } from "react";
import {
    Card,
    CardBody,
    CardHeader,
    Col,
    Row,
    FormGroup,
    Button
} from "reactstrap";
import PaqueteTabla from './paqueteTabla'
import PaqueteDetalle from './paqueteDetalle'
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import API from './../Constantes/Api';
import { ToastContainer, toast } from "react-toastify";
import { firstOrDefault, Any } from "mini-linq-js"

const initialpaqueteState = {
    step: 1,
    editMode: "None",
    currentPaquete: {
        id: "",
        nombre: "",
        codigoPaquete:"",
        descripcion: "",
        precio: 0,
        enabled: true,
    },
};

// const heightStyle = {
//     height: "600px"
// };

const heightStyleDiv = {
    height: "600px",
    paddingLeft: '0px',
    paddingRight: '0px'
};
const initialServicioState = [
];

const Paquete = props => {
    const [paqueteState, setPaquetestate] = useState(initialpaqueteState);
    const [paquetes, setPaquetes] = useState([]);
    const [servicios, setServicios] = useState([]);
    const [paqueteDetalle, setPaqueteDetalle] = useState(initialServicioState);
    function nextStep(supplier) {
        const { step } = paqueteState;

        if (supplier === null || supplier === undefined) {
            supplier = paqueteState.currentPaquete;
        }
        setPaquetestate({ ...paqueteState, currentPaquete: supplier, step: step + 1 });
    }

    function fetchPaquetes(query, step) {
        const index = step === undefined || step === null ? 0 : step;
        if (query === null) {
            var url = `Paquete/paged?PageSize=10&PageIndex=${index}`
            API.get(url).then(res => {
                setPaquetes(res.data);
            }).catch(error => {
                if (error.message === "Network Error") {
                    toast.warn(
                        "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                    );
                } else {
                    if (error.response.status !== undefined) {
                        if (error.response.status === 401) {
                            props.history.push("/login");
                            return <Redirect to="/login" />;
                        }
                    }
                    else {
                        toast.warn(
                            "Se encontraron problemas para realizar trsancción favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                        );
                    }
                }
            });
        }
        else {
            var url1 = `Paquete/paged?pageSize=10&pageIndex=${index}&value=${query}`;
            API.get(url1).then(res => {
                setPaquetes(res.data);
            });
        }
    }

    function fetchServicios() {
            var url = `Servicio`
            API.get(url).then(res => {
                const listaServicios = [{ id: 0, nombre: "(Seleccione un Servicio)" }].concat(
                    res.data.sort(compareNames)
                );
                setServicios(listaServicios);
            }).catch(error => {
                if (error.message === "Network Error") {
                    toast.warn(
                        "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                    );
                } else {
                    if (error.response.status !== undefined) {
                        if (error.response.status === 401) {
                            props.history.push("/login");
                            return <Redirect to="/login" />;
                        }
                    }
                    else {
                        toast.warn(
                            "Se encontraron problemas para realizar trsancción favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                        );
                    }
                }
            });
    }

    function compareNames(currentItem, nextItem) {
        const currentName = currentItem.nombre;
        const nextName = nextItem.nombre;

        let comparison = 0;
        if (currentName > nextName) {
            comparison = 1;
        } else if (currentName < nextName) {
            comparison = -1;
        }
        return comparison;
    }

    function add(newPaquete) {
        if (
            !newPaquete ||
            !newPaquete.nombre ||
            !newPaquete.descripcion
        ) {
            toast.warn("Faltan datos para poder crear un registro");
            return;
        }
        const url = `Paquete`;
        API.post(url, newPaquete).then(res => {
            if (res.id === 0) {
                toast.error("Error al intentar agregar registro");
            } else {
                toast.success("registro agregado satisfactoriamente");

                fetchPaquetes(null);
                setPaquetestate(initialpaqueteState);
            }
        }).catch(error => {
            if (error.message === "Network Error") {
                toast.warn(
                    "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                );
            } else {
                if (error.response.status !== undefined) {
                    if (error.response.status === 401) {
                        props.history.push("/login");
                        return <Redirect to="/login" />;
                    }
                }
                else {
                    toast.warn(
                        "Se encontraron problemas para realizar trsancción favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                    );
                }
            }
        });
    }

    function update(puesto) {
        if (
            !puesto ||
            !puesto.nombre
        ) {

            toast.warn("Faltan datos para poder crear un registro");
            return;
        }
        const url = `Paquete`;
        API.put(url, puesto).then(res => {
            if (res.id === 0) {
                toast.error("Error al intentar agregar puesto");
            } else {
                toast.success("puesto agregado satisfactoriamente");

                // setUsers([...users.users, userAdded]);
                fetchPaquetes(null);
                setPaquetestate(initialpaqueteState);
            }
        }).catch(error => {
            if (error.message === "Network Error") {
                toast.warn(
                    "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                );
            } else {
                if (error.response.status !== undefined) {
                    if (error.response.status === 401) {
                        props.history.push("/login");
                        return <Redirect to="/login" />;
                    }
                }
                else {
                    toast.warn(
                        "Se encontraron problemas para realizar trsancción favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                    );
                }
            }
        });
    }

    const editRow = paquete => {
        setPaqueteDetalle(initialServicioState);
        paquete.detalle.map(p =>
            {
                const service = {
                    id: p.id,
                    nombre: p.servicio.nombre,
                    descripcion: p.servicio.descripcion,
                    codigoServicio: p.servicio.codigoPaquete,
                    servicioId: p.servicio.id,
                }
                    
                    setPaqueteDetalle(paqueteDetalle => [...paqueteDetalle, service]);
            })
        
        setPaquetestate({
            ...paqueteState,
            editMode: "Editing",
            currentPaquete: paquete,
            step: 2,
        });

        
    };

    const disabledRegister = id => {
        var usuario = sessionStorage.getItem("userId");
        const url = `Paquete/disabled`;

        const register = {
            id: id,
            user: usuario,
        }
        API.put(url, register).then(res => {
            if (res.data === null || res.data === undefined) {
                toast.error("error al intentar deshabilitar registro");
                return;
            }
            fetchPaquetes(null);
            setPaquetestate(initialpaqueteState);
            toast.success("Registro deshabilitado satisfactoriamente");
        }).catch(error => {
            if (error.message === "Network Error") {
                toast.warn(
                    "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                );
            } else {
                if (error.response.status !== undefined) {
                    if (error.response.status === 401) {
                        props.history.push("/login");
                        return <Redirect to="/login" />;
                    }
                }
                else {
                    toast.warn(
                        "Se encontraron problemas para realizar trsancción favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                    );
                }
            }
        });
    };

    function deleteDetailRegisterEdit (id) {
        var usuario = sessionStorage.getItem("userId");

        const url = `PaqueteDetalle?id=${id}`;
        API.delete(url).then(res => {
            if (res.data === null || res.data === undefined) {
                toast.error("error al intentar deshabilitar registro");
                return;
            }
            var lista = paqueteDetalle;
            var filtered = lista.filter(i => i.id !== id)
            setPaqueteDetalle(filtered);
            toast.success("Registro eliminado satisfactoriamente");
        }).catch(error => {
            if (error.message === "Network Error") {
                toast.warn(
                    "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                );
            } else {
                if (error.response.status !== undefined) {
                    if (error.response.status === 401) {
                        props.history.push("/login");
                        return <Redirect to="/login" />;
                    }
                }
                else {
                    toast.warn(
                        "Se encontraron problemas para realizar trsancción favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                    );
                }
            }
        });
    };

   

    function nextPage(step) {
        fetchPaquetes(null, step);
    }

    function prevPage(step) {
        fetchPaquetes(null, step)
    }

    function prevStep() {
        const { step } = paqueteState;

        setPaquetestate({ ...paqueteState, step: step - 1 });
    }

    useEffect(() => {
        fetchPaquetes(null);
        fetchServicios();
    }, []);


    function GetCurrentStepComponent(step) {
        switch (step) {
            case 1:
                return (
                    <CardBody >
                        <FormGroup row>
                            <Col />
                            <Col xs="auto">
                                <Button
                                    type="button"
                                    color="success"
                                    onClick={() => nextStep()}>
                                    <i className="fa fa-new" /> Agregar Paquete</Button>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col xs="12">
                                <PaqueteTabla fetchPaquetes={fetchPaquetes} paquetes={paquetes} nextPage={nextPage}
                                    prevPage={prevPage} editRow={editRow} disabledRegister={disabledRegister} />
                            </Col>
                        </FormGroup>
                    </CardBody>
                );
            case 2:
                return <PaqueteDetalle currentState={paqueteState} setPaquetestate={setPaquetestate}
                    nextStep={nextStep} prevStep={prevStep}
                    add={add}
                    update={update} servicios={servicios} paqueteDetalle={paqueteDetalle} setPaqueteDetalle={setPaqueteDetalle}
                    deleteDetailRegisterEdit={deleteDetailRegisterEdit} fetchPaquetes={fetchPaquetes}
                />;
            default:
                return null;
        }
    }

    const userStep = GetCurrentStepComponent(paqueteState.step);

    return (
        <div className="animated fadeIn" >
            <Card >
                <Row>
                    <Col>
                        <Card>
                            <CardHeader>
                                <i className="fa fa-align-justify" /> Paquetes
                            </CardHeader>
                            {userStep}
                        </Card>
                    </Col>
                </Row>
            </Card>
            <ToastContainer autoClose={5000} />
        </div>
    );
}
export default Paquete;