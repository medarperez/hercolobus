import React, { useState, useEffect } from "react";
import {
    Col,
    Table,
    FormGroup,
    InputGroup,
    InputGroupAddon,
    Button,
    Input
} from "reactstrap";
import moment from "moment";

const step = 1;


const ContratoTabla = props => {
    const {
        fetchContratos,
        contratos,
        nextPage,
        prevPage,
        editRow,
        disabledRegister
    } = props;
    const [searchValue, setSearchValue] = useState("");

    function nextStep() {
        nextPage(step + 1);
    }

    function prevStep() {
        prevPage(step - 1);
    }

    function handleValueChange(e) {
        fetchContratos(e.target.value);
        setSearchValue(e.target.value);
    }

    const thStyle = {
        background: "#20a8d8",
        color: "white"
    };

    const userRows =
        contratos === null ? (
            <Table hover bordered striped responsive size="sm">
                <thead>
                    <tr>
                        <td colSpan={16}>Buscando...</td>
                    </tr>
                </thead>
            </Table>
        ) : contratos.length === 0 ? (
            <Table hover bordered striped responsive size="sm">
                <thead>
                    <tr>
                        <td colSpan={16}>
                            No se encontraron usuarios con el filtro especificado.</td>
                    </tr>
                </thead>
            </Table>
        ) : (
                    <div>
                        <FormGroup row>
                            <Col>
                                <InputGroup>
                                    <InputGroupAddon addonType="prepend">
                                        <Button type="button" color="primary">
                                            <i className="fa fa-search" /> Buscar
                                     </Button>
                                    </InputGroupAddon>
                                    <Input
                                        type="text"
                                        id="input1-group2"
                                        name="input1-group2"
                                        placeholder="Buscar"
                                        value={searchValue}
                                        onChange={handleValueChange}
                                        style={{ width: "600px" }}
                                    />
                                </InputGroup>
                            </Col>
                        </FormGroup>

                        <FormGroup row>
                            <Col md="12">
                                <Table hover bordered striped responsive size="sm">
                                    <thead>
                                        <tr>
                                            {/* <th style={thStyle}>Editar</th> */}
                                            <th style={thStyle}>Borrar</th>
                                            <th style={thStyle}>ID</th>
                                            <th style={thStyle}>Cliente</th>
                                            <th style={thStyle}>Paquete</th>
                                            <th style={thStyle}>Inicio</th>
                                            <th style={thStyle}>Final</th>
                                            <th style={thStyle}>Precio Alternativo</th>
                                            <th style={thStyle}>Primer cobro</th>
                                            <th style={thStyle}>Dia Cobro</th>
                                            <th style={thStyle}>Exento</th>
                                            <th style={thStyle}>Conexion</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {contratos.items.map(item => (
                                            <tr key={item.id}>
                                                {/* <td>
                                                    <Button
                                                        block
                                                        color="warning"
                                                        onClick={() => {
                                                            editRow(item);
                                                        }}
                                                    >
                                                        Editar</Button>
                                                </td> */}
                                                <td>
                                                    <Button
                                                        block
                                                        color="danger"
                                                        onClick={() => disabledRegister(item.id)}
                                                    >Eliminar</Button>
                                                </td>
                                                <td nowrap="true">{item.id}</td>
                                                <td nowrap="true">{item.nombreCliente}</td>
                                                <td nowrap="true">{item.paquete.nombre}</td>
                                                <td nowrap="true">{moment(item.fechaInicio).format('YYYY-MM-DD')}</td>
                                                <td nowrap="true">{item.fechaFinal == null ? 'N/A' : moment(item.fechaFinal).format('YYYY-MM-DD')}</td>
                                                <td nowrap="true">{item.costoAlternativo}</td>
                                                <td nowrap="true">{moment(item.fechaInicioCobro).format('YYYY-MM-DD')}</td>
                                                <td nowrap="true">{item.diaMaximoParaPago}</td>
                                                <td nowrap="true">{item.esContratoExentoCobro ? 'SI' : 'NO'}</td>
                                                <td nowrap="true">{item.direccionDeConexion}</td>

                                            </tr>
                                        ))}
                                    </tbody>
                                </Table>
                            </Col>
                            <Col md="12">
                                <InputGroup>
                                    <InputGroupAddon addonType="prepend">
                                        <Button type="button" color="primary" onClick={prevStep}>
                                            <i className="icon-arrow-left-circle" /> Anterior
                                </Button>
                                    </InputGroupAddon>
                                    <div
                                        className="animated fadeIn"
                                        style={{
                                            marginTop: "7px",
                                            marginLeft: "8px",
                                            marginRight: "8px"
                                        }}
                                    >
                                        <h5>
                                            <strong>Paginas: {contratos.pageCount}</strong>
                                        </h5>
                                    </div>
                                    <div
                                        className="animated fadeIn"
                                        style={{
                                            marginTop: "7px",
                                            marginLeft: "8px",
                                            marginRight: "8px"
                                        }}
                                    >
                                        <h5>
                                            <strong>Actual: {contratos.pageIndex}</strong>
                                        </h5>
                                    </div>
                                    <InputGroupAddon addonType="prepend">
                                        <Button type="button" color="primary" onClick={nextStep}>
                                            Proximo <i className="icon-arrow-right-circle" />
                                        </Button>
                                    </InputGroupAddon>
                                </InputGroup>
                            </Col>
                        </FormGroup>
                    </div>
                );
    return userRows;
}
export default ContratoTabla;