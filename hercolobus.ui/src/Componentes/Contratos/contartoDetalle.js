import React, { useState, useEffect } from "react";
import {
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    FormGroup,
    Input,
    Label,
    Table,
    Row,
    ModalFooter,
    ModalHeader,
    Modal,
    ModalBody,
    InputGroup,
    InputGroupAddon,
} from "reactstrap";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer, toast } from "react-toastify";
import API from './../Constantes/Api';
import moment from "moment";
import { Redirect } from "react-router-dom";
import checkmark from "../../../src/assets/img/brand/checkmark.svg";

const ContratoDetalle = props => {
    const { currentState, setContratostate, nextStep, prevStep, add, update, paquetes } = props;
    const [clientes, setClientes] = useState([]);
    const [clienteSelected, setClienteSelected] = useState([]);
    const [value, setValue] = useState("");

    // const handleInputChange = event => {
    //     const { name, value } = event.target;
    //     setContratoState({
    //         ...currentState,
    //         currentContrato: {
    //             ...currentState.currentContrato,
    //             [name]: value
    //         }
    //     });
    // };

    const thStyle = {
        background: "#20a8d8",
        color: "white",
    };

    function handleValueChange(e) {

        fetchclientes(e.target.value);
        setValue(e.target.value);
    }

    const togglePrimary = () => {
        setContratostate({
            ...currentState,
            state: {
                large: !currentState.state.large,
            }
        })
        fetchclientes(null, 0);
    };

    const paquetesOptions = paquetes.map(p => {
        return (
            <option id={p.id} key={p.id} value={p.id}>
                {p.nombre}
            </option>
        );
    });

    const nextPageProjects = () => {
        fetchclientes(value, clientes.pageIndex + 1);
    }

    function prevPageProjects() {
        fetchclientes(value, clientes.pageIndex - 1)
    }

    const handleInputChange = event => {
        const target = event.target;
        const value = target.type === "checkbox" ? target.checked : target.value;
        const name = target.name;

        // console.log("value", value);
        // console.log("name", name);
        setContratostate({
            ...currentState,
            currentContrato: {
                ...currentState.currentContrato,
                [name]: value
            }
        });
    };


    const handleInputNumericChange = event => {
        const target = event.target;
        const value = target.type === "checkbox" ? target.checked : target.value;
        const name = target.name;

        // console.log("value", value);
        // console.log("name", name);
        setContratostate({
            ...currentState,
            currentContrato: {
                ...currentState.currentContrato,
                [name]: parseInt(value, 10)
            }
        });
    };
    const handleNumericInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        setContratostate({
            ...currentState,
            currentContrato: {
                ...currentState.currentContrato,
                [name]: parseFloat(value)
            }
        });

    }

    const selectCliente = cliente => {
        setClienteSelected(cliente);
        setContratostate({
            ...currentState,
            state: {
                large: !currentState.state.large,
            }
        })
    };

    const handleCleanValues = () => {
        const initialClientState = {
            step: 1,
            editMode: "None",
            currentContrato: {
                id: "",
                nombreCliente: "",
                nombrePaquete: "",
                clienteId: 0,
                paqueteId: 0,
                fechaInicio: Date.now,
                fechaFinal: undefined,
                fechaInicioCobro: Date.now,
                diaMaximoParaPago: 0,
                esContratoExentoCobro: false,
                direccionDeConexion: "",
                enabled: true,
                costoAlternativo: 0,
            },
            state: {
                modal: false,
                large: false,
                large1: false,
                small: false,
                primary: false,
                success: false,
                warning: false,
                danger: false,
                info: false,
                uploadFile: false,
                className: null
            },
        };
        setContratostate(initialClientState);
    };

    const handleSaveChanges = () => {
        var usuario = sessionStorage.getItem("userId");
        if (clienteSelected.nombre === undefined || clienteSelected.id === 0) {
            toast.warn("El cliente es necesario!");
            return;
        }
        if (currentState.currentContrato.paqueteId === undefined || currentState.currentContrato.paqueteId === 0) {
            toast.warn("El paquete es necesario!");
            return;
        }
        switch (currentState.editMode) {
            case 'Editing': {
                const service = {
                    id: currentState.currentContrato.id,
                    nombreCliente: currentState.currentContrato.nombreCliente,
                    nombrePaquete: currentState.currentContrato.nombrePaquete,
                    clienteId: currentState.currentContrato.clienteId,
                    paqueteId: currentState.currentContrato.paqueteId,
                    fechaInicio: currentState.currentContrato.fechaInicio,
                    fechaFinal: currentState.currentContrato.fechaFinal,
                    fechaInicioCobro: currentState.currentContrato.fechaInicioCobro,
                    diaMaximoParaPago: currentState.currentContrato.diaMaximoParaPago,
                    esContratoExentoCobro: currentState.currentContrato.esContratoExentoCobro,
                    direccionDeConexion: currentState.currentContrato.direccionDeConexion,
                    costoAlternativo: currentState.currentContrato.costoAlternativo,
                    enabled: true,
                    user: usuario,
                }
                update(service);
                break;
            }

            default: {
                const newService = {
                    nombreCliente: clienteSelected.nombreCliente,
                    nombrePaquete: currentState.currentContrato.nombrePaquete,
                    clienteId: clienteSelected.id,
                    paqueteId: currentState.currentContrato.paqueteId,
                    fechaInicio: currentState.currentContrato.fechaInicio,
                    fechaFinal: currentState.currentContrato.fechaFinal,
                    fechaInicioCobro: currentState.currentContrato.fechaInicioCobro,
                    diaMaximoParaPago: currentState.currentContrato.diaMaximoParaPago,
                    esContratoExentoCobro: currentState.currentContrato.esContratoExentoCobro,
                    direccionDeConexion: currentState.currentContrato.direccionDeConexion,
                    costoAlternativo: currentState.currentContrato.costoAlternativo,
                    user: usuario,
                }
                add(newService);
                break;
            }
        };
    }


    function fetchclientes(query, step) {
        const index = step === undefined || step === null ? 0 : step;
        if (query === null) {
            var url = `Cliente/paged?PageSize=10&PageIndex=${index}`
            API.get(url).then(res => {
                setClientes(res.data);
            }).catch(error => {
               
                if (error.message === 'Network Error') {
                    toast.error("Error de red");
                    props.history.push('/login');
                    return (<Redirect to='/login'></Redirect>);
                }
                if (error.message !== '') {
                    toast.error("Error de red");
                    return (<Redirect to='/login'></Redirect>);
                }
                if (error.response.status === 401) {
                    props.history.push('/login');
                    return (<Redirect to='/login'></Redirect>)
                }
                console.log(error.response);
            });
        }
        else {
            var url1 = `Cliente/paged?pageSize=10&pageIndex=${index}&value=${query}`;
            API.get(url1).then(res => {
                setClientes(res.data);
            });
        }
    }


    switch (currentState.editMode) {
        case 'Editing': return (

            <div className="animated fadeIn">
                <Card>
                    <CardHeader>
                        <strong> Datos a editar </strong>
                    </CardHeader>
                    <CardBody>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="lastName">Cliente</Label>
                                    <Input type="text" id="clienteId" name="clienteId"
                                        value={clienteSelected.nombre}
                                        // onChange={handleInputChange}
                                        disabled={true}
                                        required />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <Row>
                                    <Label htmlFor="Segundo Nombre"> Lista Clientes </Label>
                                </Row>
                                <Row>
                                    <Button color="primary" onClick={togglePrimary} >Seleccionar Cliente</Button>
                                    <Modal isOpen={currentState.state.large} toggle={togglePrimary}
                                        className={'modal-lg'}>
                                        <ModalHeader toggle={togglePrimary}>Seleccionar Cliente</ModalHeader>
                                        <ModalBody>
                                            <FormGroup row>
                                                <Col md="12">
                                                    <InputGroup>
                                                        <InputGroupAddon addonType="prepend">
                                                            <Button type="button" color="primary">
                                                                <i className="fa fa-search" /> Buscar</Button>
                                                        </InputGroupAddon>
                                                        <Input
                                                            type="text"
                                                            id="input1-group2"
                                                            name="input1-group2"
                                                            placeholder="Buscar"
                                                            value={value}
                                                            onChange={handleValueChange} />
                                                    </InputGroup>
                                                </Col>
                                            </FormGroup>
                                            <FormGroup row>
                                                <Col md="12">
                                                    {clientes === null ? (
                                                        <tr>
                                                            <td colSpan={16}>Buscando...</td>
                                                        </tr>
                                                    ) : clientes.length === 0 ? (
                                                        <tr>
                                                            <td colSpan={16}>
                                                                No se encontraron proyectos.
                                                    </td>
                                                        </tr>
                                                    ) : (
                                                                <Table hover bordered striped responsive size="sm">
                                                                    <thead>
                                                                        <tr>
                                                                            <th style={thStyle}>Seleccionar</th>
                                                                            <th style={thStyle}>Nombre</th>
                                                                            <th style={thStyle}>Apellido</th>
                                                                            <th style={thStyle}>Identidad</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>

                                                                        {clientes.items.map(cliente => (
                                                                            <tr key={cliente.id}>
                                                                                <td>
                                                                                    <Button
                                                                                        onClick={() => {
                                                                                            selectCliente(cliente);
                                                                                        }}>
                                                                                        <img src={checkmark} height="50%" width="50%" alt="checkmark" />
                                                                                    </Button></td>
                                                                                <td>{cliente.nombre}</td>
                                                                                <td>{cliente.apellido}</td>
                                                                                <td>{cliente.identidad}</td>
                                                                            </tr>
                                                                        ))}
                                                                    </tbody>
                                                                </Table>
                                                            )}
                                                </Col>
                                                <Col md="12">
                                                    <InputGroup>
                                                        <InputGroupAddon addonType="prepend">
                                                            <Button type="button" color="primary" onClick={prevPageProjects}>
                                                                <i className="icon-arrow-left-circle" /> Anterior</Button>
                                                        </InputGroupAddon>
                                                        <div className="animated fadeIn" style={{ marginTop: '7px', marginLeft: '8px', marginRight: '8px', }}><h5><strong>Paginas: {clientes.pageCount}</strong></h5></div>
                                                        <div className="animated fadeIn" style={{ marginTop: '7px', marginLeft: '8px', marginRight: '8px', }}><h5><strong>Actual: {clientes.pageIndex}</strong></h5></div>
                                                        <InputGroupAddon addonType="prepend">
                                                            <Button type="button" color="primary" onClick={nextPageProjects}>
                                                                Proximo  <i className="icon-arrow-right-circle" /></Button>
                                                        </InputGroupAddon>
                                                    </InputGroup>
                                                </Col>
                                            </FormGroup>
                                        </ModalBody>
                                        <ModalFooter>
                                            <Button color="secondary" onClick={togglePrimary}>Cancel</Button>
                                        </ModalFooter>
                                    </Modal>
                                </Row>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label>Paquetes</Label>
                                    <Input
                                        type="select"
                                        name="clienteId"
                                        onChange={handleInputNumericChange}
                                        required
                                    >
                                        {paquetesOptions}
                                    </Input>
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="creationDate">Inicio</Label>
                                    <Input
                                        type="date"
                                        name="creationDate"
                                        id="creationDate"
                                        placeholder="Inicio de contrato"
                                        value={moment(currentState.currentContrato.fechaInicio).format('YYYY-MM-DD')}
                                        onChange={handleInputChange}
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="creationDate">Fecha Primer Pago</Label>
                                    <Input
                                        type="date"
                                        name="creationDate"
                                        id="creationDate"
                                        placeholder="Inicio de contrato"
                                        value={moment(currentState.currentContrato.fechaInicioCobro).format('YYYY-MM-DD')}
                                        onChange={handleInputChange}
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="diaMaximoParaPago">Dia de pago</Label>
                                    <Input
                                        type="number"
                                        name="diaMaximoParaPago"
                                        id="diaMaximoParaPago"
                                        placeholder="Indique un dia del mes"
                                        value={currentState.currentContrato.diaMaximoParaPago}
                                        onChange={handleInputChange}
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="direccionDeConexion">Costo Alternativo</Label>
                                    <Input
                                        type="number"
                                        name="costoAlternativo"
                                        id="costoAlternativo"
                                        placeholder="Precio del paquete"
                                        value={currentState.currentContrato.costoAlternativo}
                                        onChange={handleNumericInputChange}
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col >
                                <FormGroup>
                                    <Label htmlFor="direccionDeConexion">Direccion Conexion</Label>
                                    <Input
                                        type="text"
                                        name="diaMaximoParaPago"
                                        id="diaMaximoParaPago"
                                        placeholder="Direccion donde esta el paquete "
                                        value={currentState.currentContrato.direccionDeConexion}
                                        onChange={handleInputChange}
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                    </CardBody>
                    <CardFooter>
                        <Button type="submit" size="sm" color="success" onClick={handleSaveChanges}>
                            <i className="fa fa-dot-circle-o" /> Guardar
          </Button>
                        <Button type="reset" size="sm" color="danger" onClick={handleCleanValues}>
                            <i className="fa fa-ban" /> Cancelar
          </Button>
                    </CardFooter>
                </Card>
            </div>
        );
        default: return (

            <div className="animated fadeIn">
                <Card>
                    <CardHeader>
                        <strong> Datos de nuevo contrato </strong>
                    </CardHeader>
                    <CardBody>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="lastName">Cliente</Label>
                                    <Input type="text" id="clienteId" name="clienteId"
                                        value={clienteSelected.nombre}
                                        // onChange={handleInputChange}
                                        disabled={true}
                                        required />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <Row>
                                    <Label htmlFor="Segundo Nombre"> Lista Proyectos </Label>
                                </Row>
                                <Row>
                                    <Button color="primary" onClick={togglePrimary} >Seleccionar Cliente</Button>
                                    <Modal isOpen={currentState.state.large} toggle={togglePrimary}
                                        className={'modal-lg'}>
                                        <ModalHeader toggle={togglePrimary}>Seleccionar Cliente</ModalHeader>
                                        <ModalBody>
                                            <FormGroup row>
                                                <Col md="12">
                                                    <InputGroup>
                                                        <InputGroupAddon addonType="prepend">
                                                            <Button type="button" color="primary">
                                                                <i className="fa fa-search" /> Buscar</Button>
                                                        </InputGroupAddon>
                                                        <Input
                                                            type="text"
                                                            id="input1-group2"
                                                            name="input1-group2"
                                                            placeholder="Buscar"
                                                            value={value}
                                                            onChange={handleValueChange} />
                                                    </InputGroup>
                                                </Col>
                                            </FormGroup>
                                            <FormGroup row>
                                                <Col md="12">
                                                    {clientes === null ? (
                                                        <tr>
                                                            <td colSpan={16}>Buscando...</td>
                                                        </tr>
                                                    ) : clientes.length === 0 ? (
                                                        <tr>
                                                            <td colSpan={16}>
                                                                No se encontraron proyectos.
                                                    </td>
                                                        </tr>
                                                    ) : (
                                                                <Table hover bordered striped responsive size="sm">
                                                                    <thead>
                                                                        <tr>
                                                                            <th style={thStyle}>Seleccionar</th>
                                                                            <th style={thStyle}>Nombre</th>
                                                                            <th style={thStyle}>Apellido</th>
                                                                            <th style={thStyle}>Identidad</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>

                                                                        {clientes.items.map(cliente => (
                                                                            <tr key={cliente.id}>
                                                                                <td>
                                                                                    <Button
                                                                                        onClick={() => {
                                                                                            selectCliente(cliente);
                                                                                        }}>
                                                                                        <img src={checkmark} height="50%" width="50%" alt="checkmark" />
                                                                                    </Button></td>
                                                                                <td>{cliente.nombre}</td>
                                                                                <td>{cliente.apellido}</td>
                                                                                <td>{cliente.identidad}</td>
                                                                            </tr>
                                                                        ))}
                                                                    </tbody>
                                                                </Table>
                                                            )}
                                                </Col>
                                                <Col md="12">
                                                    <InputGroup>
                                                        <InputGroupAddon addonType="prepend">
                                                            <Button type="button" color="primary" onClick={prevPageProjects}>
                                                                <i className="icon-arrow-left-circle" /> Anterior</Button>
                                                        </InputGroupAddon>
                                                        <div className="animated fadeIn" style={{ marginTop: '7px', marginLeft: '8px', marginRight: '8px', }}><h5><strong>Paginas: {clientes.pageCount}</strong></h5></div>
                                                        <div className="animated fadeIn" style={{ marginTop: '7px', marginLeft: '8px', marginRight: '8px', }}><h5><strong>Actual: {clientes.pageIndex}</strong></h5></div>
                                                        <InputGroupAddon addonType="prepend">
                                                            <Button type="button" color="primary" onClick={nextPageProjects}>
                                                                Proximo  <i className="icon-arrow-right-circle" /></Button>
                                                        </InputGroupAddon>
                                                    </InputGroup>
                                                </Col>
                                            </FormGroup>
                                        </ModalBody>
                                        <ModalFooter>
                                            <Button color="secondary" onClick={togglePrimary}>Cancel</Button>
                                        </ModalFooter>
                                    </Modal>
                                </Row>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label>Paquetes</Label>
                                    <Input
                                        type="select"
                                        name="paqueteId"
                                        onChange={handleInputNumericChange}
                                        value={currentState.currentContrato.paqueteId}
                                        required
                                    >
                                        {paquetesOptions}
                                    </Input>
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="creationDate">Inicio</Label>
                                    <Input
                                        type="date"
                                        name="fechaInicio"
                                        id="fechaInicio"
                                        placeholder="Inicio de contrato"
                                        value={moment(currentState.currentContrato.fechaInicio).format('YYYY-MM-DD')}
                                        onChange={handleInputChange}
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="creationDate">Fecha Primer Pago</Label>
                                    <Input
                                        type="date"
                                        name="fechaInicioCobro"
                                        id="fechaInicioCobro"
                                        placeholder="Inicio de contrato"
                                        value={moment(currentState.currentContrato.fechaInicioCobro).format('YYYY-MM-DD')}
                                        onChange={handleInputChange}
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="diaMaximoParaPago">Dia de pago</Label>
                                    <Input
                                        type="number"
                                        name="diaMaximoParaPago"
                                        id="diaMaximoParaPago"
                                        placeholder="Indique un dia del mes"
                                        value={currentState.currentContrato.diaMaximoParaPago}
                                        onChange={handleInputChange}
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="direccionDeConexion">Costo Alternativo</Label>
                                    <Input
                                        type="number"
                                        name="costoAlternativo"
                                        id="costoAlternativo"
                                        placeholder="Precio del paquete"
                                        value={currentState.currentContrato.costoAlternativo}
                                        onChange={handleNumericInputChange}
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col >
                                <FormGroup>
                                    <Label htmlFor="direccionDeConexion">Direccion Conexion</Label>
                                    <Input
                                        type="text"
                                        name="direccionDeConexion"
                                        id="direccionDeConexion"
                                        placeholder="Direccion donde esta el paquete "
                                        value={currentState.currentContrato.direccionDeConexion}
                                        onChange={handleInputChange}
                                    />
                                </FormGroup>
                            </Col>
                        </Row>

                    </CardBody>
                    <CardFooter>
                        <Button type="submit" size="sm" color="success" onClick={handleSaveChanges}>
                            <i className="fa fa-dot-circle-o" /> Guardar
          </Button>
                        <Button type="reset" size="sm" color="danger" onClick={handleCleanValues}>
                            <i className="fa fa-ban" /> Cancelar
          </Button>
                    </CardFooter>
                </Card>
                <ToastContainer autoClose={5000} />
            </div>
        );

    }
}
export default ContratoDetalle;