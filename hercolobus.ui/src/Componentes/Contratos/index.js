import React, { useState, useEffect } from "react";
import {
    Card,
    CardBody,
    CardHeader,
    Col,
    Row,
    FormGroup,
    Button
} from "reactstrap";
import ContratoTabla from './contratoTabla'
import ContartoDetalle from './contartoDetalle'
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import API from './../Constantes/Api';
import { ToastContainer, toast } from "react-toastify";
import ContratoDetalle from "./contartoDetalle";



const initialContratoState = {
    step: 1,
    editMode: "None",
    currentContrato: {
        id: "",
        nombreCliente: "",
        nombrePaquete: "",
        clienteId: 0,
        paqueteId: 0,
        fechaInicio: Date.now, 
        fechaFinal: undefined,
        fechaInicioCobro: Date.now,
        diaMaximoParaPago: 0,
        esContratoExentoCobro: false,
        direccionDeConexion: "",
        enabled: true,
        costoAlternativo: 0,
    },
    state: {
        modal: false,
        large: false,
        large1: false,
        small: false,
        primary: false,
        success: false,
        warning: false,
        danger: false,
        info: false,
        uploadFile: false,
        className: null
    },
};

// const heightStyle = {
//     height: "600px"
// };

const heightStyleDiv = {
    height: "600px",
    paddingLeft: '0px',
    paddingRight: '0px'
};


const Contrato = props => {
    const [contratoState, setContratostate] = useState(initialContratoState);
    const [contratos, setContratos] = useState([]);
    const [paquetes, setPaquetes] = useState([]);

    function nextStep(supplier) {
        const { step } = contratoState;

        if (supplier === null || supplier === undefined) {
            supplier = contratoState.currentContrato;
        }
        setContratostate({ ...contratoState, currentContrato: supplier, step: step + 1 });
    }

    function fetchContratos(query, step) {
        const index = step === undefined || step === null ? 0 : step;
        //  var rol = sessionStorage.getItem('rolId');
        //  if (rol === "3") {
        //      props.history.push('/dashboard');
        //      return (<Redirect to='/dashboard'></Redirect>);
        //  }
        if (query === null) {
            var url = `Contrato/paged?PageSize=10&PageIndex=${index}`
            API.get(url).then(res => {
                setContratos(res.data);
            }).catch(error => {
                if (error.message === "Network Error") {
                    toast.warn(
                        "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                    );
                } else {
                    if (error.response.status !== undefined) {
                        if (error.response.status === 401) {
                            props.history.push("/login");
                            return <Redirect to="/login" />;
                        }
                    }
                    else {
                        toast.warn(
                            "Se encontraron problemas para realizar trsancción favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                        );
                    }
                }
            });
        }
        else {
            var url1 = `Contrato/paged?pageSize=10&pageIndex=${index}&value=${query}`;
            API.get(url1).then(res => {
                setContratos(res.data);
            });
        }
    }

    function fetchPaquetes() {
        var url = `Paquete`
        API.get(url).then(res => {
            const listaServicios = [{ id: 0, nombre: "(Seleccione un Paquete)" }].concat(
                res.data.sort(compareNames)
            );
            setPaquetes(listaServicios);
        }).catch(error => {
            if (error.message === "Network Error") {
                toast.warn(
                    "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                );
            } else {
                if (error.response.status !== undefined) {
                    if (error.response.status === 401) {
                        props.history.push("/login");
                        return <Redirect to="/login" />;
                    }
                }
                else {
                    toast.warn(
                        "Se encontraron problemas para realizar trsancción favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                    );
                }
            }
        });
    }

    function compareNames(currentItem, nextItem) {
        const currentName = currentItem.nombre;
        const nextName = nextItem.nombre;

        let comparison = 0;
        if (currentName > nextName) {
            comparison = 1;
        } else if (currentName < nextName) {
            comparison = -1;
        }
        return comparison;
    }

    function add(newPuesto) {
        if (
            !newPuesto ||
            !newPuesto.clienteId ||
            !newPuesto.paqueteId
        ) {
            toast.warn("Faltan datos para poder crear un registro");
            return;
        }
        const url = `Contrato`;

        API.post(url, newPuesto).then(res => {
            if (res.id === 0) {
                toast.error("Error al intentar agregar puesto");
            } else {
                toast.success("puesto agregado satisfactoriamente");

                fetchContratos(null);
                setContratostate(initialContratoState);
            }
        }).catch(error => {
            if (error.message === "Network Error") {
                toast.warn(
                    "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                );
            } else {
                if (error.response.status !== undefined) {
                    if (error.response.status === 401) {
                        props.history.push("/login");
                        return <Redirect to="/login" />;
                    }
                }
                else {
                    toast.warn(
                        "Se encontraron problemas para realizar trsancción favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                    );
                }
            }
        });
    }

    function update(puesto) {
        if (
            !puesto ||
            !puesto.nombre
        ) {

            toast.warn("Faltan datos para poder crear un registro");
            return;
        }
        const url = `Contrato`;
        API.put(url, puesto).then(res => {
            if (res.id === 0) {
                toast.error("Error al intentar agregar puesto");
            } else {
                toast.success("puesto agregado satisfactoriamente");

                // setUsers([...users.users, userAdded]);
                fetchContratos(null);
                setContratostate(initialContratoState);
            }
        }).catch(error => {
            if (error.message === "Network Error") {
                toast.warn(
                    "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                );
            } else {
                if (error.response.status !== undefined) {
                    if (error.response.status === 401) {
                        props.history.push("/login");
                        return <Redirect to="/login" />;
                    }
                }
                else {
                    toast.warn(
                        "Se encontraron problemas para realizar trsancción favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                    );
                }
            }
        });
    }

    const editRow = puesto => {
        setContratostate({
            ...contratoState,
            editMode: "Editing",
            currentContrato: puesto,
            step: 2,
        });
    };

    const disabledRegister = id => {
        var usuario = sessionStorage.getItem("usuarioId");
        const url = `Contrato/disabled`;

        const register = {
            id: id,
            user: usuario,
        }
        API.put(url, register).then(res => {
            if (res.data === null || res.data === undefined) {
                toast.error("error al intentar deshabilitar puesto");
                return;
            }
            fetchContratos(null);
            setContratostate(initialContratoState);
            toast.success("Registro deshabilitado satisfactoriamente");
        }).catch(error => {
            if (error.message === "Network Error") {
                toast.warn(
                    "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                );
            } else {
                if (error.response.status !== undefined) {
                    if (error.response.status === 401) {
                        props.history.push("/login");
                        return <Redirect to="/login" />;
                    }
                }
                else {
                    toast.warn(
                        "Se encontraron problemas para realizar trsancción favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                    );
                }
            }
        });
    };

    function nextPage(step) {
        fetchContratos(null, step);
    }

    function prevPage(step) {
        fetchContratos(null, step)
    }

    function prevStep() {
        const { step } = contratoState;

        setContratostate({ ...contratoState, step: step - 1 });
    }

    useEffect(() => {
        fetchContratos(null);
        fetchPaquetes();
        // fectRoles();
    }, []);


    function GetCurrentStepComponent(step) {
        switch (step) {
            case 1:
                return (
                    <CardBody >
                        <FormGroup row>
                            <Col />
                            <Col xs="auto">
                                <Button
                                    type="button"
                                    color="success"
                                    onClick={() => nextStep()}>
                                    <i className="fa fa-new" /> Agregar contrato</Button>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col xs="12">
                                <ContratoTabla fetchContratos={fetchContratos} contratos={contratos} nextPage={nextPage}
                                    prevPage={prevPage} editRow={editRow} disabledRegister={disabledRegister} />
                            </Col>
                        </FormGroup>
                    </CardBody>
                );
            case 2:
                return <ContratoDetalle currentState={contratoState} setContratostate={setContratostate}
                    nextStep={nextStep} prevStep={prevStep}
                    add={add}
                    update={update} paquetes={paquetes}
                />;
            default:
                return null;
        }
    }

    const userStep = GetCurrentStepComponent(contratoState.step);

    return (
        <div className="animated fadeIn" >
            <Card >
                <Row>
                    <Col>
                        <Card>
                            <CardHeader>
                                <i className="fa fa-align-justify" /> Contratos
                            </CardHeader>
                            {userStep}
                        </Card>
                    </Col>
                </Row>
            </Card>
            <ToastContainer autoClose={5000} />
        </div>
    );
}
export default Contrato;