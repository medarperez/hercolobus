using System;
using System.Collections.Generic;
using hercolobus.services.Core;

namespace hercolobus.services.Features.Puestos
{
    public interface IPuestoAppService : IDisposable
    {
        PuestoPagedDto GetPaged(PagedGeneralRequest request);
        PuestoDto Create(PuestoRequest request);
        PuestoDto Update(PuestoRequest request);
        PuestoDto Disabled(PuestoRequest request);
        IEnumerable<PuestoDto> GetAllPuestos();
        string Delete(int id); 
    }
}