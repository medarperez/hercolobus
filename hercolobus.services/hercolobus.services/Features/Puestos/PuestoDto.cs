using hercolobus.services.Core;

namespace hercolobus.services.Features.Puestos
{
    public class PuestoDto : ResponseBase
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }

    }
}