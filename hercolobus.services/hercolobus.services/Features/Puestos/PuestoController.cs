using System;
using System.Collections.Generic;
using hercolobus.services.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace hercolobus.services.Features.Puestos
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PuestoController : ControllerBase
    {
        private readonly IPuestoAppService _puestoAppService;
        public PuestoController(IPuestoAppService puestoAppService)
        {
            if (puestoAppService == null) throw new ArgumentException(nameof(puestoAppService));

            _puestoAppService = puestoAppService;
        }

        [HttpGet]
        // [Route("paged")]
        [Authorize]
        public ActionResult<IEnumerable<PuestoDto>> Get()
        {
            return Ok(_puestoAppService.GetAllPuestos());
        }

        [HttpGet]
        [Route("paged")]
        [Authorize]
        public ActionResult<PuestoPagedDto> GetPaged([FromQuery]PagedGeneralRequest request)
        {
            return Ok(_puestoAppService.GetPaged(request));
        }

        [HttpPost]
        [Authorize]
        public ActionResult<PuestoDto> Post([FromBody]PuestoRequest request)
        {
            return Ok(_puestoAppService.Create(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<PuestoDto> Put([FromBody] PuestoRequest request)
        {
            return Ok(_puestoAppService.Update(request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_puestoAppService.Delete(Id));
        }

        [HttpPut]
        [Route("disabled")]
        public ActionResult<PuestoDto> Disabled([FromBody] PuestoRequest request)
        {
            return Ok(_puestoAppService.Disabled(request));
        }
    }
}