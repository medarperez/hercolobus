using System;
using System.Collections.Generic;

namespace hercolobus.services.Features.ReportesCobros
{
    public interface IReporteCobroAppService : IDisposable
    {
         IEnumerable<ReporteGeneralCobroDto> ObtenerCobrosActuales();
    }
}