using System;
using System.Collections.Generic;
using hercolobus.services.Core;

namespace hercolobus.services.Features.ReportesCobros
{
    public class ReporteGeneralCobroDto : ResponseBase
    {
        public int CodigoCliente { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int CodigoPaquete { get; set; }
        public string Paquete { get; set; }
        public DateTime FechaInicio { get; set; }
        public int DiasFacturados { get; set; } 
        public string DireccionDeConexion { get; set; }
        public int MesesFacturados { get; set; }
        public Double TotalFacturado { get; set; }
        public IEnumerable<ReporteGeneralCobroDto> Detalle { get; set; }

    }
}