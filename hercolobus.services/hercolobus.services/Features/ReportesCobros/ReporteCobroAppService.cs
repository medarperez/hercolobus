using System;
using System.Collections.Generic;
using hercolobus.services.Context;

namespace hercolobus.services.Features.ReportesCobros
{
    public class ReporteCobroAppService : IReporteCobroAppService
    {
        private HercolobusContext _context;

        public ReporteCobroAppService(HercolobusContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));

            _context = context;
        }
        public IEnumerable<ReporteGeneralCobroDto> ObtenerCobrosActuales()
        {
            throw new System.NotImplementedException();
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
        }

    }
}