using System;
using hercolobus.services.Core;

namespace hercolobus.services.Features.ReportesCobros
{
    public class ReporteGeneralCobroDetalleDto : ResponseBase
    {
        public double SaldoMes { get; set; }
        public DateTime MesFacturado { get; set; }
    }
}