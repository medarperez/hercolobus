using System;

namespace hercolobus.services.Features.Paquetes
{
    public class PaqueteDomainService : IPaqueteDomainService
    {
        public Paquete Create(PaqueteRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Nombre)) throw new ArgumentNullException(nameof(request.Nombre));

            Paquete zona = new Paquete.Builder()
            .ConNombre(request.Nombre)
            .ConDescripcion(request.Descripcion)
            .ConCodigo(request.CodigoPaquete)
            .ConPrecio(request.Precio)
            .ConAuditFields(request.User)
            .Build();

            return zona;
        }

        public Paquete Update(PaqueteRequest request, Paquete _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(request.Nombre, request.Descripcion, 
            request.CodigoPaquete,request.Precio, request.User);

            return _oldRegister;
        }

        public Paquete Disabled(PaqueteRequest request, Paquete _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Disbaled(request.User);

            return _oldRegister;
        }


        public PaqueteDetalle CreateDetail(PaqueteDetalleRequest request)
        {
            if (request.CodigoPauete <= 0) throw new ArgumentNullException(nameof(request.CodigoPauete));
            if (request.CodigoServicio <= 0) throw new ArgumentNullException(nameof(request.CodigoServicio));

            PaqueteDetalle zona = new PaqueteDetalle.Builder()
            .ConCodigoPaquete(request.CodigoPauete)
            .ConCodigoServicio(request.CodigoServicio)
            .ConAuditFields(request.User)
            .Build();

            return zona;
        }

        public PaqueteDetalle UpdateDetail(PaqueteDetalleRequest request, PaqueteDetalle _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(request.CodigoServicio, request.User);

            return _oldRegister;
        }

        public PaqueteDetalle DisabledDetail(PaqueteDetalleRequest request, PaqueteDetalle _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Disbaled(request.User);

            return _oldRegister;
        }


        public void Dispose()
        {
        }
    }
}