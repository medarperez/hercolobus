using System;
using hercolobus.services.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace hercolobus.services.Features.Paquetes
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PaqueteDetalleController : ControllerBase
    {
        private readonly IPaqueteAppService _paqueteAppService;
        public PaqueteDetalleController(IPaqueteAppService puestoAppService)
        {
            if (puestoAppService == null) throw new ArgumentException(nameof(puestoAppService));

            _paqueteAppService = puestoAppService;
        }


        [HttpPost]
        [Authorize]
        public ActionResult<PaqueteDetalleDto> Post([FromBody]PaqueteDetalleRequest request)
        {
            return Ok(_paqueteAppService.CreateDetail(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<PaqueteDetalleDto> Put([FromBody] PaqueteDetalleRequest request)
        {
            return Ok(_paqueteAppService.UpdateDetail(request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_paqueteAppService.DeleteDetail(Id));
        }

        [HttpPut]
        [Route("disabled")]
        public ActionResult<PaqueteDetalleDto> Disabled([FromBody] PaqueteDetalleRequest request)
        {
            return Ok(_paqueteAppService.DisabledDetail(request));
        }
    }
}