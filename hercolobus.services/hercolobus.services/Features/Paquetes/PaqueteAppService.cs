using System;
using System.Collections.Generic;
using System.Linq;
using hercolobus.services.Context;
using hercolobus.services.Core;
using hercolobus.services.Features.Servicios;
using Microsoft.EntityFrameworkCore;

namespace hercolobus.services.Features.Paquetes
{
    public class PaqueteAppService : IPaqueteAppService
    {
        private HercolobusContext _context;
        private readonly IPaqueteDomainService _paqueteDomainService;

        public PaqueteAppService(HercolobusContext context, IPaqueteDomainService paqueteDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (paqueteDomainService == null) throw new ArgumentException(nameof(paqueteDomainService));

            _context = context;
            _paqueteDomainService = paqueteDomainService;
        }
        public PaqueteDto Create(PaqueteRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newPaquete = _paqueteDomainService.Create(request);

            _context.Paquetes.Add(newPaquete);
            _context.SaveChanges();

            var paquete = _context.Paquetes.FirstOrDefault(s => s.Nombre == request.Nombre && s.Enabled == true);
            if(paquete == null) 
            {
                return new PaqueteDto
                {
                    Nombre = newPaquete.Nombre,
                    Descripcion = newPaquete.Descripcion,
                    CodigoPaquete = newPaquete.CodigoPaquete,
                    Precio = newPaquete.Precio,
                    Enabled = newPaquete.Enabled
                };
            }
            if (request.Detalle == null)
            {
                return new PaqueteDto
                {
                    Nombre = newPaquete.Nombre,
                    Descripcion = newPaquete.Descripcion,
                    CodigoPaquete = newPaquete.CodigoPaquete,
                    Precio = newPaquete.Precio,
                    Enabled = newPaquete.Enabled
                };
            }
            if (!request.Detalle.Any())
            {
                return new PaqueteDto
                {
                    Nombre = newPaquete.Nombre,
                    Descripcion = newPaquete.Descripcion,
                    CodigoPaquete = newPaquete.CodigoPaquete,
                    Precio = newPaquete.Precio,
                    Enabled = newPaquete.Enabled
                };
            }
            foreach (var item in request.Detalle)
            {
                var requestDetail = new PaqueteDetalleRequest
                {
                    CodigoPauete = paquete.Id,
                    CodigoServicio = item.CodigoServicio,
                    User = request.User
                };
                var newDetail = _paqueteDomainService.CreateDetail(requestDetail);
                _context.PaquetesDetalle.Add(newDetail);
            }

            _context.SaveChanges();
            var nuevopaquete = _context.Paquetes.Include(c => c.Detalle).FirstOrDefault(s => s.Nombre == request.Nombre && s.Enabled == true);
            return new PaqueteDto
            {
                Nombre = nuevopaquete.Nombre,
                Descripcion = nuevopaquete.Descripcion,
                CodigoPaquete = nuevopaquete.CodigoPaquete,
                Enabled = nuevopaquete.Enabled,
                Detalle = nuevopaquete.Detalle == null ? new List<PaqueteDetalleDto>()
                        : nuevopaquete.Detalle
                            .Select(t => new PaqueteDetalleDto
                            {
                                Id = t.Id,
                                CodigoPauete = t.CodigoPauete,
                                CodigoServicio = t.CodigoServicio
                            })
            };
        }

        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var puesto = _context.Paquetes.FirstOrDefault(s => s.Id == id);
            if (puesto == null) throw new Exception("Error al intentar obtener servicio");
            _context.Paquetes.Remove(puesto);
            _context.SaveChanges();

            return string.Empty;
        }

        public PaquetePagedDto GetPaged(PagedGeneralRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;

            if (string.IsNullOrEmpty(request.Value))
            {
                var count = Convert.ToDecimal(_context.Paquetes.Where(s => s.Enabled == true).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Paquetes.Where(s => s.Enabled == true).Include(c => c.Detalle)
                .OrderByDescending(s => s.TransactionDate)
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new PaquetePagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Items = lista.Select(s => new PaqueteDto
                    {
                        Id = s.Id,
                        Nombre = s.Nombre,
                        Descripcion = s.Descripcion,
                        CodigoPaquete = s.CodigoPaquete,
                        Precio = s.Precio,
                        Enabled = s.Enabled,
                        Detalle = s.Detalle == null ? new List<PaqueteDetalleDto>() 
                        : s.Detalle
                            .Select(t => new PaqueteDetalleDto
                                {
                                    Id = t.Id, 
                                    CodigoPauete = t.CodigoPauete,
                                    CodigoServicio = t.CodigoServicio,
                                    Servicio = ObtenerServicio(t.CodigoServicio)
                                    
                                })
                    }).ToList()
                };
            }
            else
            {

                var count = Convert.ToDecimal(_context.Paquetes.Where(s => s.Nombre.Contains(request.Value) && s.Enabled == true).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Paquetes.Where(s => s.Nombre.Contains(request.Value)).Include(c => c.Detalle)
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new PaquetePagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    Items = lista.Select(s => new PaqueteDto
                    {
                        Id = s.Id,
                        Nombre = s.Nombre,
                        Descripcion = s.Descripcion,
                        CodigoPaquete = s.CodigoPaquete,
                        Precio = s.Precio,
                        Enabled = s.Enabled,
                        Detalle = s.Detalle == null ? new List<PaqueteDetalleDto>()
                        : s.Detalle
                            .Select(t => new PaqueteDetalleDto
                            {
                                Id = t.Id,
                                CodigoPauete = t.CodigoPauete,
                                CodigoServicio = t.CodigoServicio,
                                Servicio = ObtenerServicio(t.CodigoServicio)
                            })
                    }).ToList()
                };
            }
        }

        private ServicioDto ObtenerServicio(int codigoServicio)
        {
            var servicio = _context.Servicios.FirstOrDefault(s => s.Id == codigoServicio && s.Enabled == true);
            if(servicio == null) return new ServicioDto();

            return new ServicioDto
            {
                Id = servicio.Id,
                Nombre = servicio.Nombre,
                Descripcion = servicio.Descripcion,
                CodigoServicio = servicio.CodigoServicio,
                Enabled = servicio.Enabled
            };
        }

        public PaqueteDto Update(PaqueteRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldRegisterInfo = _context.Paquetes.FirstOrDefault(s => s.Id == request.Id);
            if (oldRegisterInfo == null) return new PaqueteDto { ValidationErrorMessage = "Error al obtener info de los Paquetes" };

            var rolUpdate = _paqueteDomainService.Update(request, oldRegisterInfo);

            _context.Paquetes.Update(oldRegisterInfo);
            _context.SaveChanges();

            return new PaqueteDto
            {
                Id = oldRegisterInfo.Id,
                Nombre = oldRegisterInfo.Nombre,
                Descripcion = oldRegisterInfo.Descripcion,
                CodigoPaquete = oldRegisterInfo.CodigoPaquete,
                Precio = oldRegisterInfo.Precio,
                Enabled = oldRegisterInfo.Enabled
            };
        }

        public PaqueteDto Disabled(PaqueteRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldRegisterInfo = _context.Paquetes.FirstOrDefault(s => s.Id == request.Id);
            if (oldRegisterInfo == null) return new PaqueteDto { ValidationErrorMessage = "Error al obtener info de puesto" };

            var userUpdate = _paqueteDomainService.Disabled(request, oldRegisterInfo);

            _context.Paquetes.Update(oldRegisterInfo);
            _context.SaveChanges();

            return new PaqueteDto
            {
                Id = oldRegisterInfo.Id,
                Nombre = oldRegisterInfo.Nombre,
                Descripcion = oldRegisterInfo.Descripcion,
                CodigoPaquete = oldRegisterInfo.CodigoPaquete,
                Precio = oldRegisterInfo.Precio,
                Enabled = oldRegisterInfo.Enabled
            };
        }

        public IEnumerable<PaqueteDto> GetAllPaquetes()
        {
            var roles = _context.Paquetes.Where(s => s.Enabled == true).Include(s => s.Detalle).ToList();

            return roles.Select(t => new PaqueteDto
            {
                Id = t.Id,
                Nombre = t.Nombre,
                Descripcion = t.Descripcion
            });
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_paqueteDomainService != null) _paqueteDomainService.Dispose();
        }

        public PaqueteDetalleDto CreateDetail(PaqueteDetalleRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.CodigoPauete <= 0) throw new ArgumentException(nameof(request));
            var newDetallePaquete = _paqueteDomainService.CreateDetail(request);

            _context.PaquetesDetalle.Add(newDetallePaquete);
            _context.SaveChanges();

            var nuevoPaqueteDetalle = _context.PaquetesDetalle.OrderByDescending(t => t.TransactionDate).
              FirstOrDefault(s => s.Enabled == true 
                && s.CodigoPauete == newDetallePaquete.CodigoPauete && s.CodigoServicio == newDetallePaquete.CodigoServicio);
                

            return new PaqueteDetalleDto
            {
                Id = nuevoPaqueteDetalle == null ? 0 : nuevoPaqueteDetalle.Id,
                CodigoPauete = newDetallePaquete.CodigoPauete,
                CodigoServicio = newDetallePaquete.CodigoServicio,
                Enabled = newDetallePaquete.Enabled
            };
        }

        public PaqueteDetalleDto UpdateDetail(PaqueteDetalleRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldRegisterInfo = _context.PaquetesDetalle.FirstOrDefault(s => s.Id == request.Id);
            if (oldRegisterInfo == null) return new PaqueteDetalleDto { ValidationErrorMessage = "Error al obtener info de los Paquetes" };

            var detailUpdate = _paqueteDomainService.UpdateDetail(request, oldRegisterInfo);

            _context.PaquetesDetalle.Update(oldRegisterInfo);
            _context.SaveChanges();

            return new PaqueteDetalleDto
            {
                Id = oldRegisterInfo.Id,
                CodigoPauete = oldRegisterInfo.CodigoPauete,
                CodigoServicio = oldRegisterInfo.CodigoServicio,
                Enabled = oldRegisterInfo.Enabled
            };
        }

        public PaqueteDetalleDto DisabledDetail(PaqueteDetalleRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldRegisterInfo = _context.PaquetesDetalle.FirstOrDefault(s => s.Id == request.Id);
            if (oldRegisterInfo == null) return new PaqueteDetalleDto { ValidationErrorMessage = "Error al obtener info" };

            var paqueteUpdate = _paqueteDomainService.DisabledDetail(request, oldRegisterInfo);

            _context.PaquetesDetalle.Update(oldRegisterInfo);
            _context.SaveChanges();

            return new PaqueteDetalleDto
            {
                Id = oldRegisterInfo.Id,
                CodigoPauete = oldRegisterInfo.CodigoPauete,
                CodigoServicio = oldRegisterInfo.CodigoServicio,
                Enabled = oldRegisterInfo.Enabled
            };
        }

        public string DeleteDetail(int id)
        {
            if(id <= 0) throw new ArgumentException(nameof(id));

            var puesto = _context.PaquetesDetalle.FirstOrDefault(s => s.Id == id);
            if (puesto == null) throw new Exception("Error al intentar obtener paquete");
            _context.PaquetesDetalle.Remove(puesto);
            _context.SaveChanges();

            return string.Empty;
        }
    }
}