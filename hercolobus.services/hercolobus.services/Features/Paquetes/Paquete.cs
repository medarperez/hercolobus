using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using hercolobus.services.Core;
using hercolobus.services.Features.Contratos;

namespace hercolobus.services.Features.Paquetes
{
    [Table("Paquete")]
    public class Paquete : Entity
    {
        public string CodigoPaquete { get; private set; }
        public string Nombre { get; private set; }
        public string Descripcion { get; private set; }
        public decimal Precio { get; private set; }
        public ICollection<PaqueteDetalle> Detalle { get; set; }
        public ICollection<Contrato> Contratos { get; set; }

        public void Update(
           string _name, string _description, string _codigoPaquete, decimal _precio, string _user)
        {
            Nombre = _name;
            Descripcion = _description;
            CodigoPaquete = _codigoPaquete;
            Precio = _precio;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "Modified";
            ModifiedBy = _user ?? "Application";
            TransactionUId = Guid.NewGuid();
            Enabled = true;
        }

        public void Disbaled(string _user)
        {
            CrudOperation = "Disabled";
            TransactionDate = DateTime.Now;
            TransactionType = "DisabledRegister";
            ModifiedBy = _user ?? "Application";
            TransactionUId = Guid.NewGuid();
            Enabled = false;
        }

        public class Builder
        {
            private readonly Paquete _paquete = new Paquete();

            public Builder ConCodigo(string codigo)
            {
                _paquete.CodigoPaquete = codigo;
                return this;
            }

            public Builder ConNombre(string name)
            {
                _paquete.Nombre = name;
                return this;
            }

            public Builder ConDescripcion(string description)
            {
                _paquete.Descripcion = description;
                return this;
            }

            public Builder ConPrecio(decimal precio)
            {
                _paquete.Precio = precio;
                return this;
            }

            public Builder ConAuditFields(string user)
            {
                _paquete.CrudOperation = "Added";
                _paquete.TransactionDate = DateTime.Now;
                _paquete.TransactionType = "New";
                _paquete.ModifiedBy = string.IsNullOrEmpty(user) ? "Aplication" : user;
                _paquete.TransactionUId = Guid.NewGuid();
                _paquete.Enabled = true;

                return this;
            }

            public Paquete Build()
            {
                return _paquete;
            }
        }

    }
}