using hercolobus.services.Core;

namespace hercolobus.services.Features.Paquetes
{
    public class PruebaRequest : RequestBase
    {
        public string CodigoPaquete { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public decimal Precio { get; set; }
    }
}