using System;

namespace hercolobus.services.Features.Paquetes
{
    public interface IPaqueteDomainService : IDisposable
    {
        Paquete Create(PaqueteRequest request);
        Paquete Update(PaqueteRequest request, Paquete _oldRegister);
        Paquete Disabled(PaqueteRequest request, Paquete _oldRegister);

        PaqueteDetalle CreateDetail(PaqueteDetalleRequest request);
        PaqueteDetalle UpdateDetail(PaqueteDetalleRequest request, PaqueteDetalle _oldRegister);
        PaqueteDetalle DisabledDetail(PaqueteDetalleRequest request, PaqueteDetalle _oldRegister);
    }
}