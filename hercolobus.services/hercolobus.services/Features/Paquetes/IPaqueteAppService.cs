using System;
using System.Collections.Generic;
using hercolobus.services.Core;

namespace hercolobus.services.Features.Paquetes
{
    public interface IPaqueteAppService : IDisposable
    {
        PaquetePagedDto GetPaged(PagedGeneralRequest request);
        PaqueteDto Create(PaqueteRequest request);
        PaqueteDto Update(PaqueteRequest request);
        PaqueteDto Disabled(PaqueteRequest request);
        IEnumerable<PaqueteDto> GetAllPaquetes();
        PaqueteDetalleDto CreateDetail(PaqueteDetalleRequest request);
        PaqueteDetalleDto UpdateDetail(PaqueteDetalleRequest request);
        PaqueteDetalleDto DisabledDetail(PaqueteDetalleRequest request);
        string DeleteDetail(int id); 
    }
}