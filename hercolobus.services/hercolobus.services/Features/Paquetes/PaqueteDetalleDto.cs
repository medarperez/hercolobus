using hercolobus.services.Core;
using hercolobus.services.Features.Servicios;

namespace hercolobus.services.Features.Paquetes
{
    public class PaqueteDetalleDto : ResponseBase
    {
        public int CodigoPauete { get;  set; }
        public int CodigoServicio { get;  set; }

        public string Paquete { get; set; }
        public string NombreServicio { get; set; }

        public ServicioDto Servicio { get; set; }
    }
}