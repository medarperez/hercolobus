using System;
using System.ComponentModel.DataAnnotations.Schema;
using hercolobus.services.Core;
using hercolobus.services.Features.Servicios;

namespace hercolobus.services.Features.Paquetes
{
    [Table("PaqueteDetalle")]
    public class PaqueteDetalle : Entity
    {
        public int CodigoPauete { get; private set; }
        public int CodigoServicio { get; private set; }

        public Servicio Servicio { get; set; }
        public Paquete Paquete { get; set; }

        public void Update(
        int _codigoServicio, string _user)
        {
            CodigoServicio = _codigoServicio;  
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "Modified";
            ModifiedBy = _user ?? "Application";
            TransactionUId = Guid.NewGuid();
            Enabled = true;
        }

        public void Disbaled(string _user)
        {
            CrudOperation = "Disabled";
            TransactionDate = DateTime.Now;
            TransactionType = "DisabledRegister";
            ModifiedBy = _user ?? "Application";
            TransactionUId = Guid.NewGuid();
            Enabled = false;
        }

        public class Builder
        {
            private readonly PaqueteDetalle _paqueteDetalle = new PaqueteDetalle();

            public Builder ConCodigoPaquete(int codigoPaquete)
            {
                _paqueteDetalle.CodigoPauete = codigoPaquete;
                return this;
            }

            public Builder ConCodigoServicio(int codigoServicio)
            {
                _paqueteDetalle.CodigoServicio = codigoServicio;
                return this;
            }

            public Builder ConAuditFields(string user)
            {
                _paqueteDetalle.CrudOperation = "Added";
                _paqueteDetalle.TransactionDate = DateTime.Now;
                _paqueteDetalle.TransactionType = "New";
                _paqueteDetalle.ModifiedBy = string.IsNullOrEmpty(user) ? "Aplication" : user;
                _paqueteDetalle.TransactionUId = Guid.NewGuid();
                _paqueteDetalle.Enabled = true;

                return this;
            }

            public PaqueteDetalle Build()
            {
                return _paqueteDetalle;
            }
        }
    }
}