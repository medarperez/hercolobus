using hercolobus.services.Core;

namespace hercolobus.services.Features.Paquetes
{
    public class PaqueteDetalleRequest : RequestBase
    {
        public int CodigoPauete { get; set; }
        public int CodigoServicio { get; set; }
    }
}