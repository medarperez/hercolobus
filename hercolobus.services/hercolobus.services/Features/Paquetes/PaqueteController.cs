using System;
using System.Collections.Generic;
using hercolobus.services.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace hercolobus.services.Features.Paquetes
{
    [Route("api/[controller]")]
    [ApiController]
    // [Authorize]
    public class PaqueteController : ControllerBase
    {
        private readonly IPaqueteAppService _paqueteAppService;
        public PaqueteController(IPaqueteAppService paqueteAppService)
        {
            if (paqueteAppService == null) throw new ArgumentException(nameof(paqueteAppService));

            _paqueteAppService = paqueteAppService;
        }


        [HttpGet]
        [Authorize]
        public ActionResult<PaquetePagedDto> Get()
        {
            return Ok(_paqueteAppService.GetAllPaquetes());
        }

        [HttpGet]
        [Route("paged")]
        [Authorize]
        public ActionResult<PaquetePagedDto> GetPaged([FromQuery]PagedGeneralRequest request)
        {
            return Ok(_paqueteAppService.GetPaged(request));
        }

        [HttpPost]
        [Authorize]
        public ActionResult<PaqueteDto> Post([FromBody]PaqueteRequest request)
        {    
            return Ok(_paqueteAppService.Create(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<PaqueteDto> Put([FromBody] PaqueteRequest request)
        {
            return Ok(_paqueteAppService.Update(request));
        }

        // [HttpDelete]
        // [Authorize]
        // public ActionResult<string> Delete(int Id)
        // {
        //     return Ok(_paqueteAppService.Delete(Id));
        // }

        [HttpPut]
        [Route("disabled")]
        public ActionResult<PaqueteDto> Disabled([FromBody] PaqueteRequest request)
        {
            return Ok(_paqueteAppService.Disabled(request));
        } 
    }
}