using System.Collections.Generic;
using hercolobus.services.Core;

namespace hercolobus.services.Features.Paquetes
{
    public class PaquetePagedDto : ResponseBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<PaqueteDto> Items { get; set; }
    }
}