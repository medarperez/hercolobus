using hercolobus.services.Core;

namespace hercolobus.services.Features.Usuarios
{
    public class UsuarioRequest : RequestBase
    {
        public string UsuarioId { get;  set; }
        public string Nombre { get;  set; }
        public string Correo { get;  set; }
        public int PuestoId { get;  set; }
        public int RolId { get;  set; }
        public string Password { get; set; }
    }
}