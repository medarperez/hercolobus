using System;
using System.Collections.Generic;
using System.Linq;
using hercolobus.services.Context;
using hercolobus.services.Core;
using hercolobus.services.Features.Puestos;
using hercolobus.services.Features.Roles;
using Microsoft.EntityFrameworkCore;

namespace hercolobus.services.Features.Usuarios
{
    public class UsuarioAppService : IUsuarioAppService
    {
        private HercolobusContext _context;
        private readonly IUsuarioDomainService _usuarioDomainService;

        public UsuarioAppService(HercolobusContext context,
                                   IUsuarioDomainService usuarioDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (usuarioDomainService == null) throw new ArgumentException(nameof(usuarioDomainService));

            _context = context;
            _usuarioDomainService = usuarioDomainService;
        }
        public UsuarioDto CreateUser(UsuarioRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newUsuario = _usuarioDomainService.Create(request);

            _context.Usuarios.Add(newUsuario);
            _context.SaveChanges();

            var usuario = _context.Usuarios.FirstOrDefault(s => s.UsuarioId == request.UsuarioId);
            var newPassword = _usuarioDomainService.CreatePassword(request, request.Password, usuario.Id);
            _context.Passwords.Add(newPassword);
            _context.SaveChanges();

            return new UsuarioDto
            {
                Nombre = newUsuario.Nombre,
                Correo = newUsuario.Correo,
                PuestoId = newUsuario.PuestoId,
                RolId = newUsuario.RolId,
                Enabled = newUsuario.Enabled
            };
        }

        public string DeteleUser(string userId)
        {
            if (string.IsNullOrEmpty(userId)) throw new ArgumentException(nameof(userId));

            var user = _context.Usuarios.FirstOrDefault(s => s.UsuarioId == userId);
            if (user == null) throw new Exception("User not exists");
            _context.Usuarios.Remove(user);
            _context.SaveChanges();

            return string.Empty;
        }

        public UsuarioDto Disabled(UsuarioRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldUsuarioInfo = _context.Usuarios.FirstOrDefault(s => s.Id == request.Id);
            if (oldUsuarioInfo == null) return new UsuarioDto { ValidationErrorMessage = "Error al obtener info de usuario" };

            var userUpdate = _usuarioDomainService.Disabled(request, oldUsuarioInfo);

            _context.Usuarios.Update(oldUsuarioInfo);
            _context.SaveChanges();

            return new UsuarioDto
            {
                Id = oldUsuarioInfo.Id,
                Nombre = oldUsuarioInfo.Nombre,
                Correo = oldUsuarioInfo.Correo,
                PuestoId = oldUsuarioInfo.PuestoId,
                RolId = oldUsuarioInfo.RolId,
                Enabled = oldUsuarioInfo.Enabled
            };
        }
        public UsuarioPagedDto GetPagedUser(PagedGeneralRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;

            if (string.IsNullOrEmpty(request.Value))
            {
                var count = Convert.ToDecimal(_context.Usuarios.Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Usuarios
                    .OrderByDescending(s => s.TransactionDate)
                    .Skip(request.PageSize * (request.PageIndex - 1))
                    .Take(request.PageSize)
                    .Include(c => c.Puesto)
                    .Include(c => c.Rol)
                    .ToList();
                return new UsuarioPagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Items = lista.Select(s => new UsuarioDto
                    {
                        Id = s.Id,
                        UsuarioId = s.UsuarioId,
                        Nombre = s.Nombre,
                        Correo = s.Correo,
                        PuestoId = s.PuestoId,
                        RolId = s.RolId,
                        Enabled = s.Enabled,
                        Puesto = s.Puesto == null ? new PuestoDto() :  new PuestoDto 
                        {
                            Id = s.Puesto.Id,
                            Nombre = s.Puesto.Nombre,
                            Descripcion = s.Puesto.Descripcion
                        },
                        NombrePuesto = s.Puesto == null ? "N/A": s.Puesto.Nombre ?? "N/A",
                        Rol = s.Rol == null ? new RolDto() : new RolDto
                        {
                            Id = s.Rol.Id,
                            Name = s.Rol.Name,
                            Description = s.Rol.Description
                        },
                        NombreRol = s.Rol == null ? "N/A" : s.Rol.Name ?? "N/A",
                        
                    }).ToList()
                };
            }
            else
            {

                var count = Convert.ToDecimal(_context.Usuarios.Where(s => s.UsuarioId.Contains(request.Value)).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Usuarios.Where(s => s.UsuarioId.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize)
                .Include(c => c.Puesto)
                    .Include(c => c.Rol).ToList();
                return new UsuarioPagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    Items = lista.Select(s => new UsuarioDto
                    {
                        Id = s.Id,
                        UsuarioId = s.UsuarioId,
                        Nombre = s.Nombre,
                        Correo = s.Correo,
                        PuestoId = s.PuestoId,
                        RolId = s.RolId,
                        Enabled = s.Enabled,
                        Puesto = s.Puesto == null ? new PuestoDto() : new PuestoDto
                        {
                            Id = s.Puesto.Id,
                            Nombre = s.Puesto.Nombre,
                            Descripcion = s.Puesto.Descripcion
                        },
                        NombrePuesto = s.Puesto == null ? "N/A" : s.Puesto.Nombre ?? "N/A",
                        Rol = s.Rol == null ? new RolDto() : new RolDto
                        {
                            Id = s.Rol.Id,
                            Name = s.Rol.Name,
                            Description = s.Rol.Description
                        },
                        NombreRol = s.Rol == null ? "N/A" : s.Rol.Name ?? "N/A",
                    }).ToList()
                };
            }
        }

        public UsuarioDto UpdateUser(UsuarioRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldUsuarioInfo = _context.Usuarios.FirstOrDefault(s => s.Id == request.Id);
            if (oldUsuarioInfo == null) return new UsuarioDto { ValidationErrorMessage = "Error al obtener info de usuario" };

            var userUpdate = _usuarioDomainService.Update(request, oldUsuarioInfo);

            _context.Usuarios.Update(oldUsuarioInfo);
            _context.SaveChanges();

            return new UsuarioDto
            {
                Id = oldUsuarioInfo.Id,
                Nombre = oldUsuarioInfo.Nombre,
                Correo = oldUsuarioInfo.Correo,
                PuestoId = oldUsuarioInfo.PuestoId,
                RolId = oldUsuarioInfo.RolId,
                Enabled = oldUsuarioInfo.Enabled
            };
        }

        public IEnumerable<Usuario> ObtenerUsuarioPorId(string id)
        {
            return _context.Usuarios.Where(s => s.UsuarioId.Contains(id)).Include(c => c.Puesto)
                    .Include(c => c.Rol).Include(c => c.Passwords);
        }

        public Usuario ObtenerUsuarioInfo(int id)
        {
            return _context.Usuarios.FirstOrDefault(s => s.Id == id && s.Enabled == true);
        }

        public string ChangeUserPassword(PasswordResetRequest request)
        {
            EncriptorHelper encryp = new EncriptorHelper();
            if (request == null) throw new ArgumentNullException(nameof(request));
            if (string.IsNullOrEmpty(request.Password)) throw new ArgumentNullException(nameof(request.Password));
            if (string.IsNullOrEmpty(request.NewPassword)) throw new ArgumentNullException(nameof(request.NewPassword));
            if (string.IsNullOrEmpty(request.User)) throw new ArgumentNullException(nameof(request.User));

            var user = _context.Usuarios.Include(c => c.Passwords).FirstOrDefault(s => s.UsuarioId == request.User);
            var passwords = user.Passwords;
            if(passwords == null) return "Usuario no encontrado";
            if (!passwords.Any()) return "Usuario no encontrado";
            var password = passwords.FirstOrDefault(s => s.Enabled == true);
            if (password == null) return "Usuario no encontrado";  
            if (user.UsuarioId == user.UsuarioId && encryp.VerifiedPassword(request.Password, password.PasswordHash))
            {

                var DisabledPassword = _usuarioDomainService.ChangePassword(user.UsuarioId, password);
                _context.Passwords.Update(DisabledPassword);

                var newPassword = _usuarioDomainService.CreatePassword(request, request.NewPassword, user.Id);
                _context.Passwords.Add(newPassword);
                _context.SaveChanges();
                return string.Empty;
            }
            else
            {
                return "Usuario no encontrado";
            }
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_usuarioDomainService != null) _usuarioDomainService.Dispose();
        }

    }
}