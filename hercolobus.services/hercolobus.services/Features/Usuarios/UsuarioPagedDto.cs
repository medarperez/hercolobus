using System.Collections.Generic;
using hercolobus.services.Core;

namespace hercolobus.services.Features.Usuarios
{
    public class UsuarioPagedDto : ResponseBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<UsuarioDto> Items { get; set; }
    }
}