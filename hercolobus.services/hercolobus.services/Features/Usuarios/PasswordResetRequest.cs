using hercolobus.services.Core;

namespace hercolobus.services.Features.Usuarios
{
    public class PasswordResetRequest : RequestBase
    {
        public string Password { get; set; }
        public string NewPassword { get; set; }
    }
}