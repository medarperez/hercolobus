using System;
using hercolobus.services.Features.Passwords;

namespace hercolobus.services.Features.Usuarios
{
    public interface IUsuarioDomainService : IDisposable
    {
        Usuario Create(UsuarioRequest request);
        Usuario Update(UsuarioRequest request, Usuario _oldRegister);
        Usuario Disabled(UsuarioRequest request, Usuario _oldRegister);
        Usuario DisablePassword(Usuario request, string user);
        Password CreatePassword(UsuarioRequest request, string password, int usuarioId);

        Password CreatePassword(PasswordResetRequest request, string password, int usuarioId);

        Password ChangePassword(string usuarioId, Password passwordInfoOld);
    }
}