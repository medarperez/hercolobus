using hercolobus.services.Core;
using hercolobus.services.Features.Puestos;
using hercolobus.services.Features.Roles;

namespace hercolobus.services.Features.Usuarios
{
    public class UsuarioDto : ResponseBase
    {
        public string UsuarioId { get; set; }
        public string Nombre { get; set; }
        public string Correo { get; set; }
        public int PuestoId { get; set; }
        public int RolId { get; set; }

        public string NombrePuesto { get; set; }
        public string NombreRol { get; set; }
        public PuestoDto Puesto { get; set; }
        public RolDto Rol { get; set; }
    }
}