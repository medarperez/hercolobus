using System;
using System.ComponentModel.DataAnnotations.Schema;
using hercolobus.services.Core;
using hercolobus.services.Features.Servicios;

namespace hercolobus.services.Features.Facturas
{
    [Table("FacturaDetalle")]
    public class FacturaDetalle : Entity
    {
      public int FacturaId { get; private set; }
      public int CodigoServicio { get; private set; }
      public decimal Precio { get; private set; }

      public Factura Factura { get; set; }   

        public void Disbaled(string _user)
        {
            CrudOperation = "Disabled";
            TransactionDate = DateTime.Now;
            TransactionType = "DisabledRegister";
            ModifiedBy = _user ?? "Application";
            TransactionUId = Guid.NewGuid();
            Enabled = false;
        }

        public class Builder
        {
            private readonly FacturaDetalle _facturaDetalle = new FacturaDetalle();

            public Builder ConCodigo(int codigo)
            {
                _facturaDetalle.FacturaId = codigo;
                return this;
            }

            public Builder ConCodigoServicio(int codigoServicio)
            {
                _facturaDetalle.CodigoServicio = codigoServicio;
                return this;
            }

            public Builder ConPrecio(decimal precio)
            {
                _facturaDetalle.Precio = precio;
                return this;
            }

            public Builder ConAuditFields(string user)
            {
                _facturaDetalle.CrudOperation = "Added";
                _facturaDetalle.TransactionDate = DateTime.Now;
                _facturaDetalle.TransactionType = "New";
                _facturaDetalle.ModifiedBy = string.IsNullOrEmpty(user) ? "Aplication" : user;
                _facturaDetalle.TransactionUId = Guid.NewGuid();
                _facturaDetalle.Enabled = true;

                return this;
            }

            public FacturaDetalle Build()
            {
                return _facturaDetalle;
            }
        }
    }
}