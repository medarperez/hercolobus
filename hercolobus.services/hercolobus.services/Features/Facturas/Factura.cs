using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using hercolobus.services.Core;
using hercolobus.services.Features.Cobros;
using hercolobus.services.Features.Contratos;

namespace hercolobus.services.Features.Facturas
{
    [Table("Factura")]
    public class Factura : Entity
    {
        public string Estado { get; private set; }
        public DateTime FechaEmision { get; private set; }
        public string MesFacturado { get; private set; }
        public DateTime Vencimiento { get; private set; }
        public int ContratoId { get; private set; }
        public Contrato Contrato { get; set; }
        public ICollection<Cobro> Cobros { get; set; }
        public ICollection<FacturaDetalle> Detalle { get; set; }

        public void Disbaled(string _user)
        {
            CrudOperation = "Disabled";
            TransactionDate = DateTime.Now;
            TransactionType = "DisabledRegister";
            ModifiedBy = _user ?? "Application";
            TransactionUId = Guid.NewGuid();
            Enabled = false;
            Estado = "ANULADA";
        }

        public class Builder
        {
            private readonly Factura _factura = new Factura();

            public Builder ConEstado(string estado)
            {
                _factura.Estado = estado;
                return this;
            }
            public Builder ConMesFacturado(string mesFacturado)
            {
                _factura.Estado = mesFacturado;
                return this;
            }

            public Builder ConFechaEmision(DateTime fechaEmision)
            {
                _factura.FechaEmision = fechaEmision;
                return this;
            }
            public Builder ConFechaVencimiento(DateTime fechaVencimiento)
            {
                _factura.Vencimiento = fechaVencimiento;
                return this;
            }

            public Builder ConContrato(int contrato)
            {
                _factura.ContratoId = contrato;
                return this;
            }

            public Builder ConDetalle(IEnumerable<FacturaDetalle> detalle)
            {
                _factura.Detalle = detalle.ToList();
                return this;
            }

            public Builder ConAuditFields(string user)
            {
                _factura.CrudOperation = "Added";
                _factura.TransactionDate = DateTime.Now;
                _factura.TransactionType = "New";
                _factura.ModifiedBy = string.IsNullOrEmpty(user) ? "Aplication" : user;
                _factura.TransactionUId = Guid.NewGuid();
                _factura.Enabled = true;
                _factura.Estado = "PENDIENTE";

                return this;
            }

            public Factura Build()
            {
                return _factura;
            }
        }

    }
}