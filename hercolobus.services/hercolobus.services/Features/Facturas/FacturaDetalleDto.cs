using System;
using hercolobus.services.Core;

namespace hercolobus.services.Features.Facturas
{
    public class FacturaDetalleDto : ResponseBase
    {
        public int FacturaId { get;  set; }
        public string MesFacturado { get;  set; }
        public DateTime FechaInicio { get;  set; }
        public DateTime FechaFinal { get;  set; }
    }
}