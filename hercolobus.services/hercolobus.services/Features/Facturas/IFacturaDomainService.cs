using System;

namespace hercolobus.services.Features.Facturas
{
    public interface IFacturaDomainService : IDisposable
    {
        Factura Create(FacturaRequest request);
        Factura Disabled(FacturaRequest request, Factura _oldRegister);

        FacturaDetalle CreateDetail(FacturaDetalleRequest request);
        FacturaDetalle DisabledDetail(FacturaDetalleRequest request, FacturaDetalle _oldRegister); 
    }
}