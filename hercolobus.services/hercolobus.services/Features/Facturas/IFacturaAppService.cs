using System;
using System.Collections.Generic;

namespace hercolobus.services.Features.Facturas
{
    public interface IFacturaAppService : IDisposable
    {
       IEnumerable<FacturaDto> GenerarFacturasPendientes(); 
    }
}