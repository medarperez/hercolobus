using System;
using hercolobus.services.Core;

namespace hercolobus.services.Features.Facturas
{
    public class FacturaDetalleRequest : RequestBase
    {
        public int FacturaId { get; set; }
        public int ServicioId { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFinal { get; set; }
        public int PaqueteId { get; set; }
    }
}