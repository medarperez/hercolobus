using System;
using System.Collections.Generic;
using hercolobus.services.Core;

namespace hercolobus.services.Features.Facturas
{
    public class FacturaRequest : RequestBase
    {
        public string Estado { get; set; }
        public DateTime FechaEmision { get; set; }
        public DateTime FechaVencimiento { get; set; }
        public int ContratoId { get; set; }
        public string MesFacturado { get; set; }
        public IEnumerable<FacturaDetalle> FacturaDetalle { get; set; }
        public IEnumerable<FacturaDetalleRequest> Detalle { get; set; }
    }
}