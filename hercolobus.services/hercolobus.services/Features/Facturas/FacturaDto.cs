using System;
using System.Collections.Generic;
using hercolobus.services.Core;
using hercolobus.services.Features.Contratos;

namespace hercolobus.services.Features.Facturas
{
    public class FacturaDto : ResponseBase
    {
        public string Estado { get;  set; }
        public DateTime FechaEmision { get;  set; }
        public int ContratoId { get;  set; }
        public string MesFacturado { get; set; }
        public ContratoDto Contrato { get; set; }
        public ICollection<FacturaDetalleDto> Detalle { get; set; }
        public Decimal Total { get; set; }
    }
}