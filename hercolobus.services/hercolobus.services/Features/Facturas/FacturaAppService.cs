using System;
using System.Collections.Generic;
using hercolobus.services.Context;

namespace hercolobus.services.Features.Facturas
{
    public class FacturaAppService : IFacturaAppService
    {
        private HercolobusContext _context;
        private readonly IFacturaDomainService _facturaDomainService;
        public FacturaAppService(HercolobusContext context, IFacturaDomainService facturaDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (facturaDomainService == null) throw new ArgumentException(nameof(facturaDomainService));

            _context = context;
            _facturaDomainService = facturaDomainService; 
        }
        public IEnumerable<FacturaDto> GenerarFacturasPendientes()
        {
            throw new System.NotImplementedException();
        }

        public void Dispose()
        {
        }
    }
}