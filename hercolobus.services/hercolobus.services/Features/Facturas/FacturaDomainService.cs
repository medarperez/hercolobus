using System;
using System.Collections.Generic;
using System.Linq;

namespace hercolobus.services.Features.Facturas
{
     public class FacturaDomainService : IFacturaDomainService
    {
        public Factura Create(FacturaRequest request)
        {
            if (request.ContratoId == 0) throw new ArgumentNullException(nameof(request.ContratoId));

            Factura zona = new Factura.Builder()
            .ConContrato(request.ContratoId)
            .ConMesFacturado(request.MesFacturado)
            .ConFechaEmision(request.FechaEmision)
            .ConFechaVencimiento(request.FechaVencimiento)
            .ConEstado(request.Estado)
            .ConDetalle(request.FacturaDetalle)
            .ConAuditFields(request.User)
            .Build();

            return zona;
        }

        public Factura Disabled(FacturaRequest request, Factura _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Disbaled(request.User);

            return _oldRegister;
        }

        public FacturaDetalle CreateDetail(FacturaDetalleRequest request)
        {
            if (request.FacturaId <= 0) throw new ArgumentNullException(nameof(request.FacturaId));

            FacturaDetalle zona = new FacturaDetalle.Builder()
            .ConCodigo(request.FacturaId)
            .ConCodigoServicio(request.ServicioId)
            .ConAuditFields(request.User)
            .Build();

            return zona;
        }

        public FacturaDetalle DisabledDetail(FacturaDetalleRequest request, FacturaDetalle _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Disbaled(request.User);

            return _oldRegister;
        }

        public void Dispose()
        {
        }
    }
}