using System;
using System.ComponentModel.DataAnnotations.Schema;
using hercolobus.services.Core;
using hercolobus.services.Features.Facturas;

namespace hercolobus.services.Features.Cobros
{
    [Table("Cobros")]
    public class Cobro : Entity
    {
        public int FacturaId { get; private set; }
        public decimal PagoTotal { get; private set; }
        public string Comentario { get; private set; }
        public Factura Factura { get; set; }

        public void Disbaled(string _user)
        {
            CrudOperation = "Disabled";
            TransactionDate = DateTime.Now;
            TransactionType = "DisabledRegister";
            ModifiedBy = _user ?? "Application";
            TransactionUId = Guid.NewGuid();
            Enabled = false;
        }

        public class Builder
        {
            private readonly Cobro _cobro = new Cobro();

            public Builder ConFacturaId(int facturaId)
            {
                _cobro.FacturaId = facturaId;
                return this;
            }
            public Builder ConComentario(string comentario)
            {
                _cobro.Comentario = comentario;
                return this;
            }
            public Builder ConPago(decimal pagoTotal)
            {
                _cobro.PagoTotal = pagoTotal;
                return this;
            }
            public Builder ConAuditFields(string user)
            {
                _cobro.CrudOperation = "Added";
                _cobro.TransactionDate = DateTime.Now;
                _cobro.TransactionType = "New";
                _cobro.ModifiedBy = string.IsNullOrEmpty(user) ? "Aplication" : user;
                _cobro.TransactionUId = Guid.NewGuid();
                _cobro.Enabled = true;

                return this;
            }

            public Cobro Build()
            {
                return _cobro;
            }
        }

    }
}