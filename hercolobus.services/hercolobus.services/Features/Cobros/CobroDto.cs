using hercolobus.services.Core;

namespace hercolobus.services.Features.Cobros
{
    public class CobroDto : ResponseBase
    {
        public int FacturaId { get; set; }
        public decimal PagoTotal { get; set; }
        public string Comentario { get; set; }
    }
}