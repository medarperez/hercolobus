using System;
using System.Collections.Generic;

namespace hercolobus.services.Features.Cobros
{
    public interface ICobroAppService : IDisposable
    {
        CobroDto Create(CobroRequest request);
        CobroDto Disabled(CobroRequest request);
        IEnumerable<CobroDto> GetAllServicios(CobroRequest request);
    }
}