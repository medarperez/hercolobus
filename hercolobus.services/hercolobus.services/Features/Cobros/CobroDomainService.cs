using System;

namespace hercolobus.services.Features.Cobros
{
    public class CobroDomainService : ICobroDomainService
    {
        public Cobro Create(CobroRequest request)
        {
            if (request.FacturaId == 0) throw new ArgumentNullException(nameof(request.FacturaId));

            Cobro zona = new Cobro.Builder()
            .ConFacturaId(request.FacturaId)
            .ConComentario(request.Comentario)
            .ConPago(request.PagoTotal)
            .ConAuditFields(request.User)
            .Build();

            return zona;
        }

        public Cobro Disabled(CobroRequest request, Cobro _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Disbaled(request.User);

            return _oldRegister;
        }

        public void Dispose()
        {
        }
    }
}