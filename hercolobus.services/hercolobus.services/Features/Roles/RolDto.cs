using hercolobus.services.Core;

namespace hercolobus.services.Features.Roles
{
    public class RolDto : ResponseBase
    {
        public string Name { get;  set; }
        public string Description { get; set; }
    }
}