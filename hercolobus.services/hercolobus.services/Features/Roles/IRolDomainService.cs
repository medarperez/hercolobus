using System;

namespace hercolobus.services.Features.Roles
{
    public interface IRolDomainService : IDisposable
    {
        Rol Create(RolRequest request);
        Rol Update(RolRequest request, Rol _oldRegister);
        Rol Disabled(RolRequest request, Rol _oldRegister);
    }
}