using System;
using System.Collections.Generic;
using hercolobus.services.Core;

namespace hercolobus.services.Features.Roles
{
    public interface IRolAppService : IDisposable
    {
        RolPagedDto GetPaged(PagedGeneralRequest request);
        RolDto Create(RolRequest request);
        RolDto Update(RolRequest request);
        RolDto Disabled(RolRequest request);
        IEnumerable<RolDto> GetAllRoles();
        string Delete(int id);
    }
}