using System;

namespace hercolobus.services.Features.Roles
{
    public class RolDomainService : IRolDomainService
    {
        public Rol Create(RolRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Name)) throw new ArgumentNullException(nameof(request.Name));

            Rol zona = new Rol.Builder()
            .WithName(request.Name)
            .WithDescription(request.Description)
            .WithAuditFields(request.User)
            .Build();

            return zona;
        }

        public Rol Update(RolRequest request, Rol _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(request.Name, request.Description, request.User);

            return _oldRegister;
        }

        public Rol Disabled(RolRequest request, Rol _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Disbaled(request.User);

            return _oldRegister;
        }

        public void Dispose()
        {
        }
    }
}