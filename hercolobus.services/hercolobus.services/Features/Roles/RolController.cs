using System;
using System.Collections.Generic;
using hercolobus.services.Core;
using Microsoft.AspNetCore.Mvc;

namespace hercolobus.services.Features.Roles
{
    [Route("api/[controller]")]
    [ApiController]
    // [Authorize]
    public class RolController : ControllerBase
    {   
        private readonly IRolAppService _rolAppService;
        public RolController(IRolAppService rolAppService)
        {
            if (rolAppService == null) throw new ArgumentException(nameof(rolAppService));

            _rolAppService = rolAppService;
        }
        [HttpGet]
        [Route("paged")]
        //[Authorize]
        public ActionResult<RolPagedDto> GetPaged([FromQuery]PagedGeneralRequest request)
        {
            return Ok(_rolAppService.GetPaged(request));
        }

        [HttpGet]
        // [Route("paged")]
        //[Authorize]
        public ActionResult<IEnumerable<RolDto>> Get()
        {
            return Ok(_rolAppService.GetAllRoles());
        }

        [HttpPost]
        //[Authorize]
        public ActionResult<RolDto> Post([FromBody]RolRequest request)
        {
            return Ok(_rolAppService.Create(request));
        }

        [HttpPut]
        //[Authorize]
        public ActionResult<RolDto> Put([FromBody] RolRequest request)
        {
            return Ok(_rolAppService.Update(request));
        }

        [HttpDelete]
        //[Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_rolAppService.Delete(Id));
        }

        [HttpPut]
        [Route("disabled")]
        public ActionResult<RolDto> Disabled([FromBody] RolRequest request)
        {
            return Ok(_rolAppService.Disabled(request));
        } 
    }
}