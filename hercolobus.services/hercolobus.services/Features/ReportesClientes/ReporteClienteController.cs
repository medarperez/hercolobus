using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace hercolobus.services.Features.ReportesClientes
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ReporteClienteController : ControllerBase
    {
        private readonly IReporteClientesAppService _reporteClientesAppService;
        public ReporteClienteController(IReporteClientesAppService reporteClientesAppService)
        {
            _reporteClientesAppService = reporteClientesAppService ?? throw new ArgumentException(nameof(reporteClientesAppService));
        }

        [HttpGet]
        [Authorize]
        public ActionResult<ReporteClienteDTO> Get([FromQuery]ReporteClienteRequest request)
        {
            return Ok(_reporteClientesAppService.ObtenerReporte(request));
        }
    }
}