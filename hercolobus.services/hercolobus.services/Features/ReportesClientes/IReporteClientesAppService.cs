using System;
using System.Collections.Generic;

namespace hercolobus.services.Features.ReportesClientes
{
    public interface IReporteClientesAppService : IDisposable
    {
        List<ReporteClienteDTO> ObtenerReporte(ReporteClienteRequest request);
    }
}