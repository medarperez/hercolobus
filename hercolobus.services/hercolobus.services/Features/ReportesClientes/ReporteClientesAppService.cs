using System;
using System.Collections.Generic;
using System.Linq;
using hercolobus.services.Context;
using Microsoft.EntityFrameworkCore;

namespace hercolobus.services.Features.ReportesClientes
{
    public class ReporteClientesAppService : IReporteClientesAppService
    {
        private HercolobusContext _context;
        public ReporteClientesAppService(HercolobusContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public List<ReporteClienteDTO> ObtenerReporte(ReporteClienteRequest request)
        {
           if(request == null)
           {
               return GenerarReporteCompleto();
           }
           if(request.CodigioCliente > 0)
           {
               return GenerarRreportePorCodigoCliente(request.CodigioCliente);
           }

            return GenerarReporteCompleto();
        }

        private List<ReporteClienteDTO> GenerarRreportePorCodigoCliente(int codigioCliente)
        {
            var clientes = _context.Clientes.Where(t => t.Id == codigioCliente)
                                .Include(c => c.Contratos)
                                        .ThenInclude(x => x.Paquete);

            return (from item in clientes.ToList()
                    select new ReporteClienteDTO
                    {
                        ContratoCodigo = "CONTR-0" + item.Contratos.FirstOrDefault().Id,
                        CodigoPaquete = "PAQ-0" + item.Contratos.FirstOrDefault().PaqueteId,
                        Paquete = item.Contratos.FirstOrDefault() == null ?  "" : item.Contratos.FirstOrDefault().Paquete.Nombre,
                        CodigoCliente = "CL-0" + item.Id,
                        Cliente = (string.IsNullOrEmpty(item.Nombre) ? string.Empty : item.Nombre)
                                    + (string.IsNullOrEmpty(item.Apellido) ? string.Empty : item.Apellido),
                        Identidad = item.Identidad,
                        Telefono = item.Telefono,
                        Celular = item.Celular,
                        Correo = item.Correo,
                        InicioContrato = item.Contratos.FirstOrDefault() == null ? DateTime.Now : item.Contratos.FirstOrDefault().FechaInicio,
                        EstadoConexion = item.Contratos.FirstOrDefault() == null ? "N/A" :  item.Contratos.FirstOrDefault().Enabled ? "Activo" : "Inactivo",
                        DescripcionPaquete = item.Contratos.FirstOrDefault() == null ?  "N/A" : item.Contratos.FirstOrDefault().Paquete.Descripcion,
                        Precio = item.Contratos.FirstOrDefault() == null ?  0 : item.Contratos.FirstOrDefault().Paquete.Precio,
                    }).ToList();
        }

        private List<ReporteClienteDTO> GenerarReporteCompleto()
        {
            var clientes = _context.Clientes
                                .Include(c => c.Contratos)
                                        .ThenInclude(x => x.Paquete);

            return (from item in clientes.ToList()
                    select new ReporteClienteDTO
                    {
                        ContratoCodigo = item.Contratos.FirstOrDefault() == null ? "" : "CONTR-0" + item.Contratos.FirstOrDefault().Id,
                        CodigoPaquete = item.Contratos.FirstOrDefault() == null ? "" : "PAQ-0" + item.Contratos.FirstOrDefault().PaqueteId,
                        Paquete = item.Contratos.FirstOrDefault() == null ? "" : item.Contratos.FirstOrDefault().Paquete.Nombre,
                        CodigoCliente = "CL-0" + item.Id,
                        Cliente = (string.IsNullOrEmpty(item.Nombre) ? string.Empty : item.Nombre)
                                    + (string.IsNullOrEmpty(item.Apellido) ? string.Empty : item.Apellido),
                        Identidad = item.Identidad,
                        Telefono = item.Telefono,
                        Celular = item.Celular,
                        Correo = item.Correo,
                        InicioContrato = item.Contratos.FirstOrDefault() == null ? DateTime.Now : item.Contratos.FirstOrDefault().FechaInicio,
                        EstadoConexion = item.Contratos.FirstOrDefault() == null ? "" : item.Contratos.FirstOrDefault().Enabled ? "Activo" : "Inactivo",
                        DescripcionPaquete = item.Contratos.FirstOrDefault() == null ? "" : item.Contratos.FirstOrDefault().Paquete.Descripcion,
                        Precio = item.Contratos.FirstOrDefault() == null ? 0 : item.Contratos.FirstOrDefault().Paquete.Precio,
                    }).ToList();
        }

        public void Dispose()
        {
        }
    }
}