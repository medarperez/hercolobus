using hercolobus.services.Core;

namespace hercolobus.services.Features.ReportesClientes
{
    public class ReporteClienteRequest : RequestBase
    {
        public string NombreCliente { get; set; }
        public int CodigioCliente { get; set; }
        public string Departamento { get; set; }
    }
}