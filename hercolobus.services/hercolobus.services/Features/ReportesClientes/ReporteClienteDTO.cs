using System;
using hercolobus.services.Core;

namespace hercolobus.services.Features.ReportesClientes
{
    public class ReporteClienteDTO : ResponseBase
    {
        public string ContratoCodigo { get; set; }
        public string CodigoPaquete { get; set; }
        public string Paquete { get; set; }
        public string CodigoCliente { get; set; }
        public string Cliente { get; set; }
        public string Identidad { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string Correo { get; set; }
        public DateTime InicioContrato { get; set; }
        public string EstadoConexion { get; set; }
        public string DescripcionPaquete { get; set; }
        public Decimal Precio { get; set; }
    }
}