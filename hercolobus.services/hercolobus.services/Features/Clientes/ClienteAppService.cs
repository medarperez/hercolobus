using System;
using System.Collections.Generic;
using System.Linq;
using hercolobus.services.Context;
using hercolobus.services.Core;

namespace hercolobus.services.Features.Clientes
{
    public class ClienteAppService : IClienteAppService
    {
        private HercolobusContext _context;
        private readonly IClienteDomainService _clienteDomainService;

        public ClienteAppService(HercolobusContext context, IClienteDomainService clienteDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (clienteDomainService == null) throw new ArgumentException(nameof(clienteDomainService));

            _context = context;
            _clienteDomainService = clienteDomainService;
        }
        public ClienteDto Create(ClienteRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newCliente = _clienteDomainService.Create(request);

            _context.Clientes.Add(newCliente);
            _context.SaveChanges();

            return new ClienteDto
            {
                Nombre = newCliente.Nombre,
                Apellido = newCliente.Apellido,
                Identidad = newCliente.Identidad,
                Direccion = newCliente.Direccion,
                Telefono = newCliente.Telefono,
                Celular = newCliente.Celular,
                Correo = newCliente.Correo,
                Enabled = newCliente.Enabled
            };
        }

        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var Cliente = _context.Clientes.FirstOrDefault(s => s.Id == id);
            if (Cliente == null) throw new Exception("Error al intentar obtener Cliente");
            _context.Clientes.Remove(Cliente);
            _context.SaveChanges();

            return string.Empty;
        }

        public ClientePagedDto GetPaged(PagedGeneralRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;

            if (string.IsNullOrEmpty(request.Value))
            {
                var count = Convert.ToDecimal(_context.Clientes.Where(s => s.Enabled == true).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Clientes.Where(s => s.Enabled == true)
                .OrderByDescending(s => s.TransactionDate)
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new ClientePagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Items = lista.Select(s => new ClienteDto
                    {
                        Id = s.Id,
                        Nombre = s.Nombre,
                        Apellido = s.Apellido,
                        Identidad = s.Identidad,
                        Direccion = s.Direccion,
                        Telefono = s.Telefono,
                        Celular = s.Celular,
                        Correo = s.Correo,
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
            else
            {

                var count = Convert.ToDecimal(_context.Clientes.Where(s => s.Nombre.Contains(request.Value) && s.Enabled == true).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Clientes.Where(s => s.Nombre.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new ClientePagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    Items = lista.Select(s => new ClienteDto
                    {
                        Id = s.Id,
                        Nombre = s.Nombre,
                        Apellido = s.Apellido,
                        Identidad = s.Identidad,
                        Direccion = s.Direccion,
                        Telefono = s.Telefono,
                        Celular = s.Celular,
                        Correo = s.Correo,
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
        }

        public ClienteDto Update(ClienteRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldClienteInfo = _context.Clientes.FirstOrDefault(s => s.Id == request.Id);
            if (oldClienteInfo == null) return new ClienteDto { ValidationErrorMessage = "Error al obtener info de los Clientes" };

            var rolUpdate = _clienteDomainService.Update(request, oldClienteInfo);

            _context.Clientes.Update(oldClienteInfo);
            _context.SaveChanges();

            return new ClienteDto
            {
                Id = oldClienteInfo.Id,
                Nombre = oldClienteInfo.Nombre,
                Apellido = oldClienteInfo.Apellido,
                Identidad = oldClienteInfo.Identidad,
                Direccion = oldClienteInfo.Direccion,
                Telefono = oldClienteInfo.Telefono,
                Celular = oldClienteInfo.Celular,
                Correo = oldClienteInfo.Correo,
                Enabled = oldClienteInfo.Enabled
            };
        }

        public ClienteDto Disabled(ClienteRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldUsuarioInfo = _context.Clientes.FirstOrDefault(s => s.Id == request.Id);
            if (oldUsuarioInfo == null) return new ClienteDto { ValidationErrorMessage = "Error al obtener info de Cliente" };

            var userUpdate = _clienteDomainService.Disabled(request, oldUsuarioInfo);

            _context.Clientes.Update(oldUsuarioInfo);
            _context.SaveChanges();

            return new ClienteDto
            {
                Id = oldUsuarioInfo.Id,
                Nombre = oldUsuarioInfo.Nombre,
                Apellido = oldUsuarioInfo.Apellido,
                Identidad = oldUsuarioInfo.Identidad,
                Direccion = oldUsuarioInfo.Direccion,
                Telefono = oldUsuarioInfo.Telefono,
                Celular = oldUsuarioInfo.Celular,
                Correo = oldUsuarioInfo.Correo,
                Enabled = oldUsuarioInfo.Enabled
            };
        }

        public IEnumerable<ClienteDto> GetAllClientes()
        {
            var roles = _context.Clientes.Where(s => s.Enabled == true).ToList();

            return roles.Select(t => new ClienteDto
            {
                Id = t.Id,
                Nombre = t.Nombre,
                Apellido = t.Apellido,
                Identidad = t.Identidad,
                Direccion = t.Direccion,
                Telefono = t.Telefono,
                Celular = t.Celular,
                Correo = t.Correo,
                Enabled = t.Enabled
            });
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_clienteDomainService != null) _clienteDomainService.Dispose();
        } 
    }
}