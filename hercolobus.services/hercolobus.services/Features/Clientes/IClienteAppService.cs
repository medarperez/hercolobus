using System;
using System.Collections.Generic;
using hercolobus.services.Core;

namespace hercolobus.services.Features.Clientes
{
    public interface IClienteAppService : IDisposable
    {
        ClientePagedDto GetPaged(PagedGeneralRequest request);
        ClienteDto Create(ClienteRequest request);
        ClienteDto Update(ClienteRequest request);
        ClienteDto Disabled(ClienteRequest request);
        IEnumerable<ClienteDto> GetAllClientes();
        string Delete(int id);
    }
}