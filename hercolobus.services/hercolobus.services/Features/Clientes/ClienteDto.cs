using hercolobus.services.Core;

namespace hercolobus.services.Features.Clientes
{
    public class ClienteDto : ResponseBase
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Identidad { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string Correo { get; set; }
    }
}