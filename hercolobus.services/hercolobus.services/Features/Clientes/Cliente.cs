using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using hercolobus.services.Core;
using hercolobus.services.Features.Contratos;

namespace hercolobus.services.Features.Clientes
{
    [Table("Clientes")]
    public class Cliente : Entity
    {
        public string Nombre { get; private set; }
        public string Apellido { get; private set; }
        public string Identidad { get; private set; }
        public string Direccion { get; private set; }
        public string Telefono { get; private set; }
        public string Celular { get; private set; }
        public string Correo { get; private set; }

        public ICollection<Contrato> Contratos { get; set; }

        public void Update(
           string _name, string _apellido, string _identidad, string _direccion, 
           string _telefono, string _celular, string _correo, string _user)
        {
            Nombre = _name;
            Apellido = _apellido;
            Identidad = _identidad;
            Direccion = _direccion;
            Telefono = _telefono;
            Celular =_celular;
            Correo = _correo;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModifiedClient";
            ModifiedBy = _user ?? "Application";
            TransactionUId = Guid.NewGuid();
            Enabled = true;
        }

        public void Disbaled(string _user)
        {
            CrudOperation = "Disabled";
            TransactionDate = DateTime.Now;
            TransactionType = "DisabledRegister";
            ModifiedBy = _user ?? "Application";
            TransactionUId = Guid.NewGuid();
            Enabled = false;
        }

        public class Builder
        {
            private readonly Cliente _cliente = new Cliente();

            public Builder ConNombre(string name)
            {
                _cliente.Nombre = name;
                return this;
            }

            public Builder ConApellido(string apellido)
            {
                _cliente.Apellido = apellido;
                return this;
            }

            public Builder ConIdentidad(string identidad)
            {
                _cliente.Identidad = identidad;
                return this;
            }

            public Builder ConTelefono(string telefono)
            {
                _cliente.Telefono = telefono;
                return this;
            }

            public Builder ConDireccion(string direccion)
            {
                _cliente.Direccion = direccion;
                return this;
            }

            public Builder ConCelular(string celular)
            {
                _cliente.Celular = celular;
                return this;
            }

            public Builder ConCorreo(string correo)
            {
                _cliente.Correo = correo;
                return this;
            }
            public Builder ConAuditFields(string user)
            {
                _cliente.CrudOperation = "Added";
                _cliente.TransactionDate = DateTime.Now;
                _cliente.TransactionType = "NewProject";
                _cliente.ModifiedBy = string.IsNullOrEmpty(user) ? "Aplication" : user;
                _cliente.TransactionUId = Guid.NewGuid();
                _cliente.Enabled = true;

                return this;
            }
            public Cliente Build()
            {
                return _cliente;
            }
        }
    }
}