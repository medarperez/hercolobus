using System;

namespace hercolobus.services.Features.Clientes
{
    public class ClienteDomainService : IClienteDomainService
    {
        public Cliente Create(ClienteRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Nombre)) throw new ArgumentNullException(nameof(request.Nombre));

            Cliente zona = new Cliente.Builder()
            .ConNombre(request.Nombre)
            .ConApellido(request.Apellido)
            .ConIdentidad(request.Identidad)
            .ConDireccion(request.Direccion)
            .ConCorreo(request.Correo)
            .ConTelefono(request.Telefono)
            .ConCelular(request.Celular)
            .ConAuditFields(request.User)
            .Build();

            return zona;
        }

        public Cliente Update(ClienteRequest request, Cliente _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(request.Nombre, request.Apellido, request.Identidad, 
            request.Direccion, request.Telefono, request.Celular, request.Correo, request.User);

            return _oldRegister;
        }

        public Cliente Disabled(ClienteRequest request, Cliente _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Disbaled(request.User);

            return _oldRegister;
        }

        public void Dispose()
        {
        }
    }
}