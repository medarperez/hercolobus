using System;

namespace hercolobus.services.Features.Clientes
{
    public interface IClienteDomainService : IDisposable
    {
        Cliente Create(ClienteRequest request);
        Cliente Update(ClienteRequest request, Cliente _oldRegister);
        Cliente Disabled(ClienteRequest request, Cliente _oldRegister);
    }
}