using System.Collections.Generic;
using hercolobus.services.Core;

namespace hercolobus.services.Features.Clientes
{
    public class ClientePagedDto : ResponseBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<ClienteDto> Items { get; set; }
    }
}