using System;
using hercolobus.services.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace hercolobus.services.Features.Clientes
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ClienteController : ControllerBase
    {
        private readonly IClienteAppService _clienteAppService;
        public ClienteController(IClienteAppService clienteAppService)
        {
            if (clienteAppService == null) throw new ArgumentException(nameof(clienteAppService));

            _clienteAppService = clienteAppService;
        }

        [HttpGet]
        [Route("paged")]
        [Authorize]
        public ActionResult<ClientePagedDto> GetPaged([FromQuery]PagedGeneralRequest request)
        {
            return Ok(_clienteAppService.GetPaged(request));
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ClienteDto> Post([FromBody]ClienteRequest request)
        {
            return Ok(_clienteAppService.Create(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<ClienteDto> Put([FromBody] ClienteRequest request)
        {
            return Ok(_clienteAppService.Update(request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_clienteAppService.Delete(Id));
        }

        [HttpPut]
        [Route("disabled")]
        [Authorize]
        public ActionResult<ClienteDto> Disabled([FromBody] ClienteRequest request)
        {
            return Ok(_clienteAppService.Disabled(request));
        }  
    }
}