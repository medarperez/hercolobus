using System;
using System.Collections.Generic;
using System.Linq;
using hercolobus.services.Context;
using hercolobus.services.Core;
using hercolobus.services.Features.Clientes;
using hercolobus.services.Features.Paquetes;
using hercolobus.services.Features.Servicios;
using Microsoft.EntityFrameworkCore;

namespace hercolobus.services.Features.Contratos
{
    public class ContratoAppService : IContratoAppService
    {
        private readonly HercolobusContext _context;
        private readonly IContratoDomainService _contratoDomainService;

        public ContratoAppService(HercolobusContext context, IContratoDomainService contratoDomainService)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _contratoDomainService = contratoDomainService ?? throw new ArgumentException(nameof(contratoDomainService));
        }
        public ContratoDto Create(ContratoRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newRegister = _contratoDomainService.Create(request);

            _context.Contratos.Add(newRegister);      
            _context.SaveChanges();

            return new ContratoDto
            {
                ClienteId = newRegister.ClienteId,
                PaqueteId = newRegister.PaqueteId,
                FechaInicio = newRegister.FechaInicio,
                FechaFinal = newRegister.FechaFinal,
                FechaInicioCobro = newRegister.FechaInicioCobro,
                DiaMaximoParaPago = newRegister.DiaMaximoParaPago,
                EsContratoExentoCobro = newRegister.EsContratoExentoCobro,
                Enabled = newRegister.Enabled,
                CostoAlternativo = newRegister.CostoAlternativo
            };
        }

        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var puesto = _context.Contratos.FirstOrDefault(s => s.Id == id);
            if (puesto == null) throw new Exception("Error al intentar obtener puesto");
            _context.Contratos.Remove(puesto);
            _context.SaveChanges();

            return string.Empty;
        }

        public ContratoPagedDto GetPaged(PagedGeneralRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;

            if (string.IsNullOrEmpty(request.Value))
            {
                var count = Convert.ToDecimal(_context.Contratos.Where(s => s.Enabled == true).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Contratos.Where(s => s.Enabled == true).Include(i => i.Cliente).Include(p => p.Paquete)
                .OrderByDescending(s => s.TransactionDate)
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new ContratoPagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Items = lista.Select(s => new ContratoDto
                    {
                        Id = s.Id,
                        ClienteId = s.ClienteId,
                        PaqueteId = s.PaqueteId,
                        FechaInicio = s.FechaInicio,
                        FechaFinal = s.FechaFinal,
                        FechaInicioCobro = s.FechaInicioCobro,
                        DiaMaximoParaPago = s.DiaMaximoParaPago,
                        EsContratoExentoCobro = s.EsContratoExentoCobro,
                        NombreCliente = ObtenerNombreCliente(s.Cliente),
                        DireccionDeConexion = s.DireccionDeConexion,
                        CostoAlternativo = s.CostoAlternativo,
                        Cliente = s.Cliente == null ? new ClienteDto() : new ClienteDto 
                        {
                            Id = s.Cliente.Id,
                            Nombre = s.Cliente.Nombre,
                            Apellido = s.Cliente.Apellido,
                            Identidad = s.Cliente.Identidad,
                            Direccion = s.Cliente.Direccion,
                            Telefono = s.Cliente.Telefono,
                            Celular = s.Cliente.Celular,
                            Correo = s.Cliente.Correo
                        },
                        Paquete = s.Paquete ==  null ? new PaqueteDto() : new PaqueteDto 
                        {
                            Id = s.Paquete.Id,
                            Nombre = s.Paquete.Nombre,
                            Descripcion = s.Paquete.Descripcion,
                            Enabled = s.Paquete.Enabled,
                            CodigoPaquete = s.Paquete.CodigoPaquete,
                            Precio =  s.Paquete.Precio,
                            Detalle = s.Paquete.Detalle == null ? new List<PaqueteDetalleDto>() 
                                : !s.Paquete.Detalle.Any() ? new List<PaqueteDetalleDto>() 
                                    : s.Paquete.Detalle.Select(d => new PaqueteDetalleDto
                                        {
                                             Id = d.Id,
                                             CodigoPauete = d.CodigoPauete,
                                             CodigoServicio = d.CodigoServicio,
                                             Enabled = d.Enabled,
                                             Servicio = d.Servicio == null ? new ServicioDto() 
                                                : new ServicioDto
                                                    {
                                                        Id = d.Servicio.Id,
                                                        Nombre = d.Servicio.Nombre,
                                                        Descripcion = d.Servicio.Descripcion,
                                                        Enabled = d.Servicio.Enabled,
                                                        CodigoServicio = d.Servicio.CodigoServicio
                                                    }
                                        })
                        }, 
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
            else
            {
                // return new ContratoPagedDto();
                var clientes = _context.Clientes.Where(s => s.Nombre.Contains(request.Value)).Select(s => s.Id);
                var count = Convert.ToDecimal(_context.Contratos.Where(s => clientes.Contains(s.Id) && s.Enabled == true).Count());
                var lista = _context.Contratos.Where(s => clientes.Contains(s.Id) && s.Enabled == true).Include(i => i.Cliente).Include(p => p.Paquete)
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;   
                return new ContratoPagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Items = lista.Select(s => new ContratoDto
                    {
                        Id = s.Id,
                        ClienteId = s.ClienteId,
                        PaqueteId = s.PaqueteId,
                        FechaInicio = s.FechaInicio,
                        FechaFinal = s.FechaFinal,
                        FechaInicioCobro = s.FechaInicioCobro,
                        DiaMaximoParaPago = s.DiaMaximoParaPago,
                        EsContratoExentoCobro = s.EsContratoExentoCobro,
                        NombreCliente = ObtenerNombreCliente(s.Cliente),
                        DireccionDeConexion = s.DireccionDeConexion,
                        CostoAlternativo = s.CostoAlternativo,
                        Cliente = s.Cliente == null ? new ClienteDto() : new ClienteDto
                        {
                            Id = s.Cliente.Id,
                            Nombre = s.Cliente.Nombre,
                            Apellido = s.Cliente.Apellido,
                            Identidad = s.Cliente.Identidad,
                            Direccion = s.Cliente.Direccion,
                            Telefono = s.Cliente.Telefono,
                            Celular = s.Cliente.Celular,
                            Correo = s.Cliente.Correo
                        },
                        Paquete = s.Paquete == null ? new PaqueteDto() : new PaqueteDto
                        {
                            Id = s.Paquete.Id,
                            Nombre = s.Paquete.Nombre,
                            Descripcion = s.Paquete.Descripcion,
                            Enabled = s.Paquete.Enabled,
                            CodigoPaquete = s.Paquete.CodigoPaquete,
                            Precio = s.Paquete.Precio,
                            Detalle = s.Paquete.Detalle == null ? new List<PaqueteDetalleDto>()
                                : !s.Paquete.Detalle.Any() ? new List<PaqueteDetalleDto>()
                                    : s.Paquete.Detalle.Select(d => new PaqueteDetalleDto
                                    {
                                        Id = d.Id,
                                        CodigoPauete = d.CodigoPauete,
                                        CodigoServicio = d.CodigoServicio,
                                        Enabled = d.Enabled,
                                        Servicio = d.Servicio == null ? new ServicioDto()
                                                : new ServicioDto
                                                {
                                                    Id = d.Servicio.Id,
                                                    Nombre = d.Servicio.Nombre,
                                                    Descripcion = d.Servicio.Descripcion,
                                                    Enabled = d.Servicio.Enabled,
                                                    CodigoServicio = d.Servicio.CodigoServicio
                                                }
                                    })
                        },
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
        }

        private string ObtenerNombreCliente(Cliente cliente)
        {
            return cliente == null ? "No disponible" 
                : string.IsNullOrEmpty(cliente.Nombre) 
                    ? "No disponible" : string.IsNullOrEmpty(cliente.Apellido) 
                        ? cliente.Nombre 
                            : (cliente.Nombre + ' ' + cliente.Apellido);
        }


        private string ObtenerNombrePaquete(Paquete paquete)
        {
            return paquete == null ? "No disponible"
                : string.IsNullOrEmpty(paquete.Nombre)
                    ? "No disponible" :  paquete.Nombre;
        }
        public ContratoDto Update(ContratoRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldpuestoInfo = _context.Contratos.FirstOrDefault(s => s.Id == request.Id);
            if (oldpuestoInfo == null) return new ContratoDto { ValidationErrorMessage = "Error al obtener info de los Contratos" };

            var rolUpdate = _contratoDomainService.Update(request, oldpuestoInfo);

            _context.Contratos.Update(oldpuestoInfo);
            _context.SaveChanges();

            return new ContratoDto
            {
                Id = oldpuestoInfo.Id,
                ClienteId = oldpuestoInfo.ClienteId,
                PaqueteId = oldpuestoInfo.PaqueteId,
                FechaInicio = oldpuestoInfo.FechaInicio,
                FechaFinal = oldpuestoInfo.FechaFinal,
                FechaInicioCobro = oldpuestoInfo.FechaInicioCobro,
                DiaMaximoParaPago = oldpuestoInfo.DiaMaximoParaPago,
                EsContratoExentoCobro = oldpuestoInfo.EsContratoExentoCobro,
            };
        }

        public ContratoDto Disabled(ContratoRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldRegisterInfo = _context.Contratos.FirstOrDefault(s => s.Id == request.Id);
            if (oldRegisterInfo == null) return new ContratoDto { ValidationErrorMessage = "Error al obtener info de puesto" };

            var userUpdate = _contratoDomainService.Disabled(request, oldRegisterInfo);

            _context.Contratos.Update(oldRegisterInfo);
            _context.SaveChanges();

            return new ContratoDto
            {
                Id = oldRegisterInfo.Id,
                ClienteId = oldRegisterInfo.ClienteId,
                PaqueteId = oldRegisterInfo.PaqueteId,
                FechaInicio = oldRegisterInfo.FechaInicio,
                FechaFinal = oldRegisterInfo.FechaFinal,
                FechaInicioCobro = oldRegisterInfo.FechaInicioCobro,
                DiaMaximoParaPago = oldRegisterInfo.DiaMaximoParaPago,
                EsContratoExentoCobro = oldRegisterInfo.EsContratoExentoCobro,
            };
        }

        public IEnumerable<ContratoDto> GetAllContratos()
        {
            var roles = _context.Contratos.Where(s => s.Enabled == true)
            .Include(c => c.Cliente)
            .Include(p => p.Paquete).ToList();

            return roles.Select(s => new ContratoDto
            {
                Id = s.Id,
                ClienteId = s.ClienteId,
                PaqueteId = s.PaqueteId,
                FechaInicio = s.FechaInicio,
                FechaFinal = s.FechaFinal,
                FechaInicioCobro = s.FechaInicioCobro,
                DiaMaximoParaPago = s.DiaMaximoParaPago,
                EsContratoExentoCobro = s.EsContratoExentoCobro,
                Cliente = s.Cliente == null ? new ClienteDto() : new ClienteDto
                {
                    Id = s.Cliente.Id,
                    Nombre = s.Cliente.Nombre,
                    Apellido = s.Cliente.Apellido,
                    Identidad = s.Cliente.Identidad,
                    Direccion = s.Cliente.Direccion,
                    Telefono = s.Cliente.Telefono,
                    Celular = s.Cliente.Celular,
                    Correo = s.Cliente.Correo
                },
                Paquete = s.Paquete == null ? new PaqueteDto() : new PaqueteDto
                {
                    Id = s.Paquete.Id,
                    Nombre = s.Paquete.Nombre,
                    Descripcion = s.Paquete.Descripcion,
                    Enabled = s.Paquete.Enabled,
                    CodigoPaquete = s.Paquete.CodigoPaquete,
                    Precio = s.Paquete.Precio,
                    Detalle = s.Paquete.Detalle == null ? new List<PaqueteDetalleDto>()
                                : !s.Paquete.Detalle.Any() ? new List<PaqueteDetalleDto>()
                                    : s.Paquete.Detalle.Select(d => new PaqueteDetalleDto
                                    {
                                        Id = d.Id,
                                        CodigoPauete = d.CodigoPauete,
                                        CodigoServicio = d.CodigoServicio,
                                        Enabled = d.Enabled,
                                        Servicio = d.Servicio == null ? new ServicioDto()
                                                : new ServicioDto
                                                {
                                                    Id = d.Servicio.Id,
                                                    Nombre = d.Servicio.Nombre,
                                                    Descripcion = d.Servicio.Descripcion,
                                                    Enabled = d.Servicio.Enabled,
                                                    CodigoServicio = d.Servicio.CodigoServicio
                                                }
                                    })
                },
                Enabled = s.Enabled
            });
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_contratoDomainService != null) _contratoDomainService.Dispose();
        }

    }
}