using System;
using hercolobus.services.Core;

namespace hercolobus.services.Features.Contratos
{
    public class ContratoRequest : RequestBase
    {
        public int ClienteId { get;  set; }
        public int PaqueteId { get;  set; }
        public DateTime FechaInicio { get;  set; }
        public DateTime? FechaFinal { get;  set; }
        public DateTime FechaInicioCobro { get;  set; }
        public int DiaMaximoParaPago { get;  set; }
        public bool EsContratoExentoCobro { get;  set; }
        public string DireccionDeConexion { get; set; }
        public decimal CostoAlternativo { get; set; }
        
        
    }
}