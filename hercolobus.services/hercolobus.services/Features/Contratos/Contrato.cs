using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using hercolobus.services.Core;
using hercolobus.services.Features.Clientes;
using hercolobus.services.Features.Facturas;
using hercolobus.services.Features.Paquetes;

namespace hercolobus.services.Features.Contratos {
    [Table ("Contratos")]
    public class Contrato : Entity {
        public int ClienteId { get; private set; }
        public int PaqueteId { get; private set; }
        public DateTime FechaInicio { get; private set; }
        public DateTime? FechaFinal { get; private set; }
        public DateTime FechaInicioCobro { get; private set; }
        public int DiaMaximoParaPago { get; private set; }
        public bool EsContratoExentoCobro { get; private set; }
        public string DireccionDeConexion { get; private set; }
        public decimal CostoAlternativo { get; private set; }

        public Cliente Cliente { get; set; }
        public Paquete Paquete { get; set; }
        public ICollection<Factura> Facturas { get; set; }

        public void Update (int _paqueteId, DateTime _fechaInicio,
            DateTime? _fechafinal, DateTime _fechaInicioCobro, int _diaPago, bool esExento, string _direccionDeConexion, string _user, decimal costoAlternativo) {
            PaqueteId = _paqueteId;
            FechaInicio = _fechaInicio;
            FechaFinal = _fechafinal;
            FechaInicioCobro = _fechaInicioCobro;
            DiaMaximoParaPago = _diaPago;
            EsContratoExentoCobro = esExento;
            DireccionDeConexion = _direccionDeConexion;
            CostoAlternativo = costoAlternativo;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "Modified";
            ModifiedBy = _user ?? "Application";
            TransactionUId = Guid.NewGuid ();
            Enabled = true;
        }

        public void Disbaled (string _user) {
            CrudOperation = "Disabled";
            TransactionDate = DateTime.Now;
            TransactionType = "DisabledRegister";
            ModifiedBy = _user ?? "Application";
            TransactionUId = Guid.NewGuid ();
            Enabled = false;
        }

        public class Builder {
            private readonly Contrato _Contrato = new Contrato ();

            public Builder ConCodigoCliente (int codigo) {
                _Contrato.ClienteId = codigo;
                return this;
            }

            public Builder ConCostoAlternativo (decimal costoAlternativo) {
                _Contrato.CostoAlternativo = costoAlternativo;
                return this;
            }
            public Builder ConCodigoPaquete (int codigo) {
                _Contrato.PaqueteId = codigo;
                return this;
            }

            public Builder ConFechas (DateTime fechaInicio, DateTime? FechaFinal, DateTime fechaInicioPago) {
                _Contrato.FechaInicio = fechaInicio;
                _Contrato.FechaFinal = FechaFinal;
                _Contrato.FechaInicioCobro = fechaInicioPago;
                return this;
            }

            public Builder ConDiaDePago (int dia) {
                _Contrato.DiaMaximoParaPago = dia;
                return this;
            }

            public Builder ConContratoExentoPago (bool esExento) {
                _Contrato.EsContratoExentoCobro = esExento;
                return this;
            }

            public Builder ConDireccionDeConexion (string direccionDeConexion) {
                _Contrato.DireccionDeConexion = direccionDeConexion;
                return this;
            }
            public Builder ConAuditFields (string user) {
                _Contrato.CrudOperation = "Added";
                _Contrato.TransactionDate = DateTime.Now;
                _Contrato.TransactionType = "New";
                _Contrato.ModifiedBy = string.IsNullOrEmpty (user) ? "Aplication" : user;
                _Contrato.TransactionUId = Guid.NewGuid ();
                _Contrato.Enabled = true;

                return this;
            }

            public Contrato Build () {
                return _Contrato;
            }
        }

    }
}