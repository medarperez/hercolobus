using System;
using System.Collections.Generic;
using hercolobus.services.Core;

namespace hercolobus.services.Features.Contratos
{
    public interface IContratoAppService : IDisposable
    {
        ContratoPagedDto GetPaged(PagedGeneralRequest request);
        ContratoDto Create(ContratoRequest request);
        ContratoDto Update(ContratoRequest request);
        ContratoDto Disabled(ContratoRequest request);
        // IEnumerable<ContratoDto> GetAllContratos();
        string Delete(int id);
    }
}