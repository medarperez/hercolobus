using System;
using System.Collections.Generic;
using hercolobus.services.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace hercolobus.services.Features.Contratos
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ContratoController : ControllerBase
    {
        private readonly IContratoAppService _contratoAppService;
        public ContratoController(IContratoAppService contratoAppService)
        {
            if (contratoAppService == null) throw new ArgumentException(nameof(contratoAppService));

            _contratoAppService = contratoAppService;
        }

        // [HttpGet]
        // // [Route("paged")]
        // [Authorize]
        // public ActionResult<IEnumerable<ContratoDto>> Get()
        // {
        //     return Ok(_contratoAppService.GetAllPuestos());
        // }

        [HttpGet]
        [Route("paged")]
        [Authorize]
        public ActionResult<ContratoPagedDto> GetPaged([FromQuery]PagedGeneralRequest request)
        {
            return Ok(_contratoAppService.GetPaged(request));
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ContratoDto> Post([FromBody]ContratoRequest request)
        {
            return Ok(_contratoAppService.Create(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<ContratoDto> Put([FromBody] ContratoRequest request)
        {
            return Ok(_contratoAppService.Update(request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_contratoAppService.Delete(Id));
        }

        [HttpPut]
        [Route("disabled")]
        public ActionResult<ContratoDto> Disabled([FromBody] ContratoRequest request)
        {
            return Ok(_contratoAppService.Disabled(request));
        }
    }
}