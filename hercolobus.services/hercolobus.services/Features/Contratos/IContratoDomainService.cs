using System;

namespace hercolobus.services.Features.Contratos
{
    public interface IContratoDomainService : IDisposable
    {
        Contrato Create(ContratoRequest request);
        Contrato Update(ContratoRequest request, Contrato _oldRegister);
        Contrato Disabled(ContratoRequest request, Contrato _oldRegister); 
    }
}