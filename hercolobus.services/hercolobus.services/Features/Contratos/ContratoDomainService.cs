using System;

namespace hercolobus.services.Features.Contratos
{
    public class ContratoDomainService : IContratoDomainService
    {
        public Contrato Create(ContratoRequest request)
        {
            if (request.ClienteId <= 0) throw new ArgumentNullException(nameof(request.ClienteId));
            if (request.PaqueteId <= 0) throw new ArgumentNullException(nameof(request.PaqueteId));

            Contrato registro = new Contrato.Builder()
            .ConCodigoCliente(request.ClienteId)
            .ConCodigoPaquete(request.PaqueteId)
            .ConContratoExentoPago(request.EsContratoExentoCobro)
            .ConFechas(request.FechaInicio, request.FechaFinal, request.FechaInicioCobro)
            .ConDiaDePago(request.DiaMaximoParaPago)
            .ConDireccionDeConexion(request.DireccionDeConexion)
            .ConCostoAlternativo(request.CostoAlternativo)
            .ConAuditFields(request.User)
            .Build();

            return registro;
        }

        public Contrato Update(ContratoRequest request, Contrato _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(request.PaqueteId, request.FechaInicio, request.FechaFinal, 
                request.FechaInicioCobro, request.DiaMaximoParaPago, request.EsContratoExentoCobro, 
                request.DireccionDeConexion, request.User, request.CostoAlternativo);

            return _oldRegister;
        }

        public Contrato Disabled(ContratoRequest request, Contrato _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Disbaled(request.User);

            return _oldRegister;
        }

        public void Dispose()
        {
        } 
    }
}