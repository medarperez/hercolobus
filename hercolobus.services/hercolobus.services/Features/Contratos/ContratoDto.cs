using System;
using hercolobus.services.Core;
using hercolobus.services.Features.Clientes;
using hercolobus.services.Features.Paquetes;

namespace hercolobus.services.Features.Contratos
{
    public class ContratoDto : ResponseBase
    {
        public int ClienteId { get; set; }
        public int PaqueteId { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime? FechaFinal { get; set; }
        public DateTime FechaInicioCobro { get; set; }
        public int DiaMaximoParaPago { get; set; }
        public bool EsContratoExentoCobro { get; set; }
        public string DireccionDeConexion { get; set; }
        public decimal CostoAlternativo { get; set; }
        
        

        public string NombreCliente { get; set; }
        public string NombrePquete { get; set; }

        public ClienteDto Cliente { get; set; }
        public PaqueteDto Paquete { get; set; }
    }
}