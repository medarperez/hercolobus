using System.Collections.Generic;
using hercolobus.services.Core;

namespace hercolobus.services.Features.Contratos
{
    public class ContratoPagedDto : ResponseBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<ContratoDto> Items { get; set; }
    }
}