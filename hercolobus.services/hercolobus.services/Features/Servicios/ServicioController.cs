using System;
using hercolobus.services.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace hercolobus.services.Features.Servicios
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ServicioController : ControllerBase
    {
        private readonly IServicioAppService _servicioAppService;
        public ServicioController(IServicioAppService servicioAppService)
        {
            if (servicioAppService == null) throw new ArgumentException(nameof(servicioAppService));

            _servicioAppService = servicioAppService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<ServicioPagedDto> Get()
        {
            return Ok(_servicioAppService.GetAllServicios());
        }

        [HttpGet]
        [Route("paged")]
        [Authorize]
        public ActionResult<ServicioPagedDto> GetPaged([FromQuery]PagedGeneralRequest request)
        {
            return Ok(_servicioAppService.GetPaged(request));
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ServicioDto> Post([FromBody]ServicioRequest request)
        {
            return Ok(_servicioAppService.Create(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<ServicioDto> Put([FromBody] ServicioRequest request)
        {
            return Ok(_servicioAppService.Update(request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_servicioAppService.Delete(Id));
        }

        [HttpPut]
        [Route("disabled")]
        public ActionResult<ServicioDto> Disabled([FromBody] ServicioRequest request)
        {
            return Ok(_servicioAppService.Disabled(request));
        }
    }
}