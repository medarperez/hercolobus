using System;
using System.ComponentModel.DataAnnotations.Schema;
using hercolobus.services.Core;

namespace hercolobus.services.Features.Servicios
{
    [Table("Servicios")]
    public class Servicio : Entity
    {
        public string Nombre { get; private set; }
        public string Descripcion { get; private set; }
        public string CodigoServicio { get; private set; }

        public void Update(
           string _name, string _description, string _codigoServicio, string _user)
        {
            Nombre = _name;
            Descripcion = _description;
            CodigoServicio = _codigoServicio;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModifiedServicio";
            ModifiedBy = _user ?? "Application";
            TransactionUId = Guid.NewGuid();
            Enabled = true;
        }

        public void Disbaled(string _user)
        {
            CrudOperation = "Disabled";
            TransactionDate = DateTime.Now;
            TransactionType = "DisabledRegister";
            ModifiedBy = _user ?? "Application";
            TransactionUId = Guid.NewGuid();
            Enabled = false;
        }

        public class Builder
        {
            private readonly Servicio _servicio = new Servicio();

            public Builder ConNombre(string name)
            {
                _servicio.Nombre = name;
                return this;
            }

            public Builder ConDescripcion(string description)
            {
                _servicio.Descripcion = description;
                return this;
            }

            public Builder ConCodigoServicio(string codigoServicio)
            {
                _servicio.CodigoServicio = codigoServicio;
                return this;
            }

            public Builder ConAuditFields(string user)
            {
                _servicio.CrudOperation = "Added";
                _servicio.TransactionDate = DateTime.Now;
                _servicio.TransactionType = "NewServicio";
                _servicio.ModifiedBy = string.IsNullOrEmpty(user) ? "Aplication" : user;
                _servicio.TransactionUId = Guid.NewGuid();
                _servicio.Enabled = true;

                return this;
            }

            public Servicio Build()
            {
                return _servicio;
            }
        }
    }
}