using System;
using System.Collections.Generic;
using System.Linq;
using hercolobus.services.Context;
using hercolobus.services.Core;

namespace hercolobus.services.Features.Servicios
{
    public class ServicioAppService : IServicioAppService
    {
        private HercolobusContext _context;
        private readonly IServicioDomainService _servicioDomainService;

        public ServicioAppService(HercolobusContext context, IServicioDomainService servicioDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (servicioDomainService == null) throw new ArgumentException(nameof(servicioDomainService));

            _context = context;
            _servicioDomainService = servicioDomainService;
        }
        public ServicioDto Create(ServicioRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newpuesto = _servicioDomainService.Create(request);

            _context.Servicios.Add(newpuesto);
            _context.SaveChanges();

            return new ServicioDto
            {
                Nombre = newpuesto.Nombre,
                Descripcion = newpuesto.Descripcion,
                Enabled = newpuesto.Enabled
            };
        }

        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var puesto = _context.Servicios.FirstOrDefault(s => s.Id == id);
            if (puesto == null) throw new Exception("Error al intentar obtener servicio");
            _context.Servicios.Remove(puesto);
            _context.SaveChanges();

            return string.Empty;
        }

        public ServicioPagedDto GetPaged(PagedGeneralRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;

            if (string.IsNullOrEmpty(request.Value))
            {
                var count = Convert.ToDecimal(_context.Servicios.Where(s => s.Enabled == true).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Servicios.Where(s => s.Enabled == true)
                .OrderByDescending(s => s.TransactionDate)
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new ServicioPagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Items = lista.Select(s => new ServicioDto
                    {
                        Id = s.Id,
                        Nombre = s.Nombre,
                        Descripcion = s.Descripcion,
                        CodigoServicio = s.CodigoServicio,
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
            else
            {

                var count = Convert.ToDecimal(_context.Servicios.Where(s => s.Nombre.Contains(request.Value) && s.Enabled == true).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Servicios.Where(s => s.Nombre.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new ServicioPagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    Items = lista.Select(s => new ServicioDto
                    {
                        Id = s.Id,
                        Nombre = s.Nombre,
                        Descripcion = s.Descripcion,
                        CodigoServicio = s.CodigoServicio,
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
        }

        public ServicioDto Update(ServicioRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldRegisterInfo = _context.Servicios.FirstOrDefault(s => s.Id == request.Id);
            if (oldRegisterInfo == null) return new ServicioDto { ValidationErrorMessage = "Error al obtener info de los Servicios" };

            var rolUpdate = _servicioDomainService.Update(request, oldRegisterInfo);

            _context.Servicios.Update(oldRegisterInfo);
            _context.SaveChanges();

            return new ServicioDto
            {
                Id = oldRegisterInfo.Id,
                Nombre = oldRegisterInfo.Nombre,
                Descripcion = oldRegisterInfo.Descripcion,
                CodigoServicio = oldRegisterInfo.CodigoServicio,
                Enabled = oldRegisterInfo.Enabled
            };
        }

        public ServicioDto Disabled(ServicioRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldRegisterInfo = _context.Servicios.FirstOrDefault(s => s.Id == request.Id);
            if (oldRegisterInfo == null) return new ServicioDto { ValidationErrorMessage = "Error al obtener info de puesto" };

            var userUpdate = _servicioDomainService.Disabled(request, oldRegisterInfo);

            _context.Servicios.Update(oldRegisterInfo);
            _context.SaveChanges();

            return new ServicioDto
            {
                Id = oldRegisterInfo.Id,
                Nombre = oldRegisterInfo.Nombre,
                Descripcion = oldRegisterInfo.Descripcion,
                CodigoServicio = oldRegisterInfo.CodigoServicio,
                Enabled = oldRegisterInfo.Enabled
            };
        }

        public IEnumerable<ServicioDto> GetAllServicios()
        {
            var roles = _context.Servicios.Where(s => s.Enabled == true).ToList();

            return roles.Select(t => new ServicioDto
            {
                Id = t.Id,
                Nombre = t.Nombre,
                Descripcion = t.Descripcion
            });
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_servicioDomainService != null) _servicioDomainService.Dispose();
        }

    }
}