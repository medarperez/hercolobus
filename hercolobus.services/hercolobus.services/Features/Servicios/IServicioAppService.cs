using System;
using System.Collections.Generic;
using hercolobus.services.Core;

namespace hercolobus.services.Features.Servicios
{
    public interface IServicioAppService : IDisposable
    {
        ServicioPagedDto GetPaged(PagedGeneralRequest request);
        ServicioDto Create(ServicioRequest request);
        ServicioDto Update(ServicioRequest request);
        ServicioDto Disabled(ServicioRequest request);
        IEnumerable<ServicioDto> GetAllServicios();
        string Delete(int id);
    }
}