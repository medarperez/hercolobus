using System.Collections.Generic;
using hercolobus.services.Core;

namespace hercolobus.services.Features.Servicios
{
    public class ServicioPagedDto : ResponseBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<ServicioDto> Items { get; set; }
    }
}