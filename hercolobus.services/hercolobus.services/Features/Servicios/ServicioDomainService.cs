using System;

namespace hercolobus.services.Features.Servicios
{
    public class ServicioDomainService : IServicioDomainService
    {
        public Servicio Create(ServicioRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Nombre)) throw new ArgumentNullException(nameof(request.Nombre));

            Servicio zona = new Servicio.Builder()
            .ConNombre(request.Nombre)
            .ConDescripcion(request.Descripcion)
            .ConCodigoServicio(request.CodigoServicio)
            .ConAuditFields(request.User)
            .Build();

            return zona;
        }

        public Servicio Update(ServicioRequest request, Servicio _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(request.Nombre, request.Descripcion, request.CodigoServicio, request.User);

            return _oldRegister;
        }

        public Servicio Disabled(ServicioRequest request, Servicio _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Disbaled(request.User);

            return _oldRegister;
        }

        public void Dispose()
        {
        } 
    }
}