using System;

namespace hercolobus.services.Features.Servicios
{
    public interface IServicioDomainService : IDisposable
    {
        Servicio Create(ServicioRequest request);
        Servicio Update(ServicioRequest request, Servicio _oldRegister);
        Servicio Disabled(ServicioRequest request, Servicio _oldRegister);
    }
}