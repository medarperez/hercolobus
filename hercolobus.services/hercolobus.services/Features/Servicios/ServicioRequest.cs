using hercolobus.services.Core;

namespace hercolobus.services.Features.Servicios
{
    public class ServicioRequest : RequestBase
    {
        public string Nombre { get;  set; }
        public string Descripcion { get;  set; }
        public string CodigoServicio { get;  set; }
    }
}