namespace hercolobus.services.Core
{
    public class ResponseBase
    {
        public int Id { get; set; }
        public string Status { get; set; }
        public string ValidationErrorMessage { get; set; }
        public bool Enabled { get; set; }

        public bool HasValidationError()
        {
            return !string.IsNullOrWhiteSpace(ValidationErrorMessage);
        }
    }
}