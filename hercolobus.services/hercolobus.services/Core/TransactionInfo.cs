using System;

namespace hercolobus.services.Core
{
    public sealed class TransactionInfo
    {
        private TransactionInfo()
        {

        }

        public DateTime CreationDate { get; private set; }
        public Guid TransactionUId { get; private set; }
        public string ModifiedBy { get; private set; }
        public DateTime TransactionDate { get; private set; }
        public string TransactionType { get; private set; }
        public string CrudOperation { get; private set; }


        public class Builder
        {
            private readonly TransactionInfo _transactionInfo = new TransactionInfo();

            public Builder WithModifiedBy(string modifiedBy)
            {
                _transactionInfo.ModifiedBy = modifiedBy;
                return this;
            }

            public Builder WithTransactionType(string transactionType)
            {
                _transactionInfo.TransactionType = transactionType;
                return this;
            }


            public TransactionInfo Build()
            {
                return _transactionInfo;
            }
        }
    }
}