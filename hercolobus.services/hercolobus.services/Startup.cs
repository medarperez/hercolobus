﻿using System;
using System.Text;
using hercolobus.services.Context;
using hercolobus.services.Features.Clientes;
using hercolobus.services.Features.Contratos;
using hercolobus.services.Features.Paquetes;
using hercolobus.services.Features.Puestos;
using hercolobus.services.Features.ReportesClientes;
using hercolobus.services.Features.Roles;
using hercolobus.services.Features.Servicios;
using hercolobus.services.Features.Usuarios;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.Hosting;
using hercolobus.services.hercolobus.services.Context;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Parametros;
using Nostromo = hercolobus.services.hercolobus.services.FeaturesNostromo;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Catalogo;

namespace hercolobus.services
{
    public class Startup
    {
        private const string AllowAllOriginsPolicy = "AllowAllOriginsPolicy";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add service and create Policy with options
            services.AddCors(options =>
            {
                options.AddPolicy(AllowAllOriginsPolicy,
                    builder =>
                    {
                        builder.AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                    });
            });

            services.AddControllers();

            // var appSettingsSection = Configuration.GetSection("AppSettings");
            // services.Configure<AppSettings>(appSettingsSection);

            // var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(Configuration["ApiAuth:SecretKey"]);

            services.AddAuthentication(x =>
           {
               x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
               x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
           })
           .AddJwtBearer(x =>
           {
               x.RequireHttpsMetadata = false;
               x.SaveToken = true;
               x.TokenValidationParameters = new TokenValidationParameters
               {
                   ValidateIssuerSigningKey = true,
                   IssuerSigningKey = new SymmetricSecurityKey(key),
                   ValidateIssuer = false,
                   ValidateAudience = false
               };
           });


            services.AddDbContext<HercolobusContext>(
                options => options.UseMySql(
                    Configuration.GetConnectionString("DefaultConnection1"), 
                    mySqlOptionsAction: mySqlOptions => { mySqlOptions.EnableRetryOnFailure(); }));

            services.AddDbContext<NostromoContext>(
                options => options.UseMySql(
                    Configuration.GetConnectionString("AraShopConnection"),
                    mySqlOptionsAction: mySqlOptions => { mySqlOptions.EnableRetryOnFailure(); }));

;

            services.AddMvc();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            services.AddTransient<HercolobusContext>();
            services.AddScoped<IPuestoDomainService, PuestoDomainService>();
            services.AddScoped<IPuestoAppService, PuestoAppService>();
            services.AddScoped<IRolDomainService, RolDomainService>();
            services.AddScoped<IRolAppService, RolAppService>();
            services.AddScoped<IUsuarioDomainService, UsuarioDomainService>();
            services.AddScoped<IUsuarioAppService, UsuarioAppService>();
            services.AddScoped<IServicioDomainService, ServicioDomainService>();
            services.AddScoped<IServicioAppService, ServicioAppService>();
            services.AddScoped<IPaqueteDomainService, PaqueteDomainService>();
            services.AddScoped<IPaqueteAppService, PaqueteAppService>();
            services.AddScoped<IClienteDomainService, ClienteDomainService>();
            services.AddScoped<IClienteAppService, ClienteAppService>();
            services.AddScoped<IContratoDomainService, ContratoDomainService>();
            services.AddScoped<IContratoAppService, ContratoAppService>();
            services.AddScoped<IReporteClientesAppService, ReporteClientesAppService>();



            //AraShopContext
            services.AddTransient<NostromoContext>();
            services.AddScoped<IParametroDomainService, ParametroDomainService>();
            services.AddScoped<IParametroAppService, ParametroAppService>();
            services.AddScoped<Nostromo.Puestos.IPuestoDomainService, Nostromo.Puestos.PuestoDomainService>();
            services.AddScoped<Nostromo.Puestos.IPuestoAppService, Nostromo.Puestos.PuestoAppService>();
            services.AddScoped<Nostromo.Rols.IRolDomainService, Nostromo.Rols.RolDomainService>();
            services.AddScoped<Nostromo.Rols.IRolAppService, Nostromo.Rols.RolAppService>();
            services.AddScoped<Nostromo.Usuarios.IUsuarioDomainService, Nostromo.Usuarios.UsuarioDomainService>();
            services.AddScoped<Nostromo.Usuarios.IUsuarioAppService, Nostromo.Usuarios.UsuarioAppService>();
            services.AddScoped<ICatalogoDomainService, CatalogoDomainService>();
            services.AddScoped<ICatalogoAppService, CatalogoAppService>();


            services.AddSwaggerGen(c =>
           {
               c.SwaggerDoc("v1", new OpenApiInfo { Title = "Hercolobus Services API", Version = "v1" });
           });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }


            // app.UseAuthentication();
            // app.UseCors("CorsPolicy");
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Hercolobus Services API V1");
                c.RoutePrefix = string.Empty;
            });

            app.UseRouting();
            app.UseAuthentication();
            app.UseCors(AllowAllOriginsPolicy);
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
