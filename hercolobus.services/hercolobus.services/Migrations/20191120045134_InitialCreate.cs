﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace hercolobus.services.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Puestos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "NOW()"),
                    TransactionUId = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    CrudOperation = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Enabled = table.Column<bool>(unicode: false, nullable: false),
                    RowVersion = table.Column<DateTime>(fixedLength: true, maxLength: 8, rowVersion: true, nullable: false),
                    Nombre = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Descripcion = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Puestos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Rols",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "NOW()"),
                    TransactionUId = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    CrudOperation = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Enabled = table.Column<bool>(unicode: false, nullable: false),
                    RowVersion = table.Column<DateTime>(fixedLength: true, maxLength: 8, rowVersion: true, nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Description = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rols", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Usuarios",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "NOW()"),
                    TransactionUId = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    CrudOperation = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Enabled = table.Column<bool>(unicode: false, nullable: false),
                    RowVersion = table.Column<DateTime>(fixedLength: true, maxLength: 8, rowVersion: true, nullable: false),
                    UsuarioId = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    Nombre = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    Correo = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    PuestoId = table.Column<int>(nullable: false),
                    RolId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuarios", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Usuarios_Puestos_PuestoId",
                        column: x => x.PuestoId,
                        principalTable: "Puestos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Usuarios_Rols_RolId",
                        column: x => x.RolId,
                        principalTable: "Rols",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Passwords",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "NOW()"),
                    TransactionUId = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    CrudOperation = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Enabled = table.Column<bool>(unicode: false, nullable: false),
                    RowVersion = table.Column<DateTime>(fixedLength: true, maxLength: 8, rowVersion: true, nullable: false),
                    UserId = table.Column<int>(unicode: false, maxLength: 100, nullable: false),
                    PasswordHash = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    PasswordHashConfirmacion = table.Column<string>(unicode: false, maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Passwords", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Passwords_Usuarios_UserId",
                        column: x => x.UserId,
                        principalTable: "Usuarios",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Passwords_UserId",
                table: "Passwords",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Usuarios_PuestoId",
                table: "Usuarios",
                column: "PuestoId");

            migrationBuilder.CreateIndex(
                name: "IX_Usuarios_RolId",
                table: "Usuarios",
                column: "RolId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Passwords");

            migrationBuilder.DropTable(
                name: "Usuarios");

            migrationBuilder.DropTable(
                name: "Puestos");

            migrationBuilder.DropTable(
                name: "Rols");
        }
    }
}
