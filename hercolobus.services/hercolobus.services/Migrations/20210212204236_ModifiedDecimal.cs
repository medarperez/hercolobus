﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace hercolobus.services.Migrations
{
    public partial class ModifiedDecimal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "CostoAlternativo",
                table: "Contratos",
                type: "decimal(18, 9)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(65,30)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "CostoAlternativo",
                table: "Contratos",
                type: "decimal(65,30)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18, 9)");
        }
    }
}
