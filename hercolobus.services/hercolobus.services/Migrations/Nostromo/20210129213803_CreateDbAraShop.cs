﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace hercolobus.services.Migrations.Nostromo
{
    public partial class CreateDbAraShop : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Parametros",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "NOW()"),
                    TransactionUId = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    CrudOperation = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Enabled = table.Column<bool>(unicode: false, nullable: false),
                    RowVersion = table.Column<DateTime>(fixedLength: true, maxLength: 8, rowVersion: true, nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
                    Empresa = table.Column<string>(unicode: false, maxLength: 80, nullable: false),
                    Direccion = table.Column<string>(unicode: false, maxLength: 150, nullable: false),
                    Contacto = table.Column<string>(unicode: false, maxLength: 150, nullable: false),
                    Impuesto = table.Column<decimal>(type: "decimal(18, 9)", nullable: false),
                    Rtn = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    RazonSocial = table.Column<string>(unicode: false, maxLength: 150, nullable: false),
                    Cai = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    RangoFacturaInicio = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    RangoFacturaFinal = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    FechaLimiteEmision = table.Column<DateTime>(nullable: false),
                    UltimaFacturaGenerada = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    UltimoCorrelativoUtilizado = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Parametros", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Parametros");
        }
    }
}
