using hercolobus.services.Context.maps;
using hercolobus.services.Features.Clientes;
using hercolobus.services.Features.Cobros;
using hercolobus.services.Features.Contratos;
using hercolobus.services.Features.Facturas;
using hercolobus.services.Features.Paquetes;
using hercolobus.services.Features.Passwords;
using hercolobus.services.Features.Puestos;
using hercolobus.services.Features.Roles;
using hercolobus.services.Features.Servicios;
using hercolobus.services.Features.Usuarios;
using Microsoft.EntityFrameworkCore;

namespace hercolobus.services.Context
{
    public class HercolobusContext : DbContext
    {
        public HercolobusContext(DbContextOptions<HercolobusContext> context) : base(context)
        {

        }


        //public DbSet<User> Usuarios { get; set; }
        //public DbSet<UserPassword> Passwords { get; set; }

        public DbSet<Puesto> Puestos { get; set; }
        public DbSet<Rol> Rols { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Password> Passwords { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Servicio> Servicios { get; set; }
        public DbSet<Paquete> Paquetes { get; set; }
        public DbSet<PaqueteDetalle> PaquetesDetalle { get; set; }
        public DbSet<Contrato> Contratos { get; set; }
        public DbSet<Cobro> Cobros {get; set;}
        public DbSet<Factura> Facturas {get; set;}
        public DbSet<FacturaDetalle> FacturaDetalle {get; set;}
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new PuestoMap());
            modelBuilder.ApplyConfiguration(new RolMap());
            modelBuilder.ApplyConfiguration(new UsuarioMap());
            modelBuilder.ApplyConfiguration(new PasswordMap());
            modelBuilder.ApplyConfiguration(new ClienteMap());
            modelBuilder.ApplyConfiguration(new ServicioMap());
            modelBuilder.ApplyConfiguration(new PaqueteMap());
            modelBuilder.ApplyConfiguration(new PaqueteDetalleMap());
            modelBuilder.ApplyConfiguration(new ContratoMap());
            modelBuilder.ApplyConfiguration(new CobroMap());
            modelBuilder.ApplyConfiguration(new FacturaMap());
            modelBuilder.ApplyConfiguration(new FacturaDetalleMap());
            //modelBuilder.ApplyConfiguration(new UserPasswordMap());
        }
    }
}