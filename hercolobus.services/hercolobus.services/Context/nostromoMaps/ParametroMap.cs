using hercolobus.services.Core;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Parametros;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace hercolobus.services.hercolobus.services.Context.nostromoMaps
{
    public class ParametroMap : EntityMap<Parametro>
    {
        public override void Configure(EntityTypeBuilder<Parametro> builder)
        {
            builder.Property(t => t.Empresa).HasColumnName("Empresa").IsRequired().IsUnicode(false).HasMaxLength(80);
            builder.Property(t => t.Direccion).HasColumnName("Direccion").IsRequired().IsUnicode(false).HasMaxLength(150);
            builder.Property(t => t.Contacto).HasColumnName("Contacto").IsRequired().IsUnicode(false).HasMaxLength(150);
            builder.Property(t => t.Impuesto).HasColumnName("Impuesto").IsRequired().HasColumnType("decimal(18, 9)");
            builder.Property(t => t.Rtn).HasColumnName("Rtn").IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.RazonSocial).HasColumnName("RazonSocial").IsRequired().IsUnicode(false).HasMaxLength(150);
            builder.Property(t => t.Cai).HasColumnName("Cai").IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.RangoFacturaInicio).HasColumnName("RangoFacturaInicio").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.RangoFacturaFinal).HasColumnName("RangoFacturaFinal").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.FechaLimiteEmision).HasColumnName("FechaLimiteEmision").IsRequired();
            builder.Property(t => t.UltimaFacturaGenerada).HasColumnName("UltimaFacturaGenerada").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.UltimoCorrelativoUtilizado).HasColumnName("UltimoCorrelativoUtilizado");

            base.Configure(builder);
        }
    }
}