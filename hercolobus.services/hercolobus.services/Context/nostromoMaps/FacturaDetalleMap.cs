using hercolobus.services.Core;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Factura;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace hercolobus.services.hercolobus.services.Context.nostromoMaps
{
    public class FacturaDetalleMap : EntityMap<FacturaDetalle>
    {
        public override void Configure(EntityTypeBuilder<FacturaDetalle> builder)
        {
            builder.Property(t => t.FacturaId).HasColumnName("FacturaId").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.IdEncabezado).HasColumnName("IdEncabezado").IsRequired();
            builder.Property(t => t.IdLinea).HasColumnName("IdLinea").IsRequired();
            builder.Property(t => t.IdArticulo).HasColumnName("IdArticulo").IsRequired();
            builder.Property(t => t.Descripcion).HasColumnName("Descripcion").IsUnicode(false).HasMaxLength(120);
            builder.Property(t => t.Precio).HasColumnName("Precio").HasColumnType("decimal(18, 9)");
            builder.Property(t => t.Cantidad).HasColumnName("Cantidad");

            builder.HasOne(t => t.Encabezado).WithMany(t => t.Detalle).HasForeignKey(x => x.IdEncabezado);

            base.Configure(builder);
        }
    }
}