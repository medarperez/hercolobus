using hercolobus.services.Core;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Passwords;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace hercolobus.services.hercolobus.services.Context.nostromoMaps
{
    public class PasswordMap : EntityMap<Password>
    {
        public override void Configure(EntityTypeBuilder<Password> builder)
        {
            builder.Property(t => t.UserId).HasColumnName("UserId").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.PasswordHash).HasColumnName("PasswordHash").IsRequired().IsUnicode(false).HasMaxLength(200);
            builder.Property(t => t.PasswordHashConfirmacion).HasColumnName("PasswordHashConfirmacion").IsUnicode(false).HasMaxLength(200);

            builder.HasOne(t => t.User).WithMany(t => t.Passwords).HasForeignKey(x => x.UserId);

            base.Configure(builder);
        }
    }
}