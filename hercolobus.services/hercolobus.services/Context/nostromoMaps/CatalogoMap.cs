using hercolobus.services.Core;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Catalogo;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace hercolobus.services.hercolobus.services.Context.nostromoMaps
{
    public class CatalogoMap : EntityMap<Catalogo>
    {
        public override void Configure(EntityTypeBuilder<Catalogo> builder)
        {
            builder.Property(t => t.CodigoArticulo).HasColumnName("CodigoArticulo").IsUnicode(false).HasMaxLength(60);
            builder.Property(t => t.Articulo).HasColumnName("Articulo").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Descripcion).HasColumnName("Descripcion").IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.Precio).HasColumnName("Precio").IsUnicode().HasColumnType("decimal(18, 9)");
            builder.Property(t => t.IdParametro).HasColumnName("IdParametro").IsRequired();

            builder.HasOne(t => t.Parametro).WithMany(t => t.Productos).HasForeignKey(x => x.IdParametro);

            base.Configure(builder);
        }
    }
}