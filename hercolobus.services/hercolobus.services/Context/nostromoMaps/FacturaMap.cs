using hercolobus.services.Core;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Factura;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace hercolobus.services.hercolobus.services.Context.nostromoMaps
{
    public class FacturaMap : EntityMap<Factura>
    {
        public override void Configure(EntityTypeBuilder<Factura> builder)
        {
            builder.Property(t => t.FacturaId).HasColumnName("FacturaId").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Cliente).HasColumnName("Cliente").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Estado).HasColumnName("Estado").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.CAI).HasColumnName("CAI").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.RangoInicial).HasColumnName("RangoInicial").IsRequired().IsUnicode().HasMaxLength(150);
            builder.Property(t => t.RangoFinal).HasColumnName("RangoFinal").IsRequired().IsUnicode().HasMaxLength(150);
            builder.Property(t => t.RtnCliente).HasColumnName("RtnCliente").IsRequired().IsUnicode().HasMaxLength(150);
            builder.Property(t => t.IdParametro).HasColumnName("IdParametro").IsRequired();

            builder.HasOne(t => t.Parametro).WithMany(t => t.Facturas).HasForeignKey(x => x.IdParametro);

            base.Configure(builder);
        }
    }
}