
using hercolobus.services.hercolobus.services.Context.nostromoMaps;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Catalogo;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Factura;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Parametros;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Passwords;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Puestos;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Rols;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Usuarios;
using Microsoft.EntityFrameworkCore;


namespace hercolobus.services.hercolobus.services.Context
{
    public class NostromoContext : DbContext
    {
        public NostromoContext(DbContextOptions<NostromoContext> context) : base(context)
        {

        }

        public DbSet<Parametro> Parametros { get; set; }
        public DbSet<Puesto> Puestos { get; set; }
        public DbSet<Rol> Rols { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Password> Passwords { get; set; }
        public DbSet<Catalogo> Productos { get; set; }
        public DbSet<Factura> Facturas { get; set; }
        public DbSet<FacturaDetalle> FacturasDetalle { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ParametroMap());
            modelBuilder.ApplyConfiguration(new PuestoMap());
            modelBuilder.ApplyConfiguration(new RolMap());
            modelBuilder.ApplyConfiguration(new nostromoMaps.UsuarioMap());
            modelBuilder.ApplyConfiguration(new PasswordMap());
            modelBuilder.ApplyConfiguration(new CatalogoMap());
            modelBuilder.ApplyConfiguration(new nostromoMaps.FacturaMap());
            modelBuilder.ApplyConfiguration(new nostromoMaps.FacturaDetalleMap());
        }
    }
}