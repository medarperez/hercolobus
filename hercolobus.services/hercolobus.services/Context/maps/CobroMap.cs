using hercolobus.services.Core;
using hercolobus.services.Features.Cobros;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace hercolobus.services.Context.maps
{
    public class CobroMap: EntityMap<Cobro>
    {
        public override void Configure(EntityTypeBuilder<Cobro> builder)
        {
            builder.Property(t => t.FacturaId).HasColumnName("FacturaId").IsRequired();
            builder.Property(t => t.Comentario).HasColumnName("Comentario").IsUnicode(false).HasMaxLength(250);
            builder.Property(t => t.PagoTotal).HasColumnName("PagoTotal").IsRequired().IsUnicode(false).HasColumnType("decimal(18, 9)");

            builder.HasOne(t => t.Factura).WithMany(t => t.Cobros).HasForeignKey(x => x.FacturaId);
            base.Configure(builder);
        }
    }
}