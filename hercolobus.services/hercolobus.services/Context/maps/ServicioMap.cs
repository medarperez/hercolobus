using hercolobus.services.Core;
using hercolobus.services.Features.Servicios;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace hercolobus.services.Context.maps
{
    public class ServicioMap : EntityMap<Servicio>
    {
        public override void Configure(EntityTypeBuilder<Servicio> builder)
        {
            builder.Property(t => t.Nombre).HasColumnName("Nombre").IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.Descripcion).HasColumnName("Descripcion").IsRequired().IsUnicode(false).HasMaxLength(150);
            builder.Property(t => t.CodigoServicio).HasColumnName("CodigoServicio").IsRequired().IsUnicode(false).HasMaxLength(50);

            base.Configure(builder);
        } 
    }
}