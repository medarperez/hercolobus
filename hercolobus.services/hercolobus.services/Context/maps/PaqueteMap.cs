using hercolobus.services.Core;
using hercolobus.services.Features.Paquetes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace hercolobus.services.Context.maps
{
    public class PaqueteMap : EntityMap<Paquete>
    {
        public override void Configure(EntityTypeBuilder<Paquete> builder)
        {
            builder.Property(t => t.Nombre).HasColumnName("Nombre").IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.Descripcion).HasColumnName("Descripcion").IsRequired().IsUnicode(false).HasMaxLength(150);
            builder.Property(t => t.CodigoPaquete).HasColumnName("CodigoPaquete").IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.Precio).HasColumnName("Precio").HasColumnType("decimal(18, 9)");

            base.Configure(builder);
        }
    }
}