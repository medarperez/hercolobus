using hercolobus.services.Core;
using hercolobus.services.Features.Facturas;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace hercolobus.services.Context.maps
{
    public class FacturaDetalleMap: EntityMap<FacturaDetalle>
    {
        public override void Configure(EntityTypeBuilder<FacturaDetalle> builder)
        {
            builder.Property(t => t.FacturaId).HasColumnName("FacturaId").IsRequired();
            builder.Property(t => t.CodigoServicio).HasColumnName("CodigoServicio").IsRequired();
            builder.Property(t => t.Precio).HasColumnName("Precio").IsUnicode(false).HasColumnType("decimal(18, 9)");

            builder.HasOne(t => t.Factura).WithMany(t => t.Detalle).HasForeignKey(x => x.FacturaId);
            base.Configure(builder);
        }
    }
}