using hercolobus.services.Core;
using hercolobus.services.Features.Paquetes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace hercolobus.services.Context.maps
{
    public class PaqueteDetalleMap : EntityMap<PaqueteDetalle>
    {
        public override void Configure(EntityTypeBuilder<PaqueteDetalle> builder)
        {
            builder.Property(t => t.CodigoPauete).HasColumnName("NoCodigoPauetembre").IsRequired();
            builder.Property(t => t.CodigoServicio).HasColumnName("CodigoServicio").IsRequired();

            builder.HasOne(t => t.Paquete).WithMany(t => t.Detalle).HasForeignKey(x => x.CodigoPauete);

            base.Configure(builder);
        }
    }
}