using hercolobus.services.Core;
using hercolobus.services.Features.Roles;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace hercolobus.services.Context.maps
{
    public class RolMap : EntityMap<Rol>
    {
        public override void Configure(EntityTypeBuilder<Rol> builder)
        {
            builder.Property(t => t.Name).HasColumnName("Name").IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.Description).HasColumnName("Description").IsRequired().IsUnicode(false).HasMaxLength(50);

            base.Configure(builder);
        }
    }
}