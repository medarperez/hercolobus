using hercolobus.services.Core;
using hercolobus.services.Features.Puestos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace hercolobus.services.Context.maps
{
    public class PuestoMap : EntityMap<Puesto>
    {
        public override void Configure(EntityTypeBuilder<Puesto> builder)
        {
            builder.Property(t => t.Nombre).HasColumnName("Nombre").IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.Descripcion).HasColumnName("Descripcion").IsRequired().IsUnicode(false).HasMaxLength(50);

            base.Configure(builder);
        }
    }
}