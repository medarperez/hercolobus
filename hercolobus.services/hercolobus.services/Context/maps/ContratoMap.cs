using hercolobus.services.Core;
using hercolobus.services.Features.Contratos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace hercolobus.services.Context.maps {
    public class ContratoMap : EntityMap<Contrato> {
        public override void Configure (EntityTypeBuilder<Contrato> builder) {
            builder.Property (t => t.ClienteId).HasColumnName ("ClienteId").IsRequired ();
            builder.Property (t => t.PaqueteId).HasColumnName ("PaqueteId").IsRequired ();
            builder.Property (t => t.FechaInicio).HasColumnName ("FechaInicio").IsRequired ();
            builder.Property (t => t.FechaFinal).HasColumnName ("FechaFinal");
            builder.Property (t => t.FechaInicioCobro).HasColumnName ("FechaInicioCobro").IsRequired ();
            builder.Property (t => t.DiaMaximoParaPago).HasColumnName ("DiaMaximoParaPago").IsRequired ();
            builder.Property (t => t.EsContratoExentoCobro).HasColumnName ("EsContratoExentoCobro").IsRequired ();
            builder.Property (t => t.DireccionDeConexion).HasColumnName ("DireccionDeConexion");
            builder.Property (t => t.CostoAlternativo).HasColumnName ("CostoAlternativo").HasColumnType ("decimal(18, 9)");;

            builder.HasOne (t => t.Cliente).WithMany (t => t.Contratos).HasForeignKey (x => x.ClienteId);
            builder.HasOne (t => t.Paquete).WithMany (t => t.Contratos).HasForeignKey (x => x.PaqueteId);

            base.Configure (builder);
        }
    }
}