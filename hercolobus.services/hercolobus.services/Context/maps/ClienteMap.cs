using hercolobus.services.Core;
using hercolobus.services.Features.Clientes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace hercolobus.services.Context.maps
{
    public class ClienteMap : EntityMap<Cliente>
    {
        public override void Configure(EntityTypeBuilder<Cliente> builder)
        {
            builder.Property(t => t.Nombre).HasColumnName("Nombre").IsRequired().IsUnicode(false).HasMaxLength(200);
            builder.Property(t => t.Apellido).HasColumnName("Apellido").IsRequired().IsUnicode(false).HasMaxLength(200);
            builder.Property(t => t.Identidad).HasColumnName("Identidad").IsUnicode(false).HasMaxLength(200);
            builder.Property(t => t.Direccion).HasColumnName("Direccion").IsUnicode(false).HasMaxLength(200);
            builder.Property(t => t.Telefono).HasColumnName("Telefono").IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.Celular).HasColumnName("Celular").IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.Correo).HasColumnName("Correo").IsUnicode(false).HasMaxLength(50);


            // builder.HasOne(t => t.User).WithMany(t => t.Passwords).HasForeignKey(x => x.UserId);

            base.Configure(builder);
        }
    }
}