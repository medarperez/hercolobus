using hercolobus.services.Core;
using hercolobus.services.Features.Facturas;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace hercolobus.services.Context.maps
{
    public class FacturaMap : EntityMap<Factura>
    {
        public override void Configure(EntityTypeBuilder<Factura> builder)
        {
            builder.Property(t => t.ContratoId).HasColumnName("ContratoId").IsRequired();
            builder.Property(t => t.FechaEmision).HasColumnName("FechaEmision").IsRequired();
            builder.Property(t => t.Vencimiento).HasColumnName("Vencimiento");
            builder.Property(t => t.MesFacturado).HasColumnName("MesFacturado").IsRequired().HasMaxLength(50);
            builder.Property(t => t.Estado).HasColumnName("Estado").IsRequired().HasMaxLength(50);
    
            builder.HasOne(t => t.Contrato).WithMany(t => t.Facturas).HasForeignKey(x => x.ContratoId);
            base.Configure(builder);
        }
    }
}