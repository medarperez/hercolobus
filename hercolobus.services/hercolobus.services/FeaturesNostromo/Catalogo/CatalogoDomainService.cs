using System;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Catalogo
{
    public class CatalogoDomainService : ICatalogoDomainService
    {
        public Catalogo Create(CatalogoRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Articulo)) throw new ArgumentNullException(nameof(request.Articulo));

            Catalogo linea = new Catalogo.Builder()
            .ConNombre(request.Articulo)
            .ConDescripcion(request.Descripcion)
            .ConCodigoArticulo(request.CodigoArticulo)
            .ConPrecio(request.Precio)
            .ConParametro(request.IdParametro)
            .ConAuditFields(request.User)
            .Build();

            return linea;
        }

        public Catalogo Update(CatalogoRequest request, Catalogo _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.ActualizarArticulo(request.Articulo, request.Descripcion,
                             request.Precio, request.CodigoArticulo, request.User, request.IdParametro);

            return _oldRegister;
        }

        public Catalogo Disabled(CatalogoRequest request, Catalogo _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Disbaled(request.User);

            return _oldRegister;
        }

        public void Dispose()
        {
        }
    }
}