using System;
using System.Collections.Generic;
using hercolobus.services.Core;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Catalogo
{
    public interface ICatalogoAppService : IDisposable
    {
        CatalogoPagedDto GetPaged(PagedGeneralRequest request);
        CatalogoDto Create(CatalogoRequest request);
        CatalogoDto Update(CatalogoRequest request);
        CatalogoDto Disabled(CatalogoRequest request);
        IEnumerable<CatalogoDto> GetAllArticulos(string user);
        string Delete(int id);
    }
}