using System.Collections.Generic;
using hercolobus.services.Core;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Catalogo
{
    public class CatalogoPagedDto : ResponseBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<CatalogoDto> Items { get; set; }
    }
}