using System;
using System.Collections.Generic;
using hercolobus.services.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Catalogo
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CatalogoController : ControllerBase
    {
        private readonly ICatalogoAppService _catalogoAppService;
        public CatalogoController(ICatalogoAppService catalogoAppService)
        {
            _catalogoAppService = catalogoAppService ?? throw new ArgumentException(nameof(catalogoAppService));
        }

        [HttpGet]
        // [Route("paged")]
        [Authorize]
        public ActionResult<IEnumerable<CatalogoDto>> Get(string user)
        {
            return Ok(_catalogoAppService.GetAllArticulos(user));
        }

        [HttpGet]
        [Route("paged")]
        [Authorize]
        public ActionResult<CatalogoPagedDto> GetPaged([FromQuery] PagedGeneralRequest request)
        {
            return Ok(_catalogoAppService.GetPaged(request));
        }

        [HttpPost]
        [Authorize]
        public ActionResult<CatalogoDto> Post([FromBody] CatalogoRequest request)
        {
            return Ok(_catalogoAppService.Create(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<CatalogoDto> Put([FromBody] CatalogoRequest request)
        {
            return Ok(_catalogoAppService.Update(request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_catalogoAppService.Delete(Id));
        }

        [HttpPut]
        [Route("disabled")]
        public ActionResult<CatalogoDto> Disabled([FromBody] CatalogoRequest request)
        {
            return Ok(_catalogoAppService.Disabled(request));
        }
    }
}