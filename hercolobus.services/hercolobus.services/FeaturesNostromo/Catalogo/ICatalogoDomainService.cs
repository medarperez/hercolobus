using System;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Catalogo
{
    public interface ICatalogoDomainService : IDisposable
    {
        Catalogo Create(CatalogoRequest request);
        Catalogo Update(CatalogoRequest request, Catalogo _oldRegister);
        Catalogo Disabled(CatalogoRequest request, Catalogo _oldRegister);
    }
}