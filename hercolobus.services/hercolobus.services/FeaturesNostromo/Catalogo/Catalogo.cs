using System;
using System.ComponentModel.DataAnnotations.Schema;
using hercolobus.services.Core;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Parametros;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Catalogo
{
    [Table("Catalogo")]
    public class Catalogo : Entity
    {
        public string CodigoArticulo { get; private set; }
        public string Articulo { get; private set; }
        public string Descripcion { get; private set; }
        public decimal Precio { get; private set; }

        public int IdParametro { get; private set; }
        public Parametro Parametro { get; set; }
        
        

        public void ActualizarArticulo(
                string _nombre, string _descripcion, decimal _precio, string _codigoArticulo, string _user, int idParametro)
        {
            CodigoArticulo = _codigoArticulo;
            Articulo = _nombre;
            Descripcion = _descripcion;
            Precio = _precio;
            IdParametro = idParametro;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModifiedProject";
            ModifiedBy = _user ?? "Application";
            TransactionUId = Guid.NewGuid();
            Enabled = true;
        }

        public void Disbaled(string _user)
        {
            CrudOperation = "Disabled";
            TransactionDate = DateTime.Now;
            TransactionType = "DisabledRegister";
            ModifiedBy = _user ?? "Application";
            TransactionUId = Guid.NewGuid();
            Enabled = false;
        }

        public class Builder
        {
            private readonly Catalogo _catalogo = new Catalogo();

            public Builder ConCodigoArticulo(string codigo)
            {
                _catalogo.CodigoArticulo = codigo;
                return this;
            }

            public Builder ConParametro(int idParametro)
            {
                _catalogo.IdParametro = idParametro;
                return this;
            }
            public Builder ConNombre(string name)
            {
                _catalogo.Articulo = name;
                return this;
            }

            public Builder ConDescripcion(string description)
            {
                _catalogo.Descripcion = description;
                return this;
            }

            public Builder ConPrecio(decimal precio)
            {
                _catalogo.Precio = precio;
                return this;
            }

            public Builder ConAuditFields(string user)
            {
                _catalogo.CrudOperation = "Added";
                _catalogo.TransactionDate = DateTime.Now;
                _catalogo.TransactionType = "NewProject";
                _catalogo.ModifiedBy = string.IsNullOrEmpty(user) ? "Aplication" : user;
                _catalogo.TransactionUId = Guid.NewGuid();
                _catalogo.Enabled = true;

                return this;
            }

            public Catalogo Build()
            {
                return _catalogo;
            }
        }
    }
}