using System;
using System.Collections.Generic;
using System.Linq;
using hercolobus.services.Core;
using hercolobus.services.hercolobus.services.Context;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Catalogo
{
    public class CatalogoAppService : ICatalogoAppService
    {
        private readonly NostromoContext _context;
        private readonly ICatalogoDomainService _catalogoDomainService;
        public CatalogoAppService(NostromoContext context, ICatalogoDomainService catalogoDomainService)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _catalogoDomainService = catalogoDomainService ?? throw new ArgumentException(nameof(catalogoDomainService));
        }
        public CatalogoDto Create(CatalogoRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (string.IsNullOrEmpty(request.User)) return new CatalogoDto();
            var usuario = _context.Usuarios.FirstOrDefault(s => s.UsuarioId == request.User);
            if (usuario == null) return new CatalogoDto();
           request.IdParametro = usuario.IdParametro;
            var newRow = _catalogoDomainService.Create(request);
          

            _context.Productos.Add(newRow);
            _context.SaveChanges();

            return new CatalogoDto
            {
                Articulo = newRow.Articulo,
                CodigoArticulo = newRow.CodigoArticulo,
                Descripcion = newRow.Descripcion,
                Precio = newRow.Precio,
                Enabled = newRow.Enabled
            };
        }

        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var row = _context.Productos.FirstOrDefault(s => s.Id == id);
            if (row == null) throw new Exception("Error al intentar obtener puesto");
            _context.Productos.Remove(row);
            _context.SaveChanges();

            return string.Empty;
        }

        public CatalogoDto Disabled(CatalogoRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldRowInfo = _context.Productos.FirstOrDefault(s => s.Id == request.Id);
            if (oldRowInfo == null) return new CatalogoDto { ValidationErrorMessage = "Error al obtener info de puesto" };

            var userUpdate = _catalogoDomainService.Disabled(request, oldRowInfo);

            _context.Productos.Update(oldRowInfo);
            _context.SaveChanges();

            return CatalogoDto.From(oldRowInfo);
        }

        public IEnumerable<CatalogoDto> GetAllArticulos(string user)
        {
            if(string.IsNullOrEmpty(user)) return new List<CatalogoDto>();

            var usuario = _context.Usuarios.FirstOrDefault(s => s.UsuarioId == user);
            if(usuario == null) return new List<CatalogoDto>();

            var rows = _context.Productos.
                    Where(s => s.Enabled == true && s.IdParametro == usuario.IdParametro).
                        OrderBy(s => s.Articulo).ToList();

            return CatalogoDto.FromReserves(rows);
        }

        public CatalogoPagedDto GetPaged(PagedGeneralRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;
            if (string.IsNullOrEmpty(request.User)) return new CatalogoPagedDto();
            var usuario = _context.Usuarios.FirstOrDefault(s => s.UsuarioId == request.User);
            if (usuario == null) return new CatalogoPagedDto(); ;

            if (string.IsNullOrEmpty(request.Value))
            {
                var count = Convert.ToDecimal(_context.Productos
                    .Where(s => s.Enabled == true && s.IdParametro == usuario.IdParametro).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Productos.Where(s => s.Enabled == true && s.IdParametro == usuario.IdParametro)
                .OrderByDescending(s => s.TransactionDate)
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new CatalogoPagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Items = CatalogoDto.FromReserves(lista),
                };
            }
            else
            {

                var count = Convert.ToDecimal(_context.Productos
                .Where(s => s.Articulo.Contains(request.Value) && s.Enabled == true && s.IdParametro == usuario.IdParametro).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Productos.Where(s => s.Articulo.Contains(request.Value) && s.IdParametro == usuario.IdParametro)
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new CatalogoPagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    Items = CatalogoDto.FromReserves(lista),
                };
            }
        }

        public CatalogoDto Update(CatalogoRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldRowInfo = _context.Productos.FirstOrDefault(s => s.Id == request.Id);
            if (oldRowInfo == null) return new CatalogoDto { ValidationErrorMessage = "Error al obtener info de los puestos" };

            var rolUpdate = _catalogoDomainService.Update(request, oldRowInfo);

            _context.Productos.Update(oldRowInfo);
            _context.SaveChanges();

            return CatalogoDto.From(rolUpdate);
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_catalogoDomainService != null) _catalogoDomainService.Dispose();
        }
    }
}