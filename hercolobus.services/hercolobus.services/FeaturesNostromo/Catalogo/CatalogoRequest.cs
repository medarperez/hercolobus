using hercolobus.services.Core;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Catalogo
{
    public class CatalogoRequest : RequestBase
    {

        public string CodigoArticulo { get; set; }
        public string Articulo { get; set; }
        public string Descripcion { get; set; }
        public decimal Precio { get; set; }
        public int IdParametro { get; set; }     
    }
}