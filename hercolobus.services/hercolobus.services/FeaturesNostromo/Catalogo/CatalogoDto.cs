using System.Collections.Generic;
using System.Linq;
using hercolobus.services.Core;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Parametros;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Catalogo
{
    public class CatalogoDto : ResponseBase
    {
        public string CodigoArticulo { get; set; }
        public string Articulo { get; set; }
        public string Descripcion { get; set; }
        public decimal Precio { get; set; }
        public int IdParametro { get; set; }
        
        public ParametroDto Parametro { get; set; }
        
        


        public static List<CatalogoDto> FromReserves(IEnumerable<Catalogo> detail)
        {
            if (detail == null || !detail.Any()) return new List<CatalogoDto>();
            return (from qry in detail
                    select From(qry)).ToList();
        }

        public static CatalogoDto From(Catalogo s)
        {
            if (s == null) return new CatalogoDto();

            return new CatalogoDto
            {
                Id = s.Id,
                CodigoArticulo = s.CodigoArticulo,
                Articulo = s.Articulo,
                Descripcion = s.Descripcion,
                Precio = s.Precio,
                IdParametro = s.IdParametro,
                Parametro = s.Parametro == null ? new ParametroDto() : ParametroDto.From(s.Parametro, string.Empty, 0)
            };
        }
    }
}