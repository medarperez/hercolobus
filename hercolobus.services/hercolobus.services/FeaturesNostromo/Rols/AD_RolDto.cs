using hercolobus.services.Core;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Rols
{
    public class AD_RolDto : ResponseBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}