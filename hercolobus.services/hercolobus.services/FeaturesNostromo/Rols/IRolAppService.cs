using System;
using System.Collections.Generic;
using hercolobus.services.Core;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Rols
{
    public interface IRolAppService : IDisposable
    {
        AD_RolPagedDto GetPaged(PagedGeneralRequest request);
        AD_RolDto Create(AD_RolRequest request);
        AD_RolDto Update(AD_RolRequest request);
        AD_RolDto Disabled(AD_RolRequest request);
        IEnumerable<AD_RolDto> GetAllRoles();
        string Delete(int id);
    }
}