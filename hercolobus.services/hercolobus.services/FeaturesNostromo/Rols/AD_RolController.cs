using System;
using System.Collections.Generic;
using hercolobus.services.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Rols
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AD_RolController : ControllerBase
    {
        private readonly IRolAppService _rolAppService;
        public AD_RolController(IRolAppService rolAppService)
        {
            _rolAppService = rolAppService ?? throw new ArgumentException(nameof(rolAppService));
        }
        [HttpGet]
        [Route("paged")]
        //[Authorize]
        public ActionResult<AD_RolPagedDto> GetPaged([FromQuery] PagedGeneralRequest request)
        {
            return Ok(_rolAppService.GetPaged(request));
        }

        [HttpGet]
        // [Route("paged")]
        //[Authorize]
        public ActionResult<IEnumerable<AD_RolDto>> Get()
        {
            return Ok(_rolAppService.GetAllRoles());
        }

        [HttpPost]
        //[Authorize]
        public ActionResult<AD_RolDto> Post([FromBody] AD_RolRequest request)
        {
            return Ok(_rolAppService.Create(request));
        }

        [HttpPut]
        //[Authorize]
        public ActionResult<AD_RolDto> Put([FromBody] AD_RolRequest request)
        {
            return Ok(_rolAppService.Update(request));
        }

        [HttpDelete]
        //[Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_rolAppService.Delete(Id));
        }

        [HttpPut]
        [Route("disabled")]
        public ActionResult<AD_RolDto> Disabled([FromBody] AD_RolRequest request)
        {
            return Ok(_rolAppService.Disabled(request));
        }
    }
}