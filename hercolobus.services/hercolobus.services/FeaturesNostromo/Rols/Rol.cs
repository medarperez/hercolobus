using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using hercolobus.services.Core;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Usuarios;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Rols
{
    [Table("Rols")]
    public class Rol : Entity
    {
        public string Name { get; private set; }
        public string Description { get; private set; }

        public ICollection<Usuario> Usuarios { get; set; }

        public void Update(
            string _name, string _description, string _user)
        {
            Name = _name;
            Description = _description;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModifiedProject";
            ModifiedBy = _user ?? "Application";
            TransactionUId = Guid.NewGuid();
            Enabled = true;
        }

        public void Disbaled(string _user)
        {
            CrudOperation = "Disabled";
            TransactionDate = DateTime.Now;
            TransactionType = "DisabledRegister";
            ModifiedBy = _user ?? "Application";
            TransactionUId = Guid.NewGuid();
            Enabled = false;
        }

        public class Builder
        {
            private readonly Rol _rol = new Rol();

            public Builder WithName(string name)
            {
                _rol.Name = name;
                return this;
            }

            public Builder WithDescription(string description)
            {
                _rol.Description = description;
                return this;
            }

            public Builder WithAuditFields(string user)
            {
                _rol.CrudOperation = "Added";
                _rol.TransactionDate = DateTime.Now;
                _rol.TransactionType = "NewProject";
                _rol.ModifiedBy = string.IsNullOrEmpty(user) ? "Aplication" : user;
                _rol.TransactionUId = Guid.NewGuid();
                _rol.Enabled = true;

                return this;
            }

            public Rol Build()
            {
                return _rol;
            }
        }

    }
}