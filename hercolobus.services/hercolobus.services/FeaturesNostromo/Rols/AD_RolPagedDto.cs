using System.Collections.Generic;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Rols
{
    public class AD_RolPagedDto
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<AD_RolDto> Items { get; set; }
    }
}