using hercolobus.services.Core;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Rols
{
    public class AD_RolRequest : RequestBase
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
    }
}