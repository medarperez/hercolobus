using System;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Rols
{
    public interface IRolDomainService : IDisposable
    {
        Rol Create(AD_RolRequest request);
        Rol Update(AD_RolRequest request, Rol _oldRegister);
        Rol Disabled(AD_RolRequest request, Rol _oldRegister);
    }
}