using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using hercolobus.services.Core;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Parametros;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Passwords;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Puestos;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Rols;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Usuarios
{
    [Table("Usuarios")]
    public class Usuario : Entity
    {
        public string UsuarioId { get; private set; }
        public string Nombre { get; private set; }
        public string Correo { get; private set; }
        public int PuestoId { get; private set; }
        public int RolId { get; private set; }
        public int IdParametro { get; private set; }
        
        
        public Parametro Parametro { get; set; }
        public Puesto Puesto { get; set; }
        public Rol Rol { get; set; }
        public ICollection<Password> Passwords { get; set; }

        public void Update(
            string _name, string _correo, int puesto, int rolId, string _user, int idParametro)
        {
            Nombre = _name;
            Correo = _correo;
            PuestoId = puesto;
            IdParametro = idParametro;
            RolId = rolId;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModificarRol";
            ModifiedBy = _user ?? "Application";
            TransactionUId = Guid.NewGuid();
            Enabled = true;
        }

        public void Disbaled(string _user)
        {
            CrudOperation = "Disabled";
            TransactionDate = DateTime.Now;
            TransactionType = "DisabledRegister";
            ModifiedBy = _user ?? "Application";
            TransactionUId = Guid.NewGuid();
            Enabled = false;
        }

        public class Builder
        {
            private readonly Usuario _usuario = new Usuario();

            public Builder ConUsuario(string usuarioId)
            {
                _usuario.UsuarioId = usuarioId;
                return this;
            }
            public Builder ConParametro(int parametroId)
            {
                _usuario.IdParametro = parametroId;
                return this; 
            }
            public Builder ConNombre(string name)
            {
                _usuario.Nombre = name;
                return this;
            }

            public Builder ConCorreo(string correo)
            {
                _usuario.Correo = correo;
                return this;
            }

            public Builder ConPuesto(int puesto)
            {
                _usuario.PuestoId = puesto;
                return this;
            }

            public Builder ConRol(int rolId)
            {
                _usuario.RolId = rolId;
                return this;
            }

            public Builder ConAuditFields(string user)
            {
                _usuario.CrudOperation = "Added";
                _usuario.TransactionDate = DateTime.Now;
                _usuario.TransactionType = "NewProject";
                _usuario.ModifiedBy = string.IsNullOrEmpty(user) ? "Aplication" : user;
                _usuario.TransactionUId = Guid.NewGuid();
                _usuario.Enabled = true;

                return this;
            }

            public Usuario Build()
            {
                return _usuario;
            }
        }

    }
}