using hercolobus.services.Core;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Parametros;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Puestos;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Rols;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Usuarios
{
    public class AD_UsuarioDto : ResponseBase
    {
        public string UsuarioId { get; set; }
        public string Nombre { get; set; }
        public string Correo { get; set; }
        public int PuestoId { get; set; }
        public int RolId { get; set; }

        public string NombrePuesto { get; set; }
        public string NombreRol { get; set; }
        public AD_PuestoDto Puesto { get; set; }
        public AD_RolDto Rol { get; set; }
        public ParametroDto Parametro { get; set; }
    }
}