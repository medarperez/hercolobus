using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using hercolobus.services.Core;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Rols;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Usuarios
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AD_AutenticacionController : ControllerBase
    {
        private readonly IUsuarioAppService _usuarioAppService;
        private readonly IConfiguration _configuration;
        private readonly EncriptorHelper encryp = new EncriptorHelper();
        public AD_AutenticacionController(
            IUsuarioAppService usuarioAppService, IConfiguration configuration)
        {
            _usuarioAppService = usuarioAppService ?? throw new ArgumentNullException(nameof(usuarioAppService));
            _configuration = configuration ?? throw new ArgumentException(nameof(configuration));
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("token")]
        public ActionResult<AD_UsuarioDto> Token([FromBody] AD_UsuarioRequest request)
        {
            var user = _usuarioAppService.ObtenerUsuarioPorId(request.UsuarioId).FirstOrDefault();
            if (user == null) return BadRequest("Error");
            if (string.IsNullOrEmpty(request.Password)) return BadRequest("Error");
            if (user.Passwords == null) return BadRequest("Invalid grant username and/or password is incorrect");
            var passwords = user.Passwords;
            if (!passwords.Any()) return BadRequest("Invalid grant username and/or password is incorrect");
            var password = passwords.FirstOrDefault(s => s.Enabled);
            if (request.UsuarioId == user.UsuarioId && encryp.VerifiedPassword(request.Password, password.PasswordHashConfirmacion))
            {
                var infoUsuario = new Usuario.Builder()
                .ConNombre(user.Nombre)
                .ConUsuario(user.UsuarioId)
                .ConCorreo(user.Correo)
                .Build();
                var userRol = user.Rol;
                var rol = new Rol();
                rol = userRol;
                var claims = new[] { new Claim("UserData", JsonConvert.SerializeObject(infoUsuario)) };

                // var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(user.PasswordHash));
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["ApiAuth:SecretKey"]));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                // Generamos el Token
                var token = new JwtSecurityToken
                (
                    issuer: _configuration["ApiAuth:Issuer"],
                    audience: _configuration["ApiAuth:Audience"],
                    claims: claims,
                    expires: DateTime.UtcNow.AddDays(1),
                    notBefore: DateTime.UtcNow,
                    signingCredentials: creds
                );

                // Retornamos el token
                return Ok(
                    new TokenInfoResponse
                    {
                        UserId = user.UsuarioId,
                        RolId = rol == null ? 0 : rol.Id,
                        Rol = rol == null ? string.Empty : rol.Name ?? string.Empty,
                        Token = new JwtSecurityTokenHandler().WriteToken(token)
                    }
                );
            }
            else
            {
                return BadRequest("Invalid grant username and/or password is incorrect");
            }
        }
    }
}