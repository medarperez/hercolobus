using System;
using System.Linq;
using hercolobus.services.Core;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Passwords;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Usuarios
{
    public class UsuarioDomainService : IUsuarioDomainService
    {
        private readonly EncriptorHelper _encriptorHelper = new EncriptorHelper();
        public Usuario Create(AD_UsuarioRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.UsuarioId)) throw new ArgumentNullException(nameof(request.UsuarioId));
            if (string.IsNullOrWhiteSpace(request.Nombre)) throw new ArgumentNullException(nameof(request.Nombre));

            Usuario usuario = new Usuario.Builder()
            .ConUsuario(request.UsuarioId)
            .ConNombre(request.Nombre)
            .ConCorreo(request.Correo)
            .ConPuesto(request.PuestoId)
            .ConRol(request.RolId)
            .ConParametro(request.ParametroId)
            .ConAuditFields(request.User)
            .Build();

            return usuario;
        }

        public Usuario Update(AD_UsuarioRequest request, Usuario _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(request.Nombre, request.Correo, request.PuestoId, request.RolId, request.User, request.ParametroId);

            return _oldRegister;
        }

        public Usuario Disabled(AD_UsuarioRequest request, Usuario _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Disbaled(request.User);

            return _oldRegister;
        }

        public Usuario DisablePassword(Usuario request, string user)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (string.IsNullOrEmpty(request.UsuarioId)) throw new ArgumentException(nameof(request.UsuarioId));

            if (!request.Passwords.Any()) return new Usuario();
            var password = request.Passwords.FirstOrDefault(s => s.Enabled);

            password.DisablePassword(user);

            return request;
        }

        public Password CreatePassword(AD_UsuarioRequest request, string password, int usuarioId)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (usuarioId <= 0) throw new ArgumentException(nameof(request));
            if (String.IsNullOrEmpty(request.Password)) throw new ArgumentException(nameof(request.Password));

            return new Password.Builder()
            .WithPassword(usuarioId, _encriptorHelper.GetPasswordHash(password))
            .WithPasswordHash(_encriptorHelper.GetPasswordHash(password))
            .WithAuditFields(request.User)
            .Build();
        }

        public Password CreatePassword(AD_PasswordResetRequest request, string password, int usuarioId)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (usuarioId <= 0) throw new ArgumentException(nameof(request));
            if (String.IsNullOrEmpty(request.Password)) throw new ArgumentException(nameof(request.Password));

            return new Password.Builder()
            .WithPassword(usuarioId, _encriptorHelper.GetPasswordHash(password))
            .WithPasswordHash(_encriptorHelper.GetPasswordHash(password))
            .WithAuditFields(request.User)
            .Build();
        }

        public Password ChangePassword(string usuarioId, Password userInfoOld)
        {
            if (userInfoOld == null) throw new ArgumentException(nameof(userInfoOld));

            userInfoOld.ChangePassword(usuarioId);

            return userInfoOld;
        }
        public void Dispose()
        {
        }
    }
}