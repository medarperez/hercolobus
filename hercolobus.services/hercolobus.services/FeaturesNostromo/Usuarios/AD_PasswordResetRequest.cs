using hercolobus.services.Core;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Usuarios
{
    public class AD_PasswordResetRequest : RequestBase
    {
        public string Password { get; set; }
        public string NewPassword { get; set; }
    }
}