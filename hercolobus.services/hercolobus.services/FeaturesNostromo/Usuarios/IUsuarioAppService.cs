using System;
using System.Collections.Generic;
using hercolobus.services.Core;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Usuarios
{
    public interface IUsuarioAppService : IDisposable
    {
        AD_UsuarioPagedDto GetPagedUser(PagedGeneralRequest request);
        AD_UsuarioDto CreateUser(AD_UsuarioRequest request);
        AD_UsuarioDto UpdateUser(AD_UsuarioRequest request);
        AD_UsuarioDto Disabled(AD_UsuarioRequest request);
        IEnumerable<Usuario> ObtenerUsuarioPorId(string id);
        Usuario ObtenerUsuarioInfo(int id);
        string DeteleUser(string userId);
        string ChangeUserPassword(AD_PasswordResetRequest request);
    }
}