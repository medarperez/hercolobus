using System;
using System.Collections.Generic;
using System.Linq;
using hercolobus.services.Core;
using hercolobus.services.hercolobus.services.Context;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Puestos;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Rols;
using Microsoft.EntityFrameworkCore;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Usuarios
{
    public class UsuarioAppService : IUsuarioAppService
    {
        private readonly NostromoContext _context;
        private readonly IUsuarioDomainService _usuarioDomainService;

        public UsuarioAppService(NostromoContext context,
                                   IUsuarioDomainService usuarioDomainService)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _usuarioDomainService = usuarioDomainService ?? throw new ArgumentException(nameof(usuarioDomainService));
        }
        public AD_UsuarioDto CreateUser(AD_UsuarioRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newUsuario = _usuarioDomainService.Create(request);

            _context.Usuarios.Add(newUsuario);
            _context.SaveChanges();

            var usuario = _context.Usuarios.FirstOrDefault(s => s.UsuarioId == request.UsuarioId);
            var newPassword = _usuarioDomainService.CreatePassword(request, request.Password, usuario.Id);
            _context.Passwords.Add(newPassword);
            _context.SaveChanges();

            return new AD_UsuarioDto
            {
                Nombre = newUsuario.Nombre,
                Correo = newUsuario.Correo,
                PuestoId = newUsuario.PuestoId,
                RolId = newUsuario.RolId,
                Enabled = newUsuario.Enabled
            };
        }

        public string DeteleUser(string userId)
        {
            if (string.IsNullOrEmpty(userId)) throw new ArgumentException(nameof(userId));

            var user = _context.Usuarios.FirstOrDefault(s => s.UsuarioId == userId);
            if (user == null) throw new Exception("User not exists");
            _context.Usuarios.Remove(user);
            _context.SaveChanges();

            return string.Empty;
        }

        public AD_UsuarioDto Disabled(AD_UsuarioRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldUsuarioInfo = _context.Usuarios.FirstOrDefault(s => s.Id == request.Id);
            if (oldUsuarioInfo == null) return new AD_UsuarioDto { ValidationErrorMessage = "Error al obtener info de usuario" };

            var userUpdate = _usuarioDomainService.Disabled(request, oldUsuarioInfo);

            _context.Usuarios.Update(oldUsuarioInfo);
            _context.SaveChanges();

            return new AD_UsuarioDto
            {
                Id = oldUsuarioInfo.Id,
                Nombre = oldUsuarioInfo.Nombre,
                Correo = oldUsuarioInfo.Correo,
                PuestoId = oldUsuarioInfo.PuestoId,
                RolId = oldUsuarioInfo.RolId,
                Enabled = oldUsuarioInfo.Enabled
            };
        }
        public AD_UsuarioPagedDto GetPagedUser(PagedGeneralRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;

            if (string.IsNullOrEmpty(request.Value))
            {
                var count = Convert.ToDecimal(_context.Usuarios.Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Usuarios
                    .OrderByDescending(s => s.TransactionDate)
                    .Skip(request.PageSize * (request.PageIndex - 1))
                    .Take(request.PageSize)
                    .Include(c => c.Puesto)
                    .Include(c => c.Rol)
                    .ToList();
                return new AD_UsuarioPagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Items = lista.Select(s => new AD_UsuarioDto
                    {
                        Id = s.Id,
                        UsuarioId = s.UsuarioId,
                        Nombre = s.Nombre,
                        Correo = s.Correo,
                        PuestoId = s.PuestoId,
                        RolId = s.RolId,
                        Enabled = s.Enabled,
                        Puesto = s.Puesto == null ? new AD_PuestoDto() : new AD_PuestoDto
                        {
                            Id = s.Puesto.Id,
                            Nombre = s.Puesto.Nombre,
                            Descripcion = s.Puesto.Descripcion
                        },
                        NombrePuesto = s.Puesto == null ? "N/A" : s.Puesto.Nombre ?? "N/A",
                        Rol = s.Rol == null ? new AD_RolDto() : new AD_RolDto
                        {
                            Id = s.Rol.Id,
                            Name = s.Rol.Name,
                            Description = s.Rol.Description
                        },
                        NombreRol = s.Rol == null ? "N/A" : s.Rol.Name ?? "N/A",

                    }).ToList()
                };
            }
            else
            {

                var count = Convert.ToDecimal(_context.Usuarios.Where(s => s.UsuarioId.Contains(request.Value)).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Usuarios.Where(s => s.UsuarioId.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize)
                .Include(c => c.Puesto)
                    .Include(c => c.Rol).ToList();
                return new AD_UsuarioPagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    Items = lista.Select(s => new AD_UsuarioDto
                    {
                        Id = s.Id,
                        UsuarioId = s.UsuarioId,
                        Nombre = s.Nombre,
                        Correo = s.Correo,
                        PuestoId = s.PuestoId,
                        RolId = s.RolId,
                        Enabled = s.Enabled,
                        Puesto = s.Puesto == null ? new AD_PuestoDto() : new AD_PuestoDto
                        {
                            Id = s.Puesto.Id,
                            Nombre = s.Puesto.Nombre,
                            Descripcion = s.Puesto.Descripcion
                        },
                        NombrePuesto = s.Puesto == null ? "N/A" : s.Puesto.Nombre ?? "N/A",
                        Rol = s.Rol == null ? new AD_RolDto() : new AD_RolDto
                        {
                            Id = s.Rol.Id,
                            Name = s.Rol.Name,
                            Description = s.Rol.Description
                        },
                        NombreRol = s.Rol == null ? "N/A" : s.Rol.Name ?? "N/A",
                    }).ToList()
                };
            }
        }

        public AD_UsuarioDto UpdateUser(AD_UsuarioRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldUsuarioInfo = _context.Usuarios.FirstOrDefault(s => s.Id == request.Id);
            if (oldUsuarioInfo == null) return new AD_UsuarioDto { ValidationErrorMessage = "Error al obtener info de usuario" };

            var userUpdate = _usuarioDomainService.Update(request, oldUsuarioInfo);

            _context.Usuarios.Update(oldUsuarioInfo);
            _context.SaveChanges();

            return new AD_UsuarioDto
            {
                Id = oldUsuarioInfo.Id,
                Nombre = oldUsuarioInfo.Nombre,
                Correo = oldUsuarioInfo.Correo,
                PuestoId = oldUsuarioInfo.PuestoId,
                RolId = oldUsuarioInfo.RolId,
                Enabled = oldUsuarioInfo.Enabled
            };
        }

        public IEnumerable<Usuario> ObtenerUsuarioPorId(string id)
        {
            return _context.Usuarios.Where(s => s.UsuarioId.Contains(id)).Include(c => c.Puesto)
                    .Include(c => c.Rol).Include(c => c.Passwords);
        }

        public Usuario ObtenerUsuarioInfo(int id)
        {
            return _context.Usuarios.FirstOrDefault(s => s.Id == id && s.Enabled == true);
        }

        public string ChangeUserPassword(AD_PasswordResetRequest request)
        {
            EncriptorHelper encryp = new EncriptorHelper();
            if (request == null) throw new ArgumentNullException(nameof(request));
            if (string.IsNullOrEmpty(request.Password)) throw new ArgumentNullException(nameof(request.Password));
            if (string.IsNullOrEmpty(request.NewPassword)) throw new ArgumentNullException(nameof(request.NewPassword));
            if (string.IsNullOrEmpty(request.User)) throw new ArgumentNullException(nameof(request.User));

            var user = _context.Usuarios.Include(c => c.Passwords).FirstOrDefault(s => s.UsuarioId == request.User);
            var passwords = user.Passwords;
            if (passwords == null) return "Usuario no encontrado";
            if (!passwords.Any()) return "Usuario no encontrado";
            var password = passwords.FirstOrDefault(s => s.Enabled == true);
            if (password == null) return "Usuario no encontrado";
            if (user.UsuarioId == user.UsuarioId && encryp.VerifiedPassword(request.Password, password.PasswordHash))
            {

                var DisabledPassword = _usuarioDomainService.ChangePassword(user.UsuarioId, password);
                _context.Passwords.Update(DisabledPassword);

                var newPassword = _usuarioDomainService.CreatePassword(request, request.NewPassword, user.Id);
                _context.Passwords.Add(newPassword);
                _context.SaveChanges();
                return string.Empty;
            }
            else
            {
                return "Usuario no encontrado";
            }
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_usuarioDomainService != null) _usuarioDomainService.Dispose();
        }

    }
}