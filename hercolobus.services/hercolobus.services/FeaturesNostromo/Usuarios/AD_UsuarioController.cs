using System;
using hercolobus.services.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Usuarios
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AD_UsuarioController : ControllerBase
    {
        private readonly IUsuarioAppService _usuarioAppService;
        public AD_UsuarioController(IUsuarioAppService usuarioAppService)
        {
            _usuarioAppService = usuarioAppService ?? throw new ArgumentException(nameof(usuarioAppService));
        }
        [HttpGet]
        [Route("paged")]
        [Authorize]
        public ActionResult<AD_UsuarioPagedDto> GetPaged([FromQuery] PagedGeneralRequest request)
        {
            return Ok(_usuarioAppService.GetPagedUser(request));
        }

        [HttpPost]
        [Authorize]
        public ActionResult<AD_UsuarioDto> Post([FromBody] AD_UsuarioRequest request)
        {
            return Ok(_usuarioAppService.CreateUser(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<AD_UsuarioDto> Put([FromBody] AD_UsuarioRequest request)
        {
            return Ok(_usuarioAppService.UpdateUser(request));
        }

        [HttpPut]
        [Route("disabled")]
        public ActionResult<AD_UsuarioDto> Disabled([FromBody] AD_UsuarioRequest request)
        {
            return Ok(_usuarioAppService.Disabled(request));
        }

        [HttpPut]
        [Authorize]
        [Route("changePassword")]
        public ActionResult<string> ChangePassword([FromBody] AD_PasswordResetRequest request)
        {
            var r = string.Empty;
            return Ok(_usuarioAppService.ChangeUserPassword(request));
        }
    }
}