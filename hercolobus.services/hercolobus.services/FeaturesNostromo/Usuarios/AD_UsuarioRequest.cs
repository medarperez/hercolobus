using hercolobus.services.Core;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Usuarios
{
    public class AD_UsuarioRequest : RequestBase
    {
        public string UsuarioId { get; set; }
        public string Nombre { get; set; }
        public string Correo { get; set; }
        public int PuestoId { get; set; }
        public int RolId { get; set; }
        public string Password { get; set; }
        public int ParametroId { get; set; }
        
    }
}