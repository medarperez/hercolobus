using System;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Passwords;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Usuarios
{
    public interface IUsuarioDomainService : IDisposable
    {
        Usuario Create(AD_UsuarioRequest request);
        Usuario Update(AD_UsuarioRequest request, Usuario _oldRegister);
        Usuario Disabled(AD_UsuarioRequest request, Usuario _oldRegister);
        Usuario DisablePassword(Usuario request, string user);
        Password CreatePassword(AD_UsuarioRequest request, string password, int usuarioId);

        Password CreatePassword(AD_PasswordResetRequest request, string password, int usuarioId);

        Password ChangePassword(string usuarioId, Password passwordInfoOld);
    }
}