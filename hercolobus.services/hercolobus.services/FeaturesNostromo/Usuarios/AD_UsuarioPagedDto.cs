using System.Collections.Generic;
using hercolobus.services.Core;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Usuarios
{
    public class AD_UsuarioPagedDto : ResponseBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<AD_UsuarioDto> Items { get; set; }
    }
}