using System;
using System.Linq;
using hercolobus.services.Core;
using hercolobus.services.hercolobus.services.Context;
using Microsoft.EntityFrameworkCore;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Factura
{
    public class Ad_FacturaAppService : Iad_FacturaAppService
    {
        private readonly NostromoContext _context;
        private readonly Iad_FacturaDomainService _facturaDomainService;
        public Ad_FacturaAppService(NostromoContext context, Iad_FacturaDomainService facturaDomainService)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _facturaDomainService = facturaDomainService ?? throw new ArgumentNullException(nameof(facturaDomainService));
        }
        public AD_FacturaDto Actualizar(AD_FacturaRequest request)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));

            var oldRegister = _context.Facturas.FirstOrDefault(s => s.Id == request.Id);
            if (oldRegister == null) return new AD_FacturaDto { ValidationErrorMessage = "Error al intentar actualizar el registro" };

            var updateRegister = _facturaDomainService.Update(request, oldRegister);

            _context.Facturas.Update(updateRegister);
            _context.SaveChanges();

            return AD_FacturaDto.From(updateRegister);
        }

        public AD_FacturaDto ActualizarEstado(AD_FacturaRequest request)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));

            var oldRegister = _context.Facturas.FirstOrDefault(s => s.Id == request.Id);
            if (oldRegister == null) return new AD_FacturaDto { ValidationErrorMessage = "Error al intentar actualizar el registro" };

            var updateRegister = _facturaDomainService.UpdateStatus(request, oldRegister);

            _context.Facturas.Update(updateRegister);
            _context.SaveChanges();

            return AD_FacturaDto.From(updateRegister);
        }

        public AD_FacturaDto Crear(AD_FacturaRequest request)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));
            if (string.IsNullOrEmpty(request.User)) return new AD_FacturaDto();
            var usuario = _context.Usuarios.FirstOrDefault(s => s.UsuarioId == request.User);
            if (usuario == null) return new AD_FacturaDto();

            var parametros = _context.Parametros.FirstOrDefault(s => s.Id == usuario.IdParametro);
            if (parametros == null) return new AD_FacturaDto
            {
                ValidationErrorMessage = "Error al momento de obtener informacion de la empresa!!!"
            };
            
            request.IdParametro = usuario.IdParametro;
            parametros.UpdateUltimoCorrelativo(request.ProximoNumeroFactura, request.ProximoCorrelativo);
            var nuevoRegistro = _facturaDomainService.Create(request);

            _context.Facturas.Add(nuevoRegistro);
            _context.Parametros.Update(parametros);
            _context.SaveChanges();

            var factura = _context.Facturas.Include(s => s.Detalle).
                          FirstOrDefault(s => s.FacturaId == nuevoRegistro.FacturaId);

            if (factura == null) return new AD_FacturaDto();

            return AD_FacturaDto.From(factura);
        }

        public AD_FacturaPagedDto ObtenerListaPaginadaFacturas(PagedGeneralRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;
            if (string.IsNullOrEmpty(request.User)) return new AD_FacturaPagedDto();
            var usuario = _context.Usuarios.FirstOrDefault(s => s.UsuarioId == request.User);
            if (usuario == null) return new AD_FacturaPagedDto();

            if (string.IsNullOrEmpty(request.Value))
            {
                var count = Convert.ToDecimal(_context.Facturas
                            .Where(s => s.Enabled == true && s.IdParametro == usuario.IdParametro).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Facturas
                                        .Where(s => s.Enabled == true && s.IdParametro == usuario.IdParametro)
                                        .OrderByDescending(s => s.TransactionDate)
                                        .Skip(request.PageSize * (request.PageIndex - 1))
                                        .Take(request.PageSize)
                                        .Include(s => s.Detalle).ToList();
                                        
                return new AD_FacturaPagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Facturas = AD_FacturaDto.FromFacturas(lista.OrderByDescending(s => s.CreationDate).ToList()),
                };
            }
            else
            {

                var count = Convert.ToDecimal(_context.Facturas
                        .Where(s => s.Cliente.Contains(request.Value) && s.Enabled == true 
                                        && s.IdParametro == usuario.IdParametro).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Facturas
                                        .Where(s => s.Cliente.Contains(request.Value)
                                        && s.IdParametro == usuario.IdParametro)
                                        .Skip(request.PageSize * (request.PageIndex - 1))
                                        .Take(request.PageSize)
                                        .Include(s => s.Detalle).ToList();

                return new AD_FacturaPagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    Facturas = AD_FacturaDto.FromFacturas(lista.OrderByDescending(s => s.CreationDate).ToList()),
                };
            }
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_facturaDomainService != null) _facturaDomainService.Dispose();
        }
    }
}