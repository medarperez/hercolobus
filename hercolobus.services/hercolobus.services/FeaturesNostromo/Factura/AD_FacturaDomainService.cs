using System;
using System.Linq;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Factura
{
    public class AD_FacturaDomainService : Iad_FacturaDomainService
    {
        public Factura Create(AD_FacturaRequest request)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));


            Factura registro = new Factura.Builder()
            .ConIdFactura(request.FacturaId)
            .ConCAI(request.CAI)
            .ConRango(request.RangoInicial, request.RangoFinal)
            .ConCliente(request.Cliente)
            .ConRtnCliente(request.RtnCliente)
            .ConEstado(request.Estado)
            .ConFecha(request.FechaEmision)
            .ConDetalle(request.Detalle, request.FacturaId, request.User)
            .ConParametro(request.IdParametro)
            .WithAuditFields(request.User)
            .Build();

            if (request.Detalle == null) return registro;
            if (!request.Detalle.Any()) return registro;

            return registro;
        }

        public Factura Update(AD_FacturaRequest request, Factura _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(
                request.Cliente, request.Estado, request.CAI, 
                request.FechaEmision, request.RtnCliente, request.User, request.IdParametro);

            return _oldRegister;
        }

        public Factura UpdateStatus(AD_FacturaRequest request, Factura _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));
            if (string.IsNullOrEmpty(request.Estado)) throw new ArgumentNullException(request.Estado);

            _oldRegister.UpdateEstado(request.Estado, request.User);

            return _oldRegister;

        }

        public void Dispose()
        {
        }
    }
}