using System.Collections.Generic;
using System.Linq;
using hercolobus.services.Core;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Factura
{
    public class AD_DetalleDto : ResponseBase
    {
        public string FacturaId { get; set; }
        public int IdEncabezado { get; set; }
        public int IdLinea { get; set; }
        public int IdArticulo { get; set; }
        public string Descripcion { get; set; }
        public decimal Precio { get; set; }
        public int Cantidad { get; set; }
        public decimal Total { get; set; }



        public static List<AD_DetalleDto> FromFacturas(IEnumerable<FacturaDetalle> detail)
        {
            if (detail == null || !detail.Any()) return new List<AD_DetalleDto>();
            return (from qry in detail
                    select From(qry)).ToList();
        }

        public static AD_DetalleDto From(FacturaDetalle s)
        {
            if (s == null) return new AD_DetalleDto();

            return new AD_DetalleDto
            {
                Id = s.Id,
                FacturaId = s.FacturaId,
                IdEncabezado = s.IdEncabezado,
                IdLinea = s.IdLinea,
                IdArticulo = s.IdArticulo,
                Descripcion = s.Descripcion,
                Precio = s.Precio,
                Cantidad = s.Cantidad,
                Total = s.Precio * s.Cantidad
            };
        }

    }
}