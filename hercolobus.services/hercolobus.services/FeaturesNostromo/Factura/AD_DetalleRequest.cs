using hercolobus.services.Core;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Factura
{
    public class AD_DetalleRequest : RequestBase
    {
        public string FacturaId { get; set; }
        public int IdLinea { get; set; }
        public int IdArticulo { get; set; }
        public string Descripcion { get; set; }
        public decimal Precio { get; set; }
        public int Cantidad { get; set; }

    }
}