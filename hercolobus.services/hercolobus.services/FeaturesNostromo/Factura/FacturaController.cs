using System;
using hercolobus.services.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Factura
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FacturaController : ControllerBase
    {
        private readonly Iad_FacturaAppService _facturaAppService;
        public FacturaController(Iad_FacturaAppService facturaAppService)
        {
            _facturaAppService = facturaAppService ?? throw new ArgumentException(nameof(facturaAppService));
        }

        [HttpPost]
        [Authorize]
        public ActionResult<AD_FacturaDto> Post([FromBody] AD_FacturaRequest request)
        {
            return Ok(_facturaAppService.Crear(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<AD_FacturaDto> Put([FromBody] AD_FacturaRequest request)
        {
            return Ok(_facturaAppService.Actualizar(request));
        }

        [HttpPut]
        [Route("actualizar-estado")]
        [Authorize]
        public ActionResult<AD_FacturaDto> AtualizarEstado([FromBody] AD_FacturaRequest request)
        {
            return Ok(_facturaAppService.ActualizarEstado(request));
        }

        [HttpGet]
        [Route("paged")]
        [Authorize]
        public ActionResult<AD_FacturaPagedDto> GetPaged([FromQuery] PagedGeneralRequest request)
        {
            return Ok(_facturaAppService.ObtenerListaPaginadaFacturas(request));
        }

    }
}