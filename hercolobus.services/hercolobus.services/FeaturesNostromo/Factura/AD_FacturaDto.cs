using System;
using System.Collections.Generic;
using System.Linq;
using hercolobus.services.Core;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Parametros;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Factura
{
    public class AD_FacturaDto : ResponseBase
    {
        public string FacturaId { get; set; }
        public string Cliente { get; set; }
        public string Estado { get; set; }
        public string CAI { get; set; }
        public DateTime FechaEmision { get; set; }
        public string RangoInicial { get; private set; }
        public string RangoFinal { get; private set; }
        public string ProximoCorrelativo { get; set; }
        public int ProximoNumeroFactura { get; set; }
        public string RtnCliente { get; set; }
        public int IdParametro { get; set; }
        
        
        public ParametroDto Parametro {get;set;}
        public List<AD_DetalleDto> Detalle { get; set; }

        public static List<AD_FacturaDto> FromFacturas(IEnumerable<Factura> detail)
        {
            if (detail == null || !detail.Any()) return new List<AD_FacturaDto>();
            return (from qry in detail
                    select From(qry)).ToList();
        }

        public static AD_FacturaDto From(Factura s)
        {
            if (s == null) return new AD_FacturaDto();

            return new AD_FacturaDto
            {
                Id = s.Id,
                FacturaId = s.FacturaId,
                Cliente = s.Cliente,
                Estado = s.Estado,
                CAI = s.CAI,
                FechaEmision = s.FechaEmision,
                RangoInicial = s.RangoInicial,
                RangoFinal = s.RangoFinal,
                RtnCliente = s.RtnCliente,
                IdParametro = s.IdParametro,
                Parametro = s.Parametro == null ? new ParametroDto() : ParametroDto.From(s.Parametro, string.Empty, 0),
                Detalle = AD_DetalleDto.FromFacturas(s.Detalle ?? new List<FacturaDetalle>())
            };
        }
    }
}