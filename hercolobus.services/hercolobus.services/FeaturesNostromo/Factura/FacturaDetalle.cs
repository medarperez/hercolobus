using System;
using System.ComponentModel.DataAnnotations.Schema;
using hercolobus.services.Core;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Factura
{
    [Table("FacturaDetalle")]
    public class FacturaDetalle : Entity
    {
        public string FacturaId { get; private set; }
        public int IdEncabezado { get; private set; }
        public int IdLinea { get; private set; }
        public int IdArticulo { get; private set; }
        public string Descripcion { get; private set; }
        public decimal Precio { get; private set; }
        public int Cantidad { get; private set; }

        public Factura Encabezado { get; set; }

        public void Update(int idArticulo, string descripcion, decimal precio, int cantidad, string user)
        {
            IdArticulo = idArticulo;
            Descripcion = descripcion;
            Precio = precio;
            Cantidad = cantidad;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "Modified";
            ModifiedBy = user ?? "Application";
            TransactionUId = Guid.NewGuid();
            Enabled = true;
        }

        public class Builder
        {
            private readonly FacturaDetalle _facturaDetalle = new FacturaDetalle();

            public Builder ConIdFactura(string facturaId)
            {
                _facturaDetalle.FacturaId = facturaId;
                return this;
            }

            public Builder ConIdEncabezado(int idEncabezado)
            {
                _facturaDetalle.IdEncabezado = idEncabezado;
                return this;
            }

            public Builder ConIdLinea(int idLinea)
            {
                _facturaDetalle.IdLinea = idLinea;
                return this;
            }

            public Builder ConArticulo(int idArticulo, string descripcion, int cantidad, decimal precio)
            {
                _facturaDetalle.IdArticulo = idArticulo;
                _facturaDetalle.Descripcion = descripcion;
                _facturaDetalle.Cantidad = cantidad;
                _facturaDetalle.Precio = precio;
                return this;
            }

            public Builder WithAuditFields(string _user)
            {
                _facturaDetalle.CrudOperation = "Add";
                _facturaDetalle.TransactionDate = DateTime.Now;
                _facturaDetalle.TransactionType = "NuevoRegistro";
                _facturaDetalle.ModifiedBy = _user ?? "Application";
                _facturaDetalle.TransactionUId = Guid.NewGuid();
                _facturaDetalle.Enabled = true;
                return this;
            }

            public FacturaDetalle Build()
            {
                return _facturaDetalle;
            }
        }

    }
}