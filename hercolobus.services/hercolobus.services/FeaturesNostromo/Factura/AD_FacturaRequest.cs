using System;
using System.Collections.Generic;
using hercolobus.services.Core;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Factura
{
    public class AD_FacturaRequest : RequestBase
    {
        public string FacturaId { get; set; }
        public string Cliente { get; set; }
        public string Estado { get; set; }
        public string CAI { get; set; }
        public DateTime FechaEmision { get; set; }
        public string RangoInicial { get; set; }
        public string RangoFinal { get; set; }
        public string ProximoCorrelativo { get; set; }
        public int ProximoNumeroFactura { get; set; }
        public string RtnCliente { get; set; }
        public int IdParametro { get; set; }
        
        

        public List<AD_DetalleRequest> Detalle { get; set; }
    }
}