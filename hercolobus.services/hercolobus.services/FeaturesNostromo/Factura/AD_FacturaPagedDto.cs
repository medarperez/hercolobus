using System.Collections.Generic;
using hercolobus.services.Core;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Factura
{
    public class AD_FacturaPagedDto : ResponseBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<AD_FacturaDto> Facturas { get; set; }
    }
}