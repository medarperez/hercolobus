using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using hercolobus.services.Core;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Parametros;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Factura
{
    [Table("Factura")]
    public class Factura : Entity
    {
        private Factura()
        {
            Detalle = new HashSet<FacturaDetalle>();
        }

        public string FacturaId { get; private set; }
        public string Cliente { get; private set; }
        public string Estado { get; private set; }
        public string CAI { get; private set; }
        public decimal Porcentaje { get; set; }
        public int IdParametro { get; set; }
        public DateTime FechaEmision { get; private set; }
        public string RangoInicial { get; private set; }
        public string RangoFinal { get; private set; }
        public string RtnCliente { get; private set; }

        public Parametro Parametro { get; set; }
        private ICollection<FacturaDetalle> _detalle;
        public ICollection<FacturaDetalle> Detalle
        
        {
            get { return _detalle ?? (_detalle = new HashSet<FacturaDetalle>()); }
            private set { _detalle = value; }
        }

        public void UpdateRango(string rangoInicial, string rangoFinal)
        {
            RangoInicial = rangoInicial;
            RangoFinal = rangoFinal;
        }

        public void UpdateFacturaId(string facturaId)
        {
            FacturaId = facturaId;
        }

        public void Update(
            string cliente, string estado, string cAI, DateTime fechaEmision, string rtnCliente, string user, int idParametro)
        {
            Cliente = cliente;
            Estado = estado;
            CAI = cAI;
            FechaEmision = fechaEmision;
            RtnCliente = rtnCliente;
            IdParametro = idParametro;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "Modified";
            ModifiedBy = user ?? "Application";
            TransactionUId = Guid.NewGuid();
            Enabled = true;
        }

        public void UpdateEstado(string estado, string user)
        {
            Estado = estado;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModifiedStatus";
            ModifiedBy = user ?? "Application";
            TransactionUId = Guid.NewGuid();
            Enabled = true;
        }

        public class Builder
        {
            private readonly Factura _facturaEncabezado = new Factura();

            public Builder ConIdFactura(string idFactura)
            {
                _facturaEncabezado.FacturaId = idFactura;
                return this;
            }

            public Builder ConParametro(int idParametro)
            {
                _facturaEncabezado.IdParametro = idParametro;
                return this;
            }

            public Builder ConCliente(string cliente)
            {
                _facturaEncabezado.Cliente = cliente;
                return this;
            }

            public Builder ConRtnCliente(string rtnCliente)
            {
                _facturaEncabezado.RtnCliente = rtnCliente;
                return this;
            }

            public Builder ConEstado(string estado)
            {
                _facturaEncabezado.Estado = estado;
                return this;
            }

            public Builder ConCAI(string cai)
            {
                _facturaEncabezado.CAI = cai;
                return this;
            }

            public Builder ConRango(string inicio, string final)
            {
                _facturaEncabezado.RangoInicial = inicio;
                _facturaEncabezado.RangoFinal = final;
                return this;
            }

            public Builder ConFecha(DateTime fecha)
            {
                _facturaEncabezado.FechaEmision = fecha;
                return this;
            }

            public Builder ConDetalle(List<AD_DetalleRequest> detalle, string facturaId, string user)
            {
                _facturaEncabezado.Detalle = new List<FacturaDetalle>();

                foreach (var item in detalle)
                {
                    var newDetalle = new FacturaDetalle
                                    .Builder()
                                    .ConIdFactura(facturaId)
                                    .ConIdLinea(item.IdLinea)
                                    .ConArticulo(item.IdArticulo, item.Descripcion, item.Cantidad, item.Precio)
                                    .WithAuditFields(user)
                                    .Build();
                    _facturaEncabezado.Detalle.Add(newDetalle);
                }
                return this;
            }

            public Builder WithAuditFields(string _user)
            {
                _facturaEncabezado.CrudOperation = "Add";
                _facturaEncabezado.TransactionDate = DateTime.Now;
                _facturaEncabezado.TransactionType = "NuevoRegistro";
                _facturaEncabezado.ModifiedBy = _user ?? "Application";
                _facturaEncabezado.TransactionUId = Guid.NewGuid();
                _facturaEncabezado.Enabled = true;
                return this;
            }

            public Factura Build()
            {
                return _facturaEncabezado;
            }
        }
    }
}