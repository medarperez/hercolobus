using System;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Factura
{
    public interface Iad_FacturaDomainService : IDisposable
    {
        Factura Create(AD_FacturaRequest request);
        Factura Update(AD_FacturaRequest request, Factura _oldRegister);
        Factura UpdateStatus(AD_FacturaRequest request, Factura _oldRegister);
    }
}