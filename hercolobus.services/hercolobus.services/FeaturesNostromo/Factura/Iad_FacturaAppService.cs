using System;
using hercolobus.services.Core;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Factura
{
    public interface Iad_FacturaAppService : IDisposable
    {
        AD_FacturaPagedDto ObtenerListaPaginadaFacturas(PagedGeneralRequest request);
        AD_FacturaDto Crear(AD_FacturaRequest request);
        AD_FacturaDto Actualizar(AD_FacturaRequest request);
        AD_FacturaDto ActualizarEstado(AD_FacturaRequest request);
    }
}