using System;
using System.Linq;
using hercolobus.services.hercolobus.services.Context;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Parametros
{
    public class ParametroAppService : IParametroAppService
    {
        private readonly NostromoContext _context;
        private readonly IParametroDomainService _parametroDomainService;
        public ParametroAppService(NostromoContext context, IParametroDomainService parametroDomainService)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _parametroDomainService = parametroDomainService ?? throw new ArgumentException(nameof(parametroDomainService));
        }
        public ParametroDto Create(ParametroRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newRow = _parametroDomainService.Create(request);

            _context.Parametros.Add(newRow);
            _context.SaveChanges();

            var result = _context.Parametros.FirstOrDefault();
            var ultimaFacturagenerada = result.UltimaFacturaGenerada;
            var ultimoCorrelativo = result.UltimoCorrelativoUtilizado;

            var cadenaFactura = ultimaFacturagenerada.Split('-');
            var ultimoSegmentoFactura = cadenaFactura.AsQueryable().Last();
            ultimoCorrelativo += 1;
            var proximaFactura = string.Empty;

            for (int i = 0; i < cadenaFactura.Length; i++)
            {
                if (i == (cadenaFactura.Length - 1))
                {
                    proximaFactura += ultimoCorrelativo.ToString("D" + ultimoSegmentoFactura.Length);
                }
                else
                {
                    proximaFactura += cadenaFactura[i] + "-";
                }
            }

            return ParametroDto.From(newRow, proximaFactura, ultimoCorrelativo);
        }

        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var row = _context.Parametros.FirstOrDefault(s => s.Id == id);
            if (row == null) throw new Exception("Error al intentar obtener puesto");
            _context.Parametros.Remove(row);
            _context.SaveChanges();

            return string.Empty;
        }

        public ParametroDto Get()
        {
            var result = _context.Parametros.FirstOrDefault();
            if (result == null) return new ParametroDto
            {
                ValidationErrorMessage = "Error al obtener informacion general, Revisa tu conexion a internet, si el problema persiste contacta al administrador"
            };


            var ultimaFacturagenerada = result.UltimaFacturaGenerada;
            var ultimoCorrelativo = result.UltimoCorrelativoUtilizado;

            var cadenaFactura = ultimaFacturagenerada.Split('-');
            var ultimoSegmentoFactura = cadenaFactura.AsQueryable().Last();
            ultimoCorrelativo += 1;
            var proximaFactura = string.Empty;

            for (int i = 0; i < cadenaFactura.Length; i++)
            {
                if (i == (cadenaFactura.Length - 1))
                {
                    proximaFactura += ultimoCorrelativo.ToString("D" + ultimoSegmentoFactura.Length);
                }
                else
                {
                    proximaFactura += cadenaFactura[i] + "-";
                }
            }
            return ParametroDto.From(result, proximaFactura, ultimoCorrelativo);
        }

        public ParametroDto Update(ParametroRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldRowInfo = _context.Parametros.FirstOrDefault(s => s.Id == request.Id);
            if (oldRowInfo == null) return new ParametroDto { ValidationErrorMessage = "Error al obtener info de los puestos" };

            var rolUpdate = _parametroDomainService.Update(request, oldRowInfo);

            _context.Parametros.Update(oldRowInfo);
            _context.SaveChanges();

            var result = _context.Parametros.FirstOrDefault();
            var ultimaFacturagenerada = result.UltimaFacturaGenerada;
            var ultimoCorrelativo = result.UltimoCorrelativoUtilizado;

            var cadenaFactura = ultimaFacturagenerada.Split('-');
            var ultimoSegmentoFactura = cadenaFactura.AsQueryable().Last();
            ultimoCorrelativo += 1;
            var proximaFactura = string.Empty;

            for (int i = 0; i < cadenaFactura.Length; i++)
            {
                if (i == (cadenaFactura.Length - 1))
                {
                    proximaFactura += ultimoCorrelativo.ToString("D" + ultimoSegmentoFactura.Length);
                }
                else
                {
                    proximaFactura += cadenaFactura[i] + "-";
                }
            }

            return ParametroDto.From(rolUpdate, proximaFactura, ultimoCorrelativo);
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_parametroDomainService != null) _parametroDomainService.Dispose();
        }
    }
}