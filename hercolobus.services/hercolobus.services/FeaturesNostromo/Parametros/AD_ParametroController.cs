using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Parametros
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AD_ParametroController : ControllerBase
    {
        private readonly IParametroAppService _parametroAppService;
        public AD_ParametroController(IParametroAppService parametroAppService)
        {
            _parametroAppService = parametroAppService ?? throw new ArgumentException(nameof(parametroAppService));
        }

        [HttpGet]
        // [Route("paged")]
        [Authorize]
        public ActionResult<ParametroDto> Get()
        {
            return Ok(_parametroAppService.Get());
        }


        [HttpPost]
        [Authorize]
        public ActionResult<ParametroDto> Post([FromBody] ParametroRequest request)
        {
            return Ok(_parametroAppService.Create(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<ParametroDto> Put([FromBody] ParametroRequest request)
        {
            return Ok(_parametroAppService.Update(request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_parametroAppService.Delete(Id));
        }

    }
}