using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using hercolobus.services.Core;
using hercolobus.services.Features.Facturas;
using hercolobus.services.hercolobus.services.FeaturesNostromo.Usuarios;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Parametros
{
    [Table("Parametros")]
    public class Parametro : Entity
    {
        public string Empresa { get; private set; }
        public string Direccion { get; private set; }
        public string Contacto { get; private set; }
        public decimal Impuesto { get; private set; }
        public string Rtn { get; private set; }
        public string RazonSocial { get; private set; }
        public string Cai { get; private set; }
        public string RangoFacturaInicio { get; private set; }
        public string RangoFacturaFinal { get; private set; }
        public DateTime FechaLimiteEmision { get; private set; }
        public string UltimaFacturaGenerada { get; private set; }
        public int UltimoCorrelativoUtilizado { get; private set; }


        public ICollection<Factura.Factura> Facturas { get; set; }
        public ICollection<Usuario> Usuarios { get; set; }
        public ICollection<Catalogo.Catalogo> Productos { get; set; }

        internal void UpdateUltimoCorrelativo(int ultimoCorrelativo, string ultimaFacturGenerada)
        {
            UltimoCorrelativoUtilizado = ultimoCorrelativo;
            UltimaFacturaGenerada = ultimaFacturGenerada;
        }

        internal void Update(
            string _empresa, string _direccion,
            string _contacto, decimal _impuesto,
            string _rtn, string _razonSocial,
            string _cai, string _rangofacturaInicio,
            string _rangoFacturaFinal, DateTime _fechaLimiteEmision,
            string _ultimafactura, int _ultimoCorrelativo, string _user)
        {
            Empresa = _empresa;
            Direccion = _direccion;
            Contacto = _contacto;
            Impuesto = _impuesto;
            Rtn = _rtn;
            RazonSocial = _razonSocial;
            Cai = _cai;
            RangoFacturaInicio = _rangofacturaInicio;
            RangoFacturaFinal = _rangoFacturaFinal;
            FechaLimiteEmision = _fechaLimiteEmision;
            UltimaFacturaGenerada = _ultimafactura;
            UltimoCorrelativoUtilizado = _ultimoCorrelativo;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModifiedProject";
            ModifiedBy = _user ?? "Application";
            TransactionUId = Guid.NewGuid();
            Enabled = true;
        }

        public class Builder
        {
            private readonly Parametro _parametro = new Parametro();

            public Builder ConEmpresa(
            string _empresa, string _direccion,
            string _contacto, decimal _impuesto,
            string _rtn, string _razonSocial,
            string _cai, string _rangofacturaInicio,
            string _rangoFacturaFinal, DateTime _fechaLimiteEmision,
            string _ultimafactura, int _ultimoCorrelativo)
            {
                _parametro.Empresa = _empresa;
                _parametro.Direccion = _direccion ?? string.Empty;
                _parametro.Contacto = _contacto ?? string.Empty;
                _parametro.Impuesto = _impuesto;
                _parametro.Rtn = _rtn ?? string.Empty;
                _parametro.RazonSocial = _razonSocial ?? string.Empty;
                _parametro.Cai = _cai ?? string.Empty;
                _parametro.RangoFacturaInicio = _rangofacturaInicio ?? string.Empty;
                _parametro.RangoFacturaFinal = _rangoFacturaFinal ?? string.Empty;
                _parametro.FechaLimiteEmision = _fechaLimiteEmision;
                _parametro.UltimaFacturaGenerada = _ultimafactura ?? string.Empty;
                _parametro.UltimoCorrelativoUtilizado = _ultimoCorrelativo;
                return this;

            }

            public Builder WithAuditFields(string _user)
            {
                _parametro.CrudOperation = "Add";
                _parametro.TransactionDate = DateTime.Now;
                _parametro.TransactionType = "NuevoRegistro";
                _parametro.ModifiedBy = _user ?? "Application";
                _parametro.TransactionUId = Guid.NewGuid();
                _parametro.Enabled = true;
                return this;
            }

            public Parametro Build()
            {
                return _parametro;
            }

        }

    }
}