using System;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Parametros
{
    public interface IParametroAppService : IDisposable
    {
        ParametroDto Get();
        ParametroDto Create(ParametroRequest request);
        ParametroDto Update(ParametroRequest request);
        string Delete(int id);
    }
}