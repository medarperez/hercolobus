using System;
using System.Collections.Generic;
using System.Linq;
using hercolobus.services.Core;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Parametros
{
    public class ParametroDto : ResponseBase
    {
        public string Empresa { get; set; }
        public string Direccion { get; set; }
        public string Contacto { get; set; }
        public decimal Impuesto { get; set; }
        public string Rtn { get; set; }
        public string RazonSocial { get; set; }
        public string Cai { get; set; }
        public string RangoFacturaInicio { get; set; }
        public string RangoFacturaFinal { get; set; }
        public DateTime FechaLimiteEmision { get; set; }
        public string UltimaFacturaGenerada { get; set; }
        public int UltimoCorrelativoUtilizado { get; set; }
        public string ProximoCorrelativo { get; set; }
        public int ProximoNumeroFactura { get; set; }


        public static List<ParametroDto> FromList(IEnumerable<Parametro> detail)
        {
            if (detail == null || !detail.Any()) return new List<ParametroDto>();
            return (from qry in detail
                    select From(qry, string.Empty, 0)).ToList();
        }

        public static ParametroDto From(Parametro s, string proximoCorrelativo, int proximoNumero)
        {
            if (s == null) return new ParametroDto();

            return new ParametroDto
            {
                Id = s.Id,
                Empresa = s.Empresa,
                Direccion = s.Direccion,
                Contacto = s.Contacto,
                Impuesto = s.Impuesto,
                Rtn = s.Rtn,
                RazonSocial = s.RazonSocial,
                Cai = s.Cai,
                RangoFacturaInicio = s.RangoFacturaInicio,
                RangoFacturaFinal = s.RangoFacturaFinal,
                FechaLimiteEmision = s.FechaLimiteEmision,
                UltimaFacturaGenerada = s.UltimaFacturaGenerada,
                UltimoCorrelativoUtilizado = s.UltimoCorrelativoUtilizado,
                ProximoCorrelativo = proximoCorrelativo,
                ProximoNumeroFactura = proximoNumero
            };
        }
    }
}