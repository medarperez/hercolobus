using System;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Parametros
{
    public class ParametroDomainService : IParametroDomainService
    {
        public Parametro Create(ParametroRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Empresa)) throw new ArgumentNullException(nameof(request.Empresa));

            Parametro registro = new Parametro.Builder()
            .ConEmpresa(
                request.Empresa, request.Direccion, request.Contacto,
                request.Impuesto, request.Rtn, request.RazonSocial, request.Cai,
                request.RangoFacturaInicio, request.RangoFacturaFinal, request.FechaLimiteEmision,
                request.UltimaFacturaGenerada, request.UltimoCorrelativoUtilizado)
            .WithAuditFields(request.User)
            .Build();

            return registro;
        }

        public Parametro Update(ParametroRequest request, Parametro _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(
                request.Empresa, request.Direccion, request.Contacto,
                request.Impuesto, request.Rtn, request.RazonSocial,
                request.Cai, request.RangoFacturaInicio,
                request.RangoFacturaFinal, request.FechaLimiteEmision,
                request.UltimaFacturaGenerada, request.UltimoCorrelativoUtilizado, request.User);

            return _oldRegister;
        }

        public void Dispose()
        {
        }


    }
}