using System;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Parametros
{
    public interface IParametroDomainService : IDisposable
    {
        Parametro Create(ParametroRequest request);
        Parametro Update(ParametroRequest request, Parametro _oldRegister);
    }
}