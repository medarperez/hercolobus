using System;
using hercolobus.services.Core;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Parametros
{
    public class ParametroRequest : RequestBase
    {
        public string Empresa { get; set; }
        public string Direccion { get; set; }
        public string Contacto { get; set; }
        public decimal Impuesto { get; set; }
        public string Rtn { get; set; }
        public string RazonSocial { get; set; }
        public string Cai { get; set; }
        public string RangoFacturaInicio { get; set; }
        public string RangoFacturaFinal { get; set; }
        public DateTime FechaLimiteEmision { get; set; }
        public string UltimaFacturaGenerada { get; set; }
        public int UltimoCorrelativoUtilizado { get; set; }
    }
}