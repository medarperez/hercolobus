using System;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Puestos
{
    public interface IPuestoDomainService : IDisposable
    {
        Puesto Create(AD_PuestoRequest request);
        Puesto Update(AD_PuestoRequest request, Puesto _oldRegister);
        Puesto Disabled(AD_PuestoRequest request, Puesto _oldRegister);
    }
}