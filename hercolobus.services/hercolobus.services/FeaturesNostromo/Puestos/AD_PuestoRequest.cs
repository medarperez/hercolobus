using hercolobus.services.Core;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Puestos
{
    public class AD_PuestoRequest : RequestBase
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }

    }
}