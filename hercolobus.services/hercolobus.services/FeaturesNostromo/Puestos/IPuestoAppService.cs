using System;
using System.Collections.Generic;
using hercolobus.services.Core;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Puestos
{
    public interface IPuestoAppService : IDisposable
    {
        AD_PuestoPagedDto GetPaged(PagedGeneralRequest request);
        AD_PuestoDto Create(AD_PuestoRequest request);
        AD_PuestoDto Update(AD_PuestoRequest request);
        AD_PuestoDto Disabled(AD_PuestoRequest request);
        IEnumerable<AD_PuestoDto> GetAllPuestos();
        string Delete(int id);
    }
}