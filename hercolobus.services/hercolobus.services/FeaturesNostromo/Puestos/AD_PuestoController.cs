using System;
using System.Collections.Generic;
using hercolobus.services.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Puestos
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AD_PuestoController : ControllerBase
    {
        private readonly IPuestoAppService _puestoAppService;
        public AD_PuestoController(IPuestoAppService puestoAppService)
        {
            _puestoAppService = puestoAppService ?? throw new ArgumentException(nameof(puestoAppService));
        }

        [HttpGet]
        // [Route("paged")]
        [Authorize]
        public ActionResult<IEnumerable<AD_PuestoDto>> Get()
        {
            return Ok(_puestoAppService.GetAllPuestos());
        }

        [HttpGet]
        [Route("paged")]
        [Authorize]
        public ActionResult<AD_PuestoPagedDto> GetPaged([FromQuery] PagedGeneralRequest request)
        {
            return Ok(_puestoAppService.GetPaged(request));
        }

        [HttpPost]
        [Authorize]
        public ActionResult<AD_PuestoDto> Post([FromBody] AD_PuestoRequest request)
        {
            return Ok(_puestoAppService.Create(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<AD_PuestoDto> Put([FromBody] AD_PuestoRequest request)
        {
            return Ok(_puestoAppService.Update(request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_puestoAppService.Delete(Id));
        }

        [HttpPut]
        [Route("disabled")]
        public ActionResult<AD_PuestoDto> Disabled([FromBody] AD_PuestoRequest request)
        {
            return Ok(_puestoAppService.Disabled(request));
        }
    }
}