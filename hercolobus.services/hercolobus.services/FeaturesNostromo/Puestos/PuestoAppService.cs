using System;
using System.Collections.Generic;
using System.Linq;
using hercolobus.services.Core;
using hercolobus.services.hercolobus.services.Context;

namespace hercolobus.services.hercolobus.services.FeaturesNostromo.Puestos
{
    public class PuestoAppService : IPuestoAppService
    {
        private readonly NostromoContext _context;
        private readonly IPuestoDomainService _puestoDomainService;

        public PuestoAppService(NostromoContext context, IPuestoDomainService puestoDomainService)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _puestoDomainService = puestoDomainService ?? throw new ArgumentException(nameof(puestoDomainService));
        }
        public AD_PuestoDto Create(AD_PuestoRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newpuesto = _puestoDomainService.Create(request);

            _context.Puestos.Add(newpuesto);
            _context.SaveChanges();

            return new AD_PuestoDto
            {
                Nombre = newpuesto.Nombre,
                Descripcion = newpuesto.Descripcion,
                Enabled = newpuesto.Enabled
            };
        }

        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var puesto = _context.Puestos.FirstOrDefault(s => s.Id == id);
            if (puesto == null) throw new Exception("Error al intentar obtener puesto");
            _context.Puestos.Remove(puesto);
            _context.SaveChanges();

            return string.Empty;
        }

        public AD_PuestoPagedDto GetPaged(PagedGeneralRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;

            if (string.IsNullOrEmpty(request.Value))
            {
                var count = Convert.ToDecimal(_context.Puestos.Where(s => s.Enabled == true).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Puestos.Where(s => s.Enabled == true)
                .OrderByDescending(s => s.TransactionDate)
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new AD_PuestoPagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Items = lista.Select(s => new AD_PuestoDto
                    {
                        Id = s.Id,
                        Nombre = s.Nombre,
                        Descripcion = s.Descripcion,
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
            else
            {

                var count = Convert.ToDecimal(_context.Puestos.Where(s => s.Nombre.Contains(request.Value) && s.Enabled == true).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Puestos.Where(s => s.Nombre.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new AD_PuestoPagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    Items = lista.Select(s => new AD_PuestoDto
                    {
                        Id = s.Id,
                        Nombre = s.Nombre,
                        Descripcion = s.Descripcion,
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
        }

        public AD_PuestoDto Update(AD_PuestoRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldpuestoInfo = _context.Puestos.FirstOrDefault(s => s.Id == request.Id);
            if (oldpuestoInfo == null) return new AD_PuestoDto { ValidationErrorMessage = "Error al obtener info de los puestos" };

            var rolUpdate = _puestoDomainService.Update(request, oldpuestoInfo);

            _context.Puestos.Update(oldpuestoInfo);
            _context.SaveChanges();

            return new AD_PuestoDto
            {
                Id = oldpuestoInfo.Id,
                Nombre = oldpuestoInfo.Nombre,
                Descripcion = oldpuestoInfo.Descripcion,
                Enabled = oldpuestoInfo.Enabled
            };
        }

        public AD_PuestoDto Disabled(AD_PuestoRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldUsuarioInfo = _context.Puestos.FirstOrDefault(s => s.Id == request.Id);
            if (oldUsuarioInfo == null) return new AD_PuestoDto { ValidationErrorMessage = "Error al obtener info de puesto" };

            var userUpdate = _puestoDomainService.Disabled(request, oldUsuarioInfo);

            _context.Puestos.Update(oldUsuarioInfo);
            _context.SaveChanges();

            return new AD_PuestoDto
            {
                Id = oldUsuarioInfo.Id,
                Nombre = oldUsuarioInfo.Nombre,
                Descripcion = oldUsuarioInfo.Descripcion,
                Enabled = oldUsuarioInfo.Enabled
            };
        }

        public IEnumerable<AD_PuestoDto> GetAllPuestos()
        {
            var roles = _context.Puestos.Where(s => s.Enabled == true).ToList();

            return roles.Select(t => new AD_PuestoDto
            {
                Id = t.Id,
                Nombre = t.Nombre,
                Descripcion = t.Descripcion
            });
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_puestoDomainService != null) _puestoDomainService.Dispose();
        }


    }
}